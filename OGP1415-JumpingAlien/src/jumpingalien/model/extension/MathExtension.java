package jumpingalien.model.extension;

public class MathExtension {

	public static boolean isPositiveOverflow(int a, int b){
		assert a >= 0;
		return (a+b <= b);
	}
	
	public static double roundTowardZero(double value){
		if (value > 0)
			return Math.floor(value);
		return Math.ceil(value);
	}
	
}
