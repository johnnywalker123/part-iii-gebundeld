package jumpingalien.model.extension;

import java.util.Random;

public class RandomExtension {
	
	public static double nextDouble(double start, double stop){
		double range = stop - start;
		return random.nextDouble() * range + start;
	}
	
	public static <T> T pickRandom(T element1, T element2){
		if (random.nextBoolean())
			return element1;
		else
			return element2;
	}
	
	public final static Random random = new Random();
	
	
}