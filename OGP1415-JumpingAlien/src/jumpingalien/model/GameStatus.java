package jumpingalien.model;

import be.kuleuven.cs.som.annotate.*;

@Value
public enum GameStatus{
	ADDING_OBJECTS, STARTED, PLAYER_WON, PLAYER_LOST;
	
	public boolean canAddObjects(){
		return (this == GameStatus.ADDING_OBJECTS);
	}
		
	public boolean isGameOver(){
		return ((this == GameStatus.PLAYER_WON) ||
				(this == GameStatus.PLAYER_LOST));
	}
	
	
	public boolean didPlayerWin(){
		return (this == GameStatus.PLAYER_WON);
	}
	
}




