package jumpingalien.model;
import static jumpingalien.util.Util.*;

public class Conversion {
	
	public static int convertPositionToPixel(double position){
		assert (!Double.isNaN(position));
		double toSet = position/PIXEL_LENGTH;
		if (fuzzyGreaterThanOrEqualTo(Math.ceil(toSet) - toSet,1))
			return (int) (toSet) + 1;
		return (int) (toSet);
	}
	
	public static double convertPixelToPosition(int pixel){
		return (double) (pixel*PIXEL_LENGTH);
	}
	
	public static int convertTileToPixel(int tile, int tileLength){
		return tile*tileLength;
	}
	
	public final static double PIXEL_LENGTH = 0.01;
}
