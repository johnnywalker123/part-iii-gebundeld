package jumpingalien.model;



import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Map;
import java.util.stream.Stream;

import jumpingalien.model.type.ObjectType;
import static jumpingalien.model.Conversion.*;
import static jumpingalien.util.Util.fuzzyGreaterThanOrEqualTo;
import be.kuleuven.cs.som.annotate.*;


/**
 * A class of the game world.
 * 
 * @invar | isValidDimension(getXDimension())
 * @invar | isValidDimension(getYDimension())
 * @invar | isValidTileLength(getTileLength())
 * @invar | canHaveAsXTargetTile(getXTargetTile())
 * @invar | canHaveAsYTargetTile(getYTargetTile());
 * @invar |	canHaveAsWindowWidth(getWindowWidth());
 * @invar | canHaveAsWindowHeight(getWindowHeight());
 * @invar | canHaveAsWindow(getWindow());
 * @invar | canHaveAsGameStatus(getGameStatus())
 * @invar | isValidTileLength(getTileLength())
 * @invar | hasProperTiles()
 * @invar | hasProperMazub()
 * @invar | hasProperNpcs()
 * @invar | isValidTime(getTime())
 * 
 * @author Tuur Van Daele and Antoon Purnal
 * @version 3.0
 *
 */
public class World extends TimeObject{
	
	/**
	 * 
	 * @param nbXTiles
	 * @param nbYTiles
	 * @param tileLength
	 * @param xTargetTile
	 * @param yTargetTile
	 * @param windowWidth
	 * @param windowHeight
	 * 
	 * @pre	|isValidDimension(nbXTiles*tileLength)
	 * @pre	|isValidDimension(nbYTiles*tileLength)
	 * @pre	|isValidTileLength(tileLength)
	 * @pre |isValidDimension(getXDimension())
	 * @pre |isValidDimension(getYDimension())
	 * @pre |isValidTileLength(getTileLength())
	 * @pre |canHaveAsXTargetTile(xTargetTile)
	 * @pre |canHaveAsYTargetTile(yTargetTile)
	 * @pre |canHaveAsWindowWidth(windowWidth)
	 * @pre |canHaveAsWindowHeight(windowHeight)
	 * 
	 * 
	 * @post |new.getXDimension() == nbXTiles*tileLength
	 * @post |new.getYDimension() == nbYTiles*tileLength
	 * @post |new.getTileLength() == tileLength
	 * @post |new.getXTargetTile() == xTargetTile
	 * @post |new.getYTargetTile() == yTargetTile
	 * @post |new.getWindowWidth() == windowWidth
	 * @post |new.getWindowHeight() == windowHeight
	 * @post |new.getWindow() == new Window(windowWidth, windowHeight, 0, 0)
	 * 
	 * @post |new.getGameStatus() == GameStatus.ADDING_OBJECTS
	 * @post |new.getMazub() == null
	 * @post |new.getAllPlants().isEmpty()
	 * @post |new.getAllSlimes().isEmpty()
	 * @post |new.getAllSharks().isEmpty()
	 * @post |new.getTime() == 0
	 * 
	 */
	// CONSTRUCTOR
	public World(int nbXTiles, int nbYTiles, int tileLength, int xTargetTile, int yTargetTile,
			int windowWidth, int windowHeight){
		assert isValidDimension(nbXTiles*tileLength);
		assert isValidDimension(nbYTiles*tileLength);
		assert isValidTileLength(tileLength);
		this.xDimension = nbXTiles*tileLength;
		this.yDimension = nbYTiles*tileLength;
		this.tileLength = tileLength;
		//assert canHaveAsXTargetTile(xTargetTile);
		//assert canHaveAsYTargetTile(yTargetTile);
		assert canHaveAsWindowWidth(windowWidth);
		assert canHaveAsWindowHeight(windowHeight);
		this.xTargetTile = xTargetTile;
		this.yTargetTile = yTargetTile;
		this.windowWidth = windowWidth;
		this.windowHeight = windowHeight;
		Window window = new Window(windowWidth, windowHeight,0,0);
		assert canHaveAsWindow(window);
		this.window = window; // default zero
		}
	
	
	// THE GAME
	
	/**
	 * Start the game of this world.
	 * 
	 * @pre	| canStartGame()
	 * @effect	| setGameStatus(GameStatus.STARTED)
	 */
	public void startGame(){
		assert canStartGame();
		setGameStatus(GameStatus.STARTED);
	}
	
	/**
	 * Check whether the game of this world can be started.
	 * 
	 * @return | result == (getMazub() != null)
	 */
	public boolean canStartGame(){
		return (getMazub() != null);
	}
	
	
	// GAME STATUS
	
	/**
	 * Return the game status of this world.
	 */
	@Basic
	public GameStatus getGameStatus(){
		return this.gameStatus;
	}
	
	/**
	 * Check whether this world can have the given game status as its game status.
	 * 
	 * @param 	gameStatus
	 * 			The game status to check
	 * @return  |result == (gameStatus != null)
	 */
	public boolean canHaveAsGameStatus(GameStatus gameStatus){
		return (gameStatus != null);
	}

	
	/**
	 * Check whether this world can add objects.
	 * 
	 * @return |result == getGameStatus().canAddObjects()
	 * 
	 */
	public boolean canAddObjects(){
		return getGameStatus().canAddObjects();
	}
	
	/**
	 * Check whether the game of this world is over.
	 * 
	 * @return |result == getGameStatus().isGameOver()
	 */
	public boolean isGameOver(){
		return getGameStatus().isGameOver();
	}
	
	/**
	 * Check whether the player of this world did win.
	 * 
	 * @return |result == getGameStatus().didPlayerWin()
	 */
	public boolean didPlayerWin(){
		return getGameStatus().didPlayerWin();
	}
	
	/**
	 * Set the game status of this world to the given game status.
	 * 
	 * @param status
	 * @pre		| status != null
	 * @pre		| getGameStatus().compareTo(status) < 0
	 * @post	| new.getGameStatus() == status
	 */
	@Model
	private void setGameStatus(GameStatus status){
		assert status != null;
		assert getGameStatus().compareTo(status) < 0;
		this.gameStatus = status;
	}
	
	/**
	 * Variable referencing the game status of this world.
	 */
	private GameStatus gameStatus = GameStatus.ADDING_OBJECTS;
	
	
	// WORLD DIMENSIONS
	/**
	 * Return the x dimension (in pixels) of this world.
	 */
	@Basic @Immutable
	public int getXDimension(){
		return this.xDimension;
	}
	
	/**
	 * Return the y dimension (in pixels) of this world.
	 */
	@Basic @Immutable
	public int getYDimension(){
		return this.yDimension;
	}
	
	/**
	 * Check whether the given dimension is a valid dimension for all worlds.
	 * 
	 * @param dimension
	 * @return | result == (dimension > 0)
	 */
	public static boolean isValidDimension(int dimension){
		return (dimension > 0);
	}
	
	/**
	 * Variables registering respectively the x and y dimension of this world.
	 */
	private final int xDimension, yDimension;
	
	
	// WINDOW
	
	/**
	 * Return the window of this world.
	 */
	@Basic
	public Window getWindow(){
		return this.window;
	}
	
	/**
	 * Return an array with as elements respectively the most left, the bottom, the most right and the top pixel of this window.
	 * 
	 * @return	|result == new int[]{getWindow().getXPixel(), getWindow().getYPixel(), 
							getWindow().getRightPixel(), getWindow().getTopPixel()}
	 */
	public int[] getWindowCoordinates(){
		Window window = getWindow();
		return new int[]{window.getXPixel(), window.getYPixel(), 
							window.getRightPixel(), window.getTopPixel()};
	}
	
	/**
	 * Return the window width of this world.
	 */
	@Basic @Immutable
	public int getWindowWidth(){
		return this.windowWidth;
	}
	
	/**
	 * Return the window height of this world.
	 */
	@Basic @Immutable
	public int getWindowHeight(){
		return this.windowHeight;
	}
	
	/**
	 * Check whether this world can have the given window width as its window width.
	 * 
	 * @param windowWidth
	 * @return |result == (windowWidth >0) && (windowWidth <= getXDimension())
	 */
	public boolean canHaveAsWindowWidth(int windowWidth){
		return (windowWidth > 0) && (windowWidth <= getXDimension());
	}
	
	/**
	 * Check whether this world can have the given window height as its window width.
	 * 
	 * @param windowHeight
	 * @return |result == (	(windowHeight > 0) && (windowHeight <= getYDimension())	)
	 */
	public  boolean canHaveAsWindowHeight(int windowHeight){
		return (windowHeight > 0) && (windowHeight <= getYDimension());
	}
	
	/**
	 * Check whether this world can have the given pixels as the pixels of its window.
	 * 
	 * @param xPixel
	 * @param yPixel
	 * @return |result == (	(xPixel >= 0 && xPixel + getWindowWidth() - 1 < getXDimension()) &&
	 * 		 	| (yPixel >= 0 && yPixel + getWindowHeight() - 1 < getYDimension())	 )
	 */
	public boolean canHaveAsWindowPixel(int xPixel, int yPixel){
		return (	(xPixel >= 0 && xPixel + getWindowWidth() - 1 < getXDimension()) &&
					(yPixel >= 0 && yPixel + getWindowHeight() - 1 < getYDimension())	 ) ;
	}
	
	/**
	 * Check whether this wworld can have the given window as its window.
	 * 
	 * @param window
	 * @return | result == 
	 * 			(	(window != null) &&	
	 * 				canHaveAsWindowHeight(window.getHeight()) &&
	 * 				window.getWidth() == this.getWindowWidth()	&&
	 * 				canHaveAsWindowPixel(window.getXPixel(), window.getYPixel())	)
	 */
	public boolean canHaveAsWindow(Window window){
		return (	(window != null) &&	
					canHaveAsWindowHeight(window.getHeight()) &&
					window.getWidth() == this.getWindowWidth()	&&
					canHaveAsWindowPixel(window.getXPixel(), window.getYPixel())	);
	}
	
	/**
	 * Update the game window of this world.
	 * 
	 * @effect	|setWindowPixel(computeNewWindowXPixel(),computeNewWindowYPixel())
	 */
	public void updateGameWindow(){
		int newXPixel = computeNewWindowXPixel();
		int newYPixel = computeNewWindowYPixel();
		setWindowPixel(newXPixel,newYPixel);
	}
	
	/**
	 * Return the amount of pixels update game window pixels.
	 * If mazub is closer than this amount of pixels to one of the window boundaries and the window can move in that direction,
	 * the window will remove in that direction in the next call of updateGameWindow.
	 */
	@Basic
	public int getNbUpdateGameWindowPixels(){
		return 201;
	}
	
	/**
	 * Compute the x pixel of the new window of this world.
	 * 
	 * @return  |if (getMazub().isLeftCloseTo(getWindow().getXPixel()) && (getWindow().getXPixel() > 0))
	 * 			|	then result == Math.max(getMazub().getXPixel() - getNbUpdateGameWindowPixels(),0)
	 * 			|if (getMazub().isRightCloseTo(getWindow().getRightPixel())&& (getWindow().getRightPixel()  < getXDimension()-1))
	 * 			| 	then result == Math.min(getMazub().getRightPixel() + getNbUpdateGameWindowPixels(), getXDimension()-1) - getWindow().getWidth() + 1
	 * 			|else result == getWindow().getXPixel()
	 */
	private int computeNewWindowXPixel(){
		Mazub mazub = getMazub();
		Window window = getWindow();
		if (mazub.isLeftCloseTo(window.getXPixel()) && (window.getXPixel() > 0))
			return Math.max(mazub.getXPixel() - getNbUpdateGameWindowPixels(),0); 
		if (mazub.isRightCloseTo(window.getRightPixel())
				&& (window.getRightPixel()  < getXDimension()-1))
			return Math.min(mazub.getRightPixel() + getNbUpdateGameWindowPixels(), getXDimension()-1) - window.getWidth() + 1;
		return window.getXPixel();
	}
	
	/**
	 * Compute the y pixel of the new window of this world
	 * 
	 * @return	| if (getMazub().isBottomCloseTo(getWindow().getYPixel()) && (getWindow().getYPixel() > 0))
	 * 			|	then result == Math.max(getMazub().getYPixel() - getNbUpdateGameWindowPixels(), 0)
	 * 			| if (getMazub().isTopCloseTo(getWindow().getTopPixel()) && (getWindow().getTopPixel() < getYDimension()-1))
	 * 			|	then result == Math.min(getMazub().getTopPixel() + getNbUpdateGameWindowPixels(), getYDimension()-1) - getWindow().getHeight() + 1
	 * 			| else result == getWindow().getYPixel()
	 */
	
	private int computeNewWindowYPixel(){
		Mazub mazub = getMazub();
		Window window = getWindow();
		if (mazub.isBottomCloseTo(window.getYPixel()) && (window.getYPixel() > 0))
			return Math.max(mazub.getYPixel() - getNbUpdateGameWindowPixels(), 0);
		if (mazub.isTopCloseTo(window.getTopPixel())
				&& (window.getTopPixel() < getYDimension()-1))
			return Math.min(mazub.getTopPixel() + getNbUpdateGameWindowPixels(), getYDimension()-1) - window.getHeight() + 1;	
		return window.getYPixel();
	}
	
	/**
	 * Set the pixels of the window of this world to the given x and y pixel.
	 * 
	 * @param xPixel
	 * @param yPixel
	 * @pre	|canHaveAsWindowPixel(xPixel, yPixel)
	 * @post	| new.getWindow().equals(new Window(getWindowWidth(), getWindowHeight(), xPixel, yPixel))
	 */
	
	private void setWindowPixel(int xPixel, int yPixel){
		assert canHaveAsWindowPixel(xPixel, yPixel);
		this.window = new Window(getWindowWidth(), getWindowHeight(), xPixel, yPixel);
	}
	
	/**
	 * Variable referencing the window of this world.
	 */
	private Window window;
	
	/**
	 * Variable referencing the window width of this world.
	 */
	private final int windowWidth;
	
	/**
	 * Variable referencing the window height of this world.
	 */
	private final int windowHeight;
	
	// POSITIONS
	
	/**
	 * Check whether the given game object can have its position in this world.
	 * 
	 * @param gameObject
	 * @pre	| gameObject != null
	 * @return 	| if (!isInXRange(gameObject.getXPosition()) || ! isInYRange(gameObject.getYPosition()))
	 * 			|	then result == false
	 * @return	| else if (overlapsWithImpassableTile(InfoGameObject.infoGameObjectWithoutPerimeter(gameObject)))
	 * 			|	then result == false
	 * @return 	| else result == for each gameObject in getAllGameObjects()
	 * 			|					!other.collidesWith(InfoGameObject.infoGameObjectWithoutPerimeter(gameObject)) || !gameObject.interferesMovementWith(other))
	 */
	public boolean canHaveAsPosition(GameObject gameObject){
		assert gameObject != null;
		// Not outside the WORLD
		if (!isInXRange(gameObject.getXPosition()) || ! isInYRange(gameObject.getYPosition()))
			return false;
		// Not overlapping more than one pixel with another GAME OBJECT
		Set<GameObject> others = getAllGameObjects();
		others.remove(gameObject);
		for (GameObject other: others){
			if (other.collidesWith(InfoGameObject.infoGameObjectWithoutPerimeterOf(gameObject)) && gameObject.interferesMovementWith(other))
				return false; 		
		}
		//Not overlapping more than one pixel with an IMPASSABLE TILE
		return !overlapsWithImpassableTile(InfoGameObject.infoGameObjectWithoutPerimeterOf(gameObject));
	}
	
	/**
	 * Check whether the given horizontal position is in the game world.
	 * 
	 * @param position
	 * @return result == (position < convertPixelToPosition(getXDimension()) && position > 0)
	 */
	public boolean isInXRange(double position){
		return (position < convertPixelToPosition(getXDimension()) &&
				position > 0);
	} 
	
	/**
	 * Check whether the given vertical position is in the game world.
	 * 
	 * @param position
	 * @return result == (position < convertPixelToPosition(getYDimension()) && position > 0)
	 */
	public boolean isInYRange(double position){
		return (position < convertPixelToPosition(getYDimension()) &&
				position > 0); // in feite ook passable
	}
	
	
	// TILES
	
	/**
	 * Check whether this world can have the given tile as one of its tiles.
	 * 
	 * @param tile
	 * @return result == (tile.getXTile() <= getXDimension()/getTileLength() && tile.getYTile() <= getYDimension()/ getTileLength())
	 */
	public boolean canHaveAsTile(Tile tile){
		if (tile == null)
			return false;
		// To our understanding, the above assert should not include equality -  however, when using both the debug options to display
		// the entire world as well as to colorcode the tiles, the more strict assertion does not hold true.
		 return tile.getXTile() <= getXDimension()/getTileLength() && tile.getYTile() <= getYDimension()/ getTileLength();
	}
		
		// TILE LENGTH
	
	/**
	 * Return the tile length of this world.
	 */
	@Basic @Immutable
	public int getTileLength(){
		return this.tileLength;
	}
	
	/**
	 * Check whether the given tile length is a valid tile length for all worlds.
	 * 
	 * @param tileLength
	 * @return |result == (tileLength > 0)
	 */
	public static boolean isValidTileLength(int tileLength){
		return (tileLength > 0);
	}
	
	/**
	 * Variable registering the tile length of the tiles of this world.
	 */
	private final int tileLength;
	
		// CONVERSION TO/FROM PIXEL
	
	/**
	 * Return the tile corresponding with the given pixel.
	 * 
	 * @param pixel
	 * @return	| result == (int) (pixel/getTileLength())
	 */
	public int pixelToTile(int pixel){
		return (int) (pixel/getTileLength());
	}
	
	
	/**
	 * Return the bottom left pixel of the tile with the given tile coordinates.
	 * 
	 * @param xTile
	 * @param yTile
	 * @return | result == new int[]{xTile*getTileLength(), yTile*getTileLength()}
	 */
	public int[] getBottomLeftPixelOfTile(int xTile, int yTile){
		int leftPixel = xTile*getTileLength();
		int bottomPixel = yTile*getTileLength();
		return new int[]{leftPixel, bottomPixel};
	}
	
	/**
	 * Check whether the given pixel corresponds with the bottom left pixel of a tile in this world.
	 * 
	 * @param xPixel
	 * @param yPixel
	 * @return | result == ((xPixel % getTileLength() == 0) && (yPixel % getTileLength() == 0))
	 */
	public boolean isBottomLeftPixelOfTile(int xPixel, int yPixel){
		return ((xPixel % getTileLength() == 0) &&
				(yPixel % getTileLength() == 0));
	}
	
	/**
	 * Return the most left pixel of the tile with the given horizontal tile coordinate.
	 * 
	 * @param xTile
	 * @return	| result == xTile*getTileLength()
	 */
	public int getLeftPixelOfTile(int xTile){
		return xTile*getTileLength();
	}
	
	/**
	 * Return the bottom pixel of the tile with the given vertical tile coordinate.
	 * 
	 * @param yTile
	 * @return	| result == (yTile*getTileLength())
	 */
	public int getBottomPixelOfTile(int yTile){
		return yTile*getTileLength();
	}
	
	/**
	 * Returns the tile positions of this world within the given rectangular
	 * region.
	 * 
	 * @param leftPixel
	 * @param bottomPixel
	 * @param rightPixel
	 * @param topPixel
	 * @return 	| result ==
	 * 			|	for each verticalIndex in pixelToTile(bottomPixel)..pixelToTile(topPixel)+1
	 * 			|		for each horizontalIndex in pixelToTile(leftPixel)..pixelToTile(rightPixel)+1
	 * 			|			counter = horizontalIndex - pixelToTile(leftPixel) // beginning at 0
	 * 			|			result[counter][0] == horizontalIndex
	 * 			|			result[counter][1] == verticalIndex
	 */
	public int[][] getTilePositionsIn(int leftPixel, int bottomPixel, int rightPixel, int topPixel){
		int leftBound = pixelToTile(leftPixel);
		int rightBound = pixelToTile(rightPixel);
		int bottomBound = pixelToTile(bottomPixel);
		int topBound = pixelToTile(topPixel);
		int[][] result = new int[(rightBound-leftBound+1)*(topBound-bottomBound+1)][2];
		int counter = 0;
		for (int verticalIndex = bottomBound;verticalIndex<=topBound;verticalIndex++){
			for (int horizontalIndex = leftBound;horizontalIndex<=rightBound;horizontalIndex++){
				result[counter][0] = horizontalIndex;
				result[counter][1] = verticalIndex;	
				counter++;
			}
		}
		return result;
	}
	
	
		// TARGET TILE
	
	/**
	 * Return the horizontal tile position of target tile of this world.
	 */
	@Basic @Immutable
	public int getXTargetTile(){
		return this.xTargetTile;
	}
	
	/**
	 * Return the vertical tile position of the target tile of this world.
	 */
	@Basic @Immutable
	public int getYTargetTile(){
		return this.yTargetTile;
	}
	
	/**
	 * Check whether this world can have the given horizontal tile position as the horizontal tile position of its target tile.
	 * 
	 * @param targetTile
	 * @return	result == (targetTile >= 0) && (targetTile < getXDimension()/ getTileLength())
	 */
	
	public boolean canHaveAsXTargetTile(int targetTile) {
		return (targetTile >= 0) && (targetTile <= getXDimension()/ getTileLength());
	}
	
	/**
	 * Check whether this world can have the given vertical tile position as the vertical tile position of its target tile.
	 * 
	 * @param targetTile
	 * @return result == (	(targetTile >= 0) && (targetTile < getYDimension()/ getTileLength())	)
	 */
	public boolean canHaveAsYTargetTile(int targetTile) {
		return (targetTile >= 0) && (targetTile <= getYDimension()/ getTileLength());
	}
	
	
	/**
	 * Check whether the player associated with this world, is at the target tile.
	 * 
	 * @return |result == 
	 * 		   | for some tilePosition in getTilePositionsIn(getMazub().getXPixel(), getMazub().getYPixel(), getMazub().getRightPixel(), getMazub().getTopPixel()):
	 * 		   | 	(tilePosition[0] == getXTargetTile() && tilePosition[1] == getYTargetTile())
	 */
	public boolean mazubAtTarget(){
		int[][] overlappingTilePositions = 
				getTilePositionsIn(getMazub().getXPixel(), getMazub().getYPixel(), getMazub().getRightPixel(), getMazub().getTopPixel());
		for (int[] tilePosition:overlappingTilePositions){
			if (tilePosition[0] == getXTargetTile() && tilePosition[1] == getYTargetTile())
				return true;
		}
		return false;
	}
	
	/**
	 * Update the player's victory.
	 * 
	 * @effect	| if (mazubAtTarget() && getMazub().isAlive())
	 * 			|	then setGameStatus(gameStatus.PLAYER_WON)
	 */
	@Model
	private void updatePlayerVictory(){ 
		if (mazubAtTarget() && getMazub().isAlive())
			setGameStatus(GameStatus.PLAYER_WON);
	}
	
	/**
	 * Variable registering the horizontal position of the target tile of this world.
	 */
	private final int xTargetTile;
	
	/**
	 * Variable registering the vertical position of the target tile of this world.
	 */
	private final int yTargetTile;
	
		// GEOLOGICAL FEATURES
	
	public boolean canHaveAsGeologicalFeature(GeologicalFeature feature){
		return feature != null;
	}
	
	/**
	 * Return all the overlapping tile features of the given collision object in this world.
	 * 
	 * @param collisionObject
	 * @return  | for each tilePosition in getTilePositionsIn(collisionObject.getXPixel(), collisionObject.getYPixel(), collisionObject.getRightPixel(), collisionObject.getTopPixel())
	 * 			| 	result.contains(getGeologicalFeatureAt(getLeftPixelOfTile(tilePosition[0]),getBottomPixelOfTile(tilePosition[1]))
	 */
	public Set<GeologicalFeature> getAllOverlappingTileFeatures(CollisionObject collisionObject){
		Set<GeologicalFeature> result = new HashSet<GeologicalFeature>();
		int[][] surroundingTilePositions = 
				getTilePositionsIn(collisionObject.getXPixel(), collisionObject.getYPixel(), collisionObject.getRightPixel(), collisionObject.getTopPixel());
		for (int[] tilePosition:surroundingTilePositions){
			int xPixel =getLeftPixelOfTile(tilePosition[0]);
			int yPixel =getBottomPixelOfTile(tilePosition[1]);
			result.add(getGeologicalFeatureAt(xPixel, yPixel));
		}
		return result;
	}	
	
	/**
	 * Return the geological feature of the tile of this world corresponding with the given pixel.
	 * 
	 * @param xPixel
	 * @param yPixel
	 * @pre		| isBottomLeftPixelOfTile(xPixel, yPixel)
	 * @return | if tiles.contains(new Tile(pixelToTile(xPixel), pixelToTile(yPixel), this))
	 * 		   |	then result == tiles.get(new Tile(pixelToTile(xPixel), pixelToTile(yPixel))
	 * 		   | else result == GeologicalFeature.AIR
	 */
	public GeologicalFeature getGeologicalFeatureAt(int xPixel, int yPixel){
		assert isBottomLeftPixelOfTile(xPixel, yPixel);
		int xTile = pixelToTile(xPixel);
		int yTile = pixelToTile(yPixel);
		Tile tile = new Tile(xTile, yTile, this);
		if (tiles.containsKey(tile))		// probleem met referentiesemantiek (opgelost)
			return tiles.get(tile);
		return GeologicalFeature.AIR;
	}
	
	/**
	 * Check whether the tiles and their geological features of this world are proper.
	 * 
	 * @return | for each tile in tiles.key
	 * 		 	| 	canHaveAsTile(tile) && canHaveAsGeologicalFeature(tiles.get(tile))
	 * 
	 */
	public boolean hasProperTiles(){
		for (Tile tile: tiles.keySet())
			if (!canHaveAsTile(tile) || !canHaveAsGeologicalFeature(tiles.get(tile)))
				return false;
		return true;
	}
	
	/**
	 * Check whether the given collision object overlaps with an impassable tile in this world.
	 * 
	 * @param collisionObject
	 * @return	| for some feature in getAllOverlappingTileFeatures(collisionObject)
	 * 			|	feature.isImpassable()
	 */
	public boolean overlapsWithImpassableTile(CollisionObject collisionObject){
		Set<GeologicalFeature> features = getAllOverlappingTileFeatures(collisionObject);
		for (GeologicalFeature feature: features){
			if (feature.isImpassable())
				return true;
		}
		return false;
	}
	
	/**
	 * Set the given given geological feature to the tile of this world with the given x tile and y tile.
	 * 
	 * @param xTile
	 * @param yTile
	 * @param feature
	 * @pre	|canAddObjects()
	 * @pre	|feature != null
	 * @pre |canHaveAsTile(new Tile(xTile, yTile))
	 * @post | if (feature == GeologicalFeature.AIR)
	 * 		 |	then tiles.remove(tile)
	 * @post |if (feature != GeologicalFeature.AIR)
	 * 		 |	then tiles.put(tile,feature)
	 */
	public void setGeologicalFeatureAt(int xTile, int yTile, GeologicalFeature feature){
		assert canAddObjects();
		assert feature != null;	
		Tile tile = new Tile(xTile, yTile, this);
		assert canHaveAsTile(tile);
		if (feature == GeologicalFeature.AIR){ // sparse map, air by default
			tiles.remove(tile); // vergelijkt hier objecten, moeten values zijn (opgelost)
		} else {
			tiles.put(tile, feature); // wordt overschreven indien reeds aanwezig
		}
	}
	
	/**
	 * Variable referencing a map collecting all tiles with their corresponding geological feature of this world.
	 * 
	 * @invar | tiles != null
	 * @invar | for each tile in tiles.keySet()
	 * 			| canHaveAsTile(tile) // can not be null
	 * @invar | for each tile in tiles.keySet()
	 * 			| canHaveAsGeologicalFeature(tiles.get(tile))
	 * 
	 */
	private Map<Tile, GeologicalFeature> tiles = new HashMap<Tile, GeologicalFeature>();
	
	
	// ASSOCIATION GAME OBJECTS
	
		// GAME OBJECTS
	
	/**
	 * Check whether this world has the given game object as one of the game objects attached to it.
	 * 
	 * @param gameObject
	 * @return | getAllGameObjects().contains(gameObject)
	 */
	// @Raw because in documentation of addAsGameObject & removeAsGameObject in GameObject
	@Raw
	public boolean hasAsGameObject(@Raw GameObject gameObject){
		return getAllGameObjects().contains(gameObject);
	}
	
	/**
	 * Check whether this world can have the given game object as one of its associated game objects.
	 * 
	 * @param gameObject
	 * @return | result == 
	 * 			| gameObject == null ||
	 * 			| (gameObject.isTerminated() && gameObject.getWorld() == null) || 
	 * 			| (	!gameObject.isTerminated() && 
	 * 			| isInXRange(gameObject.getXPosition()) &&
	 * 			| isInYRange(gameObject.getYPosition())	);	
	 */
	public boolean canHaveAsGameObject(GameObject gameObject){
		return gameObject == null ||
				(	!gameObject.isTerminated() && 
					canHaveAsPosition(gameObject)	);	
	}
	
	/**
	 * Check whether this world can add the given game object.
	 * 
	 * @param gameObject
	 * @return | if (!canAddObjects() || !canAddPosition(gameObject))
	 * 			| 	then return false
	 * 			|else if (gameObject instanceof Mazub)
	 * 			|	then (getMazub() == null)
	 * 			| else if (gameObject instanceof NPCObject && getNbNpcs() < getMaxNbNpcs())
	 * 			|	if (gameObject instanceof Slime)
	 * 			|		then result == canAddSchool(((Slime)gameObject))
	 * 			| 	else
	 * 			|		then result == true
	 * 			| else result == false
	 */
	public boolean canAdd(GameObject gameObject){
		if (!canAddObjects() || !canAddPosition(gameObject))
			return false;
		if (gameObject instanceof Mazub)
			return (getMazub() == null);
		if (gameObject instanceof NPCObject && getNbNpcs() < getMaxNbNpcs()){
			if (gameObject instanceof Slime)
				return canAddSchool(((Slime)gameObject));
			return true;
		}
		return false;
	}
	
	/**
	 * Check whether the given game object can be added to this world as far as its position is concerned.
	 * 
	 * @param gameObject
	 * @pre		| gameObject != null
	 * @return 	| if (!isInXRange(gameObject.getXPosition()) || ! isInYRange(gameObject.getYPosition()))
	 * 			|	then result == false
	 * 			| else if (overlapsWithImpassableTile(InfoGameObject.infoGameObjectWithoutPerimeter(gameObject)))
	 * 			|	then result == false
	 * 		 	| else then result ==  for each other in getAllGameObjects()
	 * 			|							!gameObject.collidesWith(other))
	 */
	public boolean canAddPosition(GameObject gameObject){
		assert gameObject != null;
		// Not outside the WORLD
		if (!isInXRange(gameObject.getXPosition()) || ! isInYRange(gameObject.getYPosition()))
			return false;
		// game object must not colliding with other game objects (even not one pixel in common)
		Set<GameObject> others = getAllGameObjects();
		for (GameObject other : others){
			if (gameObject.collidesWith(other)) // hier moet collidesWith want nog geen set van met wie botst
				return false;
		}
		// game object must not overlap with impassable tiles, except for its boundary pixels
		return !overlapsWithImpassableTile(InfoGameObject.infoGameObjectWithoutPerimeterOf(gameObject));
	}
	
	/**
	 * Return a set containing all the game objects of this world.
	 * 
	 * @return | result == newHashSet<GameObject>(plants.addAll(slimes.addAll(sharks.addMazub))
	 */
	private Set<GameObject> getAllGameObjects(){
		Set<GameObject> allGameObjects = new HashSet<GameObject>();
		allGameObjects.addAll(plants);
		allGameObjects.addAll(slimes);
		allGameObjects.addAll(sharks);
		if (getMazub() != null)
			allGameObjects.add(getMazub());
		if (getBuzam() != null)
			allGameObjects.add(getBuzam());
		return allGameObjects;
	}
	
	
		// NPC
	
	/**
	 * Return the maximum possible non playable characters of this world.
	 */
	@Basic
	public int getMaxNbNpcs(){
		return 100;
	}
	
	/**
	 * Return a set containing all the non playable characters of this world.
	 * 
	 * @return | result == newHashSet<GameObject>(plants.addAll(slimes.addAll(sharks))
	 */
	public Set<NPCObject> getAllNpcs(){
		Set<NPCObject> allNpc = new HashSet<>();
		allNpc.addAll(plants);
		allNpc.addAll(slimes);
		allNpc.addAll(sharks);
		if (getBuzam() != null)
			allNpc.add(getBuzam());
		return allNpc;
	}

	/**
	 * Check whether this world can have the given non-playable character as one of its NPC's associated with it.
	 * 
	 * @param NPC
	 * @return | result == canHaveAsGameObject(NPC) && (NPC != null);
	 */
	public boolean canHaveAsNpc(NPCObject NPC){
		return canHaveAsGameObject(NPC) && (NPC != null);
	}
	
	/**
	 * Check whether this world has proper NPC as NPC's associated with it.
	 * 
	 * @return	| result == (getNbNpcs() <= getMaxNbNpcs()) &&
	 * 			| for each Npc in getAllNpcs()
	 * 			| 	canHaveAsNpc(Npc) && (Npc.getWorld == this)
	 */
	public boolean hasProperNpcs(){
		for (NPCObject Npc: getAllNpcs())
			if(!canHaveAsNpc(Npc) || (Npc.getWorld() != this))
				return false;
		return getNbNpcs() <= getMaxNbNpcs();
	}
	
	
	/**
	 * Return the number of NPC's associated with this world.
	 * 
	 * @return	| result == getAllNpcs().size()
	 */
	@Raw
	public int getNbNpcs(){
		return getAllNpcs().size();
	}
	
	
	
		// MAZUB
	/**
	 * Check whether this world has the given mazub as its mazub
	 * 
	 * @param mazub
	 * @return	| result == (getMazub() == mazub)
	 */
	// Only for test purposes: getMazub() should be private
	public boolean hasAsMazub(Mazub mazub){
		return getMazub() == mazub;
	}
	
	/**
	 * Check whether this world has a proper Mazub.
	 * 
	 * @return | result == canHaveAsMazub(getMazub()) &&
	 * 			| (getMazub() == null || getMazub().getWorld() == this)
	 */
	public boolean hasProperMazub(){
		return canHaveAsGameObject(getMazub()) &&
				(getMazub() == null || getMazub().getWorld() == this);
	}
	

	/**
	 * Set the mazub of this world to the given mazub.
	 * 
	 * @param mazub
	 * @pre	| (mazub != null && mazub.getWorld() == this) || (mazub == null && getMazub().isTerminated())
	 * @post	|new.getMazub() == mazub
	 * @effect	| if (mazub == null)
	 * 			| 	then setGameStatus(gameStatus.PLAYER_LOST)
	 */
	@Raw
	void setMazub(@Raw Mazub mazub){
		assert (mazub != null && mazub.getWorld() == this) || (mazub == null && getMazub().isTerminated());
		this.mazub = mazub;
		if (mazub == null)
			setGameStatus(GameStatus.PLAYER_LOST);
	}
	
	/**
	 * Return the mazub associated with this world.
	 */
	@Basic @Raw
	private Mazub getMazub(){
		return this.mazub;
	}
	
	/**
	 * Variable referencing the mazub associated with this world.
	 */
	private Mazub mazub = null;
	
		// BUZAM
	
	/**
	 * Return the buzam associated with this world.
	 */
	@Basic @Raw
	public Buzam getBuzam(){
		return this.buzam;
	}
	
	/**
	 * Set the buzam of this world to the given buzam.
	 * 
	 * @param buzam
	 * @pre	| (buzam != null && buzam.getWorld() == this) || (buzam == null && getBuzam().isTerminated())
	 * @post	|new.getBuzam() == buzam
	 */
	@Raw
	void setBuzam(@Raw Buzam buzam){
		assert (buzam != null && buzam.getWorld() == this) || (buzam == null && getBuzam().isTerminated());
		this.buzam = buzam;
	}
	
	/**
	 * Variable referencing the buzam associated with this world.
	 */
	private Buzam buzam = null;
	
		// PLANT
	
	/**
	 * Checks whether this world has the given plant as one of the plants attached to it.
	 * 
	 * @param plant
	 * @return plants.contains(plant)
	 */
	// @Raw because in precondition of removeAsPlant
	@Basic @Raw
	public boolean hasAsPlant(Plant plant){
		return plants.contains(plant);
	}
	
	/**
	 * Return all the plants associated with this world.
	 */
	
	@Basic
	public Set<Plant> getAllPlants(){
		return new HashSet<Plant>(plants);
	}
	
	/**
	 * Add the given plant as one of the associated plants of this world.
	 * 
	 * @param plant
	 * @pre	| plant.getWorld() == this
	 * @post	| new.hasAsPlant(this)
	 */
	@Model
	void addAsPlant(@Raw Plant plant){
		assert plant.getWorld() == this;
		this.plants.add(plant);
	}
	
	/**
	 * Remove the given plant as one of the associated plants of this world.
	 * 
	 * @param plant
	 * @pre	| hasAsPlant(plant)
	 * @pre	| plant.getWorld() != this
	 * @post| !new.hasAsPlant(this)
	 */
	
	@Raw @Model
	void removeAsPlant(Plant plant){
		assert hasAsPlant(plant);
		assert plant.getWorld() != this;
		this.plants.remove(plant);
	}
	
	/**
	 * Set collecting references to the plants of this world.
	 * 
	 * @invar	| plants != null
	 * @invar	| for each plant in plants:
	 * 			| 	canHaveAsNpc(plant)
	 * @invar	| for each plant in plants:
	 * 			|	plant.getWorld() == this || (	plant.getWorld() == null && plant.isTerminated()	)
	 */
	private Set<Plant> plants = new HashSet<>();
	
		// SLIME
	
	/**
	 * Checks whether this world has the given slime as one of the slimes attached to it.
	 * 
	 * @param slime
	 * @return slimes.contains(slime)
	 */
	// @Raw because in precondition of removeAsSlime
	@Basic @Raw
	public boolean hasAsSlime(Slime slime){
		return slimes.contains(slime);
	}
		
	/**
	 * Return all the slimes associated with this world.
	 */
	
	@Basic
	public Set<Slime> getAllSlimes(){
		return new HashSet<Slime>(slimes); // information leaking
	}
	
	/**
	 * Add the given slime as one of the associated slimes of this world.
	 * 
	 * @param slime
	 * @pre	| slime.getWorld() == this
	 * @post	| new.hasAsSlime(this)
	 */
	@Model
	void addAsSlime(@Raw Slime slime){
		assert slime.getWorld() == this;
		this.slimes.add(slime);
	}
	
	/**
	 * Remove the given slime as one of the associated slimes of this world.
	 * 
	 * @param slime
	 * @pre	| hasAsSlime(slime)
	 * @pre	| slime.getWorld() != this
	 * @post	| !new.hasAsSlime(this)
	 */
	@Raw @Model
	void removeAsSlime(Slime slime){
		assert hasAsSlime(slime);
		assert slime.getWorld() != this;
		this.slimes.remove(slime);
	}
	
	/**
	 * Set collecting references to the slimes of this world.
	 * 
	 * @invar	| slimes != null
	 * @invar	| for each slime in slimes:
	 * 			| 	canHaveAsNpc(slime)
	 * @invar	| for each slime in slimes:
	 * 			|	slime.getWorld() == this || (	slime.getWorld() == null && slime.isTerminated()	)
	 */
	private Set<Slime> slimes = new HashSet<>();
	
	
		// SHARK
	
	/**
	 * Checks whether this world has the given shark as one of the sharks attached to it.
	 * 
	 * @param shark
	 * @return sharks.contains(shark)
	 */
	// @Raw because in precondition of removeAsShark
	@Basic @Raw
	public boolean hasAsShark(Shark shark){
		return sharks.contains(shark);
	}
	
	/**
	 * Return all the sharks associated with this world.
	 */
	@Basic
	public Set<Shark> getAllSharks(){
		return new HashSet<Shark>(sharks);
	}
	
	/**
	 * Add the given shark as one of the associated sharks of this world.
	 * 
	 * @param shark
	 * @pre	| shark.getWorld() == this
	 * @post	| new.hasAsShark(this)
	 */
	@Model
	void addAsShark(@Raw Shark shark){
		assert shark.getWorld() == this;
		this.sharks.add(shark);
	} 
	
	/**
	 * Remove the given shark as one of the associated sharks of this world.
	 * 
	 * @param shark
	 * @pre	| hasAsshark(plant)
	 * @pre	| shark.getWorld() != this
	 * @post	| !new.hasAsShark(this)
	 */
	@Raw @Model
	void removeAsShark(Shark shark){
		assert hasAsShark(shark);
		assert shark.getWorld() != this;
		this.sharks.remove(shark);
	}
	
	/**
	 * Set collecting references to the sharks of this world.
	 * 
	 * @invar	| slimes != null
	 * @invar	| for each shark in sharks:
	 * 			| 	canHaveAsNpc(shark)
	 * @invar	| for each shark in sharks:
	 * 			|	shark.getWorld() == this || (	shark.getWorld() == null && shark.isTerminated()	)
	 */
	private Set<Shark> sharks = new HashSet<>();
	
	
	
		// SCHOOLS
	/**
	 * Return the maximum number of schools of this world.
	 */
	@Basic
	public int getMaxNbSchools(){
		return 10;
	}
	
	/**
	 * Check whether the school of the given slime can be added.
	 * 
	 * @param newSlime
	 * @return | result == 
	 * 			| (	  	new HashSet<>(for each slime in slimes: slime.getSchool())		).add(newSlime).size() <= getMaxNbSchools
	 */
	public boolean canAddSchool(Slime newSlime){
		Set<School> schools = new HashSet<>();
		schools.add(newSlime.getSchool());
		for (Slime slime : slimes)
			schools.add(slime.getSchool());
		return (schools.size() <= getMaxNbSchools());
			
	}
	
	
	// COLLISION
	
	/**
	 * Return a set of all colliding objects with the given game object.
	 * 
	 * @param gameObject
	 * @pre	| gameObject != null
	 * @return | for each other in getAllGameObjects()
	 * 			| if (other != gameObject && gameObject.collidesWith(other))
	 * 			|	then result.hasAsCollidingObject(other)
	 */
	public Set<GameObject> getCollidingObjectsWith(GameObject gameObject){
		assert gameObject != null;
		Set<GameObject> result = new HashSet<>();
		Set<GameObject> others = getAllGameObjects();
		others.remove(gameObject);
		for (GameObject other: others)
			if (gameObject.collidesWith(other))
				result.add(other);
		return result;
	}
	
	
	/**
	 * Check whether the given game objects will intereferes with other game objects if this game object grows to the size of the given info game object.
	 * 
	 * @param infoGameObject
	 * @pre		| (infoGameObject != null)
	 * @pre		| (gameObject != null)
	 * @return	| result ==
	 * 			|	for some collidingObject in getAllGameObjects()
	 * 			|		infoGameObject.collidesWith(collidingObject) && interferesMovementWith(collidingObject) && infoGameObject.getYPixel() <= collidingObject.getYPixel()
	 */
	public boolean hasInterferingGameObjectsIfGameObjectRisesToSizeOf(InfoGameObject infoGameObject, GameObject gameObject){
		assert (infoGameObject != null);
		assert (gameObject != null);
		// Need to check all the game objects and not only the colliding objects (i.e. the game objects with one pixel in common).
		for (GameObject collidingObject: getAllGameObjects()){
			if (infoGameObject.collidesWith(collidingObject) && gameObject.interferesMovementWith(collidingObject) && infoGameObject.getYPixel() <= collidingObject.getYPixel()){
					return true;
			}
		}
		return false;
	}
	
	// TIME

	/**
	 * Return the time of this world.
	 */
	@Basic
	public double getTime(){
		return this.time;
	}
	
	/**
	 * Set the time of this world to the given time.
	 * 
	 * @param time
	 * @post	| new.getTime() == thime
	 * @throws IllegalArgumentException
	 * 			| (!isValidTime(time) || !fuzzyGreaterThanOrEqualTo(time, getTime()))
	 */
	private void setTime(double time) throws IllegalArgumentException{
		if (!isValidTime(time) || !fuzzyGreaterThanOrEqualTo(time, getTime()))
			throw new IllegalArgumentException("Illegal time " + time);
		this.time = time;
	}
	
	/**
	 * Advance the time for this world with a certain amount.
	 * 
	 * @note	No documentation is required for this method.
	 */
	public void advanceTime(double Dt) throws IllegalArgumentException, IllegalStateException{
		if (!isValidTimeInterval(Dt))
			throw new IllegalArgumentException("Illegal time interval: " + Dt);
		/*if (getGameStatus() != GameStatus.STARTED)
			throw new IllegalArgumentException("Can't advance time if the game has not been started yet");*/
		if (!isGameOver()){
			// General timer 
			setTime(getTime() + Dt);
			// Start with Mazub
			advanceTimeInFractions(getMazub(), Dt);
			if (!isGameOver()){
				// Window
				updateGameWindow();
				// Victory?
				updatePlayerVictory();
				// Other game objects
				for (GameObject gameObject: getAllNpcs()){
					advanceTimeInFractions(gameObject, Dt);
				}
			}
		}
	}
	
	/**
	 * Advance the time for the given game object with the given amount of time.
	 * 
	 * @note	No documentation is required for this method.
	 */
	private void advanceTimeInFractions(GameObject gameObject, double Dt){
		assert isValidTimeInterval(Dt);
		assert gameObject != null;
		double timeLeftToAdvance = Dt;
		double dt = gameObject.computeDtFraction(Dt);
		while(!gameObject.isTerminated()){
			if (dt < timeLeftToAdvance){
				gameObject.advanceTime(dt);
				timeLeftToAdvance -= dt;
			}
			else{
				gameObject.advanceTime(timeLeftToAdvance);
				break;
			}
		}
	}
	
	/**
	 * Variable referencing the time of this world.
	 */
	private double time = 0;
	
	
	// TERMINATION
	
	/*If the game is over, there will be no references anymore to objects of this game.
	 * Therefore we don't need to explicity terminate the world.The garbage collector will delete all the objects of this game.*/
	
	
	// STREAM
	
	/**
	 * Return a stream with all the game objects and all the terrain tiles.
	 * 
	 * @return result == Stream.concat(getAllGameObjects().stream(), getAllTerrainTiles())
	 */
    public Stream<ObjectType> stream() { 
    	return Stream.concat(getAllGameObjects().stream(), getAllTerrainTiles());
    }
    
    /**
     * Return a stream with all the terrain tiles.
     * 
     * @return result == tiles.keySet().stream().filter(
							x->tiles.get(x) == GeologicalFeature.SOLID)
     */
	public Stream<Tile> getAllTerrainTiles(){
		return tiles.keySet().stream().filter(
				x->tiles.get(x) == GeologicalFeature.SOLID);
	}
    
    
	
}
