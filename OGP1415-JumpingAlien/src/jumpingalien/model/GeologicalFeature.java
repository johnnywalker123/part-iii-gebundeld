package jumpingalien.model;

import be.kuleuven.cs.som.annotate.*;

/* Klassenaam MOET noun */

@Value
public enum GeologicalFeature {
	AIR{
		
		@Override
		public boolean isImpassable(){
			return false;
		}

		@Override
		public boolean hurtsImmediately() {
			return false;
		}
	},
	
	SOLID{
		
		@Override
		public boolean isImpassable(){
			return true;
		}

		@Override
		public boolean hurtsImmediately() {
			return false;
		}
	},
	
	WATER{
		
		@Override
		public boolean isImpassable(){
			return false;
		}

		@Override
		public boolean hurtsImmediately() {
			return false;
		}
	},
	
	MAGMA{
		
		@Override
		public boolean isImpassable(){
			return false;
		}

		@Override
		public boolean hurtsImmediately() {
			return true;
		}
	};
	
	public abstract boolean isImpassable();
	public abstract boolean hurtsImmediately();
}
