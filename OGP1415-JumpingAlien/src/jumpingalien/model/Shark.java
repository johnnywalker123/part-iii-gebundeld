package jumpingalien.model;


import static jumpingalien.model.extension.RandomExtension.*;
import static jumpingalien.util.Util.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import be.kuleuven.cs.som.annotate.*;
import jumpingalien.model.program.Program;
import jumpingalien.util.Sprite;


/**
 * A class of sharks.
 * 
 * @invar	| hasProperJumper()
 * @invar	| canHaveAsNbNonJumpingMovements(getNbNonJumpingMovements())
 * @invar	| canHaveAsRandomVerticalAcceleration(getCurrentRandomAcceleration())
 * @invar	| canHaveAsNbNonJumpingMovements(getNbNonJumpingMovements())
 * 
 * @version 3.0
 * @author  Antoon Purnal and Tuur Van Daele
 *
 */
public class Shark extends NPCObject implements Jumpable{
	
	/**
	 * Initialize a new Shark.
	 * 
	 * @param xPixel
	 * @param yPixel
	 * @param images
	 * 
	 * @effect 	| super(xPixel, yPixel, getInitialHitPoints(), images)
	 * @post	| hasProperJumper()
	 * @post	| new.getNbNonJumpingMovements() == 0
	 * @post	| new.getCurrentRandomAcceleration == 0
	 */
	public Shark(int xPixel, int yPixel, Sprite[] images, Program program) {
		super(xPixel, yPixel, getInitialHitPoints(), images, program);
		// This object can jump
		Jumper jumper = new Jumper(this);
		assert canHaveAsJumper(jumper);
		this.jumper = jumper;
	}
	
	// MOVEMENT LIMITATIONS
	
	
	/**
	 * Check whether this Shark can move horizontally (always true).
	 */
	@Basic @Override @Immutable
	public boolean canMoveHorizontally(){ // moet instance zijn omdat het de methode in de superklasse moet overschrijven
		return true;
	}
	
	/**
	 * Check whether this Shark can move vertically (always true).
	 */
	@Basic @Override @Immutable
	public boolean canMoveVertically(){
		return true;
	}
	
	
	
	// JUMPER ASSOCIATION
	
	/**
	 * Return the associated Jumper object for this Shark.
	 */
	@Basic
	public Jumper getJumper(){
		return this.jumper;
	}
	
	/**
	 * Return whether this Shark can have the supplied Jumper object as jumper object.
	 * 
	 * @param jumper
	 * 			The Jumper object for which to verify correctness.
	 * @return True if and only if the supplied Jumper is not equal to the null reference, and if the Jumper can have
	 * 			this Shark as its jumping object.
	 * 			| result == (jumper != null && jumper.canHaveAsJumpingObject(this))
	 */
	public boolean canHaveAsJumper(Jumper jumper){
		return jumper != null && jumper.canHaveAsJumpingObject(this);
	}
	
	/**
	 * Check whether this Shark currently has a proper Jumper object associated to it.
	 * 
	 * @return True if and only if this Shark can have its current Jumper as a Jumper object, and the Jumper Object is
	 * 			correspondingly associated to this Shark.
	 * 			result == (canHaveAsJumper(getJumper())
					&& getJumper().getJumpingObject() == this)
	 */
	public boolean hasProperJumper(){
		return canHaveAsJumper(getJumper())
				&& getJumper().getJumpingObject() == this;
	}
	
	/**
	 * Variable containing this Shark's associated Jumper object.
	 */
	private final Jumper jumper;
	
	
		
	// JUMPING
	
	
	/**
	 * Return the initial vertical velocity when jumping for this Shark (always 8 m/s).
	 */
	@Basic @Override @Immutable
	public double getInitialYVelocity() {
		return 2;
	}
	
	/**
	 * Initiate jump for this Shark.
	 * 
	 * @effect 
	 * 			| getJumper.startJump()
	 * 
	 * @throws IllegalStateException
	 * 			| getJumper == null || !isAlive()
	 */
	@Model @Override
	public void startJump() throws IllegalStateException{
		if (getJumper() == null)
			throw new IllegalStateException("This shark is not associated with a jumping object");
		if (!isAlive())
			throw new IllegalStateException("This shark is not alive and consequently cannot start jumping.");
		getJumper().startJump();
		resetNbNonJumpingMovements();
		setRandomAcceleration(0);
	}
	
	
	/**
	 * Terminate jump for this Shark.
	 * 
	 * @effect
	 * 			| getJumper.endJump()
	 * 
	 * @throws IllegalStateException
	 * 			| getJumper == null || !isAlive()
	 */
	@Model @Override
	public void endJump() throws IllegalStateException{
		if (getJumper() == null)
			throw new IllegalStateException("This shark is not associated with a jumping object");
		if (!isAlive())
			throw new IllegalStateException("This shark is not alive and consequently cannot end the jump.");
		getJumper().endJump();
	}


	
	//MOVEMENT
		// GENERAL
			// RANDOM MOVEMENT DURATION
	
	/**
	 * Return the minimal movement duration for this Shark (always 1).
	 */
	@Basic @Immutable
	public double getMinMovementDuration(){
		return 1;
	}
	
	/**
	 * Return the maximal movement duration for this Shark (always 4).
	 */
	@Basic @Immutable
	public double getMaxMovementDuration(){
		return 4;
	}
	
	
	/**
	 * Return the appropriate direction of movement for this NPC object.
	 * 
	 * @return | if (fuzzyEquals(getRemainingMovementDuration(),0))
	 * 			| then result == pickRandom(HorizontalDirection.RIGHT, HorizontalDirection.LEFT)
	 * @return  | if (!fuzzyEquals(getRemainingMovementDuration(),0))
	 * 			| then result == getOrientation()
	 */
	@Override
	public HorizontalDirection getAppropriateDirection() {
		if (fuzzyEquals(getRemainingMovementDuration(),0))
			return pickRandom(HorizontalDirection.RIGHT, HorizontalDirection.LEFT);
		else return getHorizontalDirection();
	}
	

	/**
	 * Check whether this Shark will jump (outcome is random).
	 * 
	 * @effect random.nextBoolean()
	 */
	public boolean shallJump(){
		return random.nextBoolean();
	}
	
	/**
	 * Check whether this Shark can jump.
	 * 
	 * @return	| (getJumper().canJump() || hasWaterAtBound(VerticalDirection.DOWN)) && getNbNonJumpingMovements() >= getNonJumpingTreshold()
	 */
	public boolean canJump(){
		return ( (getJumper().canJump() || hasWaterAtBound(VerticalDirection.DOWN)) && getNbNonJumpingMovements() >= getNonJumpingTreshold() );
		
	}
	
	/**
	 * Update the jumping state for this Shark.
	 * 
	 * @param dt
	 * @pre		| isValidTimeInterval(dt)
	 * 
	 * @throws IllegalStateException
	 * 			|  getJumper == null || !isAlive()
	 * 
	 * @effect	| if (fuzzyLessThanOrEqualTo(getRemainingMovementDuration() - dt,0) && shallJump() && canJump())
	 * 			| 	then endJump() && startJump()
	 * @effect 	| if (fuzzyLessThanOrEqualTo(getRemainingMovementDuration() - dt,0) && !(shallJump() && canJump()))
	 * 			| 	then endJump() && incrementNbNonJumpingMomevents() && setRandomAcceleration(nextDouble(getMinVerticalAcceleration(), getMaxVerticalAcceleration()))
	 */
	@Model
	private void updateJump(double dt) throws IllegalStateException{
		assert isValidTimeInterval(dt);
		if (!isAlive() || getJumper() == null){
			throw new IllegalStateException();
		}
		if (fuzzyLessThanOrEqualTo(getRemainingMovementDuration() - dt,0)){
			endJump();
			if (canJump() && shallJump()){
				startJump();
			}
			else {
				incrementNbNonJumpingMomevents();
				setRandomAcceleration(nextDouble(getMinVerticalAcceleration(), getMaxVerticalAcceleration()));
			}
		}
			
	}
	
	
	
			// AMOUNT OF NON-JUMPING MOVEMENTS
	
	/**
	 * Get the current number of consequent non-jumping movements.
	 */
	@Basic
	public int getNbNonJumpingMovements(){
		return this.nbNonJumpingMovements;
	}
	
	/**
	 * Get the minimal amount of non-jumping movements for any Shark. (always 4)
	 */
	@Basic @Immutable
	public static int getNonJumpingTreshold(){
		return 4;
	}
	
	/**
	 * Check whether this Shark can have the given number of non-jumping movements as its
	 * non-jumping movements.
	 * 
	 * @param nbNonJumpingMovements
	 * 
	 * @return	| result == (nbNonJumpingMovements >= 0)
	 */
	public boolean canHaveAsNbNonJumpingMovements(int nbNonJumpingMovements){
		return nbNonJumpingMovements >= 0;
	}
	
	
	/**
	 * Increment the nummber of non-jumping movements for this Shark.
	 * 
	 * @post | new.getNbNonJumpingMovements() == getNbNonJumpingMovements + 1
	 */
	@Model
	private void incrementNbNonJumpingMomevents(){
		this.nbNonJumpingMovements = getNbNonJumpingMovements() + 1;
	}
	
	/**
	 * Reset the nummber of non-jumping movements for this Shark.
	 * 
	 * @post | new.getNbNonJumpingMovements() == 0
	 */
	@Model
	private void resetNbNonJumpingMovements(){
		this.nbNonJumpingMovements = 0;
	}
	
	
	
	
	/**
	 * Variable containing the current amount of non-jumping movements for this Shark 
	 * 		(initialized to the threshold so the shark can jump on its first movement).
	 */
	private int nbNonJumpingMovements = getNonJumpingTreshold();
	


		
		// VELOCITY
	
	/**
	 * Return the maximal horizontal velocity for this Shark (always 4).
	 */
	@Basic @Override @Immutable
	public double getMaxXVelocity() {
		return 4;
	}
	
	/**
	 * Return the initial horizontal velocity fot this Shark (always 1).
	 */
	@Basic @Override @Immutable
	public double getInitialXVelocity() {
		return 1;
	}
	
	

		// ACCELERATION
	
	/**
	 * Get the current horizontal acceleration for this Shark.
	 * 
	 * @return |	(hasConstantXVelocity()) ? 0 : 1.5
	 */
	@Override
	public double getXAcceleration() {
		if (hasConstantXVelocity())
			return 0;
		return 1.5;
	}
	
	
	
	// VERTICAL MOVEMENT
	
		// GENERAL
	
	
	/**
	 * Check whether this Shark should fall.
	 * 
	 * @return (super.shouldFall() && !hasWaterAtBound(VerticalDirection.UP))
	 */
	@Override
	public boolean shouldFall() {
		return (super.shouldFall() && !hasWaterAtBound(VerticalDirection.UP));
	}

	
		// VELOCITY
	
	
	/**
	 * Check whether this game objects's vertical velocity should be set to zero.3
	 * 
	 * @return 	| result == ((hasWaterAtBound(VerticalDirection.UP) && getYVelocity()< 0 && getYAcceleration() == 0) || impassableEntityInDirection(VerticalDirection.UP))
	 */
	@Override
	public boolean shouldSetYVelocityToZero() {
		return ((hasWaterAtBound(VerticalDirection.UP) && getYVelocity()< 0 && getYAcceleration() == 0) || impassableEntityIn(VerticalDirection.UP));
	}
	
		// ACCELERATION
	
	/**
	 * Return the minimal vertical acceleration for any Shark, during a non-jumping period.
	 */
	@Basic @Immutable
	public static double getMinVerticalAcceleration(){
		return -0.2;
	}
	
	/**
	 *  Return the maximal vertical acceleration for any Shark, during a non-jumping period.
	 */
	@Basic @Immutable
	public static double getMaxVerticalAcceleration(){
		return 0.2;
	}
	
	/**
	 * Get the current vertical acceleration for this game Shark.
	 * 
	 * @return | if (isTerminated())
	 * 			| result ==  0
	 * @return | if (hasWaterAtBound(VerticalDirection.DOWN) && hasWaterAtBound(VerticalDirection.UP))
	 * 			| result ==  getCurrentRandomAcceleration()
	 * @return 	| else result == super.getYAcceleration()
	 */
	@Override
	public double getYAcceleration(){
		if (isTerminated())
			return 0;
		if (hasWaterAtBound(VerticalDirection.DOWN) && hasWaterAtBound(VerticalDirection.UP)){
			return getCurrentRandomAcceleration();
		}
		else
			return super.getYAcceleration();
	}
	
	/**
	 * Get the current vertical acceleration for this Shark, given that it's not jumping.
	 */
	@Basic
	public double getCurrentRandomAcceleration(){
		return this.currentRandomAcceleration;
	}
	
	/**
	 * Check whether this Shark can have the given vertical acceleration as its
	 * 	random vertical acceleration when under water.
	 * 
	 * @param randomVerticalAcceleration
	 * 
	 * @return	| result == (randomVerticalAcceleration >= getMinVerticalAcceleration() &&
				|	randomVerticalAcceleration <= getMaxVerticalAcceleration())
	 */
	public boolean canHaveAsRandomVerticalAcceleration(double randomVerticalAcceleration){
		return (randomVerticalAcceleration >= getMinVerticalAcceleration() &&
				randomVerticalAcceleration <= getMaxVerticalAcceleration());
	}
	
	/**
	 * Set the random acceleration for this Shark to the given value.
	 * 
	 * @param randomAcceleration
	 * 
	 * @pre	| canHaveAsRandomVerticalAcceleration(randomAcceleration)
	 * 
	 * @post | new.getCurrentRandomAcceleration() == randomAcceleration
	 * 
	 */
	@Model
	private void setRandomAcceleration(double randomAcceleration){
		assert canHaveAsRandomVerticalAcceleration(randomAcceleration);
		this.currentRandomAcceleration = randomAcceleration;
	}
	
	
	/**
	 * Variable containing the current random vertical acceleration.
	 */
	private double currentRandomAcceleration;
	
	// SPRITE
	
	/**
	 * Get the new appropriate sprite index for this shark.
	 * 
	 * @return	| result == ((getOrientation() == HorizontalDirection.RIGHT) ? 1 : 0);
	 */
	@Override
	public int getNewSpriteIndex(){	
		return super.getNewSpriteIndex();
	}
	
	
	// HITPOINTS
	
	/**
	 * Return the initial hitpoints for any Shark.
	 */
	@Basic @Immutable
	public static int getInitialHitPoints(){
		return 100;
	}
	
	/**
	 * Update the hitpointsof this and the given other game object caused by the collision.
	 * 
	 * @effect	| if ((other != null) && hasAsCollidingObject(other) && !this.isTerminated() && !other.isTerminated() && !(other instanceof Plant) && 
	 * 			| 	!(other instanceof Shark) && !isImmune() && !other.isImmune())
	 * 			| 		then if (other instanceof Slime)
	 * 			|				then this.updateHitPoints(-50) && this.resetImmunityTimer() && other.updateHitPoints(-50) && other.resetImmunityTimer()
	 * 			|		else if (other instanceof Mazub)
	 * 			|				if (other.isOnTopOf(this))
	 * 			|					then this.updateHitPoints(-50) && this.resetImmunityTimer()
	 * 			|				else this.updateHitPoints(-50) && this.resetImmunityTimer() && other.updateHitPoints(-50) && other.resetImmunityTimer()
	 * 			| 		else other.updateHitPointsUponCollisionWith(this)
	 * 			|		
	 */
	@Override
	public void updateHitPointsUponCollisionWith(GameObject other){
		if (other == null || !hasAsCollidingObject(other) || this.isTerminated() || other.isTerminated());
		else if (other instanceof Plant || other instanceof Shark || isImmune() || other.isImmune());
		else if(other instanceof Slime){
			this.updateHitPoints(-50);
			this.resetImmunityTimer();
			other.updateHitPoints(-50);
			other.resetImmunityTimer();
		}
		else if(other instanceof Mazub){
			this.updateHitPoints(-50);
			this.resetImmunityTimer();
			if (!other.isOnTopOf(this)){
				other.updateHitPoints(-50);
				other.resetImmunityTimer();
			}
		}
		else
			other.updateHitPointsUponCollisionWith(this);
	}
	


	// ASSOCIATION WITH WORLD
	
	
	/**
	 * Add this game object to the world.
	 * 
	 * @param	world
	 * 
	 * @effect 	| world.addAsShark(this)
	 */
	@Raw @Override
	void addAsGameObject(World world){
		world.addAsShark(this);
	}
	
	/**
	 * Remove this game object from the world.
	 * 
	 * @param	world
	 * 
	 * @effect	| world.removeAsShark(this)
	 */
	@Override
	void removeAsGameObject(@Raw World world){
		world.removeAsShark(this);
	}

	// COLLISION
	
	/**
	 * Check whether this Shark and the supplied game object block each other's movement.
	 * 
	 * @param other
	 * 			The other game object to check for.
	 * 
	 * @return If the other object is equal to the null reference, or is a plant, no movement shall be blocked.
	 * 				If the other object is a Slime or a Shark or a Mazub, movement shall be blocked.
	 * 					Otherwise, return whether the other object interferes movement with this Shark.
	 * 			| if (other == null || other instanceof Plant)
	 * 			| 	then return false
	 * 			| else if (other instanceof Slime || other instanceof Shark || other instanceof Mazub)
	 * 			|  then return true
	 * 			| else return other.interferesMovementWith(this)
	 */
	@Override
	public boolean interferesMovementWith(GameObject other) {
		if (other == null)
			return false;
		if (other instanceof Plant)
			return false;
		if (other instanceof Slime || other instanceof Shark || other instanceof Mazub)
			return true;
		return other.interferesMovementWith(this);
	}

	// TIME
	
	/**
	 * Advance the time for this Shark with a given amount.
	 * 
	 * @note	No documentation is to be written for this method.
	 */
	@Override
	void advanceTime(double dt)  throws IllegalArgumentException, IllegalStateException{
		if (!isValidTimeInterval(dt))
			throw new IllegalArgumentException("Illegal time interval: " + dt);
		if (!fuzzyLessThanOrEqualTo(getTime() + dt, getWorld().getTime()))
			throw new IllegalArgumentException("The time of the world must always be higher than the time of this shark");
		if (isTerminated())
			throw new IllegalStateException("Cannot advance the time for a terminated object");
		if (isAlive()){
			updateJump(dt);
		}
		super.advanceTime(dt);
	}

	

	// WATER
	
	/**
	 * Check whether this Shark has water at its boundary in the given direction.
	 * 
	 * @param direction
	 * 			the direction for which to check the boundary.
	 * 
	 * @return | Set<GeologicalFeature> boundFeatures = 
				| getWorld().getAllOverlappingTileFeatures(InfoGameObject.infoGameObjectPerimeter(this, direction))
					| return boundFeatures.contains(GeologicalFeature.WATER);
	 */
	public boolean hasWaterAtBound(VerticalDirection direction){
		Set<GeologicalFeature> boundFeatures = 
				 getWorld().getAllOverlappingTileFeatures(direction.perimeterOf(this));
		return boundFeatures.contains(GeologicalFeature.WATER);
	}
	
	// WEAKNESS
	
	
	/**
	 * Get this Shark's geological weaknesses and corresponding hitpoint losses
	 * 		(always a map containing (GeologicalFeature.AIR=> -2, GeologicalFeature.MAGMA=> -50))	 * 
	 */
	@Basic @Override @Immutable
	public Map<GeologicalFeature,Integer> getWeaknessesAndHitpointLosses() {
		Map <GeologicalFeature, Integer> weaknessesAndLosses =  new HashMap<GeologicalFeature, Integer>();
		weaknessesAndLosses.put(GeologicalFeature.AIR, -2);
		weaknessesAndLosses.put(GeologicalFeature.MAGMA, -50);
		return weaknessesAndLosses;
	};
	


	
	

}
