package jumpingalien.model.expression;

import jumpingalien.model.type.BoolType;

public class Negation extends UnaryExpression<BoolType,BoolType> {
	
	public Negation(Expression<BoolType> operand){
		super(operand,BoolType.class);
	}
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && BoolType.class.isAssignableFrom(getOperand().getType());
	}
	
	@Override
	public BoolType getValue(){
		return getOperand().getValue().negation();
	}
}
