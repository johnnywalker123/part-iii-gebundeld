package jumpingalien.model.expression;


import jumpingalien.model.type.*;

public abstract class BasicExpression<T extends Type> extends Expression<T> {
	
	public BasicExpression(Class<T> type) {
		super(type);
	}
	
	@Override
	public void cascadeProgramToChildren() {
		// pass
	}
	
	// IS WELL FORMED
	
	@Override
	public boolean satisfiesConstraints() {
		return true;
	}
	
	
	// SUBEXPRESSION
	
	@Override
	public final boolean hasAsSubExpression(Expression<? extends Type> expression){
		return this == expression;
	}
	
	
	// TOSTRING

	@Override
	public String toString(){
		if (getValue() != null)
			return getValue().toString();
		else return "null";
	}
	
}

