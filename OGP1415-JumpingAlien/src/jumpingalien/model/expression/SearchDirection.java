package jumpingalien.model.expression;

import java.util.Optional;

import jumpingalien.model.type.DirectionType;
import jumpingalien.model.type.ObjectType;

public class SearchDirection extends UnaryExpression<ObjectType, DirectionType> {
	
	public SearchDirection(Expression<DirectionType> direction) {
		super(direction, ObjectType.class);
	}
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && DirectionType.class.isAssignableFrom(getOperand().getType());
	}
	
	
	@Override
	public ObjectType getValue() {
		Optional<ObjectType> object = getProgram().getExecutingNpc().searchNearestGameObjectInDirection(getOperand().getValue());
		if (object.isPresent())
			return object.get();
		return null;
	}

}
