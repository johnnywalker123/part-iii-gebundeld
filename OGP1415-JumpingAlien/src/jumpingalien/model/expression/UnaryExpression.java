package jumpingalien.model.expression;

import be.kuleuven.cs.som.annotate.Basic;
import jumpingalien.model.type.*;
import be.kuleuven.cs.som.annotate.Immutable;

public abstract class UnaryExpression<T extends Type,S extends Type> extends ComposedExpression<T>{
	
	
	// CONSTRUCTOR
	protected UnaryExpression(Expression<S> operand, Class<T> type){
		super(type);
		assert canHaveAsOperand(operand);
		setOperand(operand);
	}
	
	// OPERANDS

	public Expression<S> getOperandAt(int index) throws IndexOutOfBoundsException{
		if (!canHaveAsIndex(index))
			throw new IndexOutOfBoundsException();
		return getOperand();		
	}
	
	@Basic
	public Expression<S> getOperand(){
		return this.operand;
	}
	
	private void setOperand(Expression<S> operand){
		this.operand = operand;
	}

	@Basic @Immutable
	public final int getNbOperands(){
		return 1;
	}
	
	private Expression<S> operand;
	
	// SATISFIES CONSTRAINTS
	
	@Override
	public boolean satisfiesConstraints() {
		return getOperand() != null && getOperand().satisfiesConstraints();
	}
		
}
