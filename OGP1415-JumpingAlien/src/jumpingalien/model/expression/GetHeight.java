package jumpingalien.model.expression;

import jumpingalien.model.type.*;

public class GetHeight extends UnaryExpression<DoubleType, ObjectType> {
	
		// CONSTRUCTOR
	
		public GetHeight(Expression<ObjectType> operand) {
			super(operand, DoubleType.class);
		}
		
		
		// IS WELL FORMED
		
		@Override
		public boolean satisfiesConstraints() {
			return super.satisfiesConstraints() && ObjectType.class.isAssignableFrom(getOperand().getType());
		}
		
		// VALUE
		
		@Override
		public DoubleType getValue() {
			return new DoubleType(getOperand().getValue().getHeight()); // doet conversion naar double vanzelf
		}

}
