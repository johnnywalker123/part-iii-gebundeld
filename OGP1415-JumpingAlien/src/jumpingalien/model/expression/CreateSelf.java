package jumpingalien.model.expression;

import jumpingalien.model.GameObject;

public class CreateSelf extends BasicExpression<GameObject> {
	
	public CreateSelf() {
		super(GameObject.class);
	}
	
	@Override
	public GameObject getValue() {
		return getProgram().getExecutingNpc();
	}
	
	
	
	

}
