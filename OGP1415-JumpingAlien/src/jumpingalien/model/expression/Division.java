package jumpingalien.model.expression;

import jumpingalien.model.type.DoubleType;

public class Division extends BinaryArithmeticExpression{
	
	public Division(Expression<DoubleType> left, Expression<DoubleType> right){
		super(left,right, DoubleType.class);
	}
	
	@Override
	public DoubleType getValue(){
		return getLeftOperand().getValue().divide(getRightOperand().getValue());
	}
}