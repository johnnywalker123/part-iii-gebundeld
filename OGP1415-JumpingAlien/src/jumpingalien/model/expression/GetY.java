package jumpingalien.model.expression;

import jumpingalien.model.type.DoubleType;
import jumpingalien.model.type.ObjectType;

public class GetY extends UnaryExpression<DoubleType,ObjectType> {

	// CONSTRUCTOR 
	
	public GetY(Expression<ObjectType> operand) {
		super(operand, DoubleType.class);
	}
	
	// IS WELL FORMED
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && ObjectType.class.isAssignableFrom(getOperand().getType());
	}

	// VALUE
	@Override
	public DoubleType getValue() {
		return new DoubleType(getOperand().getValue().getYPixel());
	}
}
