package jumpingalien.model.expression;

import jumpingalien.model.GameObject;
import jumpingalien.model.type.*;

public class GetHP extends UnaryExpression<DoubleType, GameObject> {

	public GetHP(Expression<GameObject> operand) {
		super(operand,DoubleType.class);
	}
	
	// IS WELL FORMED
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() &&  ObjectType.class.isAssignableFrom(getOperand().getType());
	}
	
	@Override
	public DoubleType getValue() {
		return new DoubleType(getOperand().getValue().getHitPoints());
	}
	
}
