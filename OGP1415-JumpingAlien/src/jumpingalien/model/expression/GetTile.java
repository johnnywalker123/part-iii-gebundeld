package jumpingalien.model.expression;

import jumpingalien.model.*;
import jumpingalien.model.type.*;


public class GetTile extends BinaryExpression<ObjectType, DoubleType, DoubleType> {

	public GetTile(Expression<DoubleType> leftOperand, Expression<DoubleType> rightOperand) {
		super(leftOperand, rightOperand, ObjectType.class);
	}
	
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && 
				DoubleType.class.isAssignableFrom(getLeftOperand().getType()) &&
				DoubleType.class.isAssignableFrom(getRightOperand().getType());
	}
	
	@Override
	public ObjectType getValue() {
		World world = getProgram().getExecutingNpc().getWorld();
		
		DoubleType doubleX = getLeftOperand().getValue();
		int intX = doubleX.toInteger();
		
		DoubleType doubleY = getRightOperand().getValue();
		int intY = doubleY.toInteger();
		
		return new Tile(intX, intY, world);
	}
	
}
