package jumpingalien.model.expression;

import jumpingalien.model.type.*;


public class GetX extends UnaryExpression<DoubleType, ObjectType>{

	// CONSTRUCTOR 
	
	public GetX(Expression<ObjectType> operand) {
		super(operand, DoubleType.class);
	}
	
	// IS WELL FORMED
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && ObjectType.class.isAssignableFrom(getOperand().getType());
	}

	// VALUE
	@Override
	public DoubleType getValue() {
		return new DoubleType(getOperand().getValue().getXPixel()); // doet conversion naar double vanzelf
	}
}
