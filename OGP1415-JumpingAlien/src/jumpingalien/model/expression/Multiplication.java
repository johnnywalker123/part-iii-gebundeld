package jumpingalien.model.expression;

import jumpingalien.model.type.DoubleType;

public class Multiplication extends BinaryArithmeticExpression{
	
	public Multiplication(Expression<DoubleType> left, Expression<DoubleType> right){
		super(left,right, DoubleType.class);
	}
	
	@Override
	public DoubleType getValue(){
		return getLeftOperand().getValue().multiply(getRightOperand().getValue());
	}
}