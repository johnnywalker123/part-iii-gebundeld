package jumpingalien.model.expression;

import jumpingalien.model.type.DoubleType;

public abstract class BinaryArithmeticExpression extends BinaryExpression<DoubleType, DoubleType, DoubleType> {
	
	public BinaryArithmeticExpression(Expression<DoubleType> left, Expression<DoubleType> right, 
			Class<DoubleType> type) throws IllegalArgumentException{
		super(left,right,DoubleType.class);
	}
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && 
				DoubleType.class.isAssignableFrom(getLeftOperand().getType()) &&
				DoubleType.class.isAssignableFrom(getRightOperand().getType());
	}


}
