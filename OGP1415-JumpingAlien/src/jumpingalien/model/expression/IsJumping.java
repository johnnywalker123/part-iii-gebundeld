package jumpingalien.model.expression;

import jumpingalien.model.GameObject;
import jumpingalien.model.type.BoolType;
import jumpingalien.model.type.ObjectType;

public class IsJumping extends UnaryExpression<BoolType, ObjectType> {
	
	public IsJumping(Expression<ObjectType> operand) {
		super(operand, BoolType.class);
	}
	
	// IS WELL FORMED
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && ObjectType.class.isAssignableFrom(getOperand().getType());
	}
	
	@Override
	public BoolType getValue() {
		ObjectType object = getOperand().getValue();
		if (object instanceof GameObject){
			GameObject gameObject = (GameObject) object;
			return new BoolType(gameObject.getYVelocity()>0);
		} else return new BoolType(false);
	}

}
