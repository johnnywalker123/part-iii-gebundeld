package jumpingalien.model.expression;

import jumpingalien.model.type.DoubleType;

public class SquareRoot extends UnaryExpression<DoubleType, DoubleType> {
	public SquareRoot(Expression<DoubleType> operand){
		super(operand, DoubleType.class);
		if (operand.getType() != DoubleType.class)
			throw new IllegalArgumentException("Can only squareRoot DoubleType");
	}
	
	// IS WELL FORMED
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && DoubleType.class.isAssignableFrom(getOperand().getType());
	}
	
	@Override
	public DoubleType getValue(){
		return new DoubleType(Math.sqrt(getOperand().getValue().getValue()));
	}
}
