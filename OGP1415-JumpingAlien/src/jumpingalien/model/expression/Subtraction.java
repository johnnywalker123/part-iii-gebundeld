package jumpingalien.model.expression;

import jumpingalien.model.type.DoubleType;

public class Subtraction extends BinaryArithmeticExpression{
	
	
	// CONSTRUCTOR
	
	public Subtraction(Expression<DoubleType> left, Expression<DoubleType> right) throws IllegalArgumentException{
		super(left,right,DoubleType.class);
	}
	
	// VALUE
	
	@Override
	public DoubleType getValue(){
		return getLeftOperand().getValue().minus(getRightOperand().getValue());
	}
	
	
	

}
