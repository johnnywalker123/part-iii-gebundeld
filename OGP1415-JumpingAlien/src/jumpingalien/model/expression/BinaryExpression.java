package jumpingalien.model.expression;

//import resultType.ResultType;
import be.kuleuven.cs.som.annotate.Basic;
import jumpingalien.model.type.*;
import be.kuleuven.cs.som.annotate.Immutable;

public abstract class BinaryExpression<T extends Type,S extends Type, U extends Type> extends ComposedExpression<T>{
	
	
	// CONSTRUCTOR
	protected BinaryExpression(Expression<S> left, Expression<U> right, Class<T> type){
		super(type);
		assert canHaveAsOperand(left);
		assert canHaveAsOperand(right);
		setLeftOperand(left);
		setRightOperand(right);
	}
	
	// OPERANDS
	

	public Expression<? extends Type> getOperandAt(int index) throws IndexOutOfBoundsException{
		if (!canHaveAsIndex(index))
			throw new IndexOutOfBoundsException();
		if (index == 1)
			return getLeftOperand();
		return getRightOperand();		
	}
	
	@Basic
	public Expression<S> getLeftOperand(){
		return this.leftOperand;
	}
	
	@Basic
	public Expression<U> getRightOperand(){
		return this.rightOperand;
	}
	
	// Samen met getLeftOperand() en getRightOperand() @Basic
	@Basic @Immutable
	public final int getNbOperands(){
		return 2;
	}
	
	private void setLeftOperand(Expression<S> leftOperand){
		this.leftOperand = leftOperand;
	}
	
	private void setRightOperand(Expression<U> rightOperand){
		this.rightOperand = rightOperand;
	}
	
	private Expression<S> leftOperand;
	private Expression<U> rightOperand;
	
	// SUBEXPRESSION
	
	@Override
	public boolean hasAsSubExpression(Expression<? extends Type> expression){
		// to do
		return false;
	}
	
	// IS WELL FORMED
	
	@Override
	public boolean satisfiesConstraints() {
		return (getLeftOperand() != null && getLeftOperand().satisfiesConstraints()) &&
				(getRightOperand() != null && getRightOperand().satisfiesConstraints());
	}
	
	
}
