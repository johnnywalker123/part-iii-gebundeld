package jumpingalien.model.expression;

import jumpingalien.model.type.BoolType;
import jumpingalien.model.type.Type;

public class NotEquals<S extends Type> extends BinaryExpression<BoolType,S,S> {
	
	// CONSTRUCTOR
	
	public NotEquals(Expression<S> left, Expression<S> right) throws IllegalArgumentException{
		super(left,right,BoolType.class);
	}
	
	@Override
	public boolean satisfiesConstraints() {
		return (getLeftOperand().getValue() == null || getLeftOperand().satisfiesConstraints()) &&
				(getRightOperand().getValue() == null || getRightOperand().satisfiesConstraints());
	}
	
	// VALUE
	
	@Override
	public BoolType getValue(){
		BoolType equals = new Equals<S>(getLeftOperand(), getRightOperand()).getValue();
		return equals.negation();
	}
}
