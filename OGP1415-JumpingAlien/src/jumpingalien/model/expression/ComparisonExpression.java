package jumpingalien.model.expression;

import jumpingalien.model.type.*;

public class ComparisonExpression extends BinaryExpression<BoolType, DoubleType, DoubleType> {
	
	
	public ComparisonExpression(Expression<DoubleType> left, Expression<DoubleType> right,
			ComparisonOperation operation) {
		super(left, right, BoolType.class);
		this.operation = operation;
		
	}
	
	// OPERATION
	
	public ComparisonOperation getComparisonOperation(){
		return this.operation;
	}
	
	private final ComparisonOperation operation;
	
	
	// IS WELL FORMED
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && DoubleType.class.isAssignableFrom(getLeftOperand().getType())
				&& DoubleType.class.isAssignableFrom(getRightOperand().getType());
	}
	
	@Override
	public BoolType getValue() {
		return getComparisonOperation().getComparisonResult(getLeftOperand(), getRightOperand());
	}
	
	

}
