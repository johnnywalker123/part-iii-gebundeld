package jumpingalien.model.expression;

import jumpingalien.model.GameObject;
import jumpingalien.model.type.BoolType;
import jumpingalien.model.type.ObjectType;

public class IsDead extends UnaryExpression<BoolType, ObjectType> {

	public IsDead(Expression<ObjectType> object) {
		super(object, BoolType.class);
	}
	
	// IS WELL FORMED
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && ObjectType.class.isAssignableFrom(getOperand().getType());
	}
	
	@Override
	public BoolType getValue() {
		if (GameObject.class.isAssignableFrom(getOperand().getType()))
			return new BoolType( ((GameObject)getOperand().getValue()).getHitPoints() == 0);
		else return new BoolType(false);
	}
}
