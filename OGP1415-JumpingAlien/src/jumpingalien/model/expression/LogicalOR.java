package jumpingalien.model.expression;

import jumpingalien.model.type.BoolType;

public class LogicalOR extends BinaryExpression<BoolType,BoolType,BoolType>{
	
	// CONSTRUCTOR
	public LogicalOR(Expression<BoolType> left, Expression<BoolType> right){
		super(left,right,BoolType.class);
	}
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && BoolType.class.isAssignableFrom(getLeftOperand().getType()) &&
				BoolType.class.isAssignableFrom(getRightOperand().getType());			
	}
	
	// VALUE
	public BoolType getValue(){
		return getLeftOperand().getValue().or(getRightOperand().getValue());
	}
}
