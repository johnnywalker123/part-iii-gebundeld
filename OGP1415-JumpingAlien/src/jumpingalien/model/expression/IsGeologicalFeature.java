package jumpingalien.model.expression;

import jumpingalien.model.*;
import jumpingalien.model.type.*;

public class IsGeologicalFeature extends UnaryExpression<BoolType, ObjectType> {
	
	public IsGeologicalFeature(Expression<ObjectType> operand, GeologicalFeature feature) {
		super(operand, BoolType.class);
		this.feature = feature;
	}
	
	
	// SATISFIES CONSTRAINTS
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && ObjectType.class.isAssignableFrom(getOperand().getType());
	}
	
	
	// FEATURE
	
	public GeologicalFeature getGeologicalFeature(){
		return this.feature;
	}
	
	private final GeologicalFeature feature;
	
	@Override
	public BoolType getValue() {
		if (Tile.class.isAssignableFrom(getOperand().getType()))
			return new BoolType( ((Tile)getOperand().getValue()).getGeologicalFeature() == getGeologicalFeature());
		else return new BoolType(false);
	}

}
