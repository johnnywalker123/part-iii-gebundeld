package jumpingalien.model.expression;

import jumpingalien.model.GameObject;
import jumpingalien.model.type.*;

public class IsPassable extends UnaryExpression<BoolType, ObjectType> {
	
	public IsPassable(Expression<ObjectType> operand) {
		super(operand, BoolType.class);
	}
	
	// IS WELL FORMED
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && ObjectType.class.isAssignableFrom(getOperand().getType());
	}
	
	@Override
	public BoolType getValue() {
		ObjectType operand = getOperand().getValue();
		GameObject executingGameObject = getProgram().getExecutingNpc();
		return new BoolType(operand.isPassableFor(executingGameObject));
	}

}
