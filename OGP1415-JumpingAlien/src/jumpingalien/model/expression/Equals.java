package jumpingalien.model.expression;

import jumpingalien.model.type.*;

public class Equals<S extends Type> extends BinaryExpression<BoolType,S,S> {
	// CONSTRUCTOR
	
	public Equals(Expression<S> left, Expression<S> right) throws IllegalArgumentException{
		super(left,right,BoolType.class);
	}
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && (getLeftOperand().getValue() == null || getLeftOperand().satisfiesConstraints()) &&
				(getRightOperand().getValue() == null || getRightOperand().satisfiesConstraints());
	}
	
	// VALUE
	
	@Override
	public BoolType getValue() {
		S leftValue = getLeftOperand().getValue();
		S rightValue = getRightOperand().getValue();
		if (leftValue == null)
			return new BoolType(rightValue == null);
		if (rightValue == null)
			return new BoolType(false);
		return new BoolType(leftValue.equals(rightValue));
	}
}
