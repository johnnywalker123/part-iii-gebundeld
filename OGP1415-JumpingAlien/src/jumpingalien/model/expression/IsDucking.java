package jumpingalien.model.expression;

import jumpingalien.model.*;
import jumpingalien.model.type.*;

public class IsDucking extends UnaryExpression<BoolType, ObjectType> {
	
	public IsDucking(Expression<ObjectType> operand) {
		super(operand, BoolType.class);
	}
	
	// IS WELL FORMED
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && ObjectType.class.isAssignableFrom(getOperand().getType());
	}
	
	@Override
	public BoolType getValue() {
		ObjectType object = getOperand().getValue();
		if (object instanceof Duckable){
			Duckable gameObject = (Duckable) object;
			return new BoolType(gameObject.isDucking());
		} else return new BoolType(false);
	}

}
