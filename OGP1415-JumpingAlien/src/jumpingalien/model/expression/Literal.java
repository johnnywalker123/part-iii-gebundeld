package jumpingalien.model.expression;


import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.type.*;

@Value
public class Literal<T extends Type> extends BasicExpression<T> {
	
	// CONSTRUCTOR
	public Literal(T value,Class<T> type){
		super(type);
		this.value = value;
	}
	
	//VALUE
	public T getValue(){
		return this.value;
	}
	
	private final T value;
	
}
