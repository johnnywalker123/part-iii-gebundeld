package jumpingalien.model.expression;

import jumpingalien.model.type.*;

public class Variable<T extends Type> extends BasicExpression<T> {
	
	// CONSTRUCTOR
	public Variable(String variableName, Class<T> type){
		super(type);
		this.variableName = variableName;
	}
	
	// VARIABLE NAME
	
	public String getVariableName(){
		return this.variableName;
	}
	
	private final String variableName;
	
	
	
	// VALUE
	
	public T getValue(){
		return this.value;
	}
	
	// THIS METHOD IS NOT MEANT TO BE PUBLIC. WE ONLY NEED IT IN ASSIGNMENT.
	// THE GENERIC TYPE T PROHIBITS TO ASSIGN A VALUE OF A WRONG TYPE.
	
	public void setValue(T value){
		this.value = value;
	}
	
	private T value; // MOET DEFAULT
	
}
