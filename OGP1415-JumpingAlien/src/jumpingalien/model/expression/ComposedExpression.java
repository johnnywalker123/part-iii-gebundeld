package jumpingalien.model.expression;


import be.kuleuven.cs.som.annotate.*;
import jumpingalien.model.type.*;

public abstract class ComposedExpression<T extends Type> extends Expression<T>{
	
	public ComposedExpression(Class<T> type) {
		super(type);
	}
	
	// OPERANDS
	@Basic
	public abstract Expression<? extends Type> getOperandAt(int index)
			throws IndexOutOfBoundsException;
	
	@Basic
	public abstract int getNbOperands();
	
	public boolean canHaveAsOperand(Expression<? extends Type> expression){
		return (expression != null) && !expression.hasAsSubExpression(this);
	}
		
	// Vanaf 1 beginnen tellen
	public boolean canHaveAsIndex(int index){
		return (index > 0) && (index <= getNbOperands());
	}
	
	// CASCADE PROGRAM
	
	@Override
	public void cascadeProgramToChildren() {
		for (int i = 1; i <= getNbOperands(); i++){
			getOperandAt(i).setProgram(getProgram());
		}
	}
	
	// SUBEXPRESSION
	
	@Override
	public boolean hasAsSubExpression(Expression<? extends Type> expression){
		if (this == expression)
			return true;
		for (int i = 1; i <= getNbOperands(); i++){
			if (getOperandAt(i).hasAsSubExpression(expression))
				return true;
		}
		return false;
	}
}
