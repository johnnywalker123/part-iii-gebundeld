package jumpingalien.model.expression;

import jumpingalien.model.type.*;

public class ReadVariable<T extends Type> extends BasicExpression<T> {
	// CONSTRUCTOR
	public ReadVariable(String variableName, Class<T> type){
		super(type);
		this.variableName = variableName;
	}
	
	// VARIABLE NAME
	
	public String getVariableName(){
		return this.variableName;
	}
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && getProgram().hasGlobalVariableAt(getVariableName()) &&
		getProgram().getGlobalVariableAt(getVariableName()).getType().isAssignableFrom(getType());
	}
	
	private final String variableName;
	
	// VALUE
	public T getValue(){
		return (T) (getProgram().getGlobalVariableAt(getVariableName()).getValue());
	}
	
}
