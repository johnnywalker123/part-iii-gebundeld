package jumpingalien.model.expression;

import jumpingalien.model.statement.Kind;
import jumpingalien.model.type.*;

public class IsKind extends UnaryExpression<BoolType, ObjectType> {
	
	public IsKind(Expression<ObjectType> operand, Kind kind) {
		super(operand, BoolType.class);
		this.kind = kind;
	}
	
	// IS WELL FORMED
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && ObjectType.class.isAssignableFrom(getOperand().getType());
	}
	
	// KIND
	
	public Kind getKind(){
		return this.kind;
	}
	
	
	public boolean canHaveAsKind(Kind kind){
		return kind != null;
	}
	
	private final Kind kind;
	
	@Override
	public BoolType getValue() {
		return new BoolType(getKind().isKind(getOperand().getValue()));
	}
	
	

}
