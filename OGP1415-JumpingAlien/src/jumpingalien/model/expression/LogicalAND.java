package jumpingalien.model.expression;
import jumpingalien.model.type.BoolType;

public class LogicalAND extends BinaryExpression<BoolType, BoolType, BoolType>{
	
	// CONSTRUCTOR
	public LogicalAND(Expression<BoolType> left, Expression<BoolType> right) throws IllegalArgumentException{
		super(left,right, BoolType.class);
		if(left.getType() != BoolType.class || right.getType() != BoolType.class){
			throw new IllegalArgumentException("Can only AND BoolTypes");
		}
	}
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && BoolType.class.isAssignableFrom(getLeftOperand().getType()) &&
				BoolType.class.isAssignableFrom(getRightOperand().getType());			
	}
	
	
	// VALUE
	public BoolType getValue(){
		return getLeftOperand().getValue().and(getRightOperand().getValue());
	}
}
