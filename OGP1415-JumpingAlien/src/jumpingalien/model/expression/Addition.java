package jumpingalien.model.expression;

import jumpingalien.model.type.*;

public class Addition extends BinaryArithmeticExpression{
	
	// CONSTRUCTOR
	public Addition(Expression<DoubleType> left, Expression<DoubleType> right) throws IllegalArgumentException{
		super(left,right,DoubleType.class);
	}
	

	
	// VALUE
	@Override
	public DoubleType getValue(){
		return getLeftOperand().getValue().plus(getRightOperand().getValue());
	}
}
