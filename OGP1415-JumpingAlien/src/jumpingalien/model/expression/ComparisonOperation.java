package jumpingalien.model.expression;

import jumpingalien.model.type.*;

public enum ComparisonOperation {
	
	LESS_THAN{
		
		@Override
		public BoolType getComparisonResult(Expression<DoubleType> left,
				Expression<DoubleType> right) {
			return left.getValue().lessThan(right.getValue());
		}
	},
	
	GREATER_THAN{
		
		@Override
		public BoolType getComparisonResult(Expression<DoubleType> left,
				Expression<DoubleType> right) {
			return left.getValue().greaterThan(right.getValue());
		}
		
	},
	
	LESS_THAN_OR_EQUAL{
		
		@Override
		public BoolType getComparisonResult(Expression<DoubleType> left,
				Expression<DoubleType> right) {
			return left.getValue().lessThanOrEqual(right.getValue());
		}
		
	}, 
	
	GREATER_THAN_OR_EQUAL{
		
		@Override
		public BoolType getComparisonResult(Expression<DoubleType> left,
				Expression<DoubleType> right) {
			return left.getValue().greaterThanOrEqual(right.getValue());
		}
		
	};

	public abstract BoolType getComparisonResult(Expression<DoubleType> left, Expression<DoubleType> right);
	
}
