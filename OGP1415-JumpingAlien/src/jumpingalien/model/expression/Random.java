package jumpingalien.model.expression;

import jumpingalien.model.type.DoubleType;
import be.kuleuven.cs.som.annotate.*;

public class Random extends UnaryExpression<DoubleType, DoubleType> {
	
	public Random(Expression<DoubleType> operand) {
		super(operand, DoubleType.class);
	}
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && DoubleType.class.isAssignableFrom(getOperand().getType());
	}

	
	@Basic @Override
	public DoubleType getValue() {
		return getOperand().getValue().random();
	}

}