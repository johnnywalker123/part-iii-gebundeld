package jumpingalien.model.expression;

import jumpingalien.model.type.*;

public class GetWidth extends UnaryExpression<DoubleType, ObjectType> {

	// CONSTRUCTOR
	
	public GetWidth(Expression<ObjectType> operand) {
		super(operand, DoubleType.class);
	}
	
	// IS WELL FORMED
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && ObjectType.class.isAssignableFrom(getOperand().getType());
	}
	
	// VALUE
	
	@Override
	public DoubleType getValue() {
		return new DoubleType(getOperand().getValue().getWidth()); // doet conversion naar double vanzelf
	}
	
}
