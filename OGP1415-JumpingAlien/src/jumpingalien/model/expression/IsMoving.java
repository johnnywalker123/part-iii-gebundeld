package jumpingalien.model.expression;

import jumpingalien.model.*;
import jumpingalien.model.type.*;

public class IsMoving extends BinaryExpression<BoolType, ObjectType, DirectionType> {
	
	public IsMoving(Expression<ObjectType> operand, Expression<DirectionType> direction) {
		super(operand, direction, BoolType.class);
	}
	
	// IS WELL FORMED
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints() && ObjectType.class.isAssignableFrom(getLeftOperand().getType()) &&
				DirectionType.class.isAssignableFrom(getRightOperand().getType());
	}
	
	@Override
	public BoolType getValue() {
		ObjectType object = getLeftOperand().getValue();
		if (object instanceof GameObject){
			GameObject gameObject = (GameObject) object;
			return new BoolType(gameObject.isRunning() && 
							gameObject.getHorizontalDirection() == getRightOperand().getValue());
		} else return new BoolType(false);
	}

}
