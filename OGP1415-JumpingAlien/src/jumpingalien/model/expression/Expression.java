package jumpingalien.model.expression;

import jumpingalien.model.program.Program;
import jumpingalien.model.type.*;

public abstract class Expression<T extends Type>{
	
	public Expression(Class<T> type) {
		assert (canHaveAsType(type));
		this.type = type;
	}
	
	// VALUE
	public abstract T getValue();
	
	public Class<T> getType(){
		return this.type;
	}
	
	
	// ASSOCIATED PROGRAM
	
	Program getProgram(){
		return this.program;
	}
	
	public void setProgram(Program program){
		if (shouldSetProgram()){
			this.program = program;
			cascadeProgramToChildren();
		}
	}
	
	public abstract void cascadeProgramToChildren();
	
	public boolean shouldSetProgram(){
		return getProgram() == null;
	}
	
	private Program program;
	
	
	// SATISFIES CONSTRAINTS
	
	public abstract boolean satisfiesConstraints();
	
	
	// TYPE
	public boolean canHaveAsType(Class<T> type){
		return type != null;
	}
	
	private final Class<T> type;
	
	// SUBEXPRESSION
	public abstract boolean hasAsSubExpression(Expression<? extends Type> expression);
		
	// TO STRING
	@Override
	public String toString(){
		return getValue().toString();
	}
	
}

