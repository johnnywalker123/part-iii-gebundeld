package jumpingalien.model.program;

import java.util.HashMap;

import static jumpingalien.util.Util.*;

import java.util.Iterator;

import static jumpingalien.model.program.TimePerStatement.*;

import java.util.Map;

import be.kuleuven.cs.som.annotate.*;
import jumpingalien.model.*;
import jumpingalien.model.expression.*;
import jumpingalien.model.statement.*;
import jumpingalien.model.type.*;

public class Program{
	
	// CONSTRUCTOR
	public Program(Statement mainStatement, Map<String, Class<? extends Type>> globalVariables){
		// GLOBAL VARIABLES
		for (String name : globalVariables.keySet()){
			if (DoubleType.class == globalVariables.get(name))
				currentVariables.put(name,new Variable<DoubleType>(name, DoubleType.class));
			else if (BoolType.class == globalVariables.get(name))
				currentVariables.put(name,new Variable<BoolType>(name, BoolType.class));
			else if (DirectionType.class == globalVariables.get(name))
				currentVariables.put(name,new Variable<DirectionType>(name, DirectionType.class));
			else if (ObjectType.class == globalVariables.get(name))
				currentVariables.put(name,new Variable<ObjectType>(name, ObjectType.class));
			else throw new IllegalStateException("Exception when initiating variables");
		}
		// MAIN STATEMENT
		this.mainStatement = mainStatement;
		mainStatement.setProgram(this);
		setWellFormed();
	} 

	
	// EXECUTING
	public void execute(double dt){
		try{
			if (!hasError() && isWellFormed()){
				while(!fuzzyLessThanOrEqualTo(dt,0) && getExecutingNpc().isAlive()){ // dt > 0
					executeNext();
					dt -= TIME_PER_STATEMENT;
				}
			}
		}
		catch(Throwable e){
			setTrueHasError();
		}
	}
	
	public void executeNext(){
		if (isFirstTime() || !getCurrentIterator().hasNext()){
			reInitializeVariables();
			setNewCurrentIterator();
		}
		getCurrentIterator().next().execute();
	}
	
	// IS WELL FORMED
	
	public boolean isWellFormed(){
		return this.getMainStatement().satisfiesConstraints();
	}
	
	private void setWellFormed(){
		this.isWellFormed = getMainStatement().satisfiesConstraints();
	}
	
	private boolean isWellFormed;
	
	// HAS ERROR
	
	public boolean hasError(){
		//return this.hasError;
		return false;
	}
	
	private void setTrueHasError(){
		this.hasError = true;
	}
	
	private boolean hasError;
	
	// DECLARE VARIABLES
	private void reInitializeVariables() throws IllegalStateException{
		for (String name : currentVariables.keySet()){
			Variable<? extends Type> variable = currentVariables.get(name);
			Class<? extends Type> type = variable.getType();
			if (type == DoubleType.class)
				((Variable<DoubleType>)variable).setValue(new DoubleType());
			else if (type == BoolType.class)
				((Variable<BoolType>)variable).setValue(new BoolType());
			else if (type == DirectionType.class)
				((Variable<DirectionType>)variable).setValue(null);
			else if (ObjectType.class.isAssignableFrom(type))
				((Variable<ObjectType>)variable).setValue(null);
			else throw new IllegalStateException("Exception when declaring variables");
		}
	}
	
		// CURRENT ITERATOR
	public Iterator<Statement> getCurrentIterator(){
		return this.currentIterator;
	}
	
	public boolean isFirstTime(){
		return (this.currentIterator == null);
	}
	
	private void setNewCurrentIterator(){
		this.currentIterator = getMainStatement().iterator();
	}
	
	private Iterator<Statement> currentIterator = null;
	
	
	
	// ASSOCIATIONS
	
		// MAIN STATEMENT
	@Basic
	public Statement getMainStatement(){
		return this.mainStatement;
	}
	
	private final Statement mainStatement;
	
		// GLOBAL VARIABLES
	@Basic
	public Variable<? extends Type> getGlobalVariableAt(String key){
		return currentVariables.get(key);
	}
	
	@Basic
	public boolean hasGlobalVariableAt(String key){
		return currentVariables.containsKey(key);
	}
	
	private Map<String,Variable<? extends Type>> currentVariables = new HashMap<>();
	
		// GAME OBJECT
	
	public NPCObject getExecutingNpc(){
		return this.executingNpc;
	}
	
	public boolean canHaveAsExecutingNpc(NPCObject npcObject){
		return (npcObject == null) || npcObject.canHaveAsProgram(this);
	}
	
	public boolean hasProperExecutingNpc(){
		return canHaveAsExecutingNpc(getExecutingNpc()) &&
				(getExecutingNpc() == null || getExecutingNpc().hasAsProgram(this));
	}
	
	public void setExecutingNpc(NPCObject npcObject){
		assert (	(npcObject == null) && getExecutingNpc().isTerminated()	)
					|| (	(npcObject != null) && npcObject.hasAsProgram(this)	); // applicability
		this.executingNpc = npcObject;
	}
	
	private NPCObject executingNpc;
	
	
	

}
