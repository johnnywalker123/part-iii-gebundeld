package jumpingalien.model.program;

// This symbolic constant is needed in Program and Wait. 
// To guarentee consistency, we define this symbolic constant here.

public class TimePerStatement {
	public final static double TIME_PER_STATEMENT = 0.001;
}
