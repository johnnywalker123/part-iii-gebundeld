package jumpingalien.model;




import java.util.HashMap;
import java.util.Map;

import jumpingalien.model.program.Program;
import jumpingalien.util.Sprite;
import be.kuleuven.cs.som.annotate.*;

/**
 * A class of plants.
 * 
 * @version 3.0
 * @author  Antoon Purnal and Tuur Van Daele
 *
 */
public class Plant extends NPCObject {
	
	/**
	 * Initialize a new plant.
	 * 
	 * @param xPixel
	 * @param yPixel
	 * @param images
	 * 
	 * @effect | super(xPixel, yPixel, getInitialHitPoints(), images)
	 */
	public Plant(int xPixel, int yPixel, Sprite[] images, Program program) {
		super(xPixel, yPixel, getInitialHitPoints(), images, program);
	}
	
	// MOVEMENT LIMITATIONS
	
	/**
	 * Check whether this Plant can move horizontally (always true).
	 */
	@Basic @Override @Immutable
	public boolean canMoveHorizontally(){
		return true;
	}
	
	/**
	 * Check whether this Plant can move vertically (always false).
	 */
	@Basic @Override @Immutable
	public boolean canMoveVertically(){
		return false;
	}
	
	// SIZE
	// POSITIONS AND PIXELS
		//X

		//Y

	// HORIZONTAL MOVEMENT
		// GENERAL
			// START AND END
	
	/**
	 * Return the minimal movement duration for this Plant (always 0.5).
	 */
	@Basic @Override @Immutable
	public double getMinMovementDuration() {
		return RUNNING_ALTERNATION;
	}

	/**
	 * Return the maximal movement duration for this Plant (always 0.5)
	 */
	@Basic @Override @Immutable
	public double getMaxMovementDuration() {
		return RUNNING_ALTERNATION;
	}

	

			// ORIENTATION
	
	/**
	 * Get the correct horizontal direction for this Plant
	 * @return | result ==  ((int) (getTime()/RUNNING_ALTERNATION) % 2 == 0 ) ? HorizontalDirection.LEFT : HorizontalDirection.RIGHT
	 * 		
	 */
	@Override
	public HorizontalDirection getAppropriateDirection(){
		if ((int) (getTime()/RUNNING_ALTERNATION) % 2 == 0 )
			return HorizontalDirection.LEFT;
		return HorizontalDirection.RIGHT;
	}
	
	/**
	 * Symbolic constant representing the amount of time between running alterations for this plant.
	 */
	public static final double RUNNING_ALTERNATION = 0.5;
		
		// VELOCITY
	
	/**
	 * Return the initial horizontal velocity (in m/s) for this plant (always 0.5).
	 */
	// niet basic methode als basic labelen in subklasse mag van LSP
	@Basic @Override @Immutable
	public double getInitialXVelocity(){
		return 0.5;
	}

	/**
	 * Return the maximal horizontal velocity (in m/s) for this Plant (always 0.5).
	 */
	@Basic @Override @Immutable
	public double getMaxXVelocity(){
		return 0.5;
	}

	
	/**
	 * Return the initial vertical velocity for this Plant (always 0).
	 */
	@Basic @Override @Immutable
	public double getInitialYVelocity() {
		return 0;
	}

		// ACCELERATION
	
	
	/**
	 * Return the horizontal acceleration for this Plant (always 0).
	 */
	@Basic @Override @Immutable
	public double getXAcceleration(){
		return 0;
	}


	// HITPOINTS
	
	/**
	 * Return this Plant's initial hitpoints (always 1).
	 */
	@Basic @Immutable
	public static int getInitialHitPoints(){
		return 1;
	}
	
	/**
	 * Update the hitpoints for this Plant, and the other game object it collides with.
	 * 
	 * @param	other
	 * 
	 * @effect	| if (shouldUpdateAnyHitPointsUponCollisionWith(other) && other instanceof Mazub &&
	 * 			| 	((Mazub)other).isHungry() && this.isAlive())
	 * 			|  then this.updateHitPoints(-1) &&	other.updateHitPoints(50)
	 * @effect	| if (shouldUpdateAnyHitPointsUponCollisionWith(other) && (other !instanceof Mazub)
	 * 			| then other.updateHitPointsUponCollisionWith(this)
	 * 				
	 */
	@Override
	public void updateHitPointsUponCollisionWith(GameObject other){
		if (shouldUpdateAnyHitPointsUponCollisionWith(other)){
			if(other instanceof Mazub){
				if (((Mazub)other).isHungry() && this.isAlive()){
					this.updateHitPoints(-1);
					other.updateHitPoints(50);
				}
		}
			else
				other.updateHitPointsUponCollisionWith(this);
	}
	}
	
	/**
	 * Check whether any hitpoints should be updated for the collision between this Plant and the other game object.
	 * @param other
	 * @return 	| result == (!(other == null || !hasAsCollidingObject(other) || this.isTerminated() || other.isTerminated()) ||
	 * 			! (other instanceof Plant || other instanceof Slime || other instanceof Shark))
	 */
	public boolean shouldUpdateAnyHitPointsUponCollisionWith(GameObject other){
		if (other == null || !hasAsCollidingObject(other) || this.isTerminated() || other.isTerminated())
			return false;
		else if (other instanceof Plant || other instanceof Slime || other instanceof Shark)
			return false;
		return true;
	}
	
	
	// IMMUNITY
	
	/**
	 * Check whether this Plant is immune for game object interactions (always false).
	 */
	@Basic @Override @Immutable
	public boolean isImmune(){
		return false;
	}
	

	
	// WEAKNESS
	
	/**
	 * Get this Plant's geological weaknesses and correspondig hitpoint losses (always an empty map).
	 */
	@Basic @Override @Immutable
	public Map<GeologicalFeature,Integer> getWeaknessesAndHitpointLosses() {
		return new HashMap<GeologicalFeature, Integer>();
	};
	
	

	// ASSOCIATION WITH WORLD
	
	/**
	 * Add this game object to the world.
	 * 
	 * @param	world
	 * 
	 * @effect 	| world.addAsPlant(this)
	 */
	@Raw @Override
	void addAsGameObject(World world){
		world.addAsPlant(this);
	}
	
	/**
	 * Remove this game object from the world.
	 * 
	 * @param	world
	 * 
	 * @effect	| world.removeAsPlant(this)
	 */
	@Override
	void removeAsGameObject(@Raw World world){
		world.removeAsPlant(this);
	}
	
	// SPRITE
	
	/**
	 * Get the new appropriate sprite index for this plant.
	 * 
	 * @return	| result == ((getOrientation() == HorizontalDirection.RIGHT) ? 1 : 0);
	 */
	@Override
	public int getNewSpriteIndex(){	
		return super.getNewSpriteIndex();
	}

	// COLLISION
	
	/**
	 * Return whether the supplied game object interferes movement with this plant (always false)
	 */
	@Basic @Override @Immutable
	public boolean interferesMovementWith(GameObject other) {
		return false;
	}

	// TIME
	
	/**
	 * Advance the time for this Plant with a certain amount.
	 * 
	 * @note	No documentation is required for this method.
	 */
	@Override
	public void advanceTime(double dt)  throws IllegalArgumentException, IllegalStateException {
		super.advanceTime(dt);
	}


	

	
}
