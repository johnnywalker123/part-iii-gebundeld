package jumpingalien.model;

public enum GameObjectStatus {
	ALIVE, TERMINATION_PROCESS, TERMINATED;
	
	public boolean isTerminated(){
		return (this == TERMINATED);
	}
	
	public boolean isInTerminationProcess(){
		return (this == TERMINATION_PROCESS);
	}
	
	public boolean isAlive(){
		return (this == ALIVE);
	}
}
