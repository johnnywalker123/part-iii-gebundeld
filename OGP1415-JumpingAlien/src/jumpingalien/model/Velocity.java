package jumpingalien.model;

import be.kuleuven.cs.som.annotate.*;

@Value
public class Velocity {
	
	public Velocity(double xVelocity, double yVelocity){
		assert GameObject.isValidXVelocity(xVelocity);
		this.xVelocity = xVelocity;
		this.yVelocity = yVelocity;
	}
	
	public Velocity() {
		this(0,0);
	}
	
		// X
	@Basic @Immutable
	public double getX(){
		return this.xVelocity;
	}
	
	
	private final double xVelocity;
	
	
		// Y
	
	@Basic @Immutable
	public double getY(){
		return this.yVelocity;
	}
	
	private final double yVelocity;
	
	// Override equals and hashcode
	

}
