package jumpingalien.model;

import java.util.Optional;
import java.util.stream.Stream;

import jumpingalien.model.type.DirectionType;
import jumpingalien.model.type.ObjectType;

public enum VerticalDirection implements DirectionType{
	
	UP{
		public InfoGameObject perimeterOf(GameObject gameObject){
			assert (gameObject != null);
			int verticalPerimeter;
			verticalPerimeter = gameObject.getTopPixel();
			return new InfoGameObject(gameObject.getXPixel()+1, verticalPerimeter, gameObject.getRightPixel()-1, verticalPerimeter);			
		}
		
		@Override
		public boolean isTryingToMoveInInterferingDirection(InfoGameObject perimeter, GameObject collidingObject){
			return perimeter.getYPixel() <= collidingObject.getYPixel();
		}
		
		@Override
		public Optional<ObjectType> searchNearestGameObjectInDirectionForObject(
				Stream<ObjectType> stream, GameObject gameObject) {
			return stream.filter(
					x->(x.getYPixel()>gameObject.getYPixel() && x.getRightPixel()>gameObject.getXPixel() && x.getXPixel() < gameObject.getRightPixel())).
					reduce((x,y)-> (x.getYPixel()<y.getYPixel() ? x: y));
		}
		
	},
	DOWN{
		public InfoGameObject perimeterOf(GameObject gameObject){
			assert (gameObject != null);
			int verticalPerimeter;
			verticalPerimeter = gameObject.getYPixel();
			return new InfoGameObject(gameObject.getXPixel()+1, verticalPerimeter, gameObject.getRightPixel()-1, verticalPerimeter);			
		}
		
		@Override
		public boolean isTryingToMoveInInterferingDirection(InfoGameObject perimeter, GameObject collidingObject){
			return collidingObject.getYPixel() < perimeter.getYPixel();
		}
		
		@Override
		public Optional<ObjectType> searchNearestGameObjectInDirectionForObject(
				Stream<ObjectType> stream, GameObject gameObject) {
			return stream.filter(
					x->(x.getYPixel()<gameObject.getYPixel() && x.getRightPixel()>gameObject.getXPixel() && x.getXPixel() < gameObject.getRightPixel())).
					reduce((x,y)-> (x.getYPixel()>y.getYPixel() ? x: y));
		}
	};
	
}
