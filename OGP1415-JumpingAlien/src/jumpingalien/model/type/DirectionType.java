package jumpingalien.model.type;

import java.util.Optional;
import java.util.stream.Stream;

import jumpingalien.model.GameObject;
import jumpingalien.model.InfoGameObject;

public interface DirectionType extends Type{
	
	InfoGameObject perimeterOf(GameObject gameObject);
	
	boolean isTryingToMoveInInterferingDirection(InfoGameObject perimeter, GameObject collidingObject);
	
	Optional<ObjectType> searchNearestGameObjectInDirectionForObject(Stream<ObjectType> stream, GameObject gameObject);
	
	// TO STRING
	// No need to override
}
