package jumpingalien.model.type;

import be.kuleuven.cs.som.annotate.Value;


@Value
public class BoolType implements Type{
	
	// CONSTRUCTOR
	public BoolType(boolean flag){
		this.value = flag;
	}
	
	public BoolType(){
		this(false);
	}
	
	// VALUE
	public boolean getValue(){
		return this.value;
	}
	
	private final boolean value;
	
	
	// LOGIC
	
	public BoolType negation(){
		return new BoolType(!getValue());
	}
	
	public BoolType or(BoolType other){
		return new BoolType(getValue() || other.getValue());
	}
	
	public BoolType and(BoolType other){
		return new BoolType(getValue() && other.getValue());
	}
	
	// CHECK WHETHER TRUE OR FALSE
	
	public boolean isTrue(){
		return getValue() == true;
	}
	
	public boolean isFalse(){
		return getValue() == false;
	}
	
	
	// EQUALS
	@Override
	public boolean equals(Object other){
		return (other instanceof BoolType) && 
				( this.getValue() == ((BoolType)other).getValue());
	}
	
	@Override
	public int hashCode(){
		return Boolean.hashCode(getValue());
	}
	

	
	// TO STRING
	@Override
	public String toString(){
		return Boolean.toString(getValue());
	}
}

