package jumpingalien.model.type;

import static jumpingalien.util.Util.*;
import be.kuleuven.cs.som.annotate.Value;
import static jumpingalien.model.extension.MathExtension.*;
import static jumpingalien.model.program.TimePerStatement.*;

@Value
public class DoubleType implements Type{
	
	// CONSTRUCTOR
	public DoubleType(double number){
		this.value = number;
	}
	
	public DoubleType(){
		this(0);
	}
	
	//VALUE
	public double getValue(){
		return this.value;
	}
	
	private final double value;
	
	// METHODS FOR DOUBLETYPE OPERATIONS
	
			// ARITHMETIC
	
	public DoubleType plus(DoubleType other){
		return new DoubleType(getValue()+other.getValue());
	}
	
	public DoubleType minus(DoubleType other){
		return new DoubleType(getValue()-other.getValue());
	}
	
	public DoubleType multiply(DoubleType other){
		return new DoubleType(getValue()*other.getValue());
	}
	
	public DoubleType divide(DoubleType other){
		return new DoubleType(getValue()/other.getValue());
	}
	
			// COMPARISON
	
	public BoolType lessThanOrEqual(DoubleType other){
		return new BoolType(fuzzyLessThanOrEqualTo(getValue(),other.getValue()));
	}
	
	public BoolType greaterThanOrEqual(DoubleType other){
		return new BoolType(fuzzyGreaterThanOrEqualTo(getValue(),other.getValue()));
	}
	
	public BoolType lessThan(DoubleType other){
		return greaterThanOrEqual(other).negation();
	}
	
	public BoolType greaterThan(DoubleType other){
		return lessThanOrEqual(other).negation();
	}
	
	public BoolType equals(DoubleType other){
		return new BoolType(fuzzyEquals(getValue(), other.getValue()));
	}
	
	public BoolType notEquals(DoubleType other){
		return equals(other).negation();
	}
	
	// RANDOM
	
	public DoubleType random(){
		return new DoubleType(getValue()*Math.random());
	}
	
	// TO INTEGER
	public int toInteger(){
		if (value >= Integer.MAX_VALUE)
			return Integer.MAX_VALUE;
		if (value <= Integer.MIN_VALUE)
			return Integer.MIN_VALUE;
		return (int) roundTowardZero(getValue());
	}

	// EQUALS
	@Override
	public boolean equals(Object other){
		return (other instanceof DoubleType) && 
				( this.getValue() == ((DoubleType)other).getValue());
	}
	
	@Override
	public int hashCode(){
		return Double.hashCode(getValue());
	}
	
	// TIME PER STATEMENT
	public final static DoubleType TIME_PER_STATEMENT_DOUBLETYPE = new DoubleType(TIME_PER_STATEMENT);
	
	// TO STRING
	@Override
	public String toString(){
		return Double.toString(getValue());
	}
}
