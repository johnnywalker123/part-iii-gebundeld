package jumpingalien.model.type;

import jumpingalien.model.GameObject;

public interface ObjectType extends Type{
	
	int getXPixel();
	int getYPixel();
	int getWidth();
	int getHeight();
	int getRightPixel();
	int getTopPixel();
	
	public abstract boolean isPassableFor(GameObject gameObject);

	@Override
	public abstract String toString();
	
	
}
