package jumpingalien.model;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import be.kuleuven.cs.som.annotate.*;
import jumpingalien.model.type.*;
import jumpingalien.util.*;
import static jumpingalien.model.Conversion.*;
import static jumpingalien.model.extension.MathExtension.*;
import static jumpingalien.util.Util.*;
import static jumpingalien.model.Conversion.PIXEL_LENGTH;


/**
 * A class of game objects.
 * 
 * 
 * @invar	| canHaveAsSprites(getAllImages())
 * @invar	| hasProperWorld()
 * @invar	| hasProperCollidingObjects()
 * @invar	| canHaveAsEndOfTerminationProcess(getEndTerminationProcess())
 * @invar	| canHaveAsCurrentIndex(getCurrentIndex())
 * @invar	| canHaveAsHitPoints(getHitPoints())
 * @invar	| isValidTime(getTime())
 * @invar	| isValidGameObjectStatus(getGameObjectStatus())
 * @invar	| isValidTime(getEndTerminationProcess()) || Double.isNaN(getEndTerminationProcess())
 * @invar	| canHaveAsXVelocity(getXVelocity())
 * @invar	| canHaveAsXVelocity(getInitialXVelocity())
 * @invar	| isValidHorizontalDirection(getHorizontalDirection())
 * @invar	| isValidTime(getImmunityTimer())
 * 
 * @version 3.0
 * @author  Antoon Purnal and Tuur Van Daele
 *
 */
public abstract class GameObject extends CollisionObject implements ObjectType{
	
	
	/**
	 * Initialize a new game object.
	 * 
	 * @param xPixel
	 * @param yPixel
	 * @param hitPoints
	 * @param images
	 * 
	 * @pre	 | canHaveAsSprites(images)
	 * 
	 * @post | new.getXPosition() == convertPixelToPosition(xPixel)+PIXEL_LENGTH/2
	 * @post | new.getYPosition() == convertPixelToPosition(yPixel)+PIXEL_LENGTH/2
	 * @post | new.getNbOngoingMovements() == 0
	 * @post | new.getHorizontalDirection() == HorizontalDirection.LEFT
	 * @post | new.getXVelocity() == 0
	 * @post | new.getYVelocity() == 0
	 * @post | new.getHitPoints() == hitpoints
	 * @post | Arrays.equals(new.getAllImages(), images)
	 * @post | new.getCurrentIndex() == 0
	 * @post | new.getImmunityTimer() == 0
	 * @post | new.getGameObjectStatus() == GameObjectStatus.IS_ALIVE
	 * @post | new.getWorld() == null
	 * @post | Double.isNaN(new.getEndTerminationProcess())
	 */
	protected GameObject(int xPixel, int yPixel,int hitPoints, Sprite[] images){
		assert canHaveAsSprites(images);
		this.position = new Position(convertPixelToPosition(xPixel)+PIXEL_LENGTH/2, convertPixelToPosition(yPixel)+PIXEL_LENGTH/2);
		setHitPoints(hitPoints);
		this.images = images.clone();
	}
	
	// SEARCH OBJECT
	
	/**
	 * Return the nearest game object in the given direction
	 * 
	 * @param 	direction
	 * @return	| result == 
	 * 			|		direction.searchNearestGameObjectInDirectionForObject(getWorld().stream(), this)
	 */
	public Optional<ObjectType> searchNearestGameObjectInDirection(DirectionType direction){
		Stream<ObjectType> objectStream = getWorld().stream();
		return direction.searchNearestGameObjectInDirectionForObject(objectStream, this);
	}
	
	// MOVEMENT LIMITATIONS
	
	
	/**
	 * Check whether this Game object can move horizontally.
	 */
	@Basic
	public abstract boolean canMoveHorizontally();
	
	/**
	 * Check whether this Game object can move vertically.
	 */
	@Basic
	public abstract boolean canMoveVertically();
	
	
	// SIZE
	
	/**
	 * Return the width of this game object.
	 * 
	 * @return | result == (getCurrentSprite().getWidth())
	 */
	@Override
	public int getWidth(){
		return getCurrentSprite().getWidth();
	}
	
	/**
	 * Return the height of this game object.
	 * 
	 * @return | result == (getCurrentSprite().getWidth())
	 */
	@Override
	public int getHeight(){
		return getCurrentSprite().getHeight();
	}
	
		
	// POSITIONS AND PIXELS
	
		//GENERAL
	
	/**
	 * Variable containing this game object's current position.
	 */
	private Position position;

		// X
	
	
	
	/**
	 * Return this game object's current horizontal position.
	 */
	@Basic
	public double getXPosition(){
		return this.position.getX();
	}
	
	/**
	 * Return this game object's horizontal pixel position.
	 * 
	 * @return	| result == convertPositionToPixel(getXPosition())
	 */
	@Override
	public int getXPixel(){
		return convertPositionToPixel(getXPosition());
	}
	
	
	/**
	 * Return this game object's outer right pixel position.
	 * 
	 * @return	| result == getXPixel() + getWidth()-1
	 */
	public int getRightPixel(){
		return getXPixel() + getWidth()-1;
	}
	
	
	/**
	 * Set this game object's horizontal position to the given position.
	 * 
	 * @param position
	 * @throws IllegalStateException
	 * 			| !isAlive()
	 * @post	| new.getXPosition() == position
	 * @effect	| if (!getWorld().isInXRange(position))
	 * 			|  then terminate()
	 */
	@Model
	private void setXPosition(double position) throws IllegalStateException{
		if (!isAlive())
			throw new IllegalStateException("Cannot change the position of an object that is not alive.");
		this.position = new Position(position, getYPosition());
		if (!getWorld().isInXRange(position))
				terminate();
	}
	
	/**
	 * Compute the correction for the horizontal position of this Game Object.
	 * 
	 * @param dt
	 * @return  | result <= Math.abs(getOrientation().getUnitDirection()*(getXVelocity()*dt + 0.5*getXAcceleration()*Math.pow(dt,2)))
	 */
	@Model
	private double computeXCorrection(double dt){
		return getHorizontalDirection().getUnitDirection()*(getXVelocity()*dt + 0.5*getXAcceleration()*Math.pow(dt,2));
	}
	
	
	/**
	 * Check whether this game object's horizontal position should be updated.
	 *  
	 * @return  result ==  (getOrientation() == HorizontalDirection.LEFT && !impassableEntityInDirection(HorizontalDirection.LEFT)) ||
	 * 						(getOrientation() == HorizontalDirection.RIGHT && !impassableEntityInDirection(HorizontalDirection.RIGHT))
	 */
	@Model
	private boolean xPositionShouldBeUpdated(){
		return (getHorizontalDirection() == HorizontalDirection.LEFT && !impassableEntityIn(HorizontalDirection.LEFT)) ||
				(getHorizontalDirection() == HorizontalDirection.RIGHT && !impassableEntityIn(HorizontalDirection.RIGHT));
	}
	
	/**
	 * Update this game object's horizontal position according to its current state.
	 * 
	 * @param dt
	 * @effect | if (isAlive() && xPositionShouldBeUpdated())
	 * 			|  setXPosition(getXPosition() + computeXCorrection(dt))
	 * @throws IllegalArgumentException
	 * 			| !isValidTimeInterval(dt)
	 * @throws IllegalStateException
	 * 			| !canMoveHorizontally()
	 * 
	 * 
	 */
	@Model
	private void updateXPosition(double dt) throws IllegalArgumentException, IllegalStateException{
		if (!isValidTimeInterval(dt))
			throw new IllegalArgumentException("Illegal time interval: " + dt);
		if (!canMoveHorizontally())
			throw new IllegalStateException("This game object cannot move horizontally");
		if (isAlive() && xPositionShouldBeUpdated()){
			setXPosition(getXPosition() + computeXCorrection(dt));
			}
		}

		// Y
	
	
	/**
	 * Return this game object's current vertical position.
	 */
	@Basic
	public double getYPosition(){
		return this.position.getY();
	}
	
	
	/**
	 * Return this game object's vertical pixel position.
	 * 
	 * @return	| result == convertPositionToPixel(getYPosition())
	 */
	@Override
	public int getYPixel(){
		return convertPositionToPixel(getYPosition());
	}
	
	
	/**
	 * Return this game object's outer top pixel position.
	 * 
	 * @return	| result == getYPixel() + getHeight()-1
	 */
	@Override
	public int getTopPixel(){
		return getYPixel() + getHeight()-1;
	}
	
	/**
	 * Set this game object's vertical position to the given position.
	 * 
	 * @param position
	 * 			| !isAlive()
	 * @post	| new.getYPosition() == position
	 * @effect	| if (!getWorld().isInYRange(position))
	 * 			|  then terminate()
	 * @throws IllegalStateException
	 */
	@Model
	private void setYPosition(double position) throws IllegalStateException{
		if (!isAlive())
			throw new IllegalStateException("Cannot change the position of an object that is not alive.");
		this.position = new Position(getXPosition(), position);
		if (!getWorld().isInYRange(position))
			terminate();
	}

	/**
	 * Compute the correction for the vertical position of this Game Object.
	 * @param dt
	 * 
	 * @return  | result <= Math.abs((getYVelocity()*dt + 0.5*getYAcceleration()*Math.pow(dt,2))
	 */
	@Model
	private double computeYCorrection(double dt){
		return (getYVelocity()*dt
				+ 0.5*getYAcceleration()*Math.pow(dt,2));
	}
	
	
	/**
	 * Check whether this game object's vertical position should be updated.
	 *  
	 * @return   | result == ((getYVelocity() > 0 && !impassableEntityIn(VerticalDirection.UP)) ||
				  |				(fuzzyLessThanOrEqualTo(getYVelocity(),0) && !impassableEntityIn(VerticalDirection.DOWN)))
	 */
	@Model
	private boolean yPositionShouldBeUpdated(){
		return ((getYVelocity() > 0 && !impassableEntityIn(VerticalDirection.UP)) ||
				(fuzzyLessThanOrEqualTo(getYVelocity(),0) && !impassableEntityIn(VerticalDirection.DOWN)));
	}
	
	/**
	 * Update this game object's horizontal position according to its current state.
	 * 
	 * @param dt
	 * @effect | if (isAlive() && yPositionShouldBeUpdated())
	 * 			|  setYPosition(getYPosition() + computeYCorrection(dt))
	 * @throws IllegalArgumentException
	 * 			| !isValidTimeInterval(dt)
	 * @throws IllegalStateException
	 * 			| !canMoveVertically()
	 * 
	 */
	@Model
	private void updateYPosition(double dt) throws IllegalArgumentException, IllegalStateException{
		if (!isValidTimeInterval(dt))
			throw new IllegalArgumentException("Illegal time interval: " + dt);
		if (!canMoveVertically())
			throw new IllegalStateException("This game object cannot move vertically");
		if (isAlive() && yPositionShouldBeUpdated() )
			setYPosition(getYPosition() + computeYCorrection(dt));
	}
	

	
	// HORIZONTAL MOVEMENT
	
	
	
		// GENERAL
	
	
	
			// START AND END
	
	
	/**
	 * Check whether this game object is running (i.e. moving horizontally).
	 * 
	 * @return	| getNbOngoingMovements() > 0
	 */
	public boolean isRunning(){
		return getNbOngoingMovements() > 0;
	}
	
	/**
	 * Return the current amount of ongoing movements for this game object. 
	 */
	@Basic
	public int getNbOngoingMovements(){
		return this.nbOngoingMovements;
	}
	
	/**
	 * This game object begins to move.
	 * 
	 * @param direction
	 * @pre 	| direction != null
	 * @pre		| canMoveHorizontally()
	 * @pre		| isAlive()
	 * @effect	| incrementMovements()
	 * @effect	| setHorizontalDirection(direction)
	 * @effect	| setXVelocity(getInitialXVelocity())
	 */
	@Model
	public void startMove(HorizontalDirection direction){
		assert (direction != null);
		assert (canMoveHorizontally());
		assert isAlive();
		incrementMovements();
		setHorizontalDirection(direction);
		setXVelocity(getInitialXVelocity());
	}
	

	/**
	 * This game object ends the movement.
	 * 
	 * @param direction
	 * @pre	| canMoveHorizontally()
	 * @pre	| direction != null
	 * @pre	| isAlive()
	 * @effect	| decrementMovements()
	 * @effect	| if (!isRunning())
	 * 			|	then setXVelocity(0)
	 * @effect	| if (isRunning() && getHorizontalDirection() == direction)
	 * 			|	then switchOrientation(direction) && setXVelocity(getInitialXVelocity())
	 * 
	 */
	@Model
	public void endMove(HorizontalDirection direction){
		assert (canMoveHorizontally());
		assert (direction != null);
		assert (isAlive());
		decrementMovements();
		if (!isRunning())
			setXVelocity(0);
		if (isRunning() && getHorizontalDirection() == direction){;
				switchOrientation(direction);
				setXVelocity(getInitialXVelocity());
			}
	}
	
	/**
	 * Increment the number of ongoing movements for this game object.
	 * 
	 * @post new.getNbOngoingMovements() == Math.min(2, getNbOngoingMovements()+1)
	 */
	@Model
	private void incrementMovements(){
		this.nbOngoingMovements = Math.min(2, getNbOngoingMovements()+1);
	}
	
	/**
	 * Decrement the number of ongoing movements for this game object.
	 * 
	 * @post new.getNbOngoingMovements() == Math.max(0, getNbOngoingMovements()-1)
	 */
	@Model
	private void decrementMovements(){
		this.nbOngoingMovements = Math.max(0, getNbOngoingMovements()-1);
	}
	
	/**
	 * Variable containing the current number of ongoing movements for this game object.
	 */
	private int nbOngoingMovements = 0;
			
			// ORIENTATION
	
	/**
	 * Return the horizontal direction of this game object.
	 */
	@Basic
	public HorizontalDirection getHorizontalDirection(){
		return this.horizontalDirection;
	}
	
	/**
	 * Check whether the given direction is a valid horizontal direction for all game objects.
	 * 
	 * @param direction
	 * @return | result == (direction != null)
	 */
	public static boolean isValidHorizontalDirection(HorizontalDirection direction){
		return direction != null;
	}
	
	/**
	 * Set the horizontal direction of this game object to the given horizontal direction.
	 * 
	 * @param direction
	 * @pre	| direction != null
	 * @pre	| isAlive()
	 * @post | new.getHorizontalDirection == direction
	 */
	@Model
	void setHorizontalDirection(HorizontalDirection direction){
		assert direction != null;
		assert isAlive();
		this.horizontalDirection = direction;
	}
	
	/**
	 * Switch the horizontal direction of this game object.
	 * 
	 * @param direction
	 * @pre	direction != null
	 * @pre	isAlive()
	 * @effect | if (direction == HorizontalDirection.LEFT)
	 * 			|	then setHorizontalDirection(HorizontalDirection.RIGHT)
	 * @effect | if (direction == HorizontalDirection.LEFT)
	 * 			| then setHorizontalDirection(HorizontalDirection.LEFT)
	 */
	@Model
	void switchOrientation(HorizontalDirection direction){
		assert direction != null;
		assert isAlive();
		if (direction == HorizontalDirection.LEFT)
			setHorizontalDirection(HorizontalDirection.RIGHT);
		else
			setHorizontalDirection(HorizontalDirection.LEFT);
	}
	
	/**
	 * Variable referencing the horizontal direction of this game object.
	 */
	private HorizontalDirection horizontalDirection = HorizontalDirection.LEFT;
	
	
	
	
		// VELOCITY
	
			// GENERAL
	
	/**
	 * Return the magnitude of this Game object's velocity vector.
	 * 
	 * @return Math.sqrt(Math.pow(getXVelocity(),2) + Math.pow(getYVelocity(),2))
	 */
	public double computeMagnitudeOfVelocity(){
		return Math.sqrt(Math.pow(getXVelocity(),2) + Math.pow(getYVelocity(),2));
	}
	
	
	/**
	 * Variable containing the velocity for this game object.
	 */
	private Velocity velocity = new Velocity(0,0);
	
			// X
	
	/**
	 * Return the horizontal velocity of this game object.
	 */
	@Basic
	public double getXVelocity(){
		return this.velocity.getX();
	}
	
	/**
	 * Return the maximal horizontal velocity (in m/s) for this game object.
	 * 
	 * @return | result >= getInitialXVelocity()
	 */
	public abstract double getMaxXVelocity();
	
	/**
	 * Return the initial horizontal velocity (in m/s) for this game object.
	 * 
	 * @return | result >= 0
	 */
	public abstract double getInitialXVelocity();
	
	/**
	 * Check whether this game object can have the given velocity as its horizontal velocity.
	 * 
	 * @param velocity
	 * @return | result == (velocity >= 0) && (velocity <= getMaxXVelocity())
	 */
	public boolean canHaveAsXVelocity(double velocity){
		return isValidXVelocity(velocity) && (velocity <= getMaxXVelocity());
	}
	
	/**
	 * Check whether the given velocity is a valid velocity for all game objects.
	 * 
	 * @param velocity
	 * @return result == (velocity >= 0)
	 */
	public static boolean isValidXVelocity(double velocity){
		return velocity >= 0;
	}
	
	
	/**
	 * Check whether this game object  has a constant horizontal velocity.
	 * 
	 * @return	result == (!isRunning() || getXVelocity() == getMaxXVelocity())
	 */
	public boolean hasConstantXVelocity(){
		return (!isRunning() || getXVelocity() == getMaxXVelocity());
	}	
	
	/**
	 * Set the horizontal velocity to the given horizontal velocity.
	 * 
	 * @param velocity
	 * @post	| if (isAlive && fuzzyLessThanOrEqualTo(velocity,0))
	 * 			|	then new.getXVelocity() == velocity
	 * @post	| if (isAlive() && !fuzzyLessThanOrEqualTo(velocity,0) && !Double.isNaN(velocity))
	 * 			|	then new.getXVelocity == Math.min(velocity, getMaxXVelocity())
	 */
	@Model
	private void setXVelocity(double velocity){
		if (isAlive()){
			if (fuzzyLessThanOrEqualTo(velocity,0))
				this.velocity = new Velocity(0, getYVelocity());
			else if (!Double.isNaN(velocity))
				this.velocity = new Velocity(Math.min(velocity, getMaxXVelocity()), getYVelocity());
		}
	}
	
	/**
	 * Update the horizontal velocity of this mazub with the given time interval.
	 * 
	 * @param dt
	 * @post	| if (isValidTimeInterval(dt) && isAlive())
	 * 			|	then setXVelocity(getXVelocity() + getXAcceleration()*dt)
	 */
	@Model
	private void updateXVelocity(double dt){
		if (isValidTimeInterval(dt) && isAlive())
			setXVelocity(getXVelocity() + getXAcceleration()*dt);
		}
	

	
			// Y
	
	/**
	 * Return the vertical velocity of this game object.
	 */
	@Basic
	public double getYVelocity(){
		return this.velocity.getY();
	}
	

	/**
	 * Return the initial vertical velocity for this game object.
	 */
	@Basic
	public abstract double getInitialYVelocity();
	

	/**
	 * Update the vertical velocity for this game object.
	 * 
	 * @effect 	| if ((isValidTimeInterval(dt) && isAlive()) && (shouldSetYVelocityToZero())
	 * 			|  then setYVelocity(0)
	 * @effect	| if ((isValidTimeInterval(dt) && isAlive()) && (!shouldSetYVelocityToZero())
	 * 			| then setYVelocity(getYVelocity() + getYAcceleration()*dt)
	 */
	@Model
	void updateYVelocity(double dt){
		if (isValidTimeInterval(dt) && isAlive()){
			if (shouldSetYVelocityToZero()){
				setYVelocity(0);
			}
			else
				setYVelocity(getYVelocity() + getYAcceleration()*dt);
		}
	}	
	
	/**
	 * Check whether this game objects's vertical velocity should be set to zero.
	 * 
	 * @return 	| if (impassableEntityInDirection(VerticalDirection.UP))
	 * 			| then result == true
	 */
	@Model
	boolean shouldSetYVelocityToZero(){
		return (!shouldFall() || impassableEntityIn(VerticalDirection.UP));
	}
	
	/**
	 * Set the vertical velocity of this game object to the given vertical velocity.
	 * 
	 * @param velocity
	 * @post	| if (! Double.isNaN(velocity))
	 * 			|	new.getYVelocity() == velocity
	 */
	@Model
	void setYVelocity(double velocity){
		if (! Double.isNaN(velocity))
			this.velocity = new Velocity(getXVelocity(), velocity);
	}
	
	
	
	
		// ACCELERATION
	
			// GENERAL
	/**
	 * Compute the magnitude of the acceleration of this game object.
	 * 
	 * @return	|result == Math.sqrt(Math.pow(getXAcceleration(),2) + Math.pow(getYAcceleration(),2))
	 */
	
	public double computeMagnitudeOfAcceleration(){
		return Math.sqrt(Math.pow(getXAcceleration(),2) + Math.pow(getYAcceleration(),2));
	}
	
			// X
	
	/**
	 * Return the horizontal acceleration for this game object.
	 * 
	 * @return | if (hasConstantXVelocity)
	 * 			|  then result == 0
	 * 			| else result > 0
	 */
	public abstract double getXAcceleration();	
	
	
			// Y

	/**
	 * Get the current vertical acceleration for this game object.
	 * 
	 * @return	| if (isTerminated())
	 * 			|  then result == 0
	 */
	public double getYAcceleration(){
		if (isTerminated())
			return 0;
		if (!shouldFall()){
			return 0;
		}
		return GRAVITATIONAL_ACCELERATION; 
	}
	
	/**
	 * Symbolic constant representing the gravitational acceleration.
	 */
	private final static double GRAVITATIONAL_ACCELERATION = - 10; // assumption: all objects fall with the same vertical acceleration
	
	
	// FALLING
	
	/**
	 * Check whether this game object should fall.
	 * 
	 * @return 	| if (!(canMoveVertically() && !impassableEntityInDirection(VerticalDirection.DOWN))
	 * 			| 	then result == false
	 */
	public boolean shouldFall(){
		return (canMoveVertically() && !impassableEntityIn(VerticalDirection.DOWN));
	}

	
	// HITPOINTS
	
	/**
	 * Return the hitpoints of this game object.
	 */
	@Basic
	public int getHitPoints(){
		return this.hitPoints;
	}
	
	/**
	 * Return the max. number of hitpoints for this game object.
	 */
	@Basic
	public static int getMaxHitPoints(){
		return 500;
	}
	
	public boolean canHaveAsHitPoints(int hitPoints){
		return hitPoints >= 0 && hitPoints <= getMaxHitPoints();
	}
	
	/**
	 * Update the hitpoints for this game object with the supplied hitpoints difference.
	 * 
	 * @effect | if (isPositiveOverflow(getHitPoints(), hitPointsDifference)) 
	 * 				| ? setHitPoints(getMaxHitPoints()) 
	 * 				| : setHitPoints(getHitPoints() + hitPointsDifference);
	 */
	@Model
	void updateHitPoints(int hitPointsDifference){
		if (isPositiveOverflow(getHitPoints(), hitPointsDifference))
			setHitPoints(getMaxHitPoints());
		else
			setHitPoints(getHitPoints() + hitPointsDifference);
	} 

	
	/**
	 * Update the hitpoints for this game object, and the other game object it collides with.
	 */
	@Model
	abstract void updateHitPointsUponCollisionWith(GameObject other);
	
	/**
	 * Set the hitpoints of this game object to the given hitpoints, if is alive.
	 * 
	 * @param hitPoints
	 * @post	| if (isAlive() && (hitPoints <= 0))
	 * 			|	then new.getHitPoints() == 0
	 * @effect	| if (isAlive() && (hitPoints <= 0))
	 * 			|	then startTerminationProcess()
	 * @post	| if (isAlive() && hitPoints > 0)
	 * 			| 	then new.getHitPoints() == Math.min(hitPoints, getMaxHitPoints())
	 */
	@Model
	private void setHitPoints(int hitPoints){
		if (isAlive()){
			if (hitPoints <= 0){
				this.hitPoints = 0;
				startTerminationProcess();
			}
			else
				this.hitPoints = Math.min(hitPoints, getMaxHitPoints());
		}
	}
	
	/**
	 * Variable containing the current number of hitpoints for this game object.
	 */
	private int hitPoints;
		
	
	
	// IMMUNITY
	
	/**
	 * Check whether this game object is immune for game object interactions.
	 * 
	 * @return	result == (getImmunityTimer() > 0)
	 */
	public boolean isImmune(){
		return getImmunityTimer() > 0;
	}
	
	/**
	 * Return the value of the immunity timer of this game object.
	 */
	@Basic
	public double getImmunityTimer(){
		return this.immunityTimer;
	}
	
	/**
	 * Reset the immunity timer of this game object.
	 * 
	 * @post| new.getImmunityTimer() == 0.6
	 */
	@Model
	void resetImmunityTimer(){
		this.immunityTimer = 0.6;
	}
	
	/**
	 * Update the immunity timer of this game object.
	 * 
	 * @param dt
	 * @pre !isTerminated() && isValidTimeInterval(dt)
	 * @post | new.getImmunityTimer == Math.max(0, getImmunityTimer()-dt)
	 */
	@Model
	private void updateImmunityTimer(double dt){
		assert !isTerminated() && isValidTimeInterval(dt);
		this.immunityTimer = Math.max(0, getImmunityTimer()-dt);
	}
	
	/**
	 * Variable registering the value of the immunity timer of this game object.
	 */
	private double immunityTimer = 0;
	


		
	// WEAKNESS
	
	/**
	 * Return the hitpoint loss treshold of all game objects.
	 */
	@Basic @Immutable
	public static double getHitPointLossThreshold(){
		return 0.2;
	}
	
	/**
	 * Check whether this game object should update its hitpoints.
	 * 
	 * @param feature
	 * @param wasFirstContact
	 * @return	 |result == ( wasFirstContact && feature.hurtsImmediately() )|| 
	 * 				|(weaknessTimers.containsKey(feature) && fuzzyGreaterThanOrEqualTo(weaknessTimers.get(feature),getHitPointLossThreshold()))
	 */
	public boolean shouldUpdateHitPoints(GeologicalFeature feature, boolean wasFirstContact){
		return ( wasFirstContact && feature.hurtsImmediately() )|| 
				(	weaknessTimers.containsKey(feature) && fuzzyGreaterThanOrEqualTo(weaknessTimers.get(feature),getHitPointLossThreshold())	);
	}
	
	/**
	 * Check whether this game object was not in contact with the given feature before.
	 * 
	 * @param feature
	 * @return	| result == !weaknessTimers.containsKey(feature)
	 */
	public boolean isFirstContact(GeologicalFeature feature){
		return !weaknessTimers.containsKey(feature);
	}
	
	/**
	 * Get this game object's geological weaknesses and corresponding hitpoint losses.
	 */
	@Basic @Model @Immutable
	abstract Map<GeologicalFeature, Integer> getWeaknessesAndHitpointLosses();	
	
	/**
	 * Update the weaknesses of this game object.
	 * 
	 * @param dt
	 * @pre | isValidTimeInterval(dt)
	 * @effect	| if (isAlive())
	 * 				| for each weakness in getWeaknessesAndHitpointLosses().keySet()
	 * 					|if (surroundingFeatures.contains(weakness))
	 * 					|	then updateContainingWeakness(dt, weakness)
	 * 					| else then weaknessTimers.remove(weakness)
	 */
	@Model
	private void updateWeaknesses(double dt){
		assert isValidTimeInterval(dt);
		if (isAlive()){
			Set<GeologicalFeature> surroundingFeatures = getWorld().getAllOverlappingTileFeatures(this);
			for (GeologicalFeature weakness: getWeaknessesAndHitpointLosses().keySet()){
				if (surroundingFeatures.contains(weakness)){ // wel in contact -> doe eventueel nodige acties
					updateContainingWeakness(dt, weakness);
				} else { // Niet langer in contact met deze feature -> haal eruit
					weaknessTimers.remove(weakness);
				}
			}
		}
	}
	
	/**
	 * Update the containing weakness.
	 * 
	 * @param dt
	 * @param weakness
	 * @pre	| getWorld().getAllOverlappingTileFeatures(this).contains(weakness)
	 * @effect 	| if this.(isFirstContact(weakness))
	 * 			| 	then startWeaknessTimer(dt, weakness)
	 * 			| else then updateWeaknessTimer(dt, weakness)
	 * @effect	| if (shouldUpdateHitPoints(weakness, isFirstContact(weakness)))
	 * 			|	then updateHitPoints(getWeaknessesAndHitpointLosses().get(weakness))
	 * @effect	| if (shouldUpdateHitPoints(weakness, isFirstContact(weakness) && !isFirstContact(weakness))
	 * 			|	then updateWeaknessTimer(-getHitPointLossThreshold(), weakness)
	 */
	@Model
	private void updateContainingWeakness(double dt, GeologicalFeature weakness){
		assert getWorld().getAllOverlappingTileFeatures(this).contains(weakness);
		if (isAlive()) {
		boolean wasFirstContact = isFirstContact(weakness);
		if (wasFirstContact)
			startWeaknessTimer(dt, weakness); 
		else
			updateWeaknessTimer(dt, weakness);
		if (shouldUpdateHitPoints(weakness, wasFirstContact)){
			updateHitPoints(getWeaknessesAndHitpointLosses().get(weakness));
			if (!wasFirstContact)
				updateWeaknessTimer(-getHitPointLossThreshold(), weakness);
		}
		}
	}
	
	/**
	 * Start the weakness timer for this game object the given geological feature.
	 * 
	 * @param dt
	 * @param weakness
	 * @pre	| isValidTimeInterval(dt)
	 * @pre	| !weaknessTimers.containsKey(weakness)
	 * @post | weaknessTimers.put(weakness, dt)
	 */
	@Model
	private void startWeaknessTimer(double dt, GeologicalFeature weakness){
		assert isValidTimeInterval(dt);
		assert !weaknessTimers.containsKey(weakness);
		weaknessTimers.put(weakness, dt);
	}
	
	/**
	 * Update the weakness timer for this game object of the given geological feature.
	 * 
	 * @param dt
	 * @param weakness
	 * @pre	| (isValidTimeInterval(dt) || fuzzyEquals(dt, -getHitPointLossThreshold()))
	 * @pre	| (weakness != null)
	 * @pre	| !isTerminated()
	 * @post | weaknessTimers.put(weakness, weaknessTimers.get(weakness)+dt)
	 */
	@Model
	private void updateWeaknessTimer(double dt, GeologicalFeature weakness){
		assert (isValidTimeInterval(dt) || fuzzyEquals(dt, -getHitPointLossThreshold())); // can set timer back with getHitPointLossTreshold()
		assert (weakness != null);
		assert (weaknessTimers.containsKey(weakness));
		if (isAlive()){
		double previousTime = weaknessTimers.get(weakness);
		weaknessTimers.put(weakness, previousTime+dt);
		}
	}
	
	/**
	 * Variable containing this game object's geological weaknesses and correspondig hitpoint losses.
	 * 
	 * @invar 	| weaknessTimers != null
	 * @invar	| for each feature in weaknessTimers.keySet()
	 * 			|	world.canHaveAsGeologicalFeature(feature)
	 * @invar	| for each feature in weaknessTimers.keySet()
	 * 			|	isValidTime(weaknessTimers.get(feature))
	 */
	private Map<GeologicalFeature, Double> weaknessTimers = new HashMap<GeologicalFeature, Double>();
	
	
	
	
	// SPRITES
	
	/**
	 * Return the current index of this game object.
	 */
	@Basic
	public int getCurrentIndex(){
		return this.currentIndex;
	}
	
	/**
	 * Check whether this game object can have the given index as its current index.
	 * 
	 * @param index
	 * 
	 * @return (index >= 0) && (index < getNbSprites())
	 */
	public boolean canHaveAsCurrentIndex(int index){
		return (index >= 0) && (index < getNbSprites());
	}
	
	/**
	 * Return the current sprite of this game object.
	 * 
	 * @return result == getImageAt(this.currentIndex)
	 */
	public Sprite getCurrentSprite(){
		return getImageAt(getCurrentIndex());
	}
	
	/**
	 * Return whether this game object can have the supplied sprite array.
	 * 
	 * @param images
	 * @return | if isNullOrContainsNullSprite(images) || images.length < getMinNbSprites()
	 * 			|   then result == false
	 * */
	public boolean canHaveAsSprites(Sprite[] images){
		return !isNullOrContainsNullSprite(images) && images.length >= getMinNbSprites();
	}
	
	/**
	 * Return the image of this game object at the given index.
	 * 
	 * @param index
	 * @pre | (index >= 0) && (index < getNbSprites())
	 * @return result == this.images[index]
	 */
	@Basic @Immutable
	public Sprite getImageAt(int index){
		assert (index >= 0) && (index < getNbSprites());
		return this.images[index];
	}
	
	/**
	 * Return the number of possible sprites of this game object.
	 */
	@Basic
	public int getNbSprites(){
		return images.length;
	}
	
	/**
	 * Return a clone of the array with all the images of this game object.
	 * 
	 * @return result == this.images.clone()
	 */
	public Sprite[] getAllImages(){
		return this.images.clone();
	}
	
	/**
	 * Return the minimal number of sprites for this NPC object.
	 * 
	 * @return result > 1
	 */
	public abstract int getMinNbSprites();
	
	/**
	 * Return the new sprite index of this game object.
	 * 
	 */
	public abstract int getNewSpriteIndex();
	
	/**
	 * Check whether the array of possible sprites
	 * 
	 * @param images
	 * @return | if (images == null)
	 * 			| 	then (result == true)
	 * 			|else result == (for some image in images
	 * 			|						(image == null))
	 */
	public static boolean isNullOrContainsNullSprite(Sprite[] images){
		if (images == null)
			return true;
		for (Sprite image : images){
			if (image == null)
				return true;
		}
		return false;
	}
	
	/**
	 * Set the current index of sprites of this game object to the given index.
	 * 
	 * @param index
	 * @pre | (index >= 0) && (index < getNbSprites()) && !isTerminated()
	 * @post | new.getCurrentIndex == index
	 */
	@Model
	private void setCurrentIndex(int index){
		assert (index >= 0) && (index < getNbSprites()) && !isTerminated();
		this.currentIndex = index;
	}

	/**
	 * Update the current sprite of this game object.
	 * 
	 * @effect	| setCurrentIndex(getNewSpriteIndex())
	 */
	@Model
	private void updateCurrentSprite(){
		setCurrentIndex(getNewSpriteIndex());
	}
	
	/**
	 * Variable registering the current index of this game object.
	 */
	private int currentIndex = 0;
	
	/**
	 * Variable referencing an array with the possible indices of this game object.
	 * 
	 * @invar	canHaveAsSprites(images)
	 */
	private final Sprite[] images;

	
	
	// ASSOCIATION WITH WORLD (including Mazub)

	
	/**
	 * Check whether this game object can have the given world as its world.
	 * 
	 * @param 	world
	 * 			| The world to check
	 * @return	False if the given world is effective and 
	 * 			the given world can not have this game object as one of its game objects.
	 * 			| if (	(world != null) && !world.canHaveAsGameObject(this)	)
	 * 			|	then result == false
	 * 
	 */
	public boolean canHaveAsWorld(World world){
		return world == null || world.canHaveAsGameObject(this);
	}
	
	/**
	 * Checks whether this game object as proper slimes associated with it.
	 * 
	 * @return	result == canHaveAsWorld(getWorld()) &&
	 * 				((getWorld() == null) || getWorld().hasAsGameObject(this))
	 */
	public boolean hasProperWorld(){
		return canHaveAsWorld(getWorld()) && 
				((getWorld() == null) || getWorld().hasAsGameObject(this));
	}
	
	/**
	 * Add the given world as a world for this game object.
	 * 
	 * @param world
	 * 			The world to which this game object must be attached
	 * @post	This game object is attached to the given world.
	 * 			| new.getWorld() == world
	 * @post	The given world has this game object as its game object.
	 * 			| (new world).hasAsGameObject(this)
	 * @post	If this game object was attached to some other world, 
	 * 			that world no longer references this game object as
	 * 			one of its game objects attached to it.
	 * 			| if (getWorld() != null)
	 * 			|	!(new getWorld()).hasAsGameObject(this)
	 * @throws	IllegalArgumentException
	 * 			The given world is not effective or this game object can not have this world as its world or
	 * 			the given world cannot add this.
	 * 			| world == null || !world.canAdd(this) || !canHaveAsWorld(world)
	 */
	public void addToWorld(World world) throws IllegalArgumentException{
		if (world == null || !world.canAdd(this) || !canHaveAsWorld(world))
			throw new IllegalArgumentException("World cannot add this game object."); // Want to have a report if one of the game objects couldn't be added.
		World oldWorld = getWorld();
		setWorld(world);
		addAsGameObject(world); // overriden in each game object
		if (oldWorld != null)
			removeAsGameObject(oldWorld); // overriden in each game object
	}
	
	/**
	 * Remove this game object from its associated world, if any.
	 * 
	 * @pre	isTerminated()
	 * @post This game object is attached to a non-effective world.
	 * 		| new.getWorld() == null
	 * @post If this game object was attached to an effective world,
	 * 		 then that world no longer references this game object
	 * 		 as one of its game objects attached to it.
	 * 		| if (getWorld() != null)
	 * 		|	then !(new getWorld()).hasAsGameObject(this)
	 * 
	 */
	public void removeFromWorld(){
		assert isTerminated();
		World oldWorld = getWorld();
		if (oldWorld != null){
			setWorld(null);
			removeAsGameObject(oldWorld); // overriden in each game object
		}
	}
	
	/**
	 * Return the world to which this game object is attached.
	 */
	// @Raw: in documentation setWorld()
	@Basic @Raw @Model
	public World getWorld(){
		return this.world;
	}
	
	/**
	 * The given world add this game object as one of its associated game objects.
	 * 
	 * @param world
	 * 		  The world which adds this game object.
	 * @post The given world has this game object as one of its associated game objects.
	 * 		| world.hasAsGameObject(this)
	 */
	@Raw @Model
	abstract void addAsGameObject(World world); // world wijst niet terug. World zelf voldoet aan klasse invariant
	
	/**
	 * The given world removes this game object as one of its associated game objects.
	 * 
	 * @param world
	 * 		  The world which removes this game object.
	 * @post The given world hasn't this game object as one of its associated game objects.
	 * 		| !world.hasAsGameObject(this)
	 */
	@Model
	abstract void removeAsGameObject(@Raw World world); // In world wijst game object niet terug
	
	/**
	 * Set the world to which this game object is attached to the given world.
	 * 
	 * @param 	world
	 * 			The world to which this game object must be attached.
	 * @pre		This world can have the given world as its world to be attached to.
	 * 			| canHaveAsWorld(world)
	 * @pre		This game object is terminated and the given world is not effective,
	 * 			or the given world is effective.
	 * 			| (isTerminated() && world == null) || (world != null)
	 * @pre		The given world is not effective or the given world can add this game object.
	 * 			| world == null || world.canAdd(this)
	 * @post	This game object is attached to the given world.
	 * 			| new.getWorld() == world
	 */
	@Raw @Model
	private void setWorld(@Raw World world){
		assert canHaveAsWorld(world);
		assert (world == null && isTerminated()) || (world != null); // Only set the associated world of this game object to null if this game object is terminated.
		assert world == null || world.canAdd(this); // Only add this game object to the world if the world can add this game object.
		this.world = world;
	}
	
	/**
	 * Variable referencing the world attached to this game object.
	 */
	private World world = null;
	
	
	// TIME
	
	/**
	 * Return the time of this game object.
	 */
	@Basic
	public double getTime(){
		return this.time;
	}
	
	/**
	 * Compute the the fractions of time interval in which the given time interval will be split up.
	 * 
	 * @param Dt
	 * @return | if (fuzzyEquals(computeMagnitudeOfVelocity(),0) && fuzzyEquals(computeMagnitudeOfAcceleration(),0))
	 * 			| result == Dt
	 * 			| else result == 0.01/(computeMagnitudeOfVelocity() + computeMagnitudeOfAcceleration()*Dt)
	 */
	public double computeDtFraction(double Dt){
		if (fuzzyEquals(computeMagnitudeOfVelocity(),0) && fuzzyEquals(computeMagnitudeOfAcceleration(),0))
			return Dt;
		return 0.01/(computeMagnitudeOfVelocity() + computeMagnitudeOfAcceleration()*Dt);
	}
	
	/**
	 * Set the time of this game object tot the given time.
	 * 
	 * @param time
	 * @post	| new.getTime() == time
	 * @throws IllegalArgumentException
	 * 			| (!isValidTime(time) || !fuzzyGreaterThanOrEqualTo(time, getTime()))
	 */
	@Model
	private void setTime(double time)throws IllegalArgumentException{
		if (!isValidTime(time) || !fuzzyGreaterThanOrEqualTo(time, getTime()))
			throw new IllegalArgumentException("Illegal time " + time);
		this.time = time;
	}
	
	/**
	 * Variable registering the time of this game object.
	 */
	private double time = 0;
	


	/**
	 * Advance the time for this game object with the given amount.
	 * 
	 * @note	No documentation is to be supplied for this method.
	 */
	@Model
	void advanceTime(double dt) throws IllegalArgumentException, IllegalStateException{
		if (!fuzzyLessThanOrEqualTo(getTime() + dt, getWorld().getTime()))
			throw new IllegalArgumentException("The time of the world must always be higher than the time of this game object");
		if (!isValidTimeInterval(dt))
			throw new IllegalArgumentException("Illegal time interval: " + dt);
		if (isTerminated())
			throw new IllegalStateException("Cannot advance the time for a terminated object");
		// TIME
		setTime(getTime()+dt);
		if (isAlive()){
			 
			// IMMUNITY
			updateImmunityTimer(dt);
			
			// SPRITES
			updateCurrentSprite();
			
			// HORIZONTAL MOVEMENT
			if (canMoveHorizontally() && isRunning()){
				updateXPosition(dt);
				updateXVelocity(dt);
			}
			

			// VERTICAL MOVEMENT
			if (canMoveVertically()){
			updateYPosition(dt);
			updateYVelocity(dt);
			}
				
			// COLLISIONS
			updateCollidingObjects();
			updateCollisionHitpoints();
			
			// WEAKNESSES

			updateWeaknesses(dt);


			} else {
			updateTerminationProcess();
		}
	}
	
	
// ASSOCIATION WITH COLLIDING OBJECTS
	
	/**
	 * Check whether this game object has the given game object as one of the game objects attached to it.
	 * 
	 * @param other
	 * @return result == collidingObjects.contains(other)
	 */
	// @Raw because in precondition of addAsCollidingObject & removeAsCollidingObject
	@Basic @Raw
	public boolean hasAsCollidingObject(@Raw GameObject other){
		return collidingObjects.contains(other);
	}
	
	/**
	 * Check whether this game object can have the given game object as one of its associated game objects.
	 * 
	 * @param other
	 * @return | result == !this.isTerminated() && (other != null) && this.collidesWith(other) && (this.getWorld() == other.getWorld())
	 */
	public boolean canHaveAsCollidingObject(GameObject other){
		if (this.isTerminated())
			return false;
		// If other is terminated: firstly isTerminated will be set to true, secondly all collisions will be removed and lastly other will be removed from the world.
		// This means whenever other is terminated and consequently the method removeCollision is called, this and other will still have the same world.
		// In short, canHaveAsCollidingObject works fine if a terminated object calls the method removeCollision().
		// The terminated object will not be satisfying the class invariant more but will still satisfy the representation invariant.
		return	(other != null) && this.collidesWith(other) && (this.getWorld() == other.getWorld());
	}
	
	/**
	 * Check whether this game Object has proper game objects as its game objects associated with it.
	 * 
	 * @return | result ==
	 * 			|for each collidingObject in collidingObjects
	 * 			|	canHaveAsCollidingObject(collidingObject) && collidingObject.hasAsCollidingObject(this)
	 */
	// The class invariant differs from the representation invariant.
	// In the class invariant, each colliding object (in collidingObjects) must reference back to this game object.
	// In the representation invariant, the colliding object doesn't have to reference back if the colliding object is terminated.
	public boolean hasProperCollidingObjects(){
		for (GameObject other: collidingObjects)
			if (	!canHaveAsCollidingObject(other) || !other.hasAsCollidingObject(this)	) // VERSCHILT VAN REPRESENTATIE INVARIANT!!!
				return false;
		return true;
	}
	
	/**
	 * Add the given object as one of the associated game objectsof this game object.
	 * 
	 * @param other
	 * @pre | other.hasAsCollidingObject(this)
	 * @post | new.hasAsCollidingObject(this)
	 */
	@Model
	void addAsCollidingObject(@Raw GameObject other){
		assert other.hasAsCollidingObject(this);
		collidingObjects.add(other);
	}
	
	/**
	 * Remove the given ame object as one of the associated game objects of this game object.
	 * 
	 * @param other
	 * @pre	| !other.hasAsCollidingObject(this)
	 * @pre | this.hasAsCollidingObject(other)
	 * @post | !new.hasAsCollidingObject(other)
	 */
	@Raw @Model
	void removeAsCollidingObject(GameObject other){
		assert !other.hasAsCollidingObject(this);
		assert this.hasAsCollidingObject(other); // other mag niet null
		collidingObjects.remove(other);
	}
	
	/**
	 * Add the collision of this and the other game object to both of them.
	 * 
	 * @param other
	 * @pre |canHaveAsCollidingObject(other)
	 * @pre |!other.isTerminated()
	 * @post |new.hasAsCollidingObject(other)
	 * @post |(new other).hasAsCollidingObject(this)
	 */
	@Model
	private void addCollision(GameObject other){
		assert canHaveAsCollidingObject(other);
		assert !other.isTerminated(); // Moet hier: mag geen terminated object toevoegen maar kan er wel een hebben.
		collidingObjects.add(other);
		other.addAsCollidingObject(this);
	}
	
	/**
	 * Remove the collision of this and the other game object from both of them.
	 * 
	 * @param other
	 * @pre !this.collidesWith(other) || this.isTerminated()
	 * @pre this.hasAsCollidingObject(other)
	 * @post !new.hasAsCollidingObject(other)
	 * @post !(new other).hasAsCollidingObject(this)
	 */
	
	@Model
	private void removeCollision(GameObject other){
		assert !this.collidesWith(other) || this.isTerminated();
		assert this.hasAsCollidingObject(other); // dan sowieso ook omgekeerd // other mag niet null
		collidingObjects.remove(other);
		other.removeAsCollidingObject(this);
	}
	
	/**
	 * Update the colliding objects of this game object.
	 * 
	 * @effect | if (isAlive())
	 * 			| 	then for each collidingObject in collidingObjects
	 * 			|		if (!collidesWith(collidingObject))
	 * 			|			then removeCollision(collidingObject)
	 * @effect	| if(isAlive()
	 * 			|	then for each collidingObject in getWorld().getCollidingObjectsWith(this)
	 * 			|			if (!hasAsCollidingObject(collidingObject))
	 * 			|				then addCollision(collidingObject)
	 */
	@Model
	private void updateCollidingObjects(){
		if (isAlive()){
			Set<GameObject> collidingObjectsClone = new HashSet<>(collidingObjects); // cannot remove elements from iterated set
			for (GameObject other : collidingObjectsClone){
				if (!collidesWith(other))
					removeCollision(other);
			}
			// Add new colliding objects
			for (GameObject other : getWorld().getCollidingObjectsWith(this)){
				if (!hasAsCollidingObject(other))
					addCollision(other); 
			}
		}
	}
	
	
	
	/**
	 * Set collecting references to the the objects this is colliding with.
	 * 
	 * @invar	The set of colliding objects is effective.
	 * 			| collidingObjects != null
	 * @invar	Each element in the set of colliding objects references a colliding object
	 * 			that is an acceptable colliding object for this game object.
	 * 			| for each collidingObject in collidingObjects
	 * 			|	canHaveAsCollidingObject(collidingObject)
	 * @invar	Each colliding object in the set of colliding objects is terminated or references this
	 * 			school as the school to which it is attached.
	 * 			| for each collidingObject in collidingObjects
	 * 			|	collidingObject.hasAsCollidingObject(this) || collidingObject.isTerminated()
	 */
	private Set <GameObject> collidingObjects = new HashSet<>();
	
	// COLLISION
	
	/**
	 * Update the collision hitpoints of this game object.
	 * 
	 * @effect	| if (isAlive())
	 * 			|	then for each game object in collidingObjects
	 * 			|			updateHitPointsUponCollisionWith(other)
	 */
	public void updateCollisionHitpoints(){
		if (isAlive())
			for (GameObject other: collidingObjects){
				updateHitPointsUponCollisionWith(other);
			}	
	}
	
	/**
	 * Check whether there is an impassable entity one pixel away of this game object in the given direction.
	 * 
	 * @param direction
	 * @pre	direction != null
	 * @return 	|if (getWorld().overlapsWithImpassableTile(direction.perimeterOf(this))
	 * 		 	|	then (result == true)
	 * 			| else if (interferingWithGameObjectIn(direction))
	 * 			|	then (result == true)
	 * 			| else (result == false)
	 * 
	 */
	public boolean impassableEntityIn(DirectionType direction){
		assert direction != null;
		// TILES
		if (getWorld().overlapsWithImpassableTile(direction.perimeterOf(this)))
			return true;
		// GAME OBJECTS
		if (interferingWithGameObjectIn(direction))
			return true;
		return false;
	}
	/**
	 * Check whether there is an impassable game object one pixel away of this game object in the given direction.
	 * 
	 * @param direction
	 * @pre		| direction != null
	 * @return 	| result ==
	 * 			|	for some collidingObject in collidingObjects:
	 * 			|		(	perimeter.collidesWith(collidingObject) && this.interferesMovementWith(collidingObject)) && 
	 * 			|			isMovingInInterferingDirection(direction, collidingObject)	)
	 */
	public boolean interferingWithGameObjectIn(DirectionType direction){
		assert direction != null;
		InfoGameObject perimeter = direction.perimeterOf(this);
		for (GameObject collidingObject: collidingObjects){
			if (perimeter.collidesWith(collidingObject) && this.interferesMovementWith(collidingObject)){
				if (direction.isTryingToMoveInInterferingDirection(perimeter, collidingObject))
					return true;
			}
		}
		return false;
	}

	/**
	 * Check whether this game object is on top of the given other game object.
	 * 
	 * @param other
	 * @pre		| other != null
	 * @return	| result == ((this.getYPixel() == other.getTopPixel()) &&
				|				(this.getRightPixel() >= other.getXPixel() && this.getXPixel() <= other.getRightPixel()))
	 */
	public boolean isOnTopOf(GameObject other){
		assert other != null;
		return ((this.getYPixel() == other.getTopPixel()) &&
				(this.getRightPixel() >= other.getXPixel() && this.getXPixel() <= other.getRightPixel()));
	}


	/**
	 * Return whether the supplied game object interferes movement with this game object.
	 * 
	 * @return | if (other == null)
	 * 			|  then result == false
	 */
	public abstract boolean interferesMovementWith(GameObject other);
	
	/**
	 * Check whether this game object is passable for the given game object.
	 * 
	 * @post	| result == !interferesMovementWith(gameObject)
	 */
	@Override
	public boolean isPassableFor(GameObject gameObject){
		return !interferesMovementWith(gameObject);
	}

	
	// GAMEOBJECT STATUS
	
	/**
	 * Return the game object status of this game object.
	 */
	@Basic
 	public GameObjectStatus getGameObjectStatus(){
 		return this.gameObjectStatus;
 	}
	
	/**
	 * Check whether the given game object status is a valid game object status for all game objects.
	 * 
	 * @param gameObjectStatus
	 * @return | result == (gameObjectStatus != null)
	 */
	public static boolean isValidGameObjectStatus(GameObjectStatus gameObjectStatus){
		return gameObjectStatus != null;
	}
	
	/**
	 * Check whether this game object is alive.
	 * 
	 * @return	| result == getGameObjectStatus().isAlive()
	 */
	public boolean isAlive(){
 		return getGameObjectStatus().isAlive();
 	}
 	
	/**
	 * Check whether this game object is in the termination process.
	 * 
	 * @return	| result == getGameObjectStatus().isInTerminationProcess()
	 */
	public boolean isInTerminationProcess(){
		return getGameObjectStatus().isInTerminationProcess();
	}
	
	/**
	 * Check whether this game object is terminated.
	 * 
	 * @return	result == getGameObjectStatus().isTerminated()
	 */
	@Raw
	public boolean isTerminated(){
		return getGameObjectStatus().isTerminated();
	}
	
	/**
	 * Set the game object status of this game object to the given status.
	 * 
	 * @param status
	 * @pre	| status != null
	 * @pre	| getGameObjectStatus().compareTo(status) < 0
	 * @post | new.getGameObjectStatus() == status
	 */
	@Model
	private void setGameObjectStatus(GameObjectStatus status){
		assert status != null;
		assert getGameObjectStatus().compareTo(status) < 0;
		this.gameObjectStatus = status;
	}
	
	/**
	 * Variable registering the game object status of this game object.
	 */
	private GameObjectStatus gameObjectStatus = GameObjectStatus.ALIVE;
	
	
		// TERMINATION PROCESS
	
	/**
	 * Return the the time when the termination process will end for this game object.
	 */
	@Basic
	public double getEndTerminationProcess(){
		return this.endTerminationProcess;
	}
	
	/**
	 * Return the termination treshold of this game object.
	 */
	@Basic
	public double getTerminationThreshold(){
		return 0.6;
	}
	
	/**
	 * Check whether this game object can have the end of its termination process at the given 
	 * 	time.
	 * 
	 * @param endTerminationProcess
	 * 
	 * @return	| result == Double.isNaN(endTerminationProcess) || endTerminationProcess >= 0
	 */
	public boolean canHaveAsEndOfTerminationProcess(double endTerminationProcess){
		return Double.isNaN(endTerminationProcess) || endTerminationProcess >= 0;
	}
	
	/**
	 * Update the termination proces of this game object.
	 * 
	 * @pre	| isInTerminationProcess()
	 * @effect | if (fuzzyGreaterThanOrEqualTo(getTime(), getEndTerminationProcess()))
	 * 			| 	then terminate()
	 * 
	 */
	@Model
	void updateTerminationProcess(){
		assert isInTerminationProcess();
		if (fuzzyGreaterThanOrEqualTo(getTime(), getEndTerminationProcess())){
			terminate();
		}
	}
	
	/**
	 * Start the termination process of this game object.
	 * 
	 * @pre	| getHitPoints() == 0
	 * @effect	| setGameObjectStatus(GameObjectStatus.TERMINATION_PROCESS)
	 * @effect	| if (getTerminationThreshold() == 0)
	 * 			|	then terminate()
	 * @post	| new.getEndTerminationProcess() == getTime() + getTerminationTreshold()
	 * 
	 */
	@Model
	void startTerminationProcess(){
		assert (getHitPoints() == 0);
		setGameObjectStatus(GameObjectStatus.TERMINATION_PROCESS);
		if (getTerminationThreshold() == 0)
			terminate();
		this.endTerminationProcess = getTime() + getTerminationThreshold();
	}
	
	/**
	 * Variable registering the end of the termination process of this game object.
	 */
	private double endTerminationProcess = Double.NaN;
	
		// TERMINATION
	/**
	 * Terminate this game object.
	 * 
	 * @pre	| (getHitPoints() == 0) || !getWorld().isInXRange(getXPosition())|| !getWorld().isInYRange(getYPosition())
	 * @effect	| setGameObjectStatus(GameObjectStatus.TERMINATED)
	 * @post	| for each gameObject in collidingObjects
	 * 			|	!gameObject.hasAsCollidingObject(this)
	 * @effect	| removeFromWorld()
	 */
 	public void terminate(){
	assert (getHitPoints() == 0) || !getWorld().isInXRange(getXPosition())|| !getWorld().isInYRange(getYPosition()); // limit applicability
	setGameObjectStatus(GameObjectStatus.TERMINATED);
	Set<GameObject> collidingObjectsClone = new HashSet<>(collidingObjects); // cannot remove elements from iterated set
	for (GameObject other : collidingObjectsClone)
		removeCollision(other);
	removeFromWorld();
	}
	
 	// TO STRING
	/**
	 * Return a textual representation of this game object.
	 *
	 * @return The textual representation of the name of this class.
	 *       | result.equals(this.getClass().getSimpleName())
	 */
	@Override
	public String toString(){
		return this.getClass().getSimpleName();
	}
	
	
	
}
