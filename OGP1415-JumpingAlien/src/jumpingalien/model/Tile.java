package jumpingalien.model;

import jumpingalien.model.type.*;
import be.kuleuven.cs.som.annotate.*;

@Value
public class Tile implements ObjectType {
	
	// CONSTRUCTOR
	
	public Tile(int xTile, int yTile, World world){
		assert (xTile >= 0 && yTile >= 0);
		assert(canHaveAsWorld(world));
		this.xTile = xTile;
		this.yTile = yTile;
		this.world = world;
		this.tileLength = world.getTileLength();
	}
	
	// PROPERTIES
	
	@Basic @Immutable 
	public int getXTile(){
		return this.xTile;
	}
	
	@Basic @Immutable 
	public int getYTile(){
		return this.yTile;
	}
	
	private final int xTile, yTile;
	
	private final World world;
	
	private World getWorld(){
		return this.world;
	}
	
	public boolean canHaveAsWorld(World world){
		return world != null;
	}
	
	// CONVENIENT METHODS FOR PROGRAM
	
	@Override
	public int getXPixel(){
		return Conversion.convertTileToPixel(getXTile(), getTileLength());
	}
	
	@Override
	public int getRightPixel(){
		return getXPixel()+getWidth();
	}
	
	@Override
	public int getYPixel(){
		return Conversion.convertTileToPixel(getYTile(), getTileLength());
	}
	
	@Override
	public int getTopPixel(){
		return getYPixel() + getHeight();
	}
	
	public GeologicalFeature getGeologicalFeature(){
		return getWorld().getGeologicalFeatureAt(getXPixel(), getYPixel());
	}
	
	// HEIGHT AND WIDTH
	
	@Override
	public int getWidth() {
		return getTileLength();
	}
	
	@Override
	public int getHeight() {
		return getTileLength();
	}
	
	
	public int getTileLength(){
		return this.tileLength;
	}
	
	
	
	private final int tileLength;
	
	// IS PASSABLE
	
	public boolean isPassableFor(GameObject gameObject) {
		return !getGeologicalFeature().isImpassable();
	};
	
	
	// EQUALS

	@Override
	public boolean equals(Object other){
		return (other instanceof Tile) && 
				(this.getXTile() == ((Tile)other).getXTile()) &&
				(this.getYTile() == ((Tile)other).getYTile()) &&
				(this.getWorld() == ((Tile)other).getWorld());
	}
	
	@Override
	public int hashCode(){
		return Integer.hashCode(getXTile()*getYTile() + getXTile()*getWorld().hashCode());
	}
	
 	// TO STRING
	@Override
	public String toString(){
		return this.getClass().getSimpleName();
	}

}
