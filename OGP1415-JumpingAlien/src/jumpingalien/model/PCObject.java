package jumpingalien.model;

import be.kuleuven.cs.som.annotate.*;
import jumpingalien.util.Sprite;

/**
 * A class of playable character objects.
 *
 * @version 3.0
 * @author  Antoon Purnal and Tuur Van Daele
 *
 */
public abstract class PCObject extends GameObject {

	/**
	 * Initialize a new playable Object.
	 * 
	 * @param xPixel
	 * @param yPixel
	 * @param hitPoints
	 * @param images
	 * 
	 * @effect	| super(xPixel, yPixel, hitPoints, images)
	 */
	protected PCObject(int xPixel, int yPixel,int hitPoints, Sprite[] images) {
		super(xPixel, yPixel, hitPoints, images);
	}
	
	
	// MOVEMENT LIMITATIONS
	
	/**
	 * Check whether this playable Game Object can move horizontally (always true).
	 */
	@Basic @Override
	public boolean canMoveHorizontally() {
		return true;
	}

	/**
	 * Check whether this playable Game Object can move vertically (always true).
	 */
	@Basic @Override
	public boolean canMoveVertically() {
		return true;
	}
	
	
	// SPRITES 
	
	/**
	 * Return the minimal amount of sprites for any playable Game Object. (always 10)
	 */
	@Basic @Override
	public int getMinNbSprites(){
		return 10;
	}
	
	/**
	 * Check whether this playable object can have the supplied sprite array as its sprites.
	 * 
	 * @param images
	 *  
	 * @return result == !isNullOrContainsNullSprite(images) && images.length >= getMinNbSprites() && (images.length % 2 == 0)
	 */
	@Override
	public boolean canHaveAsSprites(Sprite[] images){
		return super.canHaveAsSprites(images) && (images.length % 2 == 0);
	}

	
}
