package jumpingalien.model;




import static jumpingalien.util.Util.fuzzyLessThanOrEqualTo;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import jumpingalien.util.Sprite;
import be.kuleuven.cs.som.annotate.*;


/**
 * A class of Mazub's.
 * 
 * @invar	| hasProperJumper()
 * @invar	| hasProperDucker()
 * @invar	| canHaveAsIndices(getCurrentIndices())
 * @invar	| isValidMovement(getLastMovement())
 * @invar	| isValidTime(getAlternationTimer())
 * 
 * @version 3.0
 * @author  Antoon Purnal and Tuur Van Daele

 *
 */
public class Mazub extends PCObject implements Duckable, Jumpable{
	
	/**
	 * Create a new mazub with the given horizontal pixel, the given vertical pixel and the given images.
	 * 
	 * @param xPixel
	 * @param yPixel
	 * @param images
	 * 
	 * @effect 	| super(xPixel, yPixel, getInitialHitPoints(), images)
	 * @post	| hasProperJumper()
	 * @post	| hasProperDucker()
	 * 
	 * 
	 * @post	| Double.isNaN(new.getLastMovement())
	 * @post	| new.getAlternationTimer() == 0
	 */
	
	public Mazub(int xPixel, int yPixel, Sprite[] images) {
		super(xPixel, yPixel, getInitialHitPoints(), images);
		// This game object can jump
		Jumper jumper = new Jumper(this);
		assert canHaveAsJumper(jumper);
		this.jumper = jumper;
		// This game object can duck 
		Ducker ducker = new Ducker(this);
		assert canHaveAsDucker(ducker);
		this.ducker = ducker;
	}
	
	
	// JUMPING ASSOCIATION

	/**
	 * Return the associated Jumper object for this Mazub.
	 */
	@Basic
	public Jumper getJumper(){
		return this.jumper;
	}
	
	/**
	 * Return whether this Mazub can have the supplied Jumper object as jumper object.
	 * 
	 * @param jumper
	 * 			The Jumper object for which to verify correctness.
	 * @return True if and only if the supplied Jumper is not equal to the null reference, and if the Jumper can have
	 * 			this Mazub as its jumping object.
	 * 			| result == (jumper != null && jumper.canHaveAsJumpingObject(this))
	 */
	public boolean canHaveAsJumper(Jumper jumper){
		return jumper != null && jumper.canHaveAsJumpingObject(this);
	}
	
	
	/**
	 * Check whether this Mazub currently has a proper Jumper object associated to it.
	 * 
	 * @return True if and only if this Mazub can have its current Jumper as a Jumper object, and the Jumper Object is
	 * 			correspondingly associated to this Mazub.
	 * 			result == (canHaveAsJumper(getJumper())
					&& getJumper().getJumpingObject() == this)
	 */
	public boolean hasProperJumper(){
		return canHaveAsJumper(getJumper())
				&& getJumper().getJumpingObject() == this;
	}
	
	
	/**
	 * Variable containing this Mazub's associated Jumper object.
	 */
	private final Jumper jumper;
	
	
	
	// JUMPING
	
	
	/**
	 * Initiate jump for this Mazub.
	 * 
	 * @effect Start the jump for this Mazub's associated Jumper object.
	 * 			| getJumper.startJump()
	 * 
	 * @throws IllegalStateException
	 * 			This Mazub is not associated with a jumping object or this Mazub is not alive.
	 * 			| getJumper == null || !isAlive()
	 */
	public void startJump() throws IllegalStateException{
		if (getJumper() == null)
			throw new IllegalStateException("Mazub is not associated with a jumping object");
		if (!isAlive())
			throw new IllegalStateException("This Mazub is not alive and consequently cannot start jumping.");
		getJumper().startJump();
	}
	
	/**
	 * Terminate jump for this Mazub.
	 * 
	 * @effect Start the jump for this Mazub's associated Jumper object.
	 * 			| getJumper.endJump()
	 * 
	 * @throws IllegalStateException
	 * 			This Mazub is not associated with a jumping object or this Mazub is not alive.
	 * 			| getJumper == null || !isAlive()
	 */
	public void endJump() throws IllegalStateException{
		if (getJumper() == null)
			throw new IllegalStateException("Mazub not associated with a jumping object");
		if (!isAlive())
			throw new IllegalStateException("This shark is not alive and consequently cannot end the jump.");
		getJumper().endJump();
	}
	
	/**
	 * Return the initial vertical velocity when jumping for this Mazub (always 8 m/s).
	 */
	@Basic @Override @Immutable
	public double getInitialYVelocity() {
		return 8;
	}
	
	
	
	// DUCKING
	
	
	
	/**
	 * Return the associated Ducker object for this Mazub.
	 */
	@Basic
	public Ducker getDucker(){
		return this.ducker; // (moet public voor interface)
	}
	
	
	/**
	 * Return whether this Mazub can have the supplied Ducker object as ducker object.
	 * 
	 * @param ducker
	 * 			The Ducker object for which to verify correctness.
	 * @return True if and only if the supplied Ducker is not equal to the null reference, and if the Ducker can have
	 * 			this Mazub as its Ducking object.
	 * 			| result == (ducker != null && ducker.canHaveAsDuckingObject(this))
	 */
	public boolean canHaveAsDucker(Ducker ducker){
		return ducker != null && ducker.canHaveAsDuckingObject(this);
	}
	
	/**
	 * Check whether this Mazub currently has a proper Ducker object associated to it.
	 * 
	 * @return True if and only if this Mazub can have its current Ducker as a Ducker object, and the Ducker Object is
	 * 			correspondingly associated to this Mazub.
	 * 			result == (canHaveAsDucker(getDucker())
					&& getDucker().getDuckingObject() == this)
	 */
	public boolean hasProperDucker(){
		return canHaveAsDucker(getDucker()) &&
				getDucker().getDuckingObject() == this;
	}
	
	/**
	 * Return the height of the highest sprite for this Mazub.
	 * 
	 * @return The height of the highest sprite of this Mazub (assuming all the non-ducking sprites have the same height.)
	 * 			|  result == (getImageAt(0).getHeight());
	 */
	@Override
	public int getHeightOfHighestSprite() {
		return getImageAt(0).getHeight(); // misschien klasse-inv
	}
	
	/**
	 * Variable containing this Mazub's associated Ducking Object.
	 */
	private final Ducker ducker;
	
	
	
	
	
	// DUCKING
	
	
	/**
	 * Check whether this Mazub is currently ducking.
	 * 
	 * @return True if and only of this Mazub's associated Ducking object is currently ducking.
	 * 			| result == getDucker.isDucking();
	 */
	public boolean isDucking(){
		return getDucker().isDucking();
	}
	
	/**
	 * Initiate ducking for this Mazub.
	 * 
	 * @effect Initiate ducking for this Mazub's associated Ducker object.
	 * 			| getDucker().startDuck()
	 * 
	 * @throws IllegalStateException
	 * 			This Mazub is not associated with a ducker object.
	 * 			| getDucker() == null
	 */
	@Override
	public void startDuck() throws IllegalStateException{
		if (getDucker() == null)
			throw new IllegalStateException("Mazub is not associated with a ducker object");
		getDucker().startDuck();
	}

	
	/**
	 * Terminate ducking for this Mazub.
	 * 
	 * @effect Terminate ducking for this Mazub's associated Ducker object.
	 * 			| getDucker.endDuck()
	 * @throws IllegalStateException
	 */
	@Override
	public void endDuck() throws IllegalStateException{
		if (getDucker() == null)
			throw new IllegalStateException("Mazub is not associated with a ducking object");
		getDucker().endDuck();
	}
	
	/**
	 * Update the ducking state for this Mazub.
	 * 
	 * @pre	This Mazub is currently alive.
	 * 			| isAlive()
	 * @effect	Update the ducking state for this Mazub's associated Ducker object.
	 * 				| getDucker().updateDuckingState()
	 */
	private void updateDuckingState(){
		assert isAlive();
		getDucker().updateDuckingState();
	}
	


	// HORIZONTAL MOVEMENT
		// VELOCITY
	
	
	/**
	 * Return the maximal horizontal velocity for this Mazub, depending on its current state.
	 * 
	 * @return Return 1 if this Mazub is currently ducking, return 3 in all other cases.
	 * 			| if (isDucking())
	 * 			| 	then result == 1
	 * 			| else return 3
	 */
	@Override
	public double getMaxXVelocity() {
		if (isDucking())
			return 1;
		return 3;
	}
	
	/**
	 * Return the initial horizontal velocity (in m/s) for this Mazub (always 1).
	 */
	@Basic @Override
	public double getInitialXVelocity() {
		return 1;
	}

		// ACCELERATION
	

	/**
	 * Return the current acceleration (in m/s^2) of this Mazub.
	 * 
	 * @return If this Mazub currently has a constant horizontal velocity, return 0. Otherwise, return 0.9.
	 * 			| if (hasConstantXVelocity())
				|	then return 0
				| else return 0.9;
	 */
	@Override
	public double getXAcceleration() {
		if (hasConstantXVelocity())
			return 0;
		return 0.9;
	}
	


	// HITPOINTS
	
	/**
	 * Return the initial number of hitpoints for this Mazub (always 100).
	 */
	@Basic @Immutable
	public static int getInitialHitPoints(){
		return 100;
	}
	
	/**
	 * Check whether this Mazub is currently hungry.
	 * 
	 * @return True if and only if the current number of hitpoints is less than 
	 * 			the maximum number of hitpoints for this Mazub.
	 * 			| result == getHitPoints() < getMaxHitPoints()
	 */
	public boolean isHungry(){
		return getHitPoints() < getMaxHitPoints();
	}
	
	
	
	/**
	 * Update the hitpoints of this and the given other game object caused by the collision.
	 * 
	 * @effect	If and only if the other game object is effective, has as colliding object this game object, this game object is not terminated, the other game object
	 * 			is not terminated, this is not immune and other is not immune then:
		 * 			if the other is a plant and this is hungry and the other is alive, the hitpoints of this should be updated with 50 and the hitpoints of other
		 * 			must be updated with -1. If the other is a plant but this is not hungry or the other is not alive, then do nothing. If the other is a slime and
		 * 			if this is not on top of the other, then update the hitpoints of this and other with - 50 and reset the immunity timer for both of them. If the other
	 * 				is a slime and this is not on top of the other substract 50 hitpoints of the other and reset the immunity timer of the other.
	 * 				If the other is a shark and if this is on top of the other, update the hitpoints of the other with -50 and reset the immunity timer of the other.
	 * 				If the other is a shark but this is not on top of the other, update the hitpoints of the other and this with -50 and reset the immunity time of both.
	 * 				Else update the collision in the class of other.
	 * 		
	 * 			| if (other != null && hasAsCollidingObject(other) && !this.isTerminated() && !other.isTerminated() && !isImmune() && !other.isImmune())
	 * 			|	then if (other instanceof Plant)
	 * 			|			then if (isHungry() && other.isAlive())
	 * 			|					then this.updateHitPoints(50) && other.updateHitPoints(-1)
	 * 			| else if (other instanceof Slime)
	 * 			|			then if (!this.isOnTopOf(other))
	 * 			|					then other.updateHitPoints(-50) && other.resetImmunityTimer() && this.updateHitPoints(-50) && this.resetImmunityTimer()
	 * 			|				else other.updateHitPoints(-50) && other.resetImmunityTimer()
	 * 			| else if (other instanceof Shark)
	 * 			|			then if (!this.isOnTopOf(other))
	 * 			|					then other.updateHitPoints(-50) && other.resetImmunityTimer() && this.updateHitPoints(-50) && this.resetImmunityTimer()
	 * 			|				else other.updateHitPoints(-50) && other.resetImmunityTimer()
	 * 			| else other.updateHitPointsUponCollisionWith(this)
	 * 
	 */
	@Override
	void updateHitPointsUponCollisionWith(GameObject other){
		if (other == null || !hasAsCollidingObject(other) || this.isTerminated() || other.isTerminated());
		else if (isImmune() || other.isImmune());
		else if(other instanceof Plant){
			
			if (isHungry() && other.isAlive()){
				this.updateHitPoints(50);
				other.updateHitPoints(-1);
			}
		}
		else if(other instanceof Slime){
			other.updateHitPoints(-50);
			other.resetImmunityTimer();
			if (!this.isOnTopOf(other)){
				this.updateHitPoints(-50);
				this.resetImmunityTimer();
			}
		}
		else if(other instanceof Shark){
			other.updateHitPoints(-50);
			other.resetImmunityTimer();
			if (!this.isOnTopOf(other)){
				this.updateHitPoints(-50);
				this.resetImmunityTimer();
			}
		}
		else
			other.updateHitPointsUponCollisionWith(this);
	}
	
	
	// SPRITES
	
		// CURRENT INDICES
	
	/**
	 * Return the new sprite index of this Mazub
	 * 
	 * @pre		This Mazub is alive
	 * 			|isAlive()
	 * @post	If the length of the current indices is (strict) greater than one,
	 * 			then the result is the element of the current indices with as index the new calculated index.
	 * 			| if (getCurrentIndices().length > 1)
	 * 			|	then result == getCurrentIndices()[((int) (getAlternationTimer()/TIME_EACH_IMAGE) % getCurrentIndices().length)]
	 * 			Else the result is the first element of the current indices
	 * 			| else result == getCurrentIndices()[0]
	 */
	@Override
	public int getNewSpriteIndex(){
		assert isAlive();
		if (getCurrentIndices().length > 1){
				// Als state has changed ==> automatisch op 0 gezet via formule
				int newIndex = ((int) (getAlternationTimer()/TIME_EACH_IMAGE) % getCurrentIndices().length);
				return getCurrentIndices()[newIndex];
		}
		return getCurrentIndices()[0];
	}
	
	/**
	 * Return the indices of the current alternating images of this Mazub.
	 */
	public int[] getCurrentIndices(){
		return this.currentIndices;
	}
	
	/**
	 * Return whether the current alternating images of this Mazub have just changed.
	 * 
	 * @param 	newIndices
	 * 			The indices of the new alternating images.
	 * @pre		This Mazub can have the given indices as its current alternating indices.
	 * 			| canHaveAsIndices(newIndices)
	 * @return	True if and only if the given new calculated indices 
	 * 			are not equal to the old current indices of this Mazub.
	 * 			| result == (!Arrays.equals(getCurrentIndices(), newIndices))
	 */
	public boolean stateHasChanged(int[] newIndices){
		assert canHaveAsIndices(newIndices);
		return (!Arrays.equals(getCurrentIndices(), newIndices));
	}	
	
	/**
	 * Checks whether the array of images of this Mazub contains the given index.
	 * 
	 * @param 	index
	 * 			The index to check.
	 * @return	True if and only if the index is positive and
	 * 			lower than the number of sprites in the array images.
	 * 			| result == ( (index < getNbSprites()) && (index >= 0) )
	 */
	public boolean canHaveAsIndex(int index){
		return ( (index < getNbSprites()) && (index >= 0) );
	}
	
	/**
	 * Checks whether this Mazub can have the given indices as its
	 * current indices.
	 * 
	 * @param	indices
	 * 			The indices to check
	 * @post	False if and only if this Mazub cannot have the given index as one of its indices.
	 * 			| result == for each index in indices
	 * 			| 	 			canHaveAsIndex()
	 */
	public boolean canHaveAsIndices(int[] indices){
		if (indices == null)
			return false;
		// @invar	Mazub can have all indices before "index".
		//			| for each I in 0..index-1:
		//			|	canHaveAsIndex(index)
		// @invar	The number of handled indices doesn't exceed the length of the given array indices.
		//			| Arrays.asList(indices).indexOf(index) <= indices.length
 		// @variant	The number of unhandeld indices in the given array.
		//			| indices.length - Arrays.asList(indices).indexOf(index)
		for (int index: indices)
			if (!canHaveAsIndex(index))
				return false;
		return true;
	}
	
	/**
	 * Update the sprite indices and the alternation timer of this Mazub
	 * 
	 * @param 	dt
	 * 			The time interval to update with.
	 * @pre		The given time interval is a valid time interval
	 * 			| isValidTimeInterval(dt)
	 * @pre		This Mazub is alive
	 * 			| isAlive()
	 * @effect	If the state of this Mazub has changed,
	 * 			then set the alternation timer to the given time interval and
	 * 			set the current indices of this Mazub to the new applicable indices.
	 * 			| if (stateHasChanged(getApplicableIndices())
	 * 			|		then setAlternationTimer(dt) && setCurrentIndices(getApplicableIndices())
	 * 			Else then set the alteration timer of this mazub to the value of the current 
	 * 			alternation timer increased with the given timer duration.
	 * 			| else then setAlterationTimer(getAlternationTimer() + dt)
	 * 
	 */
	private void updateSpriteIndicesAndAlternationTimer(double dt){
		assert isValidTimeInterval(dt);
		assert isAlive();
		int[] newIndices = getApplicableIndices();
		if (stateHasChanged(newIndices)){
			setAlternationTimer(dt);
			setCurrentIndices(newIndices);
		}
		else 
			setAlternationTimer(getAlternationTimer()+ dt);
	}
	
	/**
	 * Set the current indices of this Mazub to the given indices.
	 * 
	 * @param 	newIndices
	 * 			The new current indices of this Mazub
	 * @pre		This Mazub can have the given indices as its current indices.
	 * 			| canHaveAsIndices(newIndices)
	 * @post	The new current indices of this Mazub are equal
	 * 			to the given indices.
	 * 			| new.getCurrentIndices() == newIndices
	 */
	@Model
	private void setCurrentIndices(int[] newIndices ){
		assert (canHaveAsIndices(newIndices));
		this.currentIndices = newIndices;
	}
	
	/**
	 * Variable referencing the indices of the current alternating images of this Mazub.
	 */
	private int[] currentIndices = new int[] {0};
	
		// Calculate new applicable indices
	/**
	 * Return an array of indices associated with the sprites that correspond with the current state of this Mazub.
	 */
	private int[] getApplicableIndices(){
		if (!isRunning()){
			if (!hasMovedLastSecond()){
				if (!isDucking())
					return new int[] {0};
				else
					return new int[] {1};
			}
			else
				if (isDucking()){
					if (getHorizontalDirection().getUnitDirection() == 1)
						return new int[] {6};
					else
						return new int [] {7};
				} else {					
					if (getHorizontalDirection().getUnitDirection() == 1)
						return new int[] {2};
					else
						return new int [] {3};
				}
		}
		else {
			if (isDucking()){
				if (getHorizontalDirection().getUnitDirection() == 1)
					return new int[] {6};
				else
					return new int [] {7};
			} else {
				if (shouldFall()){
					if (getHorizontalDirection().getUnitDirection() == 1)
						return new int[] {4};
					else
						return new int [] {5};
					
				} else {
					if (getHorizontalDirection().getUnitDirection() == 1)
						return createRunningArray(8);
					else
						return createRunningArray(9+computeM());
				}
			}
		}
	}
	
	/**
	 * Return the number of alternating images while running and not ducking.
	 * 
	 * @return The number of sprites decremented with 10 and further divided by 2.
	 * 			result == (getNbSprites() - 10) / 2
	 */
	@Immutable @Model
	private int computeM(){
		return (getNbSprites()-10)/2;
	}
	
	
	/**
	 * Create an array with all the indices of the alternating images for running in the direction 
	 * corresponding with the given startindex (in the correct order).
	 * 
	 * @param 	start
	 * 			The first index of the alternating images for running.
	 * @pre		This Mazub can have the given start as its index.
	 * 			| canHaveAsIndex(start)
	 * @return	The array with all the indices of the alternating images for running in the direction
	 * 			corresponding with the given startindex.
	 * 			|for(int index = 0; index < 1+m; index++)
	 * 			|	runningArray[index] = start+index
	 * 			|result == runningArray
	 */
	private int[] createRunningArray(int start){
		assert canHaveAsIndex(start);
		int m = computeM();
		int[] runningArray = new int[1+m];
		// @invar	The running array contains all the indices for running which are smaller than the current index.
		//			| for each index in start..index
		//			|	Arrays.asList(runningArray).contains(index)
		// @invar	"index" doesn't exceed (1 + computeM())
		// 			| index < 1 + computeM()
		// @variant	The number of unhandled indices.
		//			| computeM() - index + 1
		for(int index = 0; index < 1+m; index++){
			runningArray[index] = start+index;
		}
		return runningArray;
	}


	
		// ALTERNATION TIMER
	
	/**
	 * Return the current time (in s) of the alternation timer of this Mazub.
	 */
	@Basic
	public double getAlternationTimer(){
		return this.alternationTimer;
	}
	
	/**
	 * Return the timestamp (in s) of the last horizontal movement of this Mazub.
	 */
	@Basic
	public double getLastMovement(){
		return this.movement;
	}
	
	/**
	 * Check whether the given movement is a valid movement for all Mazubs.
	 * 
	 * @param 	movement
	 * 			The movement to be checked.
	 * @return	True if and only if the movement is positive or "not a number".
	 * 			| result == ( (movement >= 0) || Double.isNaN(movement) )
	 * @note	If movement is not smaller than zero, movement can be "not a number".
	 * 			Consequently you only need to check whether movement is not smaller than zero.
	 * @note	Movement can be "not a number" because before Mazub has run for the first time,
	 * 			it's impossible to know when he has been running for the last time.
	 * 			
	 */
	public static boolean isValidMovement(double movement){
		// result == ( (movement >= 0) || Double.isNaN(movement) )
		return ( !(movement < 0) );
	}
	
	/**
	 * Check whether this Mazub has moved horizontally in the last second.
	 * 
	 * @return  True if and only if the current value for the time isn't greater than the timestamp
	 * 			of movement incremented by one second.
	 * 			| result == (getTime() <= getLastMovement +1)
	 */
	public boolean hasMovedLastSecond(){
		return ((getTime() <= getLastMovement() + 1));
	}
	
	
	/**
	 * Set the alternation timer of this Mazub to the given time.
	 * 
	 * @param 	newTime
	 * 			The new time of the alternation timer.
	 * @post	The new value of the alterationTimer 
	 * 			is equal to the given time.
	 * 			| new.getAlternationTimer() = newTime
	 */
	@Model
	private void setAlternationTimer(double newTime){
		this.alternationTimer = newTime;
	}
	
	/**
	 * Update the timestamp for the last horizontal movement, according to this Mazub's current state.
	 * 
	 * @effect 	If this Mazub is currently running, set the timestamp of the last horizontal movement to the current time.
	 * 			| if (isRunning())
	 * 			|  then setLastMovement(getTime())
	 */
	private void registerMovement(){
		if (isRunning()){
			setLastMovement(getTime());
		}
	}
	
	/**
	 * Set the value of the last movement of this Mazub to the given movement.
	 * 
	 * @param 	movement
	 * 			The movement time to be set.
	 * @pre		The given momement time is a valid movement time for any Mazub.
	 * 			| isValidMovement(movement)
	 * @post	The new movement of this Mazub is equal to the given movement.
	 * 			| new.getLastMovement == movement
	 * 
	 */
	@Model
	private void setLastMovement(double movement){
		assert isValidMovement(movement);
		this.movement = movement;
	}
	
	/**
	 * Variable registering the current time (in s) of the alternation timer of this Mazub.
	 */
	private double alternationTimer = 0;
	
	/**
	 * Symbolic constant registering the time per image while alternating (in s) several sprites of this Mazub.
	 */
	private static final double TIME_EACH_IMAGE = 0.075;
	
	
	/**
	 * Variable to keep track of the time (in s) of the last horizontal movement (timestamp). 
	 */
	private double movement = Double.NaN;
	
	// ASSOCIATION WITH WORLD
	
	/**
	 * Add this Mazub to the given world.
	 * 
	 * @param	world 
	 * 			| the world to which this game object will be added.
	 * @effect	Set the Mazub of the given world to this Mazub.
	 * 			| world.setMazub(this)
	 */
	@Raw
	@Override
	void addAsGameObject(World world){
		// applicablity beperkt in setMazub zelf
		world.setMazub(this);
	}
	
	/**
	 * 
	 * Remove this game object from the given world.
	 * 
	 * @param	world
	 * 			| The world from which this game object will be removed
	 * 
	 * @effect 	Set the Mazub from the given world to null
	 * 			| world.setMazub(null)
	 * 			
	 */
	@Override
	void removeAsGameObject(@Raw World world){
		// applicability beperkt in setMazub zelf
		world.setMazub(null);
	}
	
	
	
	

	// ASSOCIATION WITH WINDOW

	/**
	 * Return the threshold for proximity for this Mazub and the used window.
	 * 
	 * @return 	| getWorld().getNbUpdateGameWindowPixels();
	 */
	public int getWindowThreshold(){
		return getWorld().getNbUpdateGameWindowPixels();
	}
	
	/**
	 * Check whether this Mazub's outer left pixel is close to the given horizontal boundary.
	 * 
	 * @param	xPixel
	 * 			The horizontal boundary to check.
	 * @return 	True if and only if this Mazub's left pixel, diminished with the given boundary, is not greater
	 * 			 than the window threshold.
	 * 				| return (getXPixel()-xPixel <= getWindowThreshold())
	 */
	public boolean isLeftCloseTo(int xPixel){
		return (getXPixel()-xPixel <= getWindowThreshold());
	}
	
	
	/**
	 * Check whether this Mazub's outer right pixel is close to the given horizontal boundary.
	 * 
	 * @param	xPixel
	 * 			The horizontal boundary to check.
	 * @return 	True if and only if the given boundary, diminished with this Mazub's right pixel, is not greater
	 * 			 than the window threshold.
	 * 				| return (xPixel - getRightPixel()<= getWindowThreshold())
	 */
	public boolean isRightCloseTo(int xPixel){
		return (xPixel - getRightPixel()<= getWindowThreshold());
	}
	
	/**
	 * Check whether this Mazub's bottom pixel is close to the given vertical boundary.
	 * 
	 * @param	yPixel
	 * 			The vertical boundary to check.
	 * @return 	True if and only if this Mazub's bottom pixel, diminished with the given boundary, is not greater
	 * 			 than the window threshold.
	 * 				| return (getYPixel()-yPixel <= getWindowThreshold())
	 */
	public boolean isBottomCloseTo(int yPixel){
		return (getYPixel()-yPixel <= getWindowThreshold());
	}
	
	/**
	 * Check whether this Mazub's top pixel is close to the given vertical boundary.
	 * 
	 * @param	yPixel
	 * 			The vertical boundary to check.
	 * @return 	True if and only if the given boundary, diminished with this Mazub's bottom pixel, is not greater
	 * 			 than the window threshold.
	 * 				| return (getYPixel()-yPixel <= getWindowThreshold())
	 */
	public boolean isTopCloseTo(int yPixel){
		return (yPixel-getTopPixel() <= getWindowThreshold());
	}
	

	// COLLISION
	
	/**
	 * Check whether this Mazub and the supplied game object block each other's movement.
	 * 
	 * @param other
	 * 			The other game object to check for.
	 * 
	 * @return If the other object is equal to the null reference, or is a plant, no movement shall be blocked.
	 * 				If the other object is a Slime or a Shark, or another Mazub, movement shall be blocked.
	 * 					Otherwise, return whether the other object interferes movement with this Mazub.
	 * 			| if (other == null || other instanceof Plant)
	 * 			| 	then return false
	 * 			| else if (other instanceof Slime || other instanceof Shark || other instanceof Mazub)
	 * 			|  then return true
	 * 			| else return other.interferesMovementWith(this)
	 */
	@Override
	public boolean interferesMovementWith(GameObject other) {
		if (other == null)
			return false;
		if (other instanceof Plant)
			return false;
		if (other instanceof Slime || other instanceof Shark || other instanceof Mazub)
			return true;
		return other.interferesMovementWith(this);
	}

	
	// WEAKNESS
	
	/**
	 * Get this Mazub's geological weaknesses and corresponding hitpoint losses
	 * 		(always a map containing (GeologicalFeature.WATER=> -2, GeologicalFeature.MAGMA=> -50))	 * 
	 */
	@Basic @Override
	public Map<GeologicalFeature,Integer> getWeaknessesAndHitpointLosses() {
		Map <GeologicalFeature, Integer> weaknessesAndLosses =  new HashMap<GeologicalFeature, Integer>();
		weaknessesAndLosses.put(GeologicalFeature.WATER, -2);
		weaknessesAndLosses.put(GeologicalFeature.MAGMA, -50);
		return weaknessesAndLosses;
	};

	
	// TIME
	
	

	/**
	 * Advance the time for this Mazub with the given amount.
	 * 
	 * @note	No documentation is to be supplied for this method.
	 */
	@Override
	public void advanceTime(double dt) throws IllegalArgumentException, IllegalStateException{
		if (!isValidTimeInterval(dt))
			throw new IllegalArgumentException("Illegal time interval: " + dt);
		if (!fuzzyLessThanOrEqualTo(getTime() + dt, getWorld().getTime()))
			throw new IllegalArgumentException("The time of the world must always be higher than the time of this Mazub");
		if (isTerminated())
			throw new IllegalStateException("Cannot advance the time for a terminated object");
		if (isAlive())
			updateSpriteIndicesAndAlternationTimer(dt);
		super.advanceTime(dt);
		if (isAlive()){
			updateDuckingState();
			registerMovement();
		}

	}

	// TERMINATION
	
	/**
	 * Return the termination threshold time for this Mazub (always 0).
	 */
	@Basic @Override
	public double getTerminationThreshold() {
		return 0;
	}
}
