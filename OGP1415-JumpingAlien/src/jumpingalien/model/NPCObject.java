package jumpingalien.model;

import static jumpingalien.model.extension.RandomExtension.nextDouble;
import static jumpingalien.util.Util.*;
import be.kuleuven.cs.som.annotate.*;
import jumpingalien.model.program.Program;
import jumpingalien.util.Sprite;



/**
 *	A class of NPC-objects.
 *
 * @invar	| canHaveAsRemainingMovementDuration(getRemainingMovementDuration)
 * @invar	| hasProperProgram()
 * 
 * @version 3.0
 * @author  Antoon Purnal and Tuur Van Daele
 * 
 * 
 * 
 */

public abstract class NPCObject extends GameObject {
	
	/**
	 * Initialize a new NPC object.
	 * 
	 * @param xPixel
	 * @param yPixel
	 * @param hitPoints
	 * @param images
	 * 
	 * @effect 	| super(xPixel, yPixel, hitPoints, images)
	 * @post	| new.getRemainingMovementDuration() >= getMinMovementDuration() && new.getRemainingMovementDuration() <= getMaxMovementDuration())
	 * @effect	| if (program != null)
	 * 			| 	then setProgram(program)
	 * @effect	| if (!hasProgram() && canMoveRandomly())
	 * 			|	then startMove(getAppropriateDirection()))
	 */
	protected NPCObject(int xPixel, int yPixel, int hitPoints, Sprite[] images, Program program){
		super(xPixel, yPixel, hitPoints, images);
		setMovementDuration(nextDouble(getMinMovementDuration(), getMaxMovementDuration()));
		if (program != null)
			setProgramAssociation(program); // set bidirectional association (both arrows)
		if (!hasProgram() && canMoveRandomly())
			startMove(getAppropriateDirection());
	}

	
	// ASSOCIATION WITH PROGRAM
	
	/**
	 * Check whether this non-playable character has a program.
	 * 
	 * @return	result == (getProgram() != null)
	 */
	public boolean hasProgram(){
		return (getProgram() != null);
	}
	
	/**
	 * Check whether this non-playable character has the given program as its associated program.
	 * 
	 * @param program
	 * @return | result == (getProgram() == program)
	 */
	// @Raw because in setExecutingNpc in Program
	@Raw
	public boolean hasAsProgram(Program program){
		return (getProgram() == program);
	}
	
	/**
	 * Check whether this non-playable character has the given program as its associated program.
	 * 
	 * @param program
	 * @return	result == (program == null || (!isTerminated() && program.isWellFormed()))
	 */
	public boolean canHaveAsProgram(Program program){
		return (program == null || program.isWellFormed());

	}
	
	/**
	 * Check whether this non-playable character has a proper program as its associated program.
	 * 
	 * @return	result == canHaveAsProgram(getProgram()) &&
								(!hasProgram() || getProgram().getExecutingGameObject() == this)
	 */
	public boolean hasProperProgram(){
		return canHaveAsProgram(getProgram()) &&
				(!hasProgram() || getProgram().getExecutingNpc() == this);
	}
	
	/**
	 * Return the program of this game object.
	 */
	//@Raw because hasAsProgram() is @Raw
	@Basic @Model @Raw
	private Program getProgram(){
		return this.program;
	}
	
	/**
	 * Set the program of this non-playable character to the given program.
	 * 
	 * @param program
	 * @pre		| canHaveAsProgram(program)
	 * @pre		| (program != null || isTerminated())
	 * @pre		| !hasProgram() || program == null
	 * @post	| new.getProgram() == program
	 * @effect	| if (program != null)
	 * 			|	then program.setExecutingGameObject(this)
	 */
	// Set the bidirectional association in one time
	@Model
	private void setProgramAssociation(Program program){
		assert canHaveAsProgram(program);
		assert (program != null || isTerminated()); // Only set program to null if this npc is terminated. (a npc who has a program, must have it upon termination)
		assert !hasProgram() || program == null; // Can only set an effective program once
		this.program = program;
		if (program != null)
			program.setExecutingNpc(this);
	}
	
	/**
	 * Variable registering the executing program of this non-playable character.
	 */
	private Program program;
	
	
	// CAN MOVE RANDOMLY
	
	/**
	 * Check whether this non-playable character moves randomly.
	 * 
	 */
	public boolean canMoveRandomly(){
		return true;
	}
	
	// SPRITES 
	
	/**
	 * Return the minimal number of sprites for this NPC object (at least two).
	 */
	@Basic @Override
	public int getMinNbSprites(){
		return 2;
	}
	
	/**
	 * Get the new appropriate sprite index for this NPC object.
	 * 
	 */
	@Override
	public int getNewSpriteIndex(){	
		if (getHorizontalDirection() == HorizontalDirection.RIGHT)
			return 1;
		else
			return 0;
	}

	
	// MOVEMENT
	
	/**
	 * Return the appropriate direction of movement for this NPC object.
	 */
	public abstract HorizontalDirection getAppropriateDirection();
	
	
	/**
	 * Return the remaining movement duration for this NPC object.
	 */
	@Basic
	public double getRemainingMovementDuration(){
		return this.remainingMovementDuration;
	}
	
	/**
	 * Get the minimal, respectively the maximal movement duration for this NPC object.
	 * 
	 * @return result >= 0
	 */
	public abstract double getMinMovementDuration();
	public abstract double getMaxMovementDuration();
	
	
	/**
	 * Check whether this NPC-object can have the given moment duration as the remaining
	 * duration for the ongoing movement.
	 * 
	 * @param movementDuration
	 * 
	 * @return	| result == (movementDuration >= getMinMovementDuration() &&
				| movementDuration <= getMaxMovementDuration())
	 */
	public boolean canHaveAsRemainingMovementDuration(double movementDuration){
		return (movementDuration >= 0 &&
				movementDuration <= getMaxMovementDuration());
	}
	
	/**
	 * Set the movement duration timer to the given movement duration.
	 * 
	 * @param movementDuration
	 * 
	 * @pre	 | (canHaveAsRemainingMovementDuration(movementDuration))
	 * 
	 * @post | if (movementDuration < 0 || Double.isNaN(movementDuration))
	 * 		 |   then new.getRemainingMovementDuration() == 0
	 * @post | if (movementDuration >= 0 && !Double.isNaN(movementDuration))
	 * 		 |  then new.getRemainingMovementDuration() == movementDuration
	 */
	private void setMovementDuration(double movementDuration){
		if (movementDuration < 0)
			movementDuration = 0;
		assert (canHaveAsRemainingMovementDuration(movementDuration));
		this.remainingMovementDuration = Math.max(movementDuration,0);
	}
	
	/**
	 * Update this NPC object's movement.
	 * 
	 * @param dt
	 * @pre		| (getMinMovementDuration() >= 0 &&  getMaxMovementDuration() >= 0 && 
				| getMaxMovementDuration() >= getMinMovementDuration())
	 * @effect | setMovementDuration(getRemainingMovementDuration() - dt)
	 * @effect | if (fuzzyEquals(getRemainingMovementDuration(),0))
	 * 			| then endMove(getOrientation()) && setMovementDuration(nextDouble(getMinMovementDuration(), getMaxMovementDuration())) &&
				| startMove(getAppropriateDirection())
	 */
	private void updateMovement (double dt){
		assert isValidTimeInterval(dt);
		assert (getMinMovementDuration() >= 0 &&  getMaxMovementDuration() >= 0 && 
				getMaxMovementDuration() >= getMinMovementDuration());
		setMovementDuration(getRemainingMovementDuration() - dt);
		if (fuzzyEquals(getRemainingMovementDuration(),0)){
			endMove(getHorizontalDirection());
			startMove(getAppropriateDirection());
			setMovementDuration(nextDouble(getMinMovementDuration(), getMaxMovementDuration()));
		}
	}
	
	
	/**
	 * Variable containing the remainder of the movement duration for this NPC object.
	 */
	private double remainingMovementDuration = 0;
	

	
	// TIME
	
	/**
	 * Advance the time for this NPC object.
	 * 
	 * @note	No documentation is required for this method.
	 */
	@Override
	void advanceTime(double dt) throws IllegalArgumentException,
			IllegalStateException {
		super.advanceTime(dt);
		if (isAlive()){
			if (hasProgram())
				getProgram().execute(dt);
			else if(canMoveRandomly())
				updateMovement(dt);
		}
				
	}
	
	// ASSOCIATION WITH WORLD
	/**
	 * Check whether this NPC object can have the given world as its world.
	 * 
	 * @param 	world
	 * 			| The world to check
	 * @return	True if and only if the given world is not effective or
	 * 			the given world can have this npc object as one of its npc objects.
	 * 			| result == (	(world == null) || world.canHaveAsNpc(this)	)
	 * 
	 */
	public boolean canHaveAsWorld(World world){
		return (world == null) || world.canHaveAsNpc(this);
	}
	
	// TERMINATION
	/**
	 * Terminate this non-playable character.
	 * 
	 * @effect	| super.terminate()
	 * @effect	| if (getProgram() != null)
	 * 			|	then setProgram(null) && getProgram().setExecutingGameObject(null)		
	 */
	@Override
	public void terminate(){
		super.terminate();
		Program oldProgram = getProgram();
		if (oldProgram != null){
			setProgramAssociation(null);
			oldProgram.setExecutingNpc(null);
		}
	}
	
}
