package jumpingalien.model;

import be.kuleuven.cs.som.annotate.*;

@Value
public class InfoGameObject extends CollisionObject {

	// CONSTRUCTORS
	
	public InfoGameObject(int xPixel, int yPixel, int rightPixel, int topPixel) {
		this.xPixel = xPixel;
		this.yPixel = yPixel;
		this.rightPixel = rightPixel;
		this.topPixel = topPixel;
	}
	
	public InfoGameObject(GameObject gameObject){
		this(gameObject.getXPixel(), gameObject.getYPixel(), gameObject.getRightPixel(), gameObject.getTopPixel());
	}
	
	// CREATING COMMON USED GAME OBJECTS
	
	public static InfoGameObject infoGameObjectWithoutPerimeterOf(GameObject gameObject){
		assert gameObject != null;
		return new InfoGameObject(gameObject.getXPixel()+ 1, gameObject.getYPixel()+ 1,
				gameObject.getRightPixel()-1, gameObject.getTopPixel()-1);
	}

	// POSITION
	@Basic @Immutable @Override
	public int getXPixel() {
		return this.xPixel;
	}

	@Basic @Immutable @Override
	public int getYPixel() {
		return this.yPixel;
	}
	
	@Basic @Immutable @Override
	public int getRightPixel() {
		return this.rightPixel;
	}

	@Basic @Immutable @Override
	public int getTopPixel() {
		return this.topPixel;
	}
	
	private final int xPixel, yPixel,rightPixel, topPixel;
	
	
	// TIME
	
	@Override
	public double getTime() {
		return 0;
	}

	
	// OVERRIDE EQUALS AND HASHCODE
	
	@Override
	public boolean equals(Object other) {
		if (other instanceof InfoGameObject){
			return (getXPixel() == ((InfoGameObject) other).getXPixel() &&
					getYPixel() == ((InfoGameObject) other).getYPixel() &&
					getRightPixel() == ((InfoGameObject) other).getRightPixel() &&
					getTopPixel() == ((InfoGameObject) other).getTopPixel());
		} 
		return false;
	};
	
	@Override
	public int hashCode() {
		return Integer.hashCode(getXPixel() * getYPixel() + getRightPixel()* getTopPixel()+ getTopPixel());
	};
	
}
