package jumpingalien.model;

import java.util.HashMap;

import static jumpingalien.util.Util.*;

import java.util.Map;

import jumpingalien.model.program.Program;
import jumpingalien.util.Sprite;
import be.kuleuven.cs.som.annotate.*;
import static jumpingalien.model.extension.RandomExtension.*;

/**
 * A class of slimes.
 * 
 * @invar	| hasProperSchool()
 * 
 * @version 3.0
 * @author  Antoon Purnal and Tuur Van Daele
 *
 */
public class Slime extends NPCObject{

	/**
	 * Initialize a new slime.
	 * 
	 * @param xPixel
	 * @param yPixel
	 * @param images
	 * @param school
	 * 
	 * @effect 	| super(xPixel, yPixel,getInitialHitPoints(), images)
	 * @effect	| school.addSlime(this)
	 */
	public Slime(int xPixel, int yPixel, Sprite[] images, School school, Program program) {
		super(xPixel, yPixel,getInitialHitPoints(), images, program);
		school.addSlime(this);
	}
	
	// MOVEMENT LIMITATIONS
	
	/**
	 * Check whether this Slime can move horizontally (always true).
	 */
	@Basic @Override @Immutable
	public boolean canMoveHorizontally(){
		return true;
	}
	
	/**
	 * Check whether this Slime can move vertically (always true).
	 */
	@Basic @Override @Immutable
	public boolean canMoveVertically(){
		return true;
	}
	
	// SIZE
	// POSITIONS AND PIXELS
		//X

		//Y

	// HORIZONTAL MOVEMENT
			// RANDOM MOVEMENT DURATION
	
	
	/**
	 * Return the minimal movement duration for this Slime (always 2).
	 */
	@Basic @Override @Immutable
	public double getMinMovementDuration(){
		return 2;
	}
	
	/**
	 * Return the maximal movement duration for this Slime (always 6).
	 */
	@Basic @Override @Immutable
	public double getMaxMovementDuration(){
		return 6;
	}
	
	/**
	 * Return the appropriate direction of movement for this NPC object.
	 * 
	 * @return | if (fuzzyEquals(getRemainingMovementDuration(),0))
	 * 			| then result == pickRandom(HorizontalDirection.RIGHT, HorizontalDirection.LEFT)
	 * @return  | if (!fuzzyEquals(getRemainingMovementDuration(),0))
	 * 			| then result == getOrientation()
	 */
	@Override
	public HorizontalDirection getAppropriateDirection() {
		if (fuzzyEquals(getRemainingMovementDuration(),0))
			return pickRandom(HorizontalDirection.RIGHT, HorizontalDirection.LEFT);
		else return getHorizontalDirection();
	}
	


			// ORIENTATION

		
		// VELOCITY
	
	/**
	 * Return the initial horizontal velocity for this Slime. (always 1)
	 */
	@Basic @Override @Immutable
	public double getInitialXVelocity() {
		// Initial velocity not specified, equal to Mazub by default
		return 1;
	}
	
	/**
	 * Return the maximal horizontal velocity for this Slime. (always 2.5)
	 */
	@Basic @Override @Immutable
	public double getMaxXVelocity() {
		return 2.5;
	}
	

		// ACCELERATION
	
	
	/**
	 * Return the current acceleration (in m/s^2) of this Slime.
	 * 
	 * @return If this Slime currently has a constant horizontal velocity, return 0. Otherwise, return 0.7.
	 * 			| if (hasConstantXVelocity())
	 *			|	then return 0
	 *			| else return 0.7
	 */
	@Override
	public double getXAcceleration() {
		if (hasConstantXVelocity())
			return 0;
		return 0.7;
	}
	
	// VERTICAL MOVEMENT
	
		// VELOCITY
	
	/**
	 * Return the initial vertical velocity for this Slime (always 0).
	 */
	@Basic @Override @Immutable
	public double getInitialYVelocity() {
		return 0;
	}
	
	// SPRITE
	
	/**
	 * Get the new appropriate sprite index for this sprite.
	 * 
	 * @return	| result == ((getOrientation() == HorizontalDirection.RIGHT) ? 1 : 0);
	 */
	@Override
	public int getNewSpriteIndex(){	
		return super.getNewSpriteIndex();
	}
	
	
	// HITPOINTS
	
	/**
	 * Return the initial amount of hitpoints for this slime (always 100).
	 */
	@Basic @Immutable
	public static int getInitialHitPoints(){
		return 100;
	}
	
	/**
	 * Make this Slime sacrifice a single hitpoint.
	 * 
	 * @effect | super.updateHitPoints(-1)
	 */
	void sacrificeHitPoint(){
		super.updateHitPoints(-1); 
	}
	
	/**
	 * Make this Slime receive a single hitpoint.
	 * 
	 * @effect | super.updateHitPoints(+1)
	 */
	void receiveHitPoint(){
		super.updateHitPoints(+1);
	}
	
	/**
	 *  Update the hitpoints for this game object with the supplied hitpoints difference.
	 *  
	 *  @effect	| if (hitPointsDifference < 0)
				| 	then getSchool().sacrificeHitPoint(this)
	 */
	@Override @Model
	void updateHitPoints(int hitPointsDifference){
		super.updateHitPoints(hitPointsDifference);
		if (hitPointsDifference < 0)
			getSchool().sacrificeHitPoint(this);
	}
	
	 
	/**
	 * Update the hitpoints of this and the given other game object caused by the collision.
	 * 
	 * @param other
	 * 
	 * @effect	| if (other != null && hasAsCollidingObject(other) && !this.isTerminated() && !other.isTerminated() && !isImmune() && !other.isImmune() && ! other instanceof Plant)
	 * 			| 		else if (other instanceof Slime)
	 * 			|				then if (this.isAlive() && other.isAlive())
	 * 			|						switchSchool((Slime)other)
	 * 			| 		else if (other instanceof Shark)
	 *			|				then other.updateHitPoints(-50) && other.resetImmunityTimer() && this.updateHitPoints(-50) && this.resetImmunityTimer()
	 *			|		else if (other instanceof Mazub)
	 *			|				then if (!other.isOnTopOf(other))
	 * 			|					then this.updateHitPoints(-50) && this.resetImmunityTimer() && other.updateHitPoints(-50) && other.resetImmunityTimer()
	 * 			|				else this.updateHitPoints(-50) && this.resetImmunityTimer()
	 * 			| 		else this.updateHitPointsUponCollisionWith(other)
	 * 
	 */
	@Override @Model
	void updateHitPointsUponCollisionWith(GameObject other){
		if (other == null || !hasAsCollidingObject(other) || this.isTerminated() || other.isTerminated());
		else if(other instanceof Plant || isImmune() || other.isImmune());
		else if(other instanceof Shark){
			this.updateHitPoints(-50);
			this.resetImmunityTimer();
			other.updateHitPoints(-50);
			other.resetImmunityTimer();
		}
		
		else if(other instanceof Slime){
			if (this.isAlive() && other.isAlive())
				switchSchool((Slime) other);
		}
		else if(other instanceof Mazub){
			this.updateHitPoints(-50);
			this.resetImmunityTimer();
			if (!other.isOnTopOf(this)){
				other.updateHitPoints(-50);
				other.resetImmunityTimer();
			}
		}
		else
			other.updateHitPointsUponCollisionWith(this);
	}


	
	
	
	// ASSOCIATION WITH WORLD
	
	/**
	 * Add this game object to the world.
	 * 
	 * @param world
	 * 
	 * @effect 	| world.addAsSlime(this)
	 *
	 */
	@Raw @Override
	void addAsGameObject(World world){
		world.addAsSlime(this);
	}
	
	/**
	 * Remove this game object from the world.
	 * 
	 * @param world
	 * 
	 * @effect 	| world.removeAsSlime(this)
	 */
	@Override
	void removeAsGameObject(@Raw World world){
		world.removeAsSlime(this);
	}
	
	

	// COLLISION
	
	/**
	 * Check whether this Slime and the supplied game object block each other's movement.
	 * 
	 * @param other
	 * 			The other game object to check for.
	 * 
	 * @return If the other object is equal to the null reference, or is a plant, no movement shall be blocked.
	 * 				If the other object is a Slime or a Shark or a  Mazub, movement shall be blocked.
	 * 					Otherwise, return whether the other object interferes movement with this Slime.
	 * 			| if (other == null || other instanceof Plant)
	 * 			| 	then return false
	 * 			| else if (other instanceof Slime || other instanceof Shark || other instanceof Mazub)
	 * 			|  then return true
	 * 			| else return other.interferesMovementWith(this)
	 */
	@Override
	public boolean interferesMovementWith(GameObject other) {
		if (other == null)
			return false;
		if (other instanceof Plant)
			return false;
		if (other instanceof Slime || other instanceof Shark || other instanceof Mazub)
			return true;
		return other.interferesMovementWith(this);
	}
	


	// TIME
	
	/**
	 * Advance the time for this Slime with the given amount.
	 * 
	 * @note	No documentation is required for this method.
	 */
	@Override
	void advanceTime(double dt) throws IllegalArgumentException, IllegalStateException{
		super.advanceTime(dt);
	}

		
		
	
	
	// SCHOOL 
	
	/**
	 * Get the current School to which this Slime belongs.
	 */
	@Basic
	public School getSchool(){
		return this.school;
	}
	
	/**
	 * Check whether this slime can have the supplied school as its school.
	 * 
	 * @param school
	 * 
	 * @return result == (isTerminated) ? school == null : (school != null) && (school.canHaveAsSlime(this))
	 */
	public boolean canHaveAsSchool(School school){
		if (isTerminated()) // mag school in termination process hebben
			return school == null;
		return (school != null) && (school.canHaveAsSlime(this));
	}
	 
	/**
	 * Check whether this slime currently has a proper School.
	 * 
	 * @return | result == canHaveAsSchool(getSchool()) &&
				| (getSchool() == null || getSchool().hasAsSlime(this))
	 */
	public boolean hasProperSchool(){
		return canHaveAsSchool(getSchool()) &&
				(getSchool() == null || getSchool().hasAsSlime(this));
	}
	
	/**
	 * Set the School of this Slime to the given School.
	 * 
	 * @param school
	 * 
	 * @pre	(isTerminated() && school == null) || school.hasAsSlime(this))
	 * 
	 * @post new.getSchool = school
	 */
	@Raw @Model
	void setSchool(@Raw School school){
		assert ((isTerminated() && school == null) || school.hasAsSlime(this));
		this.school = school;
	}
	
	/**
	 * Variable referring to this Slime's current school.
	 */
	private School school;
	
	
	
	// SWITCH SCHOOL

	
	/**
	 * Switch school for this slime or the supplied one, if necessary.
	 * @param other
	 * 
	 * @pre (other != null && this.isAlive() && other.isAlive())
	 * 
	 * @effect | if (getSwitchingSlime(this,other) == this)
	 * 			| then transferTo(other.getSchool())
	 * @effect | if (getSwitchingSlime(this,other) == other)
	 * 			| then other.transferTo(getSchool())
	 */
	@Model
	private void switchSchool(Slime other){
		assert (other != null);
		assert this.isAlive() && other.isAlive(); // mag niet in termination process
		Slime switchingSlime = getSwitchingSlime(this,other);
		if(switchingSlime == this)
			this.transferTo(other.getSchool());
		if(switchingSlime == other)
			other.transferTo(this.getSchool());
	}
	

	/**
	 * Return the slime that should switch schools.
	 * 
	 * @param a
	 * @param b
	 * 
	 * @return |if (a.getSchool().getNbSlimes() > b.getSchool().getNbSlimes())
	 * 			| 	then result == b
	 * 			| else if (a.getSchool().getNbSlimes() < b.getSchool().getNbSlimes())
	 * 			| 	then result == a
	 * 			| else result == null
	 */
	public static Slime getSwitchingSlime(Slime a, Slime b){
		assert (a != null) && (b != null);
		assert a.isAlive() && b.isAlive();
		int membersA = a.getSchool().getNbSlimes();
		int  membersB = b.getSchool().getNbSlimes();
		if (membersA > membersB)
			return b;
		if (membersA < membersB)
			return a;
		return null;	
	}
	
	
	/**
	 * Transfer this Slime to the given school.
	 * 
	 * @param newSchool
	 * 
	 * @effect	| School oldSchool = getSchool();
	 *			| newSchool.sacrificeHitPoint(this);
	 *			| newSchool.addSlime(this);
	 *			| oldSchool.receiveHitPoint();
	 *			| int extraHitPoints = newSchool.getNbSlimes() - oldSchool.getNbSlimes() - 1; // -1: this slime is in new school
	 *			| updateHitPoints(extraHitPoints)
	 */
	@Model
	private void transferTo(School newSchool){
		assert newSchool != null;
		// Schools
		School oldSchool = getSchool();
		newSchool.sacrificeHitPoint(this);
		newSchool.addSlime(this);
		oldSchool.receiveHitPoint();
		// Slime
		int extraHitPoints = newSchool.getNbSlimes() - oldSchool.getNbSlimes() - 1; // -1: this slime is in new school
		updateHitPoints(extraHitPoints); // kan extra getter onafh van switchSchool maar nu zorgt er voor dat alles tegelijk gebeurd
	}
	



	
	// WEAKNESS
	
	/**
	 * Get this Slime geological weaknesses and corresponding hitpoint losses
	 * 		(always a map containing (GeologicalFeature.WATER=> -2, GeologicalFeature.MAGMA=> -50))	 * 
	 */
	@Basic @Override
	public Map<GeologicalFeature,Integer> getWeaknessesAndHitpointLosses() {
		Map <GeologicalFeature, Integer> weaknessesAndLosses =  new HashMap<GeologicalFeature, Integer>();
		weaknessesAndLosses.put(GeologicalFeature.WATER, -2);
		weaknessesAndLosses.put(GeologicalFeature.MAGMA, -50);
		return weaknessesAndLosses;
	};
		

	// TERMINATION
	
	
	/**
	 * Terminate this slime.
	 * 
	 * @effect	| super.terminate() 
	 * @effect	| setSchool(null)
	 * @effect	| getSchool().removeAsSlime(this)
	 */
	@Override
	public void terminate(){
		super.terminate();
		School oldSchool = getSchool();
		setSchool(null);
		oldSchool.removeAsSlime(this);
	}

}
