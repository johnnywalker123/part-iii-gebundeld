package jumpingalien.model;

import be.kuleuven.cs.som.annotate.*;

@Value
public class Position {
	
	// CONSTRUCTOR
	
	public Position(double xPosition, double yPosition){
		this.xPosition = xPosition;
		this.yPosition = yPosition;
	}
	
		// X
	@Basic @Immutable
	public double getX(){
		return this.xPosition;
	}	
	
	
	private final double xPosition;
	
	
		// Y
	
	@Basic @Immutable
	public double getY(){
		return this.yPosition;
	}
	
	private double yPosition;
	
	// EQUALS
	
	@Override
	public boolean equals(Object other){
		return (other instanceof Position) && 
				( this.getX() == ((Position)other).getX() ) &&
				( this.getY() == ((Position)other).getY() );
		
	}
	
	@Override
	public int hashCode(){
		return Double.hashCode(getX()*getY() + getX());
	}
	
	
		
}
