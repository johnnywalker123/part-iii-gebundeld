package jumpingalien.model;

import java.util.HashSet;
import java.util.Set;

import be.kuleuven.cs.som.annotate.*;

/**
 * A class with schools.
 * 
 * @invar	| hasProperSlimes()
 *
 * @version 3.0
 * @author  Antoon Purnal and Tuur Van Daele
 *
 */
public class School {
	
	// CONSTRUCTOR
	
	// default constructor
	
	// ASSOCIATION WITH SLIMES
	
	/**
	 * Return the number of slimes associated with this school.
	 */
	@Basic
	public int getNbSlimes(){
		return this.slimes.size();
	}
	
	/**
	 * Check whether this school has the given slime as one of the slimes attached to it.
	 * 
	 * @param	|slime
	 * */
	// @Raw because in precondition removeAsSlime in School and in precondition setSchool() in Slime
	@Basic @Raw
	public boolean hasAsSlime(Slime slime){
		return this.slimes.contains(slime);
	}
	
	/**
	 * Checks whether the given slime is a valid slime for all schools.
	 * 
	 * @param 	slime
	 * @return	result == (slime != null)
	 */
	
	public static boolean isValidSlime(Slime slime){
		return (slime!= null); 
		// slime can be terminated but in that case the slime will be deleted immediately (terminate() calls removeAsSlime())
	}
	
	/**
	 * Check whether this school can have the given slime as one of its slimes.
	 * 
	 * @param 	|slime
	 * @return	| result == (	isValidSlime(slime) && !this.isTerminated()	)
	 */
	public boolean canHaveAsSlime(Slime slime){
		return isValidSlime(slime) &&
				!this.isTerminated();
	}
	
	// The class invariant differs from the representation invariant
	// In the class invariant, each slime (in slimes) must reference back to this school.
	// In the representation invariant, the slime doesn't have to reference back if the slime is terminated.
	/**
	 * Check whether this school has proper slimes associated with it.
	 * 
	 * @return	| result ==
	 * 			| for each slime in slimes:
	 * 			|	canHaveAsSlime(slime) && (slime.getSchool() == this)
	 */
	public boolean hasProperSlimes(){
		for (Slime slime : slimes)
			if (!canHaveAsSlime(slime) || (slime.getSchool() != this))
				return false;
		return true;
	}
	
	/**
	 * Add the given slime as a slime for this school.
	 * 
	 * @param 	| slime
	 * 
	 * @pre		| canHaveAsSlime(slime)
	 * @post	| new.hasAsSlime(slime)
	 * @post	| (new slime).getSchool() == this
	 * @effect	| if (slime.getSchool() != null)
	 * 			|	!slime.getSchool().hasAsGameObject(this)
	 */
	public void addSlime(Slime slime){
		assert canHaveAsSlime(slime);
		School oldSchool = slime.getSchool();
		slimes.add(slime);
		slime.setSchool(this);
		if (oldSchool != null) // Bij constructie nog geen null
			oldSchool.removeAsSlime(slime);
	}
	
	/**
	 * Remove the given slime as a slime for this school.
	 * 
	 * @param 	|slime
	 * @pre		| hasAsSlime(slime)
	 * @pre		| slime.getSchool() != this
	 * @post	| !new.hasAsSlime(slime)
	 * @effect	| if (getNbSlimes() == 0)
	 * 			|	then terminate()
	 * 
	 */
	// Enkel als slime naar andere school of als slime geterminate.
	@Raw
	@Model
	void removeAsSlime(Slime slime){
		assert hasAsSlime(slime);
		assert(slime.getSchool() != this);
		slimes.remove(slime);
		if (getNbSlimes() == 0)
			terminate();
	}
	
	
	/**
	 * Set collecting references to the slimes of this School.
	 * 
	 * @invar	| slimes != null
	 * @invar	| for each slime in slimes:
	 * 			|	canHaveAsSlime(slime)
	 * @invar	| for each slime in slimes
	 * 			|	slime.getSchool() == this || slime.isTerminated()
	 */
	private Set<Slime> slimes = new HashSet<Slime>();

	
	
	// HITPOINTS
	
	/**
	 * Each slime of this school, receives one hitpoint.
	 * 
	 * @post	| result ==
	 * 			| for each slime in slimes:
	 * 			|	(new slime).getHitPoints() == slime.getHitPoints() + 1
	 */
	void receiveHitPoint(){
		for (Slime slime : slimes)
			slime.receiveHitPoint();
	}
	
	/**
	 * Each slime of this school, loses one hitpoint.
	 * 
	 * @post	| result ==
	 * 			| for each slime in slimes:
	 * 			|	(new slime).getHitPoints() == slime.getHitPoints() - 1
	 */
	
	void sacrificeHitPoint(Slime primarySlime){
		for (Slime slime : slimes){
			if (slime != primarySlime)
				slime.sacrificeHitPoint();
		}
	}
	
	
	
	// TERMINATION
	
	/**
	 * Terminate this school.
	 * 
	 * @pre		| getNbSlimes() == 0
	 * @post	| new.isTerminated() == true
	 * 
	 */
	public void terminate(){
		assert getNbSlimes() == 0;
		this.isTerminated = true;
	}
	
	/**
	 * Check whether this school is terminated.
	 */
	@Basic
	public boolean isTerminated(){
		return this.isTerminated;
	}
	
	/**
	 * Variable registering whether this school is terminated.
	 */
	private boolean isTerminated = false;
	
}
