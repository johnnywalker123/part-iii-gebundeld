package jumpingalien.model;

import java.util.Optional;
import java.util.stream.Stream;

import jumpingalien.model.type.DirectionType;
import jumpingalien.model.type.ObjectType;


public enum HorizontalDirection implements DirectionType{
	LEFT{
		@Override
		public int getUnitDirection(){
			return -1;
		}
		
		@Override
		public InfoGameObject perimeterOf(GameObject gameObject){
			assert gameObject != null;
			int horizontalPerimeter = gameObject.getXPixel();
			return new InfoGameObject(horizontalPerimeter, gameObject.getYPixel()+1, horizontalPerimeter, gameObject.getTopPixel()-1);		
		}
		
		@Override
		public boolean isTryingToMoveInInterferingDirection(InfoGameObject perimeter, GameObject collidingObject){
			return collidingObject.getXPixel() < perimeter.getXPixel();
		}
		
		@Override
		public Optional<ObjectType> searchNearestGameObjectInDirectionForObject(
				Stream<ObjectType> stream, GameObject gameObject) {
			return stream.filter(
					x->(x.getXPixel()<gameObject.getXPixel() && x.getTopPixel()>gameObject.getYPixel() && x.getYPixel() < gameObject.getTopPixel())).
					reduce((x,y)-> (x.getXPixel()>y.getXPixel() ? x: y));
		}
	},
	
	RIGHT{
		@Override
		public int getUnitDirection(){
			return 1;
		}
		
		@Override
		public InfoGameObject perimeterOf(GameObject gameObject){
			assert gameObject != null;
			int horizontalPerimeter = gameObject.getRightPixel();
			return new InfoGameObject(horizontalPerimeter, gameObject.getYPixel()+1, horizontalPerimeter, gameObject.getTopPixel()-1);	
		}
		
		@Override
		public boolean isTryingToMoveInInterferingDirection(InfoGameObject perimeter, GameObject collidingObject){
			return perimeter.getXPixel() <= collidingObject.getXPixel();
		}
		
		@Override
		public Optional<ObjectType> searchNearestGameObjectInDirectionForObject(
				Stream<ObjectType> stream, GameObject gameObject) {
			return stream.filter(
					x->(x.getXPixel()>gameObject.getXPixel() && x.getTopPixel()>gameObject.getYPixel() && x.getYPixel() < gameObject.getTopPixel())).
					reduce((x,y)-> (x.getXPixel()<y.getXPixel() ? x: y));
		}
	};
	
	public abstract int getUnitDirection();
}
