package jumpingalien.model;

public class Jumper {
	
	// CONSTRUCTOR
	
	public Jumper(Jumpable jumpingObject){
		assert canHaveAsJumpingObject(jumpingObject);
		this.jumpingObject = jumpingObject;
	}
	
	// ASSOCIATION
	
	public boolean canHaveAsJumpingObject(Jumpable jumpingObject){
		return jumpingObject instanceof GameObject && !jumpingObject.isTerminated(); // null uitgesloten door gameObject instanceof Duckable
	}
	
	public boolean hasProperJumpingObject(){
		return canHaveAsJumpingObject(getJumpingObject()) &&
				((Jumpable)getJumpingObject()).getJumper() == this;
	}
	
	private Jumpable jumpingObject;
	
	public Jumpable getJumpingObject(){
		return this.jumpingObject; // EVENTUEEL PACKAGE ACCESSIBLE
	}
	
	
	// JUMPING
	
	public void startJump(){
		if (!getJumpingObject().isAlive())
			throw new IllegalStateException("The jumping object is not alive and consequently cannot start jumping.");
		if (canJump())
			((GameObject)getJumpingObject()).setYVelocity(getJumpingObject().getInitialYVelocity());
	}
	
	public void endJump() throws IllegalStateException{
		if (!getJumpingObject().isAlive())
			throw new IllegalStateException("The jumping object is not alive and consequently cannot end the jump.");
		if (((GameObject)getJumpingObject()).getYVelocity() > 0)
			((GameObject)getJumpingObject()).setYVelocity(0);
	}
	
	public boolean canJump() {
		return (!getJumpingObject().shouldFall() && !((GameObject)getJumpingObject()).impassableEntityIn(VerticalDirection.UP));
	}
	
	// TERMINATION
	
	// Enkel als het jumpable object wordt geterminate, moet dit geterminate worden.
	// Aangezien dit object enkel in relatie is met het jumpable object, moet deze relatie niet expliciet geterminate worden. (geen andere referenties naar dit object)


}
