package jumpingalien.model;

public interface Duckable {
	
	void startDuck();
	
	void endDuck();
	
	
	boolean isDucking();
	
	int getHeightOfHighestSprite();
	
	int getXPixel();
	
	int getRightPixel();
	
	int getYPixel();
	
	int getTopPixel();
	
	boolean isTerminated();
	
	boolean isAlive();
	
	Ducker getDucker();
	
}
