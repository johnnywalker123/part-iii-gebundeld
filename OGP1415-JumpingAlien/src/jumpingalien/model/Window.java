package jumpingalien.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;

@Value
public class Window{
	
	// CONSTRUCTOR
	
	public Window(int width, int height, int leftPixel, int bottomPixel) {
		this.width = width;
		this.height = height;
		this.leftPixel = leftPixel;
		this.bottomPixel = bottomPixel;
	}
	
	// PROPERTIES

	@Basic @Immutable
	public int getWidth(){
		return this.width;
	}
	
	@Basic @Immutable
	public int getHeight(){
		return this.height;
	}
	
	@Basic @Immutable
	public int getXPixel(){
		return this.leftPixel;
	}
	
	@Basic @Immutable
	public int getYPixel(){
		return this.bottomPixel;
	}
	
	public int getRightPixel(){
		return getXPixel() + getWidth() - 1; 
	}
	
	public int getTopPixel(){
		return getYPixel() + getHeight() - 1;
	}
	
	private final int leftPixel, bottomPixel;
	private final int width, height;
	
	// EQUALS
	
	@Override
	public boolean equals(Object other){
		return (other instanceof Window) && 
				( this.getXPixel() == ((Window)other).getXPixel() ) &&
				( this.getYPixel() == ((Window)other).getYPixel() ) &&
				( this.getWidth() == ((Window)other).getWidth() ) &&
				( this.getHeight() == ((Window)other).getHeight() );
		
	}
	
	@Override
	public int hashCode(){
		return Integer.hashCode(getXPixel() * getYPixel() * getWidth() * getHeight() + getXPixel()*getWidth() + getHeight());
	}

	
}
