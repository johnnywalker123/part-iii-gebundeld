package jumpingalien.model;

public abstract class CollisionObject extends TimeObject {

	protected CollisionObject() {};
	
	
	// BASIC INFORMATION
	public abstract int getXPixel();
	public abstract int getYPixel();
	public abstract int getRightPixel();
	public abstract int getTopPixel();
	
	
	
	// COLLISIONS WITH OTHER OBJECTS
	public boolean collidesWith(CollisionObject other){
		assert other != null;
		if (this == other)
			return false;
		if((this.getRightPixel() < other.getXPixel()) 
				|| (other.getRightPixel() < this.getXPixel())
				|| (this.getTopPixel() < other.getYPixel())
				|| (other.getTopPixel() < this.getYPixel())){
			return false;
		}
		return true;
	}
	
}
