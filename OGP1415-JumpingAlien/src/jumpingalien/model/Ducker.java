package jumpingalien.model;
import be.kuleuven.cs.som.annotate.Basic;

public class Ducker {
	
	// CONSTRUCTOR
	
	public Ducker(Duckable duckingObject){
		assert canHaveAsDuckingObject(duckingObject);
		this.duckingObject = duckingObject;
	}
	
	// ASSOCIATION
	
	public boolean canHaveAsDuckingObject(Duckable duckingObject){
		return duckingObject instanceof GameObject && !duckingObject.isTerminated();
	}
	
	public boolean hasProperDuckingObject(){
		return canHaveAsDuckingObject(getDuckingObject()) &&
				getDuckingObject().getDucker() == this;
	}
	
	@Basic
	public Duckable getDuckingObject(){
		return this.duckingObject; // EVENTUEEL PACKAGE ACCESSIBLE!!!!!!!!!!!!
	}
	
	private Duckable duckingObject;
		
	// DUCKING 
	
		void startDuck(){
			if (!getDuckingObject().isAlive())
				throw new IllegalStateException("The ducking object is not alive and consequently cannot start ducking");
			setShouldEndDuck(false);
			this.ducking = true;
		}
		
		void endDuck(){
			if (!getDuckingObject().isAlive())
				throw new IllegalStateException("The ducking object is not alive and consequently cannot stop ducking");
			if (canEndDuck()){
				stopDucking();
			} else {
				setShouldEndDuck(true);
			}
		}
		
		@Basic
		public boolean isDucking(){
			return this.ducking;
		}
		
		@Basic
		public boolean shouldEndDuck(){
			return this.shouldEndDuckWhenPossible;
		}
		
		public boolean canEndDuck(){
			assert (canHaveAsDuckingObject(getDuckingObject()));
			Duckable ducker = getDuckingObject();
			InfoGameObject highestPossibleObjectWithoutPerimeter = 
					new InfoGameObject(ducker.getXPixel()+1, ducker.getYPixel()+1, ducker.getRightPixel()-1, ducker.getYPixel() + ducker.getHeightOfHighestSprite()-1);
			World world = ((GameObject)ducker).getWorld();
			// Tiles
			if (world.overlapsWithImpassableTile(highestPossibleObjectWithoutPerimeter))
				return false;
			// Game Objects
			if (world.hasInterferingGameObjectsIfGameObjectRisesToSizeOf(highestPossibleObjectWithoutPerimeter, (GameObject) ducker))
				return false;
			return true;
			}
				
		private void stopDucking(){
			this.ducking = false;
		}
		
		private void setShouldEndDuck(boolean flag){
			this.shouldEndDuckWhenPossible = flag;
		}
		
		
		// TIME
		
		void updateDuckingState(){
			if (shouldEndDuck() && canEndDuck()){
				stopDucking();
				setShouldEndDuck(false);
			}
		}

		
		private boolean ducking, shouldEndDuckWhenPossible;
	
	}
