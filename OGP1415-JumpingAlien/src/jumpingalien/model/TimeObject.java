package jumpingalien.model;

import static jumpingalien.util.Util.fuzzyGreaterThanOrEqualTo;
import static jumpingalien.util.Util.fuzzyLessThanOrEqualTo;
import be.kuleuven.cs.som.annotate.Basic;

/**
 * A class of time objects.
 * 
 * @invar	| canHaveAsTime(getTime())
 *
 */
public abstract class TimeObject {
	
	protected TimeObject(){};
	
	/**
	 * Return the current time of this Time Object.
	 */
	@Basic
	public abstract double getTime();
	
	public static boolean isValidTime(double time){
		return fuzzyGreaterThanOrEqualTo(time, 0);
	}
	
	public static boolean isValidTimeInterval(double dt){
		return (dt > 0 && fuzzyLessThanOrEqualTo(dt, MAX_TIME_INTERVAL)); // should be dt < 0.2 and not dt <= 0.2 but in the game 0.2 is given sometimes.
	}
	
	public static final double MAX_TIME_INTERVAL = 0.2;
	

}


