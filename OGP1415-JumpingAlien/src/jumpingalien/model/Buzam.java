package jumpingalien.model;




import static jumpingalien.util.Util.fuzzyLessThanOrEqualTo;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import jumpingalien.model.program.Program;
import jumpingalien.util.Sprite;
import be.kuleuven.cs.som.annotate.*;


public class Buzam extends NPCObject implements Duckable, Jumpable{
	
	public Buzam(int xPixel, int yPixel, Sprite[] images, Program program) {
		super(xPixel, yPixel, getInitialHitPoints(),images, program);
		// This game object can jump
		Jumper jumper = new Jumper(this);
		assert canHaveAsJumper(jumper);
		this.jumper = jumper;
		// This game object can duck 
		Ducker ducker = new Ducker(this);
		assert canHaveAsDucker(ducker);
		this.ducker = ducker;
	}
	
	// JUMPING ASSOCIATION

	@Basic
	public Jumper getJumper(){
		return this.jumper;
	}
	
	
	public boolean canHaveAsJumper(Jumper jumper){
		return jumper != null && jumper.canHaveAsJumpingObject(this);
	}
	

	public boolean hasProperJumper(){
		return canHaveAsJumper(getJumper())
				&& getJumper().getJumpingObject() == this;
	}
	
	
	private final Jumper jumper;
	
	
	// JUMPING
	
	
	public void startJump() throws IllegalStateException{
		if (getJumper() == null)
			throw new IllegalStateException("Mazub is not associated with a jumping object");
		if (!isAlive())
			throw new IllegalStateException("This Mazub is not alive and consequently cannot start jumping.");
		getJumper().startJump();
	}
	
	public void endJump() throws IllegalStateException{
		if (getJumper() == null)
			throw new IllegalStateException("Mazub not associated with a jumping object");
		if (!isAlive())
			throw new IllegalStateException("This shark is not alive and consequently cannot end the jump.");
		getJumper().endJump();
	}
	
	@Basic @Override @Immutable
	public double getInitialYVelocity() {
		return 8;
	}
	
	// DUCKING
	
	@Basic
	public Ducker getDucker(){
		return this.ducker; // (moet public voor interface)
	}
	
	public boolean canHaveAsDucker(Ducker ducker){
		return ducker != null && ducker.canHaveAsDuckingObject(this);
	}
	

	public boolean hasProperDucker(){
		return canHaveAsDucker(getDucker()) &&
				getDucker().getDuckingObject() == this;
	}
	
	
	@Override
	public int getHeightOfHighestSprite() {
		return getImageAt(0).getHeight(); // misschien klasse-inv
	}
	
	
	private final Ducker ducker;
	
	
	
	
	
	// DUCKING
	
	
	public boolean isDucking(){
		return getDucker().isDucking();
	}
	

	@Override
	public void startDuck() throws IllegalStateException{
		if (getDucker() == null)
			throw new IllegalStateException("Mazub is not associated with a ducker object");
		getDucker().startDuck();
	}

	
	@Override
	public void endDuck() throws IllegalStateException{
		if (getDucker() == null)
			throw new IllegalStateException("Mazub is not associated with a ducking object");
		getDucker().endDuck();
	}
	
	
	private void updateDuckingState(){
		assert isAlive();
		getDucker().updateDuckingState();
	}
	


	// HORIZONTAL MOVEMENT
		// VELOCITY
	
	
	@Override
	public double getMaxXVelocity() {
		if (isDucking())
			return 1;
		return 3;
	}
	
	
	@Basic @Override
	public double getInitialXVelocity() {
		return 1;
	}

		// ACCELERATION
	

	@Override
	public double getXAcceleration() {
		if (hasConstantXVelocity())
			return 0;
		return 0.9;
	}
	


	// HITPOINTS

	@Basic @Immutable
	public static int getInitialHitPoints(){
		return 500;
	}
	
	
	public boolean isHungry(){
		return getHitPoints() < getMaxHitPoints();
	}
	
	
	@Override
	void updateHitPointsUponCollisionWith(GameObject other){
		if (other == null || !hasAsCollidingObject(other) || this.isTerminated() || other.isTerminated());
		else if (isImmune() || other.isImmune());
		else if(other instanceof Plant){
			
			if (isHungry() && other.isAlive()){
				this.updateHitPoints(50);
				other.updateHitPoints(-1);
			}
		}
		else if(other instanceof Slime){
			other.updateHitPoints(-50);
			other.resetImmunityTimer();
			if (!this.isOnTopOf(other)){
				this.updateHitPoints(-50);
				this.resetImmunityTimer();
			}
		}
		else if(other instanceof Shark){
			other.updateHitPoints(-50);
			other.resetImmunityTimer();
			if (!this.isOnTopOf(other)){
				this.updateHitPoints(-50);
				this.resetImmunityTimer();
			}
		}
		else if (other instanceof Mazub){
			if (!other.isOnTopOf(this)){
				other.updateHitPoints(-50);
				other.resetImmunityTimer();
			}
			if (!this.isOnTopOf(other)){
				this.updateHitPoints(-50);
				this.resetImmunityTimer();
			}
		}
		else
			other.updateHitPointsUponCollisionWith(this);
	}
	
	
	// SPRITES
	

	@Basic @Override
	public int getMinNbSprites(){
		return 10;
	}
	
	@Override
	public boolean canHaveAsSprites(Sprite[] images){
		return super.canHaveAsSprites(images) && (images.length % 2 == 0);
	}
	
		// CURRENT INDICES

	@Override
	public int getNewSpriteIndex(){
		assert isAlive();
		if (getCurrentIndices().length > 1){
				// Als state has changed ==> automatisch op 0 gezet via formule
				int newIndex = ((int) (getAlternationTimer()/TIME_EACH_IMAGE) % getCurrentIndices().length);
				return getCurrentIndices()[newIndex];
		}
		return getCurrentIndices()[0];
	}
	
	public int[] getCurrentIndices(){
		return this.currentIndices;
	}
	
	
	public boolean stateHasChanged(int[] newIndices){
		assert canHaveAsIndices(newIndices);
		return (!Arrays.equals(getCurrentIndices(), newIndices));
	}	
	

	public boolean canHaveAsIndex(int index){
		return ( (index < getNbSprites()) && (index >= 0) );
	}
	
	public boolean canHaveAsIndices(int[] indices){
		if (indices == null)
			return false;
		// @invar	Mazub can have all indices before "index".
		//			| for each I in 0..index-1:
		//			|	canHaveAsIndex(index)
		// @invar	The number of handled indices doesn't exceed the length of the given array indices.
		//			| Arrays.asList(indices).indexOf(index) <= indices.length
 		// @variant	The number of unhandeld indices in the given array.
		//			| indices.length - Arrays.asList(indices).indexOf(index)
		for (int index: indices)
			if (!canHaveAsIndex(index))
				return false;
		return true;
	}
	
	private void updateSpriteIndicesAndAlternationTimer(double dt){
		assert isValidTimeInterval(dt);
		assert isAlive();
		int[] newIndices = getApplicableIndices();
		if (stateHasChanged(newIndices)){
			setAlternationTimer(dt);
			setCurrentIndices(newIndices);
		}
		else 
			setAlternationTimer(getAlternationTimer()+ dt);
	}
	
	private void setCurrentIndices(int[] newIndices ){
		assert (canHaveAsIndices(newIndices));
		this.currentIndices = newIndices;
	}
	
	
	private int[] currentIndices = new int[] {0};
	
		// Calculate new applicable indices
	private int[] getApplicableIndices(){
		if (!isRunning()){
			if (!hasMovedLastSecond()){
				if (!isDucking())
					return new int[] {0};
				else
					return new int[] {1};
			}
			else
				if (isDucking()){
					if (getHorizontalDirection().getUnitDirection() == 1)
						return new int[] {6};
					else
						return new int [] {7};
				} else {					
					if (getHorizontalDirection().getUnitDirection() == 1)
						return new int[] {2};
					else
						return new int [] {3};
				}
		}
		else {
			if (isDucking()){
				if (getHorizontalDirection().getUnitDirection() == 1)
					return new int[] {6};
				else
					return new int [] {7};
			} else {
				if (shouldFall()){
					if (getHorizontalDirection().getUnitDirection() == 1)
						return new int[] {4};
					else
						return new int [] {5};
					
				} else {
					if (getHorizontalDirection().getUnitDirection() == 1)
						return createRunningArray(8);
					else
						return createRunningArray(9+computeM());
				}
			}
		}
	}

	
	@Immutable @Model
	private int computeM(){
		return (getNbSprites()-10)/2;
	}
	
	
	private int[] createRunningArray(int start){
		assert canHaveAsIndex(start);
		int m = computeM();
		int[] runningArray = new int[1+m];
		// @invar	The running array contains all the indices for running which are smaller than the current index.
		//			| for each index in start..index
		//			|	Arrays.asList(runningArray).contains(index)
		// @invar	"index" doesn't exceed (1 + computeM())
		// 			| index < 1 + computeM()
		// @variant	The number of unhandled indices.
		//			| computeM() - index + 1
		for(int index = 0; index < 1+m; index++){
			runningArray[index] = start+index;
		}
		return runningArray;
	}


	
		// ALTERNATION TIMER
	
	@Basic
	public double getAlternationTimer(){
		return this.alternationTimer;
	}
	
	@Basic
	public double getLastMovement(){
		return this.movement;
	}
	
	public static boolean isValidMovement(double movement){
		// result == ( (movement >= 0) || Double.isNaN(movement) )
		return ( !(movement < 0) );
	}
	
	public boolean hasMovedLastSecond(){
		return ((getTime() <= getLastMovement() + 1));
	}
	
	
	@Model
	private void setAlternationTimer(double newTime){
		this.alternationTimer = newTime;
	}
	
	private void registerMovement(){
		if (isRunning()){
			setLastMovement(getTime());
		}
	}
	
	@Model
	private void setLastMovement(double movement){
		assert isValidMovement(movement);
		this.movement = movement;
	}
	
	
	private double alternationTimer = 0;
	

	private static final double TIME_EACH_IMAGE = 0.075;
	
	private double movement = Double.NaN;
	
	// ASSOCIATION WITH WORLD
	
	@Raw
	@Override
	void addAsGameObject(World world){
		// applicability beperkt in setBazum zelf
		world.setBuzam(this);
	}
	
	@Override
	void removeAsGameObject(@Raw World world){
		// applicability beperkt in setBazum zelf
		world.setBuzam(null);
	}

	// COLLISION
	
	@Override
	public boolean interferesMovementWith(GameObject other) {
		if (other == null)
			return false;
		if (other instanceof Plant)
			return false;
		if (other instanceof Slime || other instanceof Shark || other instanceof Mazub || other instanceof Buzam)
			return true;
		return other.interferesMovementWith(this);
	}

	
	// WEAKNESS
	
	@Basic @Override
	public Map<GeologicalFeature,Integer> getWeaknessesAndHitpointLosses() {
		Map <GeologicalFeature, Integer> weaknessesAndLosses =  new HashMap<GeologicalFeature, Integer>();
		weaknessesAndLosses.put(GeologicalFeature.WATER, -2);
		weaknessesAndLosses.put(GeologicalFeature.MAGMA, -50);
		return weaknessesAndLosses;
	};
	
	// APPROPRIATE DIRECTION
	
	@Override
	public HorizontalDirection getAppropriateDirection() {
		return HorizontalDirection.LEFT;
	}

	// MOVMENT DURATION

	@Override
	public double getMinMovementDuration() {
		return 0;
	}


	@Override
	public double getMaxMovementDuration() {
		return 0;
	}

	// CAN MOVE HORIZONTALLY AND VERTICALLY

	@Override
	public boolean canMoveHorizontally() {
		return true;
	}
	
	@Override
	public boolean canMoveVertically() {
		return true;
	}
	
	// SHOULD MOVE RANDOMLY
	
	@Override
	public boolean canMoveRandomly() {
		return false;
	}

	
	// TIME

	@Override
	public void advanceTime(double dt) throws IllegalArgumentException, IllegalStateException{
		if (!isValidTimeInterval(dt))
			throw new IllegalArgumentException("Illegal time interval: " + dt);
		if (!fuzzyLessThanOrEqualTo(getTime() + dt, getWorld().getTime()))
			throw new IllegalArgumentException("The time of the world must always be higher than the time of this Mazub");
		if (isTerminated())
			throw new IllegalStateException("Cannot advance the time for a terminated object");
		if (isAlive())
			updateSpriteIndicesAndAlternationTimer(dt);
		super.advanceTime(dt);
		if (isAlive()){
			updateDuckingState();
			registerMovement();
		}
	}

	// TERMINATION
	
	@Basic @Override
	public double getTerminationThreshold() {
		return 0.6;
	}

}

