package jumpingalien.model;

public interface Jumpable {
	
	void startJump();
	
	void endJump();
	
	boolean shouldFall();
	
	boolean isTerminated();
	
	boolean isAlive();
	
	double getInitialYVelocity();
	
	Jumper getJumper();
	
	

}
