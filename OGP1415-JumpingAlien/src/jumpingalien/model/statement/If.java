package jumpingalien.model.statement;

import jumpingalien.model.expression.Expression;
import jumpingalien.model.type.*;

import java.util.*;

public class If extends ComposedStatement {
	public If(Expression<BoolType> condition,Statement ifBody, Statement elseBody){
		assert !hasAsSubStatement(ifBody) && !hasAsSubStatement(elseBody);
		assert (condition != null);
		this.condition = condition;
		this.ifBody = ifBody;
		this.elseBody = elseBody;
		getIfBody().setParent(this);
		if (elseBody != null)
			getElseBody().setParent(this);
				
	}
	
	// CASCADE PROGRAM
	
	@Override
	public void cascadeProgramToChildExpressions() {
		Expression<BoolType> condition = getCondition();
		if (condition != null)
			condition.setProgram(getProgram());
	}
	
	
	// IS WELL FORMED
	
	public boolean satisfiesConstraints() {
		return (getCondition() != null && getCondition().satisfiesConstraints() && BoolType.class.isAssignableFrom(getCondition().getType()))
				&& childrenSatisfyConstraints();
	}
	
	@Override
	public boolean childrenSatisfyConstraints() {
		Statement ifBody = getIfBody();
		Statement elseBody = getElseBody();
		return ((ifBody == null || ifBody.childrenSatisfyConstraints()) &&
				(elseBody == null || elseBody.childrenSatisfyConstraints()));
	}
	
	// ITERABLE
	
	public Iterator<Statement> iterator(){
		return new Iterator<Statement>(){		
			// IMPLEMENTING Iterator<E>
			
			public boolean hasNext(){
				return isFirstTime() || (getCurrentIterator() != null && getCurrentIterator().hasNext());
			}
			
			public Statement next() throws NoSuchElementException{
				if (!hasNext())
					throw new NoSuchElementException();
				if (isFirstTime()){
					setNewCurrentIterator(); // Pas wanneer aan if statement zelf ==> dan beslissen of if of else
					hasGivenSelf = true;
					return If.this;
				}	
				return getCurrentIterator().next();	
			}
			
            public void remove() throws UnsupportedOperationException {
                throw new UnsupportedOperationException();
            }
			
			// CURRENT ITERATOR
			
			public Iterator<Statement> getCurrentIterator(){
				return this.currentIterator;
			}
			
			public boolean isFirstTime(){
				return !hasGivenSelf;
			}
			
			private void setNewCurrentIterator(){
				assert(isFirstTime());
				if (satisfiesCondition())
					this.currentIterator = getIfBody().iterator();
				else{
					if (getElseBody() != null)
						this.currentIterator = getElseBody().iterator();
					else
						this.currentIterator = null;
				}
			}
			
			private boolean hasGivenSelf = false;
			private Iterator<Statement> currentIterator = null;
		};
	}
	
	// ATTRIBUTES
	
	public Expression<BoolType> getCondition(){
		return this.condition;
	}
	
	public boolean satisfiesCondition(){
		return getCondition().getValue().isTrue();
	}
	
	public Statement getIfBody(){
		return this.ifBody;
	}
	
	public Statement getElseBody(){
		return this.elseBody;
	}
	
	private final Expression<BoolType> condition;
	
	private final Statement ifBody, elseBody;
	
	// IS EMPTY
	public boolean isEmpty(){
		return false;
	}
	
	// PARENT AND CHILD
	@Override
	public Statement getChildAt(int index){
		assert canHaveAsIndex(index);
		if (index == 1)
			return getIfBody();
		return getElseBody();
	}
	@Override
	public int getNbChildren(){
		return 2;
	}
}
