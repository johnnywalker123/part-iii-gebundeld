package jumpingalien.model.statement;


import be.kuleuven.cs.som.annotate.Basic;
import jumpingalien.model.expression.*;
import jumpingalien.model.type.*;
import static jumpingalien.model.type.DoubleType.TIME_PER_STATEMENT_DOUBLETYPE;

public class Wait extends Action{
	// CONSTRUCTOR
	public Wait(Expression<DoubleType> duration){
		assert duration != null;
		this.duration = duration;
	}
	
	// EXECUTE
	@Override
	public void execute(){
		// pass
	}
	
	// SATISFIES CONSTRAINTS
	
	@Override
	public boolean satisfiesConstraints() {
		return getDuration() != null && getDuration().satisfiesConstraints();
	}
	
	// CASCADE PROGRAM
	
	@Override
	public void cascadeProgramToChildExpressions() {
		getDuration().setProgram(getProgram());
	};
		
	// ATTRIBUTES
	
	@Basic @Override
	public int getTimesToReturn(){
		DoubleType result = getDuration().getValue().divide(TIME_PER_STATEMENT_DOUBLETYPE);
		return result.toInteger();
	}
	
	public Expression<DoubleType> getDuration(){
		return this.duration;
	}
	
	private final Expression<DoubleType> duration;
}
