package jumpingalien.model.statement;

import jumpingalien.model.*;

public class StartJump extends Action {
	
	
	@Override
	public void execute() {
		GameObject gameObject = getProgram().getExecutingNpc();
		if (gameObject instanceof Jumpable){
			((Jumpable) gameObject).startJump();
		}
	}
	
	@Override
	public void cascadeProgramToChildExpressions() {
		// pass
	}
	
	
	

}
