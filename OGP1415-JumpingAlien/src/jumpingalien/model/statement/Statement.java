package jumpingalien.model.statement;

import java.util.Iterator;

import be.kuleuven.cs.som.annotate.Basic;
import jumpingalien.model.program.Program;

public abstract class Statement implements Iterable<Statement>{
	
	// EXECUTE
	public abstract void execute();
	
	// ITERABLE
	
	@Override
	public abstract Iterator<Statement> iterator();
	
	// IS WELL FORMED
	
	public abstract boolean satisfiesConstraints();
	
	public boolean childrenSatisfyConstraints(){
		for (int i=1; i<= getNbChildren();i++){
			if (!getChildAt(i).satisfiesConstraints())
				return false;
		}
		return true;
	}
	
	// PARENT AND CHILD
	
		// PARENT
	
	public Statement getParent(){
		return this.parent;
	}
		
	void setParent(Statement parent){ // moet in program kunnen
		assert (parent.hasAsDirectChild(this));
		this.parent = parent;
	}
	
	private Statement parent;
	

		// CHILD
	
	@Basic
	public abstract Statement getChildAt(int index);
	
	@Basic
	public abstract int getNbChildren();
	
	public boolean canHaveAsIndex(int index){
		return (index >= 1) && (index <= getNbChildren());
	}
	
	public abstract boolean hasAsDirectChild(Statement statement);

	public boolean hasChildOfType(Class<? extends Statement> statement){
		if (statement.isAssignableFrom(this.getClass()))
			return true;
		for(int i=1; i<=getNbChildren(); i++){
			Statement child = getChildAt(i);
			if (child != null && child.hasChildOfType(statement))
				return true;
		}
		return false;
	}
	
	// SUBSTATEMENT
	public abstract boolean hasAsSubStatement(Statement statement);

	// PROGRAM
	
	Program getProgram(){
		return this.program;
	}
	
	public void setProgram(Program program){
		assert(canHaveAsProgram(program));
		this.program = program;
		cascadeProgramToChildStatements();
		cascadeProgramToChildExpressions();
	}
	
	public void cascadeProgramToChildStatements(){
		for (int i = 1; i <= getNbChildren();i++){
			Statement child = getChildAt(i);
			if (child != null)
				child.setProgram(getProgram());
		}
	}

	public abstract void cascadeProgramToChildExpressions();
	
	public boolean canHaveAsProgram(Program program){
		return getProgram() == null && program != null;
	}
	
	private Program program;
	
}
