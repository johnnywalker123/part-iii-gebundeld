package jumpingalien.model.statement;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import be.kuleuven.cs.som.annotate.*;


public class Sequence extends ComposedStatement{
	
	public Sequence(List<? extends Statement> statements){
		this.statements = new ArrayList<Statement>();
		for (Statement statement : statements){
			assert !hasAsSubStatement(statement);
			this.statements.add(statement); // our version of clone
			statement.setParent(this);
		}

			
	}
	
	
	// IS WELL FORMED
	
	public boolean satisfiesConstraints() {
		return childrenSatisfyConstraints();
	};
	
	
	// PARENT AND CHILD
	@Basic
	public Statement getChildAt(int index){
		return this.statements.get(index-1);
	}
	
	@Override
	public int getNbChildren(){
		return statements.size();
	}
	
	@Override
	public void cascadeProgramToChildExpressions() {}
	
	@Basic
	public int getNbStatements(){
		return this.statements.size();
	}
	
	public boolean isEmpty(){
		return getNbStatements() == 0;
	}
	
	private final List<Statement> statements;
	
	
	
	
	// IMPLEMENTING ITERABLE
	@Override
	public Iterator<Statement> iterator(){
		
		return new Iterator<Statement>(){
			
			public boolean hasNext(){
				return (	!isFirstTime() && getCurrentIterator().hasNext()	) 
						|| canHaveAsIndex(getIndex() + 1);
			}
			
			public Statement next() throws NoSuchElementException{
				if (!hasNext())
					throw new NoSuchElementException();
				if (isFirstTime() || !getCurrentIterator().hasNext()){
					setIndex(getIndex() + 1);
					setNewCurrentIterator();
				}
				return getCurrentIterator().next(); // Is possible because has next
			}
			
            public void remove() throws UnsupportedOperationException {
                throw new UnsupportedOperationException();
            }
            
            // CURRENT ITERATOR
            
            @Basic
            public Iterator<Statement> getCurrentIterator(){
            	return this.currentIterator;
            }
            
            public boolean isFirstTime(){
            	return (getCurrentIterator() == null);
            }
            
            private void setNewCurrentIterator(){
            	this.currentIterator = getChildAt(getIndex()).iterator();
            }
            
            private Iterator<Statement> currentIterator = null;
			
			// INDEX
			
            @Basic
			public int getIndex(){
				return this.index;
			}
			
			private void setIndex(int index){
				this.index = index;
			}
			
			private int index = 0; // Eerst keer iterator gebruiken: direct +1
		};
	}
	

}
