package jumpingalien.model.statement;

import be.kuleuven.cs.som.annotate.Basic;
import jumpingalien.model.expression.*;
import jumpingalien.model.type.Type;

public class Assignment<T extends Type> extends BasicStatement {
	
	// CONSTRUCTOR
	public Assignment(String variableName, Expression<T> expression){
		//canHaveAs(): programma: globalVariable lijst checken of variabele zelfde type
		this.variableName = variableName;
		this.expression = expression;
	}
	
	// CASCADE PROGRAM TO EXPRESSIONS
	
	public void cascadeProgramToChildExpressions() {
		getExpression().setProgram(getProgram());
	}
	
	// IS WELL FORMED
	
	@Override
	public boolean satisfiesConstraints() {
		return super.satisfiesConstraints()
				&& getProgram().hasGlobalVariableAt(getVariableName())
				&& getProgram().getGlobalVariableAt(getVariableName()).getType().isAssignableFrom(getExpression().getType())
				&& getExpression().satisfiesConstraints();
	}
	
	// EXECUTE
	@Override
	public void execute() throws IllegalStateException{
		Variable<T> variable = (Variable<T>) getProgram().getGlobalVariableAt(variableName);
		variable.setValue(getExpression().getValue());
	}
	// @Runtime, this cast just cast to the raw type Variable. Consequently it's possible to store a Variable in variable which is not initiated with the
	// correct parameter.
	// Since we may assume that the given program is correct and therefore the given variable name in the constructor corresponds with a variable from the appropriate type,
	// this is not a problem.
	
	// ATTRIBUTES
	@Basic
	public String getVariableName(){
		return this.variableName;
	}
	
	@Basic
	public Expression<T> getExpression(){
		return this.expression;
	}
	
	private String variableName;
	
	private final Expression<T> expression;
	
	// TIMES TO RETURN
	@Basic @Override
	public int getTimesToReturn(){
		return 1;
	}
}
