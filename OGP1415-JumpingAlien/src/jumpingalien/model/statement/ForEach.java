package jumpingalien.model.statement;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import be.kuleuven.cs.som.annotate.Basic;
import jumpingalien.model.expression.*;
import jumpingalien.model.type.*;

public class ForEach extends Loop{
	public ForEach(String variableName, Kind kind, Expression<BoolType> where, Expression<DoubleType> sort,
			SortDirection sortDirection, Statement body){
		assert !hasAsSubStatement(body);
		assert kind != null;
		this.variableName = variableName;
		this.kind = kind;
		// WHERE
		if (where != null)
			this.where = where;
		else
			this.where = new Literal<BoolType>(new BoolType(true), BoolType.class); // where is always true
		// SORT
		this.sort = sort;
		this.sortDirection = sortDirection;
		// SET ASSOCIATION PARENT-CHILD
		this.body = body;
		getBody().setParent(this);
	}
	
	// CASCADE PROGRAM
	
	@Override
	public void cascadeProgramToChildExpressions() {
		getWhere().setProgram(getProgram());
		if (getSort() != null)
			getSort().setProgram(getProgram());
	};
	
	// IS WELL FORMED
	
	@Override
	public boolean satisfiesConstraints() {
		return getProgram().hasGlobalVariableAt(getVariableName()) &&
				ObjectType.class.isAssignableFrom(getProgram().getGlobalVariableAt(getVariableName()).getType()) &&
				getKind() != null &&
				(getWhere() == null || 
						(BoolType.class.isAssignableFrom(getWhere().getType()) && getWhere().satisfiesConstraints())) &&
				(getSort() == null ||
						(DoubleType.class.isAssignableFrom(getSort().getType()) && getSort().satisfiesConstraints())) &&
				!hasChildOfType(Action.class) && childrenSatisfyConstraints();
	}
	
	@Override
	public boolean childrenSatisfyConstraints() {
		return getBody() == null || getBody().childrenSatisfyConstraints();
	}
	

	
	
	// ITERABLE
	
	@Override
	public Iterator<Statement> iterator() {
		return new Iterator<Statement>(){
			
			// IMPLEMENTING ITERATOR<Statement>
			public boolean hasNext(){
				return isFirstTime() ||
						(	!isBroken() && 
								( ( getBodyIterator() != null && getBodyIterator().hasNext())
										|| getVariableIterator().hasNext()	)	);
			}
			
			public Statement next() throws NoSuchElementException{
				if (!hasNext())
					throw new NoSuchElementException();
				if (isFirstTime()){
					unbreakLoop();
					setVariableIterator();
					nextLoop();
					return ForEach.this;
				}
				if (!getBodyIterator().hasNext())
					nextLoop();
				return getBodyIterator().next();
			}
			
            public void remove() throws UnsupportedOperationException {
                throw new UnsupportedOperationException();
            }
            
            // GO TO NEXT LOOP
            private void nextLoop(){
            	if (getVariableIterator().hasNext()){
            		getVariable().setValue(getVariableIterator().next());
            		setNewBodyIterator();
            	}
            }
            
            // CURRENT ITERATOR
            @Basic
        	public Iterator<Statement> getBodyIterator(){
        		return this.iterator;
        	}
        	
        	private void setNewBodyIterator(){
        		this.iterator = getBody().iterator();
        	}
        	
        	private Iterator<Statement> iterator = null;
        	
        	// VARIABLE ITERATOR
        	@Basic
        	public Iterator<ObjectType> getVariableIterator(){
        		return this.variableIterator;
        	}
        	
        	public boolean isFirstTime(){ 
        		return getVariableIterator() == null;
        	}
        	
        	public Iterator<ObjectType> getNewVariableIterator(){
        		
        		// GET WHERE AND SORT EXPRESSIONS
        			// CREATE STREAM
        		Stream<ObjectType> stream = getProgram().getExecutingNpc().getWorld().stream();
        			// FILL WHERE AND SORT MAP
        		stream.filter(x -> getKind().isKind(x)).
		        		forEach(x -> {
			    			getVariable().setValue(x);
				    		whereMap.put(x,getWhere().getValue().isTrue());
			    			if (shouldSort())
				    			sortMap.put(x,getSort().getValue().getValue());
			    		});
        		
        		// FILTER BY KIND, WHERE AND SORT
        			// CREATE STREAM
	    		stream = getProgram().getExecutingNpc().getWorld().stream();
	    			// RETURN ITERATOR OF FILTERED AND SORTED STREAM
        		return stream.filter(x -> getKind().isKind(x)).
        						filter(x -> whereMap.get(x)).
        						sorted((x,y) -> {     
        			    			if(sortMap.get(x) == sortMap.get(y)) // If the stream should not be sorted, sortMap.get() will return null for both objects. As a result the comparator will always return 0.
        			    				return 0;
        			    			if (sortMap.get(x) < sortMap.get(y))
        			    				return getSortDirection().getCompareValueLessThan();
        			    			return getSortDirection().getCompareValueGreaterThan();
        			    		}).
        			    		iterator();
        	}
        	
        	private void setVariableIterator(){
        		assert isFirstTime(); // Mag maar 1 keer doen
        		this.variableIterator = getNewVariableIterator();
        	}
     
  
        	private Iterator<ObjectType> variableIterator = null;
        	private Map<ObjectType, Boolean> whereMap = new HashMap<>();
        	private Map<ObjectType, Double> sortMap = new HashMap<>();
        	
        	// CURRENT VARIABLE
        	public Variable<ObjectType> getVariable(){
        		return this.variable;
        	}
        	
        	private final Variable<ObjectType> variable = (Variable<ObjectType>) getProgram().getGlobalVariableAt(variableName);
        	// The time the iterator is created, the program is already available.
        	
        	// @Runtime, this cast just cast to the raw type Variable. Consequently it's possible to store a Variable in variable which is not initiated with the
        	// parameter ObjectType.
        	// Since we may assume that the given program is correct and therefore the given variable name in the constructor corresponds with a variable from the type
        	// ObjectType, this is not a problem.
		};
	}
	
	// ATTRIBUTES
	@Basic
	public String getVariableName(){
		return this.variableName;
	}
	
	@Basic
	public Kind getKind(){
		return this.kind;
	}
	
	@Basic
	public Expression<BoolType> getWhere(){
		return this.where;
	}
	
	@Basic
	public Expression<DoubleType> getSort(){
		return this.sort;
	}
	
	public boolean shouldSort(){
		return (getSort() != null);
	}
	
	@Basic
	public SortDirection getSortDirection(){
		return this.sortDirection;
	}
	
	private final String variableName;
	private final Kind kind;
	private final Expression<BoolType> where;
	private final Expression<DoubleType> sort;
	private final SortDirection sortDirection;
	
	// ASSOCIATIONS
	@Basic
	public Statement getBody(){
		return this.body;
	}
	
	private final Statement body;
	
	
}
