package jumpingalien.model.statement;


public abstract class Loop extends ComposedStatement {
	
	// BODY
	
	public abstract Statement getBody();
	
	// PARENT AND CHILD
	@Override
	public Statement getChildAt(int index){
		assert canHaveAsIndex(index);
		return getBody();
	}
	
	@Override
	public int getNbChildren(){
		return 1;
	}
	
	// BREAK
	public boolean isBroken(){
		return this.isBroken;
	}
	
	void breakLoop(Break breaker){
		assert (breaker.getEnclosingLoop() == this);
		this.isBroken = true;
	}
	
	void unbreakLoop(){
		this.isBroken = false;
	}
	
	private boolean isBroken;
}
