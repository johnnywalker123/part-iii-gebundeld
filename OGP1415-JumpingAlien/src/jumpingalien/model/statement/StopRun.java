package jumpingalien.model.statement;

import jumpingalien.model.HorizontalDirection;
import jumpingalien.model.expression.Expression;
import be.kuleuven.cs.som.annotate.Basic;

public class StopRun extends Action {
	
	// CONSTRUCTOR
	public StopRun(Expression<HorizontalDirection> direction){
		this.direction = direction;
	}
	

	
	// EXECUTE
	@Override
	public void execute(){
		getProgram().getExecutingNpc().endMove(getDirection().getValue());
	}
	
	// IS WELL FORMED
	
	@Override
	public boolean satisfiesConstraints() {
		return getDirection() != null && getDirection().satisfiesConstraints();
	}
	
	// CASCADE PROGRAM
	
	@Override
	public void cascadeProgramToChildExpressions() {
		getDirection().setProgram(getProgram());
	};
	
	//ATTRIBUTES
	
	@Basic
	public Expression<HorizontalDirection> getDirection(){
		return this.direction;
	}
	
	private final Expression<HorizontalDirection> direction;
	

}
