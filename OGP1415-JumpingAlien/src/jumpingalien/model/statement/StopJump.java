package jumpingalien.model.statement;

import jumpingalien.model.*;

public class StopJump extends Action {
	
	@Override
	public void execute() {
		GameObject gameObject = getProgram().getExecutingNpc();
		if (gameObject instanceof Jumpable){
			((Jumpable) gameObject).endJump();
		}
	}
	
	@Override
	public void cascadeProgramToChildExpressions() {
		// pass
	}
	

}
