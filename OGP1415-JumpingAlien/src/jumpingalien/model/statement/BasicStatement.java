package jumpingalien.model.statement;

import java.util.*;

import be.kuleuven.cs.som.annotate.Basic;

public abstract class BasicStatement extends Statement{
	
	// CHILDREN
	
	@Override
	public Statement getChildAt(int index) {
		return null;
	}
	
	@Override
	public int getNbChildren() {
		return 0;
	}
	
	@Override
	public boolean hasAsDirectChild(Statement statement){
		return false;
	}
	
	@Override
	public boolean canHaveAsIndex(int index) {
		return false;
	}
	
	// SUBSTATEMENT

	@Override
	public final boolean hasAsSubStatement(Statement statement){
		return statement == this;
	}
	
	// IS WELL FORMED
	
	public boolean childrenSatisfyConstraints() {
		return true;
	}
	
	// ITERABLE
	@Override
	public Iterator<Statement> iterator(){
		return new Iterator<Statement>(){
			public boolean hasNext(){
				return getTimesAlreadyReturned() != getTimesToReturn();
			}
			
			public Statement next() throws NoSuchElementException{
				if (!hasNext())
					throw new NoSuchElementException();
				incrementTimesAlreadyReturned();
				return BasicStatement.this;
			}
			
            public void remove() throws UnsupportedOperationException {
                throw new UnsupportedOperationException();
            }
			
			// NUMBER OF TIMES ALREADY RETURNED THIS STATEMENT
			
			@Basic
			public int getTimesAlreadyReturned(){
				return this.timesAlreadyReturned;
			}
			
			private void incrementTimesAlreadyReturned(){
				this.timesAlreadyReturned += 1;
			}
			
			private int timesAlreadyReturned;
		};
	}
	
	// TIMES TO RETURN
	public abstract int getTimesToReturn();
	

	
	// IS WELL FORMED
	
	@Override
	public boolean satisfiesConstraints() {
		return true;
	}


	
	

}
