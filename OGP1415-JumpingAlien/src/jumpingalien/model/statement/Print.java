package jumpingalien.model.statement;

import jumpingalien.model.expression.Expression;
import jumpingalien.model.type.Type;
import be.kuleuven.cs.som.annotate.Basic;

public class Print extends BasicStatement {
	// CONSTRUCTOR
	public Print(Expression<? extends Type> expression){
		this.expression = expression;
	}
	
	// EXECUTE
	@Override
	public void execute(){
		System.out.println(getExpression());
	}
	
	// CASCADE PROGRAM
	
	public void cascadeProgramToChildExpressions() {
		getExpression().setProgram(getProgram());
	};
	
	@Override
	public boolean satisfiesConstraints() {
		return getExpression() != null && getExpression().satisfiesConstraints();
	}
	
	
	
	// ATTRIBUTE
	@Basic
	public Expression<? extends Type> getExpression(){
		return this.expression;
	}
	
	private Expression<? extends Type> expression;
	
	// TIME TO RETURN
	@Basic @Override
	public int getTimesToReturn(){
		return 1;
	}
}
