package jumpingalien.model.statement;

public abstract class Action extends BasicStatement{
	
	@Override
	public int getTimesToReturn() {
		return 1;
	}
}
