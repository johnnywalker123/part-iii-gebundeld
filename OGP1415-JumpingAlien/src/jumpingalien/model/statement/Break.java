package jumpingalien.model.statement;


public class Break extends BasicStatement {

	// TIMES TO RETURN
	
	@Override
	public int getTimesToReturn() {
		return 1;
	}
	
	// CASCADE PROGRAM
	
	@Override
	public void cascadeProgramToChildExpressions() {};
	
	// IS WELL FORMED
	
	@Override
	public boolean satisfiesConstraints() {
		return getEnclosingLoop() != null;
	}
	
	// ENCLOSING LOOP
	
	public Loop getEnclosingLoop(){
		Statement parent = getParent();
		while(parent != null){
			if (parent instanceof Loop)
				return (Loop) parent;
			parent = parent.getParent();
		}
		return null;
		}
	
	// EXECUTE

	/* als er geen enclosingLoop is, is programma niet well formed
	en gaat er niets worden uitgevoerd */
	@Override
	public void execute() {
		getEnclosingLoop().breakLoop(this); 
	}
	
	
	
}
