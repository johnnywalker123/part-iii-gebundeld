package jumpingalien.model.statement;

import be.kuleuven.cs.som.annotate.Basic;

public class Skip extends Action{
	// CONSTRUCTOR
	// default constructor
	
	// EXECUTE
	@Override
	public void execute(){
		// pass
	}
	
	// CASCADE PROGRAM
	
	@Override
	public void cascadeProgramToChildExpressions() {};
	
	// TIME TO RETURN
	@Basic @Override
	public int getTimesToReturn(){
		return 1;
	}
}
