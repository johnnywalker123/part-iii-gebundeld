package jumpingalien.model.statement;

public enum SortDirection {
	ASCENDING{
		public int getCompareValueGreaterThan(){
			return 1;
		}
		
		public int getCompareValueLessThan(){
			return -1;
		}
	}, DESCENDING{
		public int getCompareValueGreaterThan(){
			return -1;
		}
		
		public int getCompareValueLessThan(){
			return 1;
		}
	};
	
	public abstract int getCompareValueGreaterThan();
	
	public abstract int getCompareValueLessThan();
}