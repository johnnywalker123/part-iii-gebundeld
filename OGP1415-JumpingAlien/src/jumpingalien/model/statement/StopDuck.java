package jumpingalien.model.statement;

import jumpingalien.model.*;

public class StopDuck extends Action {
	
	@Override
	public void execute() {
		GameObject gameObject = getProgram().getExecutingNpc();
		if (gameObject instanceof Duckable){
			((Duckable) gameObject).endDuck();
		}
	}
	
	@Override
	public void cascadeProgramToChildExpressions() {
		// pass
	}
	
	@Override
	public int getTimesToReturn() {
		return 1;
	}


}
