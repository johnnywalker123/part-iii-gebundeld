package jumpingalien.model.statement;


public abstract class ComposedStatement extends Statement {
	// EXECUTING
	
	@Override
	public void execute(){
		// pass
	}
	
	// CHILD
	
	public boolean hasAsDirectChild(Statement statement){
		for (int i = 1; i <= getNbChildren();i++){
			if (getChildAt(i).equals(statement))
				return true;
		}
		return false;
	}
	
	// SUBSTATEMENT
	
	@Override
	public boolean hasAsSubStatement(Statement statement){
		if (statement == this)
			return true;
		for (int i = 1; i <= getNbChildren(); i++){
			if (getChildAt(i) != null && getChildAt(i).hasAsSubStatement(statement))
				return true;
		}
		return false;
	}
	
}
