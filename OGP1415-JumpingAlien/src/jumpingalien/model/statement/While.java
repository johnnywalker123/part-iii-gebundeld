package jumpingalien.model.statement;

import java.util.*;

import be.kuleuven.cs.som.annotate.Basic;
import jumpingalien.model.expression.Expression;
import jumpingalien.model.type.*;

public class While extends Loop {
	
	// CONSTRUCTOR
	public While(Expression<BoolType> condition, Statement body){
		assert !hasAsSubStatement(body);
		assert condition != null;
		this.condition = condition;
		this.body = body;
		getBody().setParent(this);
	}
	
	// CASCADE PROGRAM
	
	public void cascadeProgramToChildExpressions() {
		Expression<BoolType> condition = getCondition();
		if (condition != null)
			condition.setProgram(getProgram());
	}
	
	// IS WELL FORMED
	
	public boolean satisfiesConstraints() {
		return ((getCondition() != null && getCondition().satisfiesConstraints() && BoolType.class.isAssignableFrom(getCondition().getType()))
				&& childrenSatisfyConstraints());
	}
	
	@Override
	public boolean childrenSatisfyConstraints() {
		return (getBody() == null || getBody().satisfiesConstraints());
	}
	
	// ATTRIBUTES
	
	@Basic
	public Expression<BoolType> getCondition(){
		return this.condition;
	}
	
	public boolean satisfiesCondition(){
		return this.condition.getValue().isTrue();
	}
	
	@Basic
	public Statement getBody(){
		return this.body;
	}
	
	private final Expression<BoolType> condition;
	
	private final Statement body;
	
	// isFirstTime(): in execute van while: in iterator op false
	
	// ITERABLE

	@Override
	public Iterator<Statement> iterator() {
		return new Iterator<Statement>(){
			
			// IMPLEMENTING ITERATOR<Statement>
			
			public boolean hasNext(){
				return isFirstTime() || 
						(	!hasEnded() && !isBroken()	);
			}
			
			public Statement next() throws NoSuchElementException{
				if (!hasNext())
					throw new NoSuchElementException();
				if (isFirstTime() || !getCurrentIterator().hasNext()){
					nextLoop();
					return While.this;
				}
				return getCurrentIterator().next();
			}
			
            public void remove() throws UnsupportedOperationException {
                throw new UnsupportedOperationException();
            }
            
            // GO TO NEXT LOOP
            
            public void nextLoop(){
            	if (isFirstTime())
					unbreakLoop();
            	// CONDITION
				if (satisfiesCondition())
					setNewCurrentIterator();
				else
					end();
            }
            
            // CURRENT ITERATOR
        	
            @Basic
        	public Iterator<Statement> getCurrentIterator(){
        		return this.iterator;
        	}
        	
        	public boolean isFirstTime(){
        		return (getCurrentIterator() == null);
        	}
        	
        	private void setNewCurrentIterator(){
        		this.iterator = getBody().iterator();
        	}
        	
        	private Iterator<Statement> iterator = null;
        	
        	// END
        	
        	@Basic
        	public boolean hasEnded(){
        		return this.hasEnded;
        	}
        	
        	private void end(){
        		this.hasEnded = true;
        	}
        	
        	private boolean hasEnded;
		};
	}
	
}
