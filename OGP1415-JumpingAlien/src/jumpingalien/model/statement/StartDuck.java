package jumpingalien.model.statement;

import jumpingalien.model.*;

public class StartDuck extends Action {
	
	@Override
	public void execute() {
		GameObject gameObject = getProgram().getExecutingNpc();
		if (gameObject instanceof Duckable){
			((Duckable) gameObject).startDuck();
		}
	}
	
	@Override
	public void cascadeProgramToChildExpressions() {
		// pass
	}

	

}
