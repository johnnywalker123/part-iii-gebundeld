package jumpingalien.model.statement;

import be.kuleuven.cs.som.annotate.Basic;
import jumpingalien.model.HorizontalDirection;
import jumpingalien.model.expression.Expression;

public class StartRun extends Action {
	
	// CONSTRUCTOR
	public StartRun(Expression<HorizontalDirection> direction){
		this.direction = direction;
	}
	
	// IS WELL FORMED
	
	@Override
	public boolean satisfiesConstraints() {
		return getDirection() != null && getDirection().satisfiesConstraints();
	}
	
	// EXECUTE
	@Override
	public void execute(){
		getProgram().getExecutingNpc().startMove(getDirection().getValue());
	}
	
	// CASCADE PROGRAM
	
	@Override
	public void cascadeProgramToChildExpressions() {
		getDirection().setProgram(getProgram());
	};
	
	//ATTRIBUTES
	
	@Basic
	public Expression<HorizontalDirection> getDirection(){
		return this.direction;
	}
	
	private final Expression<HorizontalDirection> direction;
}
