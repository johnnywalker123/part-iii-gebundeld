package jumpingalien.model.statement;

import jumpingalien.model.*;
import jumpingalien.model.type.ObjectType;

public enum Kind {
	MAZUB{
		public boolean isKind(ObjectType o){
			return (o instanceof Mazub);
		}
	}, BUZAM{
		public boolean isKind(ObjectType o){
			return (o instanceof Buzam);
		}
	}, SLIME{
		public boolean isKind(ObjectType o){
			return (o instanceof Slime);
		}
	}, SHARK{
		public boolean isKind(ObjectType o){
			return (o instanceof Shark);
		}
	}, PLANT{
		public boolean isKind(ObjectType o){
			return (o instanceof Plant);
		}
	}, TERRAIN{
		public boolean isKind(ObjectType o){
			return (o instanceof Tile);
		}
	}, ANY{
		public boolean isKind(ObjectType o){
			return (o instanceof ObjectType);
		}
	};
	
	public abstract boolean isKind(ObjectType o);
}
