package jumpingalien.part3.tests;

import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static org.junit.Assert.*;
import static org.junit.Assume.*;

import java.io.IOException;
import java.util.Optional;

import jumpingalien.model.Buzam;
import jumpingalien.model.School;
import jumpingalien.model.Shark;
import jumpingalien.model.Slime;
import jumpingalien.model.Tile;
import jumpingalien.model.World;
import jumpingalien.model.expression.Expression;
import jumpingalien.model.program.*;
import jumpingalien.model.statement.Statement;
import jumpingalien.model.type.BoolType;
import jumpingalien.model.type.DoubleType;
import jumpingalien.model.type.Type;
import jumpingalien.part3.facade.Facade;
import jumpingalien.part3.facade.IFacadePart3;
import jumpingalien.part3.programs.IProgramFactory;
import jumpingalien.part3.programs.ParseOutcome;
import jumpingalien.part3.programs.ProgramFactory;
import jumpingalien.part3.programs.ProgramParser;

import org.junit.Test;

public class ProgramTest {
	
	public static final String PLANT_PROGRAM_FILENAME = "resources/programs/plant.txt";
	public static final String SHARK_PROGRAM_FILENAME = "resources/programs/shark.txt";
	public static final String SLIME_PROGRAM_FILENAME = "resources/programs/slime.txt";
	public static final String BUZAM_PROGRAM_FILENAME = "resources/programs/buzam.txt";
	public static final String EXAMPLE_2_FILENAME = "resources/programs/program_example_2.txt";
	public static final String FOR_EACH_ACTION = "src/jumpingalien/part3/tests/foreach_with_action.txt";
	public static final String BREAK_INSIDE_LOOP = "src/jumpingalien/part3/tests/break_inside_loop.txt";
	public static final String OWN_PROGRAM = "src/jumpingalien/part3/tests/own_example_program.txt";
	public static final String BIG_EXPRESSION_TEST1 = "src/jumpingalien/part3/tests/big_expression_test.txt";
	public static final String BIG_EXPRESSION_TEST_2 = "src/jumpingalien/part3/tests/big_expression_test2.txt";
	public static final String SEARCH_DIRECTION_TEST = "src/jumpingalien/part3/tests/search_object_test.txt";
	public static final String FOREACH_SORT = "src/jumpingalien/part3/tests/foreach_sort.txt";
	

	

	
	public static final int FEATURE_AIR = 0;
	public static final int FEATURE_SOLID = 1;
	public static final int FEATURE_WATER = 2;
	public static final int FEATURE_MAGMA = 3;
	
	public ParseOutcome<?> parseFile(String filename) throws IOException {
		IProgramFactory<Expression<? extends Type>, Statement, Class<? extends Type>, Program> factory
				= new ProgramFactory();
		ProgramParser<Expression<? extends Type>, Statement, Class<? extends Type>, Program> parser
				= new ProgramParser<>(factory);
		Optional<Program> parseResult = parser.parseFile(filename);
		if (parseResult.isPresent())
			return ParseOutcome.success(parseResult.get());
		else return ParseOutcome.failure(parser.getErrors());
	}
	
	@Test
	public void testParseSimplestProgram() {
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("skip;");
		assertTrue(outcome.isSuccess());
	}

	@Test
	public void testParseFails() {
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("skip && 3;");
		assertFalse(outcome.isSuccess());
	}

	@Test
	public void testBreakNotWellformed() {
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("double d := 1.0; break;");
		assumeTrue(outcome.isSuccess());
		assertFalse(facade.isWellFormed((Program) outcome.getResult()));
	}
	
	@Test
	public void testWellformed() {
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("double d := 1.0; while d < 3 do if random 2 <= 1 then break; fi done");
		assumeTrue(outcome.isSuccess());
		assertTrue(facade.isWellFormed((Program) outcome.getResult()));
	}

	@Test
	public void testWellFormedProgramExample1(){
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("object o := null; while (! isdead (self)) do  o := searchobj ( left );  if (isslime(o)) then   start_run left;  wait(0.1);  else start_duck();  start_run (right);  fi done ");
		assumeTrue(outcome.isSuccess());
		assertTrue(facade.isWellFormed((Program) outcome.getResult()));
	}
	
	@Test
	public void testWellFormedProgramExample2() throws IOException{
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = parseFile(EXAMPLE_2_FILENAME);
		assumeTrue(outcome.isSuccess());
		assertTrue(facade.isWellFormed((Program) outcome.getResult()));
	}
	
	@Test
	public void testWellFormedProgramExampleBuzam() throws IOException{
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = parseFile(BUZAM_PROGRAM_FILENAME);
		assumeTrue(outcome.isSuccess());
		assertTrue(facade.isWellFormed((Program) outcome.getResult()));
	}
	
	@Test
	public void testWellFormedProgramExamplePlant() throws IOException{
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = parseFile(BUZAM_PROGRAM_FILENAME);
		assumeTrue(outcome.isSuccess());
		assertTrue(facade.isWellFormed((Program) outcome.getResult()));
	}
	
	@Test
	public void testWellFormed_False() throws IOException{
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = parseFile(FOR_EACH_ACTION);
		assumeTrue(outcome.isSuccess());
		assertFalse(facade.isWellFormed((Program) outcome.getResult()));
	}
	
	
	@Test
	public void testBreakWellformedWithinLoops() throws IOException {
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = parseFile(BREAK_INSIDE_LOOP);
		assumeTrue(outcome.isSuccess());
		assertTrue(facade.isWellFormed((Program) outcome.getResult()));
	}
	
	@Test
	public void testWellformedAddition() throws IOException {
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("double d := 1.0; double c := 0; double e; e := d+c;");
		assumeTrue(outcome.isSuccess());
		assertTrue(facade.isWellFormed((Program) outcome.getResult()));
	}
	
	@Test
	public void testNotWellformedAddition() throws IOException {
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("double d := 1.0; bool c; double e; c := true || false;  e := d+c;");
		assumeTrue(outcome.isSuccess());
		assertFalse(facade.isWellFormed((Program) outcome.getResult()));
	}
	
	@Test
	public void testNotWellformedAssignBoolToDouble() throws IOException {
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("bool d := true; double e:= 5.0; double c; double a; c := random e; a := d || c;");
		assumeTrue(outcome.isSuccess());
		assertFalse(facade.isWellFormed((Program) outcome.getResult()));
	}
	
	@Test
	public void testNotWellformedOR() throws IOException {
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("double d := 1.0; bool c; double e; c := true || d;  e := d+c;");
		assumeTrue(outcome.isSuccess());
		assertFalse(facade.isWellFormed((Program) outcome.getResult()));
	}
	
	@Test
	public void testTiming() throws IOException {
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = parseFile(OWN_PROGRAM);
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();
		assertTrue(facade.isWellFormed((Program) outcome.getResult()));
		facade.createSharkWithProgram(0, 0, spriteArrayForSize(4,4,3), program); // program needs npc to run
		program.execute(0.001);
		assertTrue(program.getGlobalVariableAt("a").getValue().equals(new DoubleType(1.0)));
		program.execute(0.005);
		assertTrue(program.getGlobalVariableAt("a").getValue().equals(new DoubleType(2.0)));
		for (int i=1; i<= 100; i++){
			DoubleType previous_a = (DoubleType) (program.getGlobalVariableAt("a").getValue());
			program.execute(0.006);
			assertTrue(program.getGlobalVariableAt("a").getValue().equals(previous_a.plus(new DoubleType(1.0))));
		}
	}
	
	
	@Test
	public void ExpressionArithmeticComparisonLogicTest() throws IOException {
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = parseFile(BIG_EXPRESSION_TEST1);
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();
		assertTrue(facade.isWellFormed((Program) outcome.getResult()));
		facade.createSharkWithProgram(0, 0, spriteArrayForSize(4,4,3), program); // program needs npc to run
		program.execute(9);
		assertTrue( ((DoubleType) program.getGlobalVariableAt("e").getValue()).equals(new DoubleType(29.0)).isTrue());
		assertTrue( ( (BoolType) program.getGlobalVariableAt("shouldBeTrue1").getValue()).isTrue());
		assertTrue( ( (BoolType) program.getGlobalVariableAt("shouldBeTrue2").getValue()).isTrue());
		assertTrue( ( (BoolType) program.getGlobalVariableAt("shouldBeTrue3").getValue()).isTrue());
		assertTrue( ( (BoolType) program.getGlobalVariableAt("shouldBeTrue4").getValue()).isTrue());
		assertFalse( ( (BoolType) program.getGlobalVariableAt("shouldBeFalse1").getValue()).isTrue());
		assertFalse( ( (BoolType) program.getGlobalVariableAt("shouldBeFalse2").getValue()).isTrue());
		assertFalse( ( (BoolType) program.getGlobalVariableAt("shouldBeFalse3").getValue()).isTrue());
		assertFalse( ( (BoolType) program.getGlobalVariableAt("shouldBeFalse4").getValue()).isTrue());
	}
	
	@Test
	public void ExpressionInspectorsTest() throws IOException{
		IFacadePart3 facade = new Facade();
		
		World world2;
		Slime slime1;
		School schoolA;
		Shark shark;
		Buzam alienOnGround;
		
				// CREATE GOOD WORLD LAYOUT: Lots of solid tiles
				world2 = facade.createWorld(500, 6, 5, 1, 1, 4, 3);
				for(int i = 0; i < 6; i++)
					facade.setGeologicalFeature(world2, i, 0, FEATURE_SOLID);
				facade.setGeologicalFeature(world2, 0, 1, FEATURE_SOLID);
				facade.setGeologicalFeature(world2, 4, 1, FEATURE_SOLID);
				facade.setGeologicalFeature(world2, 5, 1, FEATURE_SOLID);
				facade.setGeologicalFeature(world2, 5, 2, FEATURE_SOLID);
				facade.setGeologicalFeature(world2, 3, 2, FEATURE_SOLID);
				facade.setGeologicalFeature(world2, 2, 2, FEATURE_SOLID);
				for(int i = 0; i < 4; i++)
					facade.setGeologicalFeature(world2, i, 0, FEATURE_SOLID);
				
				
				
				// ADD SOME OTHER OBJECTS
				schoolA =facade.createSchool();
				slime1 = facade.createSlime(1200, 500, spriteArrayForSize(4,4,3), schoolA);
				shark = facade.createShark(1300, 550, spriteArrayForSize(4,4,3));
				facade.addSlime(world2, slime1);
				facade.addShark(world2, shark);
				
				
		ParseOutcome<?> outcome = parseFile(BIG_EXPRESSION_TEST_2);
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();
		assertTrue(facade.isWellFormed((Program) outcome.getResult()));
		// SET Buzam WITH PROGRAM ON GROUND
		alienOnGround = facade.createBuzamWithProgram(1250, 499, spriteArrayForSize(3, 3), program);
		facade.addBuzam(world2, alienOnGround);
		program.execute(9);
		assertTrue( ((DoubleType) program.getGlobalVariableAt("a").getValue()).equals(new DoubleType(1250.0)).isTrue());
		assertTrue( ((DoubleType) program.getGlobalVariableAt("b").getValue()).equals(new DoubleType(499.0)).isTrue());
		assertTrue( ((DoubleType) program.getGlobalVariableAt("c").getValue()).equals(new DoubleType(3.0)).isTrue());
		assertTrue( ((DoubleType) program.getGlobalVariableAt("d").getValue()).equals(new DoubleType(3.0)).isTrue());
		assertTrue( ((DoubleType) program.getGlobalVariableAt("e").getValue()).equals(new DoubleType(500)).isTrue());
		assertTrue( ((DoubleType) program.getGlobalVariableAt("f").getValue()).equals(new DoubleType(0.0)).isTrue());
		assertTrue( ( (BoolType) program.getGlobalVariableAt("shouldBeTrue1").getValue()).isTrue());
	}
	
	@Test
	public void testSearchObjectAllDirections() throws IOException {
		IFacadePart3 facade = new Facade();
		
		World world2;
		Slime slime1;
		Slime slime2;
		Slime slime3;
		School schoolA;
		Shark shark;
		Buzam alienOnGround;
		
				// CREATE GOOD WORLD LAYOUT: Lots of solid tiles
				world2 = facade.createWorld(500, 6, 5, 1, 1, 4, 3);
				for(int i = 0; i < 6; i++)
					facade.setGeologicalFeature(world2, i, 0, FEATURE_SOLID);
				facade.setGeologicalFeature(world2, 0, 1, FEATURE_SOLID);
				facade.setGeologicalFeature(world2, 4, 1, FEATURE_SOLID);
				facade.setGeologicalFeature(world2, 5, 1, FEATURE_SOLID);
				facade.setGeologicalFeature(world2, 5, 2, FEATURE_SOLID);
				facade.setGeologicalFeature(world2, 3, 2, FEATURE_SOLID);
				facade.setGeologicalFeature(world2, 2, 3, FEATURE_SOLID);
				for(int i = 0; i < 4; i++)
					facade.setGeologicalFeature(world2, i, 0, FEATURE_SOLID);
				
				
				
				// ADD SOME OTHER OBJECTS
				schoolA =facade.createSchool();
				slime2 = facade.createSlime(1225, 500, spriteArrayForSize(4,4,3), schoolA); // closest left
				slime1 = facade.createSlime(1200, 500, spriteArrayForSize(4,4,3), schoolA); // left, but not closest
				slime3 = facade.createSlime(1252, 2010, spriteArrayForSize(4,4,3), schoolA); // up, not closest, left in range
				shark = facade.createShark(1300, 501, spriteArrayForSize(10,10,3)); // closest right, bottom in range
				facade.addSlime(world2, slime1);
				facade.addSlime(world2, slime2);
				facade.addSlime(world2, slime3);
				facade.addShark(world2, shark);
				
				
		ParseOutcome<?> outcome = parseFile(SEARCH_DIRECTION_TEST);
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();
		assertTrue(facade.isWellFormed((Program) outcome.getResult()));
		// SET Buzam WITH PROGRAM ON GROUND
		alienOnGround = facade.createBuzamWithProgram(1250, 499, spriteArrayForSize(4, 4), program);
		facade.addBuzam(world2, alienOnGround);
		program.execute(9);
		assertTrue(program.getGlobalVariableAt("leftObject").getValue().equals(slime2));
		assertTrue(program.getGlobalVariableAt("rightObject").getValue().equals(shark));
		assertTrue(program.getGlobalVariableAt("upObject").getValue().equals(new Tile(2, 3, world2))); // closest up is a tile
		assertTrue(program.getGlobalVariableAt("downObject").getValue().equals(new Tile(2, 0, world2))); // closest down is a tile
	}
	
	@Test
	public void ForEach_sort() throws IOException {
		IFacadePart3 facade = new Facade();
		
		World world2;
		Slime slime_left;
		Slime slime_middle;
		Slime slime_right;
		School schoolA;
		Buzam alienOnGround;
		
				// CREATE GOOD WORLD LAYOUT: Lots of solid tiles
				world2 = facade.createWorld(500, 6, 5, 1, 1, 4, 3);
				for(int i = 0; i < 6; i++)
					facade.setGeologicalFeature(world2, i, 0, FEATURE_SOLID);
				facade.setGeologicalFeature(world2, 0, 1, FEATURE_SOLID);
				facade.setGeologicalFeature(world2, 4, 1, FEATURE_SOLID);
				facade.setGeologicalFeature(world2, 5, 1, FEATURE_SOLID);
				facade.setGeologicalFeature(world2, 5, 2, FEATURE_SOLID);
				facade.setGeologicalFeature(world2, 3, 2, FEATURE_SOLID);
				facade.setGeologicalFeature(world2, 2, 3, FEATURE_SOLID);
				for(int i = 0; i < 4; i++)
					facade.setGeologicalFeature(world2, i, 0, FEATURE_SOLID);
				
				
				
				// ADD SOME OTHER OBJECTS
				schoolA =facade.createSchool();
				slime_left = facade.createSlime(1100, 500, spriteArrayForSize(4,4,3), schoolA);
				slime_middle = facade.createSlime(1200, 500, spriteArrayForSize(4,4,3), schoolA);
				slime_right = facade.createSlime(1300, 500, spriteArrayForSize(4,4,3), schoolA); 
				facade.addSlime(world2, slime_left);
				facade.addSlime(world2, slime_middle);
				facade.addSlime(world2, slime_right);
				
				
		ParseOutcome<?> outcome = parseFile(FOREACH_SORT);
		System.out.println(outcome.isSuccess());
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();
		assertTrue(facade.isWellFormed((Program) outcome.getResult()));
		// SET Buzam WITH PROGRAM ON GROUND
		alienOnGround = facade.createBuzamWithProgram(700, 499, spriteArrayForSize(4, 4), program);
		facade.addBuzam(world2, alienOnGround);
		program.execute(0.020);
		assertTrue(alienOnGround.isRunning());
		assertFalse(alienOnGround.isDucking());
		assertFalse(alienOnGround.getYPosition() > 499);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
