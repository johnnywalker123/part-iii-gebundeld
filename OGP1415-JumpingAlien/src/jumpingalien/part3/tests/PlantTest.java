package jumpingalien.part3.tests;

import org.junit.Before;
import org.junit.Test;

import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static org.junit.Assert.*;
import jumpingalien.model.HorizontalDirection;
import jumpingalien.model.Mazub;
import jumpingalien.model.Plant;
import jumpingalien.model.School;
import jumpingalien.model.Slime;
import jumpingalien.model.World;
import jumpingalien.part3.facade.Facade;
import jumpingalien.part3.facade.IFacadePart3;


public class PlantTest {

		private static IFacadePart3 facade;
		private static World world;
		private static Plant plant;
		private static Plant plant2;
		private static Slime slime;
		private static Mazub alienX395Y499;
		private static Mazub alienX1000Y499;
		private static School school;
		
		public static final int FEATURE_AIR = 0;
		public static final int FEATURE_SOLID = 1;
		public static final int FEATURE_WATER = 2;
		public static final int FEATURE_MAGMA = 3;
	
	@Before
	public void setUpMutableFixtures(){
		facade = new Facade();
		world = facade.createWorld(500, 4, 6, 1, 1, 4, 3);
		school =facade.createSchool();
		slime = facade.createSlime(395, 499, spriteArrayForSize(4,4,3), school);
		for(int i = 0; i < 4; i++)
			facade.setGeologicalFeature(world, i, 0, FEATURE_SOLID);
		alienX395Y499 = facade.createMazub(395, 499, spriteArrayForSize(3, 3)); // overlapt 1 pixel met tile
		alienX1000Y499 = facade.createMazub(1000, 499, spriteArrayForSize(3, 3));
		plant = facade.createPlant(400, 501, spriteArrayForSize(1,1,3));
		plant2 = facade.createPlant(402, 501, spriteArrayForSize(1,1,3));
	} 
	
	@Test
	public void AlienEatsPlantWhenHungry(){
		facade.addPlant(world, plant);
		facade.addPlant(world, plant2);
		facade.setMazub(world, alienX395Y499);
		facade.startGame(world);
		assertTrue (alienX395Y499.getHitPoints() == 100);
		assertTrue(alienX395Y499.isHungry());
		assertTrue(plant.isAlive());
		assertFalse(alienX395Y499.collidesWith(plant));
		facade.startMoveRight(alienX395Y499);
		for (int i = 0; i < 40; i++) {
			facade.advanceTime(world, 0.01);
			if (plant.collidesWith(alienX395Y499)){
				plant.updateHitPointsUponCollisionWith(alienX395Y499);
			}
		}
		assertTrue(world.getAllPlants().contains(plant));
		assertTrue(plant.isInTerminationProcess());
		assertTrue(plant.getHitPoints() == 0);
		assertTrue(alienX395Y499.getHitPoints() == 200);
		for (int i = 0; i < 60; i++) {
			facade.advanceTime(world, 0.01);
		}
		assertFalse(world.getAllPlants().contains(plant));
		assertTrue(plant.isTerminated());
	}
	
	@Test
	public void PlantAlternatesEveryHalfASecondAndStartsAtLeft(){
		facade.addPlant(world, plant);
		facade.setMazub(world, alienX1000Y499);
		facade.startGame(world);
		assertTrue(plant.getHorizontalDirection() == HorizontalDirection.LEFT);
		for (int i = 0; i < 49; i++) {
			facade.advanceTime(world, 0.01);
		}
		assertTrue(plant.getHorizontalDirection() == HorizontalDirection.LEFT);
		facade.advanceTime(world, 0.01);
		
		assertTrue(plant.getHorizontalDirection() == HorizontalDirection.RIGHT);
		for (int i=0; i<20; i++){
			facade.advanceTime(world, 0.01);
		}
		assertTrue(plant.getHorizontalDirection() == HorizontalDirection.RIGHT);
		for (int i=0; i<30; i++){
			facade.advanceTime(world, 0.01);
		}
		System.out.println(plant.getTime());
		assertTrue(plant.getHorizontalDirection() == HorizontalDirection.LEFT);
	}
	
	@Test
	public void PlantDoesNotInteractWithSlime(){
		facade.addPlant(world, plant);
		facade.addSlime(world, slime);
		facade.setMazub(world, alienX1000Y499);
		facade.startGame(world);
		for (int i = 0; i < 20000; i++) {
			facade.advanceTime(world, 0.05);
			//probabilistisch
			if (plant.collidesWith(slime))
				assertFalse(plant.shouldUpdateAnyHitPointsUponCollisionWith(slime));
		}
		// Plant onaangetast
		assertTrue(world.hasAsPlant(plant) && plant.isAlive());
	}
	
	
	
	
	
}
