package jumpingalien.part3.tests;

import org.junit.Test;
import jumpingalien.model.GeologicalFeature;
import static org.junit.Assert.*;

public class GeologicalFeatureTest {
	@Test
	public void isImpassable_True(){
		assertTrue(GeologicalFeature.SOLID.isImpassable());
	}
	
	@Test
	public void isImpassable_False(){
		assertFalse(GeologicalFeature.AIR.isImpassable());
		assertFalse(GeologicalFeature.MAGMA.isImpassable());
		assertFalse(GeologicalFeature.WATER.isImpassable());
	}
}
