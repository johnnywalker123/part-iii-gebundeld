package jumpingalien.part3.tests;

import static jumpingalien.tests.util.TestUtils.doubleArray;
import static jumpingalien.tests.util.TestUtils.intArray;
import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import static jumpingalien.util.Util.*;
import jumpingalien.model.Buzam;
import jumpingalien.model.HorizontalDirection;
import jumpingalien.model.Mazub;
import jumpingalien.model.School;
import jumpingalien.model.Shark;
import jumpingalien.model.Slime;
import jumpingalien.model.TimeObject;
import jumpingalien.model.World;
import jumpingalien.part1.facade.IFacade;
import jumpingalien.part3.facade.Facade;
import jumpingalien.part3.facade.IFacadePart3;
import jumpingalien.util.Sprite;
import jumpingalien.util.Util;

public class BuzamTest {
	private static IFacadePart3 facade;
	private static Buzam buzam;
	private static World world;
	private static World world2;
	private static Slime slime1;
	private static School schoolA;
	private static Shark shark;
	private static Buzam buzamOnGround;
	private static Mazub mazub;
	private static Mazub mazubOnGround;
	
	
	public static final int FEATURE_AIR = 0;
	public static final int FEATURE_SOLID = 1;
	public static final int FEATURE_WATER = 2;
	public static final int FEATURE_MAGMA = 3;

	
	@Before
	public void setUpMutableFixtures(){
		// Create facade
		facade = new Facade();
		
		// Create Mazub, Buzam & world
		buzam = facade.createBuzam(0, 499, spriteArrayForSize(3, 3)); // overlapt 1 pixel met tile
		mazub = facade.createMazub(1800, 499, spriteArrayForSize(3,3));
		world = facade.createWorld(500, 4, 6, 1, 1, 4, 3);
		for(int i = 0; i < 4; i++)
			facade.setGeologicalFeature(world, i, 0, FEATURE_SOLID);
		
		// Set mazub and buzam in world
		facade.addBuzam(world, buzam);
		facade.setMazub(world, mazub);
		
		
		
		// CREATE GOOD WORLD LAYOUT: Lots of solid tiles
		world2 = facade.createWorld(500, 6, 5, 1, 1, 4, 3);
		for(int i = 0; i < 6; i++)
			facade.setGeologicalFeature(world2, i, 0, FEATURE_SOLID);
		facade.setGeologicalFeature(world2, 0, 1, FEATURE_SOLID);
		facade.setGeologicalFeature(world2, 4, 1, FEATURE_SOLID);
		facade.setGeologicalFeature(world2, 5, 1, FEATURE_SOLID);
		facade.setGeologicalFeature(world2, 5, 2, FEATURE_SOLID);
		facade.setGeologicalFeature(world2, 3, 2, FEATURE_SOLID);
		facade.setGeologicalFeature(world2, 2, 2, FEATURE_SOLID);
		for(int i = 0; i < 4; i++)
			facade.setGeologicalFeature(world2, i, 0, FEATURE_SOLID);
		
		// SET MAZUB AND BUZAM ON GROUND
		buzamOnGround = facade.createBuzam(1250, 499, spriteArrayForSize(3, 3));
		mazubOnGround = facade.createMazub(1500, 499, spriteArrayForSize(3,3));
		facade.addBuzam(world2, buzamOnGround);
		facade.setMazub(world2, mazubOnGround);
		
		// CREATE SOME OTHER OBJECTS
		schoolA =facade.createSchool();
		slime1 = facade.createSlime(1200, 500, spriteArrayForSize(4,4,3), schoolA);
		shark = facade.createShark(1200, 50, spriteArrayForSize(4,4,3));
		
		
	}
	
	@Test
	public void Buzam_SatisfyClassInvariantAfterCreation(){
		assertTrue(buzamOnGround.hasProperJumper());
		assertTrue(buzamOnGround.hasProperDucker());
		assertTrue(buzamOnGround.canHaveAsIndices(buzamOnGround.getCurrentIndices()));
		assertTrue(Buzam.isValidMovement(buzamOnGround.getLastMovement()));
		assertTrue(Buzam.isValidTime(buzamOnGround.getAlternationTimer()));
	}
	
	@Test
	public void MultipleOngoingMovementsCorrect(){
		facade.startGame(world);
		
		buzamOnGround.startMove(HorizontalDirection.RIGHT);
		buzamOnGround.startMove(HorizontalDirection.LEFT);
		
		buzamOnGround.endMove(HorizontalDirection.LEFT);
		assertTrue(buzamOnGround.isRunning() || buzamOnGround.getHorizontalDirection() == HorizontalDirection.RIGHT);
		buzamOnGround.startMove(HorizontalDirection.LEFT);
		
		buzamOnGround.endMove(HorizontalDirection.RIGHT);
		assertTrue(buzamOnGround.isRunning() || buzamOnGround.getHorizontalDirection() == HorizontalDirection.LEFT);
		buzamOnGround.endMove(HorizontalDirection.LEFT);
		assertFalse(buzamOnGround.isRunning());
		
	} 
	
	@Test
	public void BuzamDiesFromLava(){
		facade.setGeologicalFeature(world2, 2, 1, FEATURE_MAGMA);
		facade.startGame(world2);
		facade.advanceTime(world2, 0.1);
		assertTrue(buzamOnGround.getHitPoints() == 450);
		for (int i = 0; i <= 89; i++)
			facade.advanceTime(world2, 0.1);
		assertTrue(buzamOnGround.getHitPoints() == 0);
		assertTrue(world2.getBuzam() == null);
	}
	
	@Test
	public void BuzamCollidesWithSlimeAndHitPointsCorrect(){
		facade.addSlime(world2, slime1);
		facade.startGame(world2);
		while (!buzamOnGround.hasAsCollidingObject(slime1)){
			facade.advanceTime(world2, 0.1);
		}
		assertTrue(buzamOnGround.getHitPoints() == 450);
		assertTrue(slime1.getHitPoints() == 50);
		
	}
	
	
	@Test
	public void hasMovedLastSecond_AfterCreation(){
		assertFalse(buzam.hasMovedLastSecond());
	}
	
	
	@Test
	public void hasMovedLastSecond_HasMoved() throws Exception{
		facade.startGame(world);
		
		buzam.startMove(HorizontalDirection.RIGHT);
		facade.advanceTime(world, 0.01);
		
		buzam.endMove(HorizontalDirection.RIGHT);
		facade.advanceTime(world,0.01);
		
		assertTrue(buzam.hasMovedLastSecond());
	}
	
	@Test
	public void shouldFall_IsJumping() throws Exception{
		facade.startGame(world);
		
		buzam.startJump();
		facade.advanceTime(world, 0.001);
		
		assertTrue(buzam.shouldFall());
	}
	
	@Test
	public void endJump_NegativeYVelocity() throws Exception{
		facade.startGame(world);
		
		buzam.startJump();
		facade.advanceTime(world,0.19);
		buzam.endJump();
		
		assert(buzam.getYVelocity() <= 0);
	}
	
	@Test 
	public void shouldFall_StrictNegativeAccelerationAfterJumping() throws Exception{
		facade.startGame(world);
		
		buzam.startJump();
		facade.advanceTime(world,0.001);
		
		assert(buzam.getYAcceleration() < 0);
	}
	
	@Test
	public void updateYVelocity_ZeroYVelocityWhenImpassableTerrainBelowAfterJumping() throws Exception{
		facade.startGame(world);
		
		buzam.startJump();
		facade.advanceTime(world,0.000001);
		for (int i = 0;i<10;i++){
			facade.advanceTime(world,0.19);
		}
		
		assertFalse(buzam.shouldFall());
		assertEquals(buzam.getYVelocity(), 0, DEFAULT_EPSILON);
	}
	
	@Test
	public void getMaxXVelocity_Ducking() throws Exception{
		facade.startGame(world);
		
		buzam.startDuck();
		assertTrue(buzam.isDucking());
		assertEquals(buzam.getMaxXVelocity(),1,Util.DEFAULT_EPSILON);
		
		buzam.endDuck();
		assertFalse(buzam.isDucking());
		assertEquals(buzam.getMaxXVelocity(), 3,Util.DEFAULT_EPSILON);
	}
	
	@Test
	public void setXVelocity_Max1WhenDucking() throws Exception{
		facade.startGame(world);
		
		buzam.startMove(HorizontalDirection.RIGHT);
		for (int i = 0;i<15;i++){
			facade.advanceTime(world,0.19);
		}
		assertEquals(buzam.getXVelocity(), 3,DEFAULT_EPSILON);
		
		buzam.startDuck();
		facade.advanceTime(world, 0.19);
		assertEquals(buzam.getXVelocity(), 1, DEFAULT_EPSILON);
		}
	
	@Test
	public void hasConstantVelocity_isNotRunning() throws Exception{
		facade.startGame(world);
		
		buzam.startMove(HorizontalDirection.RIGHT);
		facade.advanceTime(world, 0.01);
		
		buzam.endMove(HorizontalDirection.RIGHT);
		facade.advanceTime(world,0.01);
		assertTrue(buzam.hasConstantXVelocity());
	}
	
	@Test
	public void hasConstantVelocity_MaxVelocity() throws Exception{
		facade.startGame(world);
		
		buzam.startMove(HorizontalDirection.RIGHT);
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 100; i++) {
			facade.advanceTime(world, 0.2 / 9);
		}
		assertTrue(buzam.hasConstantXVelocity());
	}
	
	@Test
	public void hasConstantVelocity_VariableVelocity() throws Exception{
		facade.startGame(world);
		
		buzam.startMove(HorizontalDirection.RIGHT);
		// still accelerating
		for (int i = 0; i < 88; i++) {
			facade.advanceTime(world, 0.2 / 9);
		}
		assertFalse(buzam.hasConstantXVelocity());
	}
	
	@Test
	public void canHaveAsIndex_IllegalCase(){
		assertFalse(buzam.canHaveAsIndex(buzam.getNbSprites() + 1));
	}
	
	@Test
	public void canHaveAsIndices_LegalCase(){
		int[] newArray = {1,2,buzam.getNbSprites() - 1};
		assertTrue(buzam.canHaveAsIndices(newArray));
	}
	
	@Test
	public void canHaveAsTime_IllegalCase(){
		facade.startGame(world);
		
		facade.advanceTime(world, 0.015);
		assertFalse(TimeObject.isValidTime(-0.05));
	}
	
	@Test
	public void isValidTime_IllegalCase(){
		assertFalse(TimeObject.isValidTime(-5));
	}
	
	@Test
	public void isValidTimeInterval_IllegalCase(){
		assertFalse(TimeObject.isValidTimeInterval(0.3));
	}
	
	@Test
	public void stateHasChanged_BeginToRun() throws Exception{
		facade.startGame(world);
		
		int[] oldIndices = buzam.getCurrentIndices();
		buzam.startMove(HorizontalDirection.RIGHT);
		facade.advanceTime(world,0.001);
		assertTrue(buzam.stateHasChanged(oldIndices));
	}
	
	@Test
	public void startMoveRightCorrect() throws Exception{
		facade.startGame(world);
		
		buzam.startMove(HorizontalDirection.RIGHT);
		facade.advanceTime(world, 0.1);

		// x_new [m] = 0 + 1 [m/s] * 0.1 [s] + 1/2 0.9 [m/s^2] * (0.1 [s])^2 =
		// 0.1045 [m] = 10.45 [cm], which falls into pixel (10, 0)

		assertArrayEquals(intArray(10, 499), facade.getLocation(buzam));
	}
	
	@Test
	public void startMoveRightMaxSpeedAtRightTime() throws Exception{
		facade.startGame(world);
		
		buzam.startMove(HorizontalDirection.RIGHT);
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 100; i++) {
			facade.advanceTime(world, 0.2 / 9);
		}

		assertArrayEquals(doubleArray(3, 0), facade.getVelocity(buzam),
				Util.DEFAULT_EPSILON);
	}
	
	@Test
	public void testAccellerationZeroWhenNotMoving() {
		assertArrayEquals(doubleArray(0.0, 0.0), facade.getAcceleration(buzam),
				Util.DEFAULT_EPSILON);
	}
	
	@Test
	public void testWalkAnimationLastFrame() throws Exception{
		// Create world
		World newWorld = facade.createWorld(500, 10, 6, 1, 1, 4, 3);
		for(int i = 0; i < 10; i++)
			facade.setGeologicalFeature(newWorld, i, 0, FEATURE_SOLID);

		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Buzam buzam = facade.createBuzam(0, 499, sprites);
		Mazub mazub = facade.createMazub(700, 499, sprites);
		
		// Set Buzam in world
		facade.addBuzam(newWorld, buzam);
		facade.setMazub(newWorld, mazub);
		
		facade.startGame(newWorld);

		buzam.startMove(HorizontalDirection.RIGHT);
		facade.advanceTime(newWorld, 0.005);
		for (int i = 0; i < m; i++) {
			facade.advanceTime(newWorld, 0.075);
		}
		
		
		assertEquals(sprites[8+m], facade.getCurrentSprite(buzam));
	}

	

}
