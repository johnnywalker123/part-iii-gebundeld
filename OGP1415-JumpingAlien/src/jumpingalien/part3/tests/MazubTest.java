package jumpingalien.part3.tests;

import static jumpingalien.tests.util.TestUtils.doubleArray;
import static jumpingalien.tests.util.TestUtils.intArray;
import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import static jumpingalien.util.Util.*;
import jumpingalien.model.HorizontalDirection;
import jumpingalien.model.Mazub;
import jumpingalien.model.School;
import jumpingalien.model.Shark;
import jumpingalien.model.Slime;
import jumpingalien.model.TimeObject;
import jumpingalien.model.World;
import jumpingalien.part1.facade.IFacade;
import jumpingalien.part3.facade.Facade;
import jumpingalien.part3.facade.IFacadePart3;
import jumpingalien.util.Sprite;
import jumpingalien.util.Util;

public class MazubTest {
	private static IFacadePart3 facade;
	private static Mazub alien;
	private static World world;
	private static World world2;
	private static Slime slime1;
	private static School schoolA;
	private static Shark shark;
	private static Mazub alienOnGround;
	
	
	public static final int FEATURE_AIR = 0;
	public static final int FEATURE_SOLID = 1;
	public static final int FEATURE_WATER = 2;
	public static final int FEATURE_MAGMA = 3;

	
	@Before
	public void setUpMutableFixtures(){
		// Create facade
		facade = new Facade();
		
		// Create Mazub & world
		alien = facade.createMazub(0, 499, spriteArrayForSize(3, 3)); // overlapt 1 pixel met tile
		world = facade.createWorld(500, 4, 6, 1, 1, 4, 3);
		for(int i = 0; i < 4; i++)
			facade.setGeologicalFeature(world, i, 0, FEATURE_SOLID);
		
		// Set mazub in world
		facade.setMazub(world, alien);
		
		
		
		// CREATE GOOD WORLD LAYOUT: Lots of solid tiles
		world2 = facade.createWorld(500, 6, 5, 1, 1, 4, 3);
		for(int i = 0; i < 6; i++)
			facade.setGeologicalFeature(world2, i, 0, FEATURE_SOLID);
		facade.setGeologicalFeature(world2, 0, 1, FEATURE_SOLID);
		facade.setGeologicalFeature(world2, 4, 1, FEATURE_SOLID);
		facade.setGeologicalFeature(world2, 5, 1, FEATURE_SOLID);
		facade.setGeologicalFeature(world2, 5, 2, FEATURE_SOLID);
		facade.setGeologicalFeature(world2, 3, 2, FEATURE_SOLID);
		facade.setGeologicalFeature(world2, 2, 2, FEATURE_SOLID);
		for(int i = 0; i < 4; i++)
			facade.setGeologicalFeature(world2, i, 0, FEATURE_SOLID);
		
		// SET MAZUB ON GROUND
		alienOnGround = facade.createMazub(1250, 499, spriteArrayForSize(3, 3));
		facade.setMazub(world2, alienOnGround);
		
		// CREATE SOME OTHER OBJECTS
		schoolA =facade.createSchool();
		slime1 = facade.createSlime(1200, 500, spriteArrayForSize(4,4,3), schoolA);
		shark = facade.createShark(1200, 50, spriteArrayForSize(4,4,3));
		
		
	}
	
	@Test
	public void Mazub_SatisfyClassInvariantAfterCreation(){
		assertTrue(alienOnGround.hasProperJumper());
		assertTrue(alienOnGround.hasProperDucker());
		assertTrue(alienOnGround.canHaveAsIndices(alienOnGround.getCurrentIndices()));
		assertTrue(Mazub.isValidMovement(alienOnGround.getLastMovement()));
		assertTrue(Mazub.isValidTime(alienOnGround.getAlternationTimer()));
	}
	
	@Test
	public void MultipleOngoingMovementsCorrect(){
		facade.startGame(world);
		
		facade.startMoveRight(alienOnGround);
		facade.startMoveLeft(alienOnGround);
		
		facade.endMoveLeft(alienOnGround);
		assertTrue(alienOnGround.isRunning() || alienOnGround.getHorizontalDirection() == HorizontalDirection.RIGHT);
		facade.startMoveLeft(alienOnGround);
		
		facade.endMoveRight(alienOnGround);
		assertTrue(alienOnGround.isRunning() || alienOnGround.getHorizontalDirection() == HorizontalDirection.LEFT);
		
		facade.endMoveLeft(alienOnGround);
		assertFalse(alienOnGround.isRunning());
		
	} 
	
	@Test
	public void MazubDiesFromLavaAndIsTerminatedInstantly(){
		facade.setGeologicalFeature(world2, 2, 1, FEATURE_MAGMA);
		facade.startGame(world2);
		facade.advanceTime(world2, 0.1);
		assertTrue(alienOnGround.getHitPoints() == 50);
		facade.advanceTime(world2, 0.1);
		assertTrue(alienOnGround.getHitPoints() == 0);
		assertTrue(alienOnGround.isTerminated());
		assertTrue(world2.hasAsMazub(null));
	}
	
	@Test
	public void MazubCollidesWithSlimeAndHitPointsCorrect(){
		facade.addSlime(world2, slime1);
		facade.startGame(world2);
		while (!alienOnGround.hasAsCollidingObject(slime1)){
			facade.advanceTime(world2, 0.1);
		}
		assertTrue(alienOnGround.getHitPoints() == 50);
		assertTrue(slime1.getHitPoints() == 50);
		
	}
	
	
	@Test
	public void hasMovedLastSecond_AfterCreation(){
		assertFalse(alien.hasMovedLastSecond());
	}
	
	
	@Test
	public void hasMovedLastSecond_HasMoved() throws Exception{
		facade.startGame(world);
		
		facade.startMoveRight(alien);
		facade.advanceTime(world, 0.01);
		
		facade.endMoveRight(alien);
		facade.advanceTime(world,0.01);
		
		assertTrue(alien.hasMovedLastSecond());
	}
	
	@Test
	public void shouldFall_IsJumping() throws Exception{
		facade.startGame(world);
		
		facade.startJump(alien);
		facade.advanceTime(world, 0.001);
		
		assertTrue(alien.shouldFall());
	}
	
	@Test
	public void endJump_NegativeYVelocity() throws Exception{
		facade.startGame(world);
		
		facade.startJump(alien);
		facade.advanceTime(world,0.19);
		facade.endJump(alien);
		
		assert(alien.getYVelocity() <= 0);
	}
	
	@Test 
	public void shouldFall_StrictNegativeAccelerationAfterJumping() throws Exception{
		facade.startGame(world);
		
		facade.startJump(alien);
		facade.advanceTime(world,0.001);
		
		assert(alien.getYAcceleration() < 0);
	}
	
	@Test
	public void updateYVelocity_ZeroYVelocityWhenImpassableTerrainBelowAfterJumping() throws Exception{
		facade.startGame(world);
		
		facade.startJump(alien);
		facade.advanceTime(world,0.000001);
		for (int i = 0;i<10;i++){
			facade.advanceTime(world,0.19);
		}
		
		assertFalse(alien.shouldFall());
		assertEquals(alien.getYVelocity(), 0, DEFAULT_EPSILON);
	}
	
	@Test
	public void getMaxXVelocity_Ducking() throws Exception{
		facade.startGame(world);
		
		facade.startDuck(alien);
		assertTrue(alien.isDucking());
		assertEquals(alien.getMaxXVelocity(),1,Util.DEFAULT_EPSILON);
		
		facade.endDuck(alien);
		assertFalse(alien.isDucking());
		assertEquals(alien.getMaxXVelocity(), 3,Util.DEFAULT_EPSILON);
	}
	
	@Test
	public void setXVelocity_Max1WhenDucking() throws Exception{
		facade.startGame(world);
		
		facade.startMoveRight(alien);
		for (int i = 0;i<15;i++){
			facade.advanceTime(world,0.19);
		}
		assertEquals(alien.getXVelocity(), 3,DEFAULT_EPSILON);
		
		facade.startDuck(alien);
		facade.advanceTime(world, 0.19);
		assertEquals(alien.getXVelocity(), 1, DEFAULT_EPSILON);
		}
	
	@Test
	public void hasConstantVelocity_isNotRunning() throws Exception{
		facade.startGame(world);
		
		facade.startMoveRight(alien);
		facade.advanceTime(world, 0.01);
		
		facade.endMoveRight(alien);
		facade.advanceTime(world,0.01);
		assertTrue(alien.hasConstantXVelocity());
	}
	
	@Test
	public void hasConstantVelocity_MaxVelocity() throws Exception{
		facade.startGame(world);
		
		facade.startMoveRight(alien);
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 100; i++) {
			facade.advanceTime(world, 0.2 / 9);
		}
		assertTrue(alien.hasConstantXVelocity());
	}
	
	@Test
	public void hasConstantVelocity_VariableVelocity() throws Exception{
		facade.startGame(world);
		
		facade.startMoveRight(alien);
		// still accelerating
		for (int i = 0; i < 88; i++) {
			facade.advanceTime(world, 0.2 / 9);
		}
		assertFalse(alien.hasConstantXVelocity());
	}
	
	@Test
	public void canHaveAsIndex_IllegalCase(){
		assertFalse(alien.canHaveAsIndex(alien.getNbSprites() + 1));
	}
	
	@Test
	public void canHaveAsIndices_LegalCase(){
		int[] newArray = {1,2,alien.getNbSprites() - 1};
		assertTrue(alien.canHaveAsIndices(newArray));
	}
	
	@Test
	public void canHaveAsTime_IllegalCase(){
		facade.startGame(world);
		
		facade.advanceTime(world, 0.015);
		assertFalse(TimeObject.isValidTime(-0.05));
	}
	
	@Test
	public void isValidTime_IllegalCase(){
		assertFalse(TimeObject.isValidTime(-5));
	}
	
	@Test
	public void isValidTimeInterval_IllegalCase(){
		assertFalse(TimeObject.isValidTimeInterval(0.3));
	}
	
	@Test
	public void stateHasChanged_BeginToRun() throws Exception{
		facade.startGame(world);
		
		int[] oldIndices = alien.getCurrentIndices();
		alien.startMove(HorizontalDirection.RIGHT);
		facade.advanceTime(world,0.001);
		assertTrue(alien.stateHasChanged(oldIndices));
	}
	
	@Test
	public void startMoveRightCorrect() throws Exception{
		facade.startGame(world);
		
		facade.startMoveRight(alien);
		facade.advanceTime(world, 0.1);

		// x_new [m] = 0 + 1 [m/s] * 0.1 [s] + 1/2 0.9 [m/s^2] * (0.1 [s])^2 =
		// 0.1045 [m] = 10.45 [cm], which falls into pixel (10, 0)

		assertArrayEquals(intArray(10, 499), facade.getLocation(alien));
	}
	
	@Test
	public void startMoveRightMaxSpeedAtRightTime() throws Exception{
		facade.startGame(world);
		
		facade.startMoveRight(alien);
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 100; i++) {
			facade.advanceTime(world, 0.2 / 9);
		}

		assertArrayEquals(doubleArray(3, 0), facade.getVelocity(alien),
				Util.DEFAULT_EPSILON);
	}
	
	@Test
	public void testAccellerationZeroWhenNotMoving() {
		assertArrayEquals(doubleArray(0.0, 0.0), facade.getAcceleration(alien),
				Util.DEFAULT_EPSILON);
	}
	
	@Test
	public void testWalkAnimationLastFrame() throws Exception{
		// Create world
		World newWorld = facade.createWorld(500, 10, 6, 1, 1, 4, 3);
		for(int i = 0; i < 10; i++)
			facade.setGeologicalFeature(newWorld, i, 0, FEATURE_SOLID);

		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Mazub newAlien = facade.createMazub(0, 499, sprites);
		
		// Set mazub in world
		facade.setMazub(newWorld, newAlien);
		
		facade.startGame(newWorld);

		facade.startMoveRight(newAlien);
		facade.advanceTime(newWorld, 0.005);
		for (int i = 0; i < m; i++) {
			facade.advanceTime(newWorld, 0.075);
		}
		
		
		assertEquals(sprites[8+m], facade.getCurrentSprite(newAlien));
	}

	

}
