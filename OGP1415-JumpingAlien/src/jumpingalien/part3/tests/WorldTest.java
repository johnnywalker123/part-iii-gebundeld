package jumpingalien.part3.tests;

import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import jumpingalien.model.GameStatus;
import jumpingalien.model.Mazub;
import jumpingalien.model.Plant;
import jumpingalien.model.Shark;
import jumpingalien.model.World;
import jumpingalien.model.Slime;
import jumpingalien.model.School;
import jumpingalien.part3.facade.Facade;
import jumpingalien.part3.facade.IFacadePart3;
import jumpingalien.util.ModelException;

public class WorldTest {
	private static IFacadePart3 facade;;
	private static World world;
	private static Mazub alienX0Y499;
	private static Slime slime;
	private static School school;
	private static Plant plant;
	
	public static final int FEATURE_AIR = 0;
	public static final int FEATURE_SOLID = 1;
	public static final int FEATURE_WATER = 2;
	public static final int FEATURE_MAGMA = 3;
	
	@Before
	public void setUpMutableFixtures(){
		facade = new Facade();
		world = facade.createWorld(500, 4, 6, 1, 1, 4, 3);
		for(int i = 0; i < 4; i++)
			facade.setGeologicalFeature(world, i, 0, FEATURE_SOLID);
		alienX0Y499 = facade.createMazub(0, 499, spriteArrayForSize(3, 3)); // overlapt 1 pixel met tile
		school = facade.createSchool();
		slime = facade.createSlime(500, 499, spriteArrayForSize(2,2,3), school);
		plant = facade.createPlant(1000, 499, spriteArrayForSize(1,1,3));
	}
	
	@Test
	public void ConstructedObjectSatisfiesClassInvariants(){
		assertTrue(World.isValidDimension(world.getXDimension()));
		assertTrue(World.isValidDimension(world.getYDimension()));
		assertTrue(World.isValidTileLength(world.getTileLength()));
		assertTrue(world.canHaveAsXTargetTile(world.getXTargetTile()));
		assertTrue(world.canHaveAsYTargetTile(world.getYTargetTile()));
		assertTrue(world.canHaveAsWindowWidth(world.getWindowWidth()));
		assertTrue(world.canHaveAsWindowHeight(world.getWindowHeight()));
		assertTrue(world.canHaveAsWindow(world.getWindow()));
		assertTrue(world.canHaveAsGameStatus(world.getGameStatus()));
		assertTrue(World.isValidTileLength(world.getTileLength()));
		assertTrue(world.hasProperTiles());
		assertTrue(world.hasProperMazub());
		assertTrue(world.hasProperNpcs());
		assertTrue(World.isValidTime(world.getTime()));
	}
	
	@Test
	public void hasProperGameObject_True(){
		// create game objects
		Set<Plant> plants = new HashSet<>();
		Set<Slime> slimes = new HashSet<>();
		Set<Shark> sharks = new HashSet<>();
		facade.createSchool();
		for (int i = 0; i < 20; i++){
			plants.add(facade.createPlant(i*5, 2000, spriteArrayForSize(1,1,3) ));
			slimes.add(facade.createSlime(10+i*5, 499, spriteArrayForSize(2,2,2), school));
			sharks.add(facade.createShark(i*5+1000, 499, spriteArrayForSize(3,3,3)));
		}
		// add game objects to world
		for (Plant plant : plants){
			facade.addPlant(world, plant);
		}
		facade.setMazub(world,alienX0Y499);
		for (Slime slime : slimes){
			facade.addSlime(world, slime);
		}
		for (Shark shark : sharks){
			facade.addShark(world, shark);
		}	
		assertTrue(world.hasProperNpcs());
		assertTrue(world.hasProperMazub());
	}
	
	@Test
	public void startGame_LegalCase(){
		assertEquals(world.getGameStatus(), GameStatus.ADDING_OBJECTS);
		facade.setMazub(world, alienX0Y499);
		facade.startGame(world);
		assertEquals(world.getGameStatus(), GameStatus.STARTED);
	}
	
	@Test
	public void didPlayerWin_True() throws Exception{
		// Create world with easy reachable target tile.
		World worldTarget1X0Y = facade.createWorld(499, 5, 2, 1, 1, 1, 0);
		for(int i = 0; i < 5; i++)
			facade.setGeologicalFeature(worldTarget1X0Y, i, 0, FEATURE_SOLID);
		facade.setMazub(worldTarget1X0Y, alienX0Y499);
		// Mazub runs to target tile
		facade.startGame(worldTarget1X0Y);
		facade.startMoveRight(alienX0Y499);
		facade.advanceTime(worldTarget1X0Y, 0.005);
		for (int i = 0; i < 10000; i++) {
			facade.advanceTime(worldTarget1X0Y, 0.1);
		}
		assertTrue(facade.didPlayerWin(worldTarget1X0Y));
		assertTrue(facade.isGameOver(worldTarget1X0Y));
	}
	
	@Test
	public void didPlayerWin_OutOfWorldRange() throws Exception{
		facade.setMazub(world, alienX0Y499);
		facade.startGame(world);
		facade.startMoveRight(alienX0Y499);
		for (int i = 0; i<100; i++)
			facade.advanceTime(world,0.197);
		assertFalse(world.isInXRange(alienX0Y499.getXPosition()));
		assertTrue(facade.isGameOver(world));
		assertFalse(facade.didPlayerWin(world));
	}
	
	@Test (expected = ModelException.class)
	public void canAdd_TooManyGameObjects() throws Exception{
		// create game objects
		Set<Plant> plants = new HashSet<>();
		Set<Slime> slimes = new HashSet<>();
		Set<Shark> sharks = new HashSet<>();
		facade.createSchool();
		for (int i = 0; i < 40; i++){
			plants.add(facade.createPlant(i*5, 2000, spriteArrayForSize(1,1,3) ));
			slimes.add(facade.createSlime(i*5, 499, spriteArrayForSize(2,2,2), school));
			sharks.add(facade.createShark(i*5+1000, 499, spriteArrayForSize(3,3,3)));
		}
		// add game objects to world
		for (Plant plant : plants){
			facade.addPlant(world, plant);
		}
		for (Slime slime : slimes){
			facade.addSlime(world, slime);
		}
		for (Shark shark : sharks){
			facade.addShark(world, shark);
		}	
	}
	
	@Test (expected = ModelException.class)
	public void canAdd_TooManySchools() throws Exception{
		for (int i = 0; i < 11; i++){
			School newSchool = facade.createSchool();
			Slime newSlime = facade.createSlime(i*10, 499, spriteArrayForSize(2,2,2), newSchool);
			facade.addSlime(world, newSlime);
		}
	}
}
