package jumpingalien.part3.programs;

import java.util.List;
import java.util.Map;

import jumpingalien.model.GameObject;
import jumpingalien.model.GeologicalFeature;
import jumpingalien.model.HorizontalDirection;
import jumpingalien.model.Tile;
import jumpingalien.model.VerticalDirection;
import jumpingalien.model.expression.*;
import jumpingalien.model.program.*;
import jumpingalien.model.statement.*;
import jumpingalien.model.type.*;

public class ProgramFactory implements IProgramFactory<Expression<? extends Type>, Statement, Class<? extends Type>, Program >{

	// @Runtime, all the casts just cast to the raw type Variable. 
	// Consequently it's possible to cast an expession to an other expression which is not initiated with the same parameter.
	// Since we may assume that the given program is correct and therefore the casts will be legal, this is not a problem.
	// If one constructs his program DIRECTLY by means of constructors in our hierarchy, 
	// the java compiler will complain if he tries to build a program that has typing errors.
	
	
	@Override
	public ReadVariable<?> createReadVariable(String variableName, Class<? extends Type> variableType,
			SourceLocation sourceLocation) {
		if (variableType == DoubleType.class)
			return new ReadVariable<DoubleType>(variableName, DoubleType.class);
		else if (variableType == BoolType.class)
			return new ReadVariable<BoolType>(variableName, BoolType.class);
		else if (variableType == DirectionType.class)
			return new ReadVariable<DirectionType>(variableName, DirectionType.class);
		else if (variableType == ObjectType.class)
			return new ReadVariable<ObjectType>(variableName, ObjectType.class);
		else throw new IllegalArgumentException("Invalid type");
		//return new ReadVariable<variableType>(variableName, variableType);
	}

	@Override
	public Literal<DoubleType> createDoubleConstant(double value, SourceLocation sourceLocation) {
		return new Literal<DoubleType>(new DoubleType(value), DoubleType.class);
	}

	@Override
	public Literal<BoolType> createTrue(SourceLocation sourceLocation) {
		return new Literal<BoolType>(new BoolType(true), BoolType.class);
	}

	@Override
	public Literal<BoolType> createFalse(SourceLocation sourceLocation) {
		return new Literal<BoolType>(new BoolType(false), BoolType.class);
	}

	@Override
	public Literal<ObjectType> createNull(SourceLocation sourceLocation) {
		return new Literal<ObjectType>(null, ObjectType.class);
	}

	@Override
	public CreateSelf createSelf(SourceLocation sourceLocation) {
		return new CreateSelf();
	}

	@Override
	public Literal<? extends DirectionType> createDirectionConstant(
			jumpingalien.part3.programs.IProgramFactory.Direction value,
			SourceLocation sourceLocation) {
		// TRANSLATE TO OUR ENUMS
		if (value == jumpingalien.part3.programs.IProgramFactory.Direction.LEFT)
			return new Literal<HorizontalDirection>(HorizontalDirection.LEFT, HorizontalDirection.class);
		else if (value == jumpingalien.part3.programs.IProgramFactory.Direction.RIGHT)
			return new Literal<HorizontalDirection>(HorizontalDirection.RIGHT, HorizontalDirection.class);
		else if (value == jumpingalien.part3.programs.IProgramFactory.Direction.UP)
			return new Literal<VerticalDirection>(VerticalDirection.UP, VerticalDirection.class);
		else if (value == jumpingalien.part3.programs.IProgramFactory.Direction.DOWN)
			return new Literal<VerticalDirection>(VerticalDirection.DOWN, VerticalDirection.class);
		else throw new IllegalArgumentException("Invalid direction");
		
	}

	@Override
	public Addition createAddition(Expression<? extends Type> left, Expression<? extends Type> right, SourceLocation sourceLocation) {
		Expression<DoubleType> newLeft = (Expression<DoubleType>) left;	
		Expression<DoubleType> newRight = (Expression<DoubleType>) right;	
		return new Addition(newLeft,newRight);
	}

	@Override
	public Subtraction createSubtraction(Expression left, Expression right, SourceLocation sourceLocation) {
		Expression<DoubleType> newLeft = (Expression<DoubleType>) left;	
		Expression<DoubleType> newRight = (Expression<DoubleType>) right;
		return new Subtraction(newLeft,newRight);
	}

	@Override
	public Multiplication createMultiplication(Expression left, Expression right, SourceLocation sourceLocation) {
		Expression<DoubleType> newLeft = (Expression<DoubleType>) left;	
		Expression<DoubleType> newRight = (Expression<DoubleType>) right;
		return new Multiplication(newLeft,newRight);
	}

	@Override
	public Division createDivision(Expression left, Expression right, SourceLocation sourceLocation) {
		Expression<DoubleType> newLeft = (Expression<DoubleType>) left;	
		Expression<DoubleType> newRight = (Expression<DoubleType>) right;
		return new Division(newLeft,newRight);
	}

	@Override
	public SquareRoot createSqrt(Expression<? extends Type> expr, SourceLocation sourceLocation) {
		Expression<DoubleType> newExpr = (Expression<DoubleType>) expr;
		return new SquareRoot(newExpr);
	}

	@Override
	public Random createRandom(Expression<? extends Type> maxValue, SourceLocation sourceLocation) {
		Expression<DoubleType> newMaxValue = (Expression<DoubleType>) maxValue;
		return new Random(newMaxValue);
	}

	@Override
	public LogicalAND createAnd(Expression left, Expression right, SourceLocation sourceLocation) {
		return new LogicalAND(left, right);
	}

	@Override
	public LogicalOR createOr(Expression left, Expression right, SourceLocation sourceLocation) {
		return new LogicalOR(left, right);
	}

	@Override
	public Negation createNot(Expression expr, SourceLocation sourceLocation) {
		return new Negation(expr);
	}

	@Override
	public ComparisonExpression createLessThan(Expression<? extends Type> left, Expression<? extends Type> right, SourceLocation sourceLocation) {
		Expression<DoubleType> newLeft = (Expression<DoubleType>) left;
		Expression<DoubleType> newRight = (Expression<DoubleType>) right;
		return new ComparisonExpression(newLeft, newRight, ComparisonOperation.LESS_THAN);
	}

	@Override
	public ComparisonExpression createLessThanOrEqualTo(Expression<? extends Type> left, Expression<? extends Type> right,
			SourceLocation sourceLocation) {
		Expression<DoubleType> newLeft = (Expression<DoubleType>) left;
		Expression<DoubleType> newRight = (Expression<DoubleType>) right;
		return new ComparisonExpression(newLeft, newRight, ComparisonOperation.LESS_THAN_OR_EQUAL);
	}

	@Override
	public ComparisonExpression createGreaterThan(Expression<? extends Type> left, Expression<? extends Type> right, SourceLocation sourceLocation) {
		Expression<DoubleType> newLeft = (Expression<DoubleType>) left;
		Expression<DoubleType> newRight = (Expression<DoubleType>) right;
		return new ComparisonExpression(newLeft, newRight, ComparisonOperation.GREATER_THAN);
	}

	@Override
	public ComparisonExpression createGreaterThanOrEqualTo(Expression<? extends Type> left, Expression<? extends Type> right,
			SourceLocation sourceLocation) {
		Expression<DoubleType> newLeft = (Expression<DoubleType>) left;
		Expression<DoubleType> newRight = (Expression<DoubleType>) right;
		return new ComparisonExpression(newLeft, newRight, ComparisonOperation.GREATER_THAN_OR_EQUAL);
	}

	@Override
	public Equals<?> createEquals(Expression<? extends Type> left, Expression<? extends Type> right, SourceLocation sourceLocation) {
		if (left.getType() == DoubleType.class)
			return new Equals<DoubleType>((Expression<DoubleType>)left, (Expression<DoubleType>)right);
		else if (left.getType() == BoolType.class)
			return new Equals<BoolType>((Expression<BoolType>) left, (Expression<BoolType>) right);
		else if (left.getType() == DirectionType.class)
			return new Equals<DirectionType>((Expression<DirectionType>) left,(Expression<DirectionType>)  right);
		else if (ObjectType.class.isAssignableFrom(left.getType()))
			return new Equals<ObjectType>((Expression<ObjectType>) left,(Expression<ObjectType>)  right);
		else throw new IllegalArgumentException("Invalid type");
	}

	@Override
	public NotEquals createNotEquals(Expression<? extends Type> left, Expression<? extends Type> right, SourceLocation sourceLocation) {
		if (left.getType() == DoubleType.class)
			return new NotEquals<DoubleType>((Expression<DoubleType>)left, (Expression<DoubleType>)right);
		else if (left.getType() == BoolType.class)
			return new NotEquals<BoolType>((Expression<BoolType>) left, (Expression<BoolType>) right);
		else if (left.getType() == DirectionType.class)
			return new NotEquals<DirectionType>((Expression<DirectionType>) left,(Expression<DirectionType>)  right);
		else if (ObjectType.class.isAssignableFrom(left.getType()))
			return new NotEquals<ObjectType>((Expression<ObjectType>) left,(Expression<ObjectType>)  right);
		else throw new IllegalArgumentException("Invalid type");
	}

	@Override
	public GetX createGetX(Expression<? extends Type> expr, SourceLocation sourceLocation) {
		Expression<ObjectType> newExpr = (Expression<ObjectType>) expr;
		return new GetX(newExpr);
	}

	@Override
	public GetY createGetY(Expression<? extends Type> expr, SourceLocation sourceLocation) {
		Expression<ObjectType> newExpr = (Expression<ObjectType>) expr;
		return new GetY(newExpr);
	}

	@Override
	public GetWidth createGetWidth(Expression<? extends Type> expr, SourceLocation sourceLocation) {
		Expression<ObjectType> newExpr = (Expression<ObjectType>) expr;
		return new GetWidth(newExpr);
	}

	@Override
	public GetHeight createGetHeight(Expression<? extends Type> expr, SourceLocation sourceLocation) {
		Expression<ObjectType> newExpr = (Expression<ObjectType>) expr;
		return new GetHeight(newExpr);
	}

	@Override
	public GetHP createGetHitPoints(Expression<? extends Type> expr, SourceLocation sourceLocation) {
		Expression<GameObject> newExpr = (Expression<GameObject>) expr;
		return new GetHP(newExpr);
	}

	@Override
	public GetTile createGetTile(Expression<? extends Type> x, Expression<? extends Type> y, SourceLocation sourceLocation) {
		Expression<DoubleType> newX = (Expression<DoubleType>) x;
		Expression<DoubleType> newY = (Expression<DoubleType>) y;
		return new GetTile(newX, newY);
	}

	@Override
	public Expression<?> createSearchObject(Expression<? extends Type> direction, SourceLocation sourceLocation) {
		Expression<DirectionType> newDirection = (Expression<DirectionType>) direction;
		return new SearchDirection(newDirection);
	}

	@Override
	public IsKind createIsMazub(Expression<? extends Type> expr, SourceLocation sourceLocation) {
		Expression<ObjectType> newExpr = (Expression<ObjectType>) expr;
		return new IsKind(newExpr, jumpingalien.model.statement.Kind.MAZUB);
	}

	@Override
	public IsKind createIsShark(Expression<? extends Type> expr, SourceLocation sourceLocation) {
		Expression<ObjectType> newExpr = (Expression<ObjectType>) expr;
		return new IsKind(newExpr, jumpingalien.model.statement.Kind.SHARK);
	}

	@Override
	public IsKind createIsSlime(Expression<? extends Type> expr, SourceLocation sourceLocation) {
		Expression<ObjectType> newExpr = (Expression<ObjectType>) expr;
		return new IsKind(newExpr, jumpingalien.model.statement.Kind.SLIME);
	}

	@Override
	public IsKind createIsPlant(Expression<? extends Type> expr, SourceLocation sourceLocation) {
		Expression<ObjectType> newExpr = (Expression<ObjectType>) expr;
		return new IsKind(newExpr, jumpingalien.model.statement.Kind.PLANT);
	}

	@Override
	public IsDead createIsDead(Expression<? extends Type> expr, SourceLocation sourceLocation) {
		Expression<ObjectType> newExpr = (Expression<ObjectType>) expr;
		return new IsDead(newExpr);
	}



	@Override
	public IsPassable createIsPassable(Expression<? extends Type> expr, SourceLocation sourceLocation) {
		Expression<ObjectType> newExpr = (Expression<ObjectType>) expr;
		return new IsPassable(newExpr);
	}
	
	@Override
	public IsGeologicalFeature createIsTerrain(Expression<? extends Type> expr, SourceLocation sourceLocation) {
		Expression<ObjectType> tile = (Expression<ObjectType>) expr;
		return new IsGeologicalFeature(tile, GeologicalFeature.SOLID);
	}

	@Override
	public IsGeologicalFeature createIsWater(Expression<? extends Type> expr, SourceLocation sourceLocation) {
		Expression<ObjectType> tile = (Expression<ObjectType>) expr;
		return new IsGeologicalFeature(tile, GeologicalFeature.WATER);
	}

	@Override
	public IsGeologicalFeature createIsMagma(Expression<? extends Type> expr, SourceLocation sourceLocation) {
		Expression<ObjectType> tile = (Expression<ObjectType>) expr;
		return new IsGeologicalFeature(tile, GeologicalFeature.MAGMA);
	}

	@Override
	public IsGeologicalFeature createIsAir(Expression<? extends Type> expr, SourceLocation sourceLocation) {
		Expression<ObjectType> tile = (Expression<ObjectType>) expr;
		return new IsGeologicalFeature(tile, GeologicalFeature.AIR);
	}

	@Override
	public IsMoving createIsMoving(Expression<? extends Type> expr, Expression<? extends Type> direction,
			SourceLocation sourceLocation) {
		Expression<ObjectType> newExpr = (Expression<ObjectType>) expr;
		Expression<DirectionType> newDirection = (Expression<DirectionType>) direction;
		return new IsMoving(newExpr, newDirection);
	}

	@Override
	public IsDucking createIsDucking(Expression<? extends Type> expr, SourceLocation sourceLocation) {
		Expression<ObjectType> newExpr = (Expression<ObjectType>) expr;
		return new IsDucking(newExpr);
	}

	@Override
	public IsJumping createIsJumping(Expression<? extends Type> expr, SourceLocation sourceLocation) {
		Expression<ObjectType> newExpr = (Expression<ObjectType>) expr;
		return new IsJumping(newExpr);
	}

	@Override
	public Assignment<? extends Type> createAssignment(String variableName, Class<? extends Type> variableType,
			Expression<? extends Type> value,
			SourceLocation sourceLocation) {
		if (variableType == DoubleType.class){
			Expression<DoubleType> newValue = (Expression<DoubleType>) value;
			return new Assignment<DoubleType>(variableName, newValue);
		}
		else if (variableType == BoolType.class){
			Expression<BoolType> newValue = (Expression<BoolType>) value;
			return new Assignment<BoolType>(variableName, newValue);
		}
		else if (variableType == DirectionType.class){
			Expression<DirectionType> newValue = (Expression<DirectionType>) value;
			return new Assignment<DirectionType>(variableName, newValue);
		}
			
		else if (variableType == ObjectType.class){
			Expression<ObjectType> newValue = (Expression<ObjectType>) value;
			return new Assignment<ObjectType>(variableName, newValue);
		}
		else throw new IllegalArgumentException("Invalid type");
	}

	@Override
	public While createWhile(Expression<? extends Type> condition, Statement body, SourceLocation sourceLocation) {
		Expression<BoolType> newCondition = (Expression<BoolType>) condition;
		return new While(newCondition, body);
	}

	@Override
	public ForEach createForEach(String variableName,jumpingalien.part3.programs.IProgramFactory.Kind variableKind, Expression<? extends Type> where, 
			Expression<? extends Type> sort, jumpingalien.part3.programs.IProgramFactory.SortDirection sortDirection, Statement body, SourceLocation sourceLocation) {
		// CONVERSION FROM GIVEN ENUM TO OUR ENUM
		jumpingalien.model.statement.Kind kind = jumpingalien.model.statement.Kind.values()[variableKind.ordinal()];
		jumpingalien.model.statement.SortDirection direction;
		if (sortDirection != null)
			direction = jumpingalien.model.statement.SortDirection.values()[sortDirection.ordinal()];
		else 
			direction = null;
		// CASTING
		Expression<BoolType> newWhere = (Expression<BoolType>) where;	
		Expression<DoubleType> newSort = (Expression<DoubleType>) sort;
		// RETURN
		return new ForEach(variableName, kind, newWhere, newSort,direction, body);
	}

	@Override
	public Break createBreak(SourceLocation sourceLocation) {
		return new Break();
	}

	@Override
	public If createIf(Expression condition, Statement ifBody, Statement elseBody,
			SourceLocation sourceLocation) {
		return new If(condition, ifBody, elseBody);
	}

	@Override
	public Print createPrint(Expression<?> value, SourceLocation sourceLocation) {
		return new Print(value);
	}

	@Override
	public StartRun createStartRun(Expression direction, SourceLocation sourceLocation) {
		return new StartRun(direction);
	}

	@Override
	public StopRun createStopRun(Expression direction, SourceLocation sourceLocation) {
		return new StopRun(direction);
	}

	@Override
	public StartJump createStartJump(SourceLocation sourceLocation) {
		return new StartJump();
	}

	@Override
	public StopJump createStopJump(SourceLocation sourceLocation) {
		return new StopJump();
	}

	@Override
	public StartDuck createStartDuck(SourceLocation sourceLocation) {
		return new StartDuck();
	}

	@Override
	public StopDuck createStopDuck(SourceLocation sourceLocation) {
		return new StopDuck();
	}

	@Override
	public Wait createWait(Expression<? extends Type> duration, SourceLocation sourceLocation) {
		Expression<DoubleType> newDuration = (Expression<DoubleType>) duration;
		return new Wait(newDuration);
	}

	@Override
	public Skip createSkip(SourceLocation sourceLocation) {
		return new Skip();
	}

	@Override
	public Sequence createSequence(List<Statement> statements, SourceLocation sourceLocation) {
		return new Sequence(statements);
	}

	@Override
	public Class<DoubleType> getDoubleType() {
		return DoubleType.class;
	}

	@Override
	public Class<BoolType> getBoolType() {
		return BoolType.class;
	}

	@Override
	public Class<ObjectType> getGameObjectType() {
		return ObjectType.class;
	}

	@Override
	public Class<DirectionType> getDirectionType() {
		return DirectionType.class;
	}

	@Override
	public Program createProgram(Statement mainStatement, Map<String, Class<? extends Type>> globalVariables) {
		return new Program(mainStatement, globalVariables);
	}

}
