package jumpingalien.part2.tests;

import org.junit.Before;
import org.junit.Test;

import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static org.junit.Assert.*;
import jumpingalien.model.HorizontalDirection;
import jumpingalien.model.Mazub;
import jumpingalien.model.Plant;
import jumpingalien.model.School;
import jumpingalien.model.Shark;
import jumpingalien.model.Slime;
import jumpingalien.model.World;
import jumpingalien.part2.facade.Facade;
import jumpingalien.part2.facade.IFacadePart2;

public class SharkTest {

	private static IFacadePart2 facade;
	private static World world;
	private static Mazub alienOnTopOfHangingBlock;
	private static Shark shark_water;
	private static Shark shark_air;
	private static Shark shark_bottom_water;
	private static Shark shark_top_air;

	
	public static final int FEATURE_AIR = 0;
	public static final int FEATURE_SOLID = 1;
	public static final int FEATURE_WATER = 2;
	public static final int FEATURE_MAGMA = 3;
	
	
	@Before
	public void setUpMutableFixtures(){
		facade = new Facade();
		
		// CREATE GOOD WORLD LAYOUT: Lots of solid tiles + water tiles
		world = facade.createWorld(500, 6, 5, 1, 1, 4, 3);
		for(int i = 0; i < 6; i++)
			facade.setGeologicalFeature(world, i, 0, FEATURE_SOLID);
		facade.setGeologicalFeature(world, 0, 1, FEATURE_SOLID);
		facade.setGeologicalFeature(world, 4, 1, FEATURE_SOLID);
		facade.setGeologicalFeature(world, 4, 3, FEATURE_SOLID);
		facade.setGeologicalFeature(world, 5, 1, FEATURE_SOLID);
		facade.setGeologicalFeature(world, 5, 2, FEATURE_SOLID);
		facade.setGeologicalFeature(world, 3, 2, FEATURE_SOLID);
		facade.setGeologicalFeature(world, 2, 2, FEATURE_SOLID);
		facade.setGeologicalFeature(world, 1, 1, FEATURE_WATER);
		facade.setGeologicalFeature(world, 2, 1, FEATURE_WATER);
		facade.setGeologicalFeature(world, 3, 1, FEATURE_WATER);

		
		// SET MAZUB ON HANGING BLOCK
		alienOnTopOfHangingBlock = facade.createMazub(1250, 1499, spriteArrayForSize(3, 3));
				
		// CREATE SHARK COMPLETELY IN WATER
		shark_water = facade.createShark(1000, 550, spriteArrayForSize(3, 3));
		
		// CREATE SHARK COMPLETELY IN AIR
		shark_air = facade.createShark(2050, 1050, spriteArrayForSize(3, 3));
		
		// CREATE SHARK WITH ONLY BOTTOM PERIMETER IN WATER
		shark_bottom_water = facade.createShark(550, 999, spriteArrayForSize(3, 3));
		
		// CREATE SHARK WITH ONLY TOP PERIMETER IN AIR
		shark_top_air = facade.createShark(650, 998, spriteArrayForSize(3, 3));
		
	}
	
	@Test
	public void Shark_SatisfiesClassInvariantRightAfterCreation(){
		shark_water.hasProperJumper();
		shark_water.canHaveAsNbNonJumpingMovements(shark_water.getNbNonJumpingMovements());
		shark_water.canHaveAsRandomVerticalAcceleration(shark_water.getCurrentRandomAcceleration());
		shark_water.canHaveAsNbNonJumpingMovements(shark_water.getNbNonJumpingMovements());
	}
	
	@Test
	public void SharkShouldFallCorrectly(){
		// Shark in water shouldn't fall and have correct YAcceleration
		facade.addShark(world, shark_water);
		assertFalse(shark_water.shouldFall());
		assertTrue(shark_water.getYAcceleration() == 0);
		
		// Shark with bottom in water should fall and have correct YAcceleration
		facade.addShark(world, shark_bottom_water);
		assertTrue(shark_bottom_water.shouldFall());
		assertTrue(shark_bottom_water.getYAcceleration() == -10);
		
		// Shark in air should fall and have correct YAcceleration
		facade.addShark(world, shark_air);
		assertTrue(shark_air.shouldFall());
		assertTrue(shark_air.getYAcceleration() == -10);
		
	}
	 
	@Test
	public void SharkDiesInAirAndIsTerminatedAtRightTime(){
		// Shark completely in air
		facade.addShark(world, shark_air);
		facade.setMazub(world, alienOnTopOfHangingBlock);
		facade.startGame(world);
		for (int i=0; i<99; i++){
		facade.advanceTime(world, 0.1);
		}
		// Should still be alive with 2 hitpoints		
		assertTrue(shark_air.isAlive() && shark_air.getHitPoints() == 2);
		
		
		// Should be dead, but not terminated
		facade.advanceTime(world, 0.1);
		assertTrue(shark_air.isInTerminationProcess() && shark_air.getHitPoints() == 0);
		assertTrue(world.hasAsShark(shark_air));
		
		// Should be terminated and removed from the game world
		for (int i=0; i<6; i++){
		facade.advanceTime(world, 0.1);
		}
		assertTrue(shark_air.isTerminated() && shark_air.getHitPoints() == 0);
		assertFalse(world.hasAsShark(shark_air));
		assertTrue(shark_air.getYAcceleration() == 0);
	}
	


	
	
	

	
	

	


		
	
}

