package jumpingalien.part2.tests;

import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import jumpingalien.model.School;
import jumpingalien.model.Slime;
import jumpingalien.part2.facade.Facade;
import jumpingalien.part2.facade.IFacadePart2;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SchoolTest {
	private static IFacadePart2 facade;
	private static School school;
	private static Slime slime;
	
	@Before
	public void setUpMutableFixtures(){
		facade = new Facade();
		school =facade.createSchool();
	}
	
	public void School_SatisfyClassInvariant(){
		assertTrue(school.hasProperSlimes());
	}
	
	public void addSlime_SatisfyClassInvariant(){
		slime = facade.createSlime(395, 499, spriteArrayForSize(4,4,3), school);
		assertTrue(school.hasProperSlimes());
	}
}
