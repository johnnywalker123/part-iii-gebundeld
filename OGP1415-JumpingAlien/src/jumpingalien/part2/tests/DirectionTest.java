package jumpingalien.part2.tests;

import org.junit.Test;

import jumpingalien.model.HorizontalDirection;
import static org.junit.Assert.*;

public class DirectionTest {
	@Test
	public void getUnitDirection_LEFT(){
		assertEquals(HorizontalDirection.LEFT.getUnitDirection(), -1);
	}
	
	@Test
	public void getUnitDirection_Right(){
		assertEquals(HorizontalDirection.RIGHT.getUnitDirection(), 1);
	}
}
