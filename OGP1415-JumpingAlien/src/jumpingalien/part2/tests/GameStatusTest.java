package jumpingalien.part2.tests;

import org.junit.Test;

import jumpingalien.model.GameStatus;
import static org.junit.Assert.*;

public class GameStatusTest {
	
	// isAddingObjects
	
	@Test
	public void canAddObjects_True(){
		assertTrue(GameStatus.ADDING_OBJECTS.canAddObjects());
	}
	
	@Test
	public void canAddObjects_False(){
		assertFalse(GameStatus.STARTED.canAddObjects());
		assertFalse(GameStatus.PLAYER_LOST.canAddObjects());
		assertFalse(GameStatus.PLAYER_WON.canAddObjects());
	}
	
	
	// isGameOver
	
	@Test 
	public void isGameOver_True(){
		assertTrue(GameStatus.PLAYER_LOST.isGameOver());
		assertTrue(GameStatus.PLAYER_WON.isGameOver());
	}
	
	@Test 
	public void isGameOver_False(){
		assertFalse(GameStatus.STARTED.isGameOver());
		assertFalse(GameStatus.ADDING_OBJECTS.isGameOver());
	}
}
