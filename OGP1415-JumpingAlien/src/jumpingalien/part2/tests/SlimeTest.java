package jumpingalien.part2.tests;

import org.junit.Before;
import org.junit.Test;

import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static org.junit.Assert.*;
import jumpingalien.model.Mazub;
import jumpingalien.model.School;
import jumpingalien.model.Slime;
import jumpingalien.model.World;
import jumpingalien.part2.facade.Facade;
import jumpingalien.part2.facade.IFacadePart2;

public class SlimeTest {

	private static IFacadePart2 facade;
	private static World world;
	private static Slime slime1;
	private static Slime slime2;
	private static Slime slime3;
	private static Slime slime4;
	private static Slime slime5;
	private static Slime slime6;
	private static Slime slime7;
	// school with three members
	private static School schoolA;
	// school with two members
	private static School schoolB;
	// school with one member
	private static School schoolC;
	// school with one member
	private static School schoolD;
	private static Mazub alienOnTopOfHangingBlock;

	
	public static final int FEATURE_AIR = 0;
	public static final int FEATURE_SOLID = 1;
	public static final int FEATURE_WATER = 2;
	public static final int FEATURE_MAGMA = 3;
	
	
	@Before
	public void setUpMutableFixtures(){
		facade = new Facade();
		
		// CREATE GOOD WORLD LAYOUT: Lots of solid tiles
		world = facade.createWorld(500, 6, 5, 1, 1, 4, 3);
		for(int i = 0; i < 6; i++)
			facade.setGeologicalFeature(world, i, 0, FEATURE_SOLID);
		facade.setGeologicalFeature(world, 0, 1, FEATURE_SOLID);
		facade.setGeologicalFeature(world, 4, 1, FEATURE_SOLID);
		facade.setGeologicalFeature(world, 5, 1, FEATURE_SOLID);
		facade.setGeologicalFeature(world, 5, 2, FEATURE_SOLID);
		facade.setGeologicalFeature(world, 3, 2, FEATURE_SOLID);
		facade.setGeologicalFeature(world, 2, 2, FEATURE_SOLID);

		
		// SET MAZUB ON HANGING BLOCK
		alienOnTopOfHangingBlock = facade.createMazub(1250, 1499, spriteArrayForSize(3, 3));
				
		// CREATE SOME SLIMES AND SCHOOLS
		schoolA =facade.createSchool();
		schoolB =facade.createSchool();
		schoolC =facade.createSchool();
		schoolD =facade.createSchool();
		slime1 = facade.createSlime(550, 500, spriteArrayForSize(4,4,3), schoolA);
		slime2 = facade.createSlime(560, 499, spriteArrayForSize(4,4,3), schoolA);
		slime3 = facade.createSlime(2750, 1500, spriteArrayForSize(4,4,3), schoolA);
		slime4 = facade.createSlime(555, 499, spriteArrayForSize(4,4,3), schoolB);
		slime5 = facade.createSlime(550, 499, spriteArrayForSize(4,4,3), schoolC);
		slime6 = facade.createSlime(1050, 550, spriteArrayForSize(4,4,3), schoolB);
		slime7 = facade.createSlime(555, 499, spriteArrayForSize(4,4,3), schoolD);		
	}
	
	@Test
	public void GameObject_SatisfiesClassInvariantAfterCreation(){
		 assertTrue(slime1.canHaveAsSprites(slime1.getAllImages()));
		 assertTrue(slime1.hasProperWorld());
		 assertTrue(slime1.hasProperCollidingObjects());
		 assertTrue(slime1.canHaveAsEndOfTerminationProcess(slime1.getEndTerminationProcess()));
		 assertTrue(slime1.canHaveAsCurrentIndex(slime1.getCurrentIndex()));
		 assertTrue(slime1.canHaveAsHitPoints(slime1.getHitPoints()));
		 assertTrue(Slime.isValidTime(slime1.getTime()));
		 assertTrue(Slime.isValidGameObjectStatus(slime1.getGameObjectStatus()));
		 assertTrue(Slime.isValidTime(slime1.getEndTerminationProcess()) || Double.isNaN(slime1.getEndTerminationProcess()));
		 assertTrue(slime1.canHaveAsXVelocity(slime1.getXVelocity()));
		 assertTrue(slime1.canHaveAsXVelocity(slime1.getInitialXVelocity()));
		 assertTrue(Slime.isValidHorizontalDirection(slime1.getHorizontalDirection()));
		 assertTrue(Slime.isValidTime(slime1.getImmunityTimer()));
	}
	
	@Test
	public void NPCObject_SatisfiesClassInvariantAfterCreation(){
		assertTrue(slime1.canHaveAsRemainingMovementDuration(slime1.getRemainingMovementDuration()));
	}
	
	@Test
	public void slimeCollisionSameSchoolHasNoEffect(){
		facade.setMazub(world, alienOnTopOfHangingBlock);
		facade.addSlime(world, slime1);
		facade.addSlime(world, slime2);
		facade.startGame(world);
		for (int i = 0; i < 10000; i++) {
			facade.advanceTime(world, 0.1);
		}
		// Nothing happened
		assertTrue(slime1.getHitPoints() == 100 && slime2.getHitPoints() == 100);
	}
	
	@Test
	public void slimeCollisionDifferentSchoolSameSizeHasNoEffect(){
		facade.setMazub(world, alienOnTopOfHangingBlock);
		facade.addSlime(world, slime5);
		facade.addSlime(world, slime7);
		facade.startGame(world);
		for (int i = 0; i < 10000; i++) {
			facade.advanceTime(world, 0.1);
			}
		// No school transfer
		assertTrue(slime5.getSchool() == schoolC && slime7.getSchool() == schoolD);
		// No hitpoints received or deducted
		assertTrue(slime5.getHitPoints() == 100 && slime7.getHitPoints() == 100);
		
	}
	
	
	@Test
	public void slimeOfSmallerSchoolTransfersToLargerSchoolAndHitPointsCorrect(){
		facade.setMazub(world, alienOnTopOfHangingBlock);
		facade.addSlime(world, slime1);
		facade.addSlime(world, slime4);
		facade.startGame(world);
		boolean collisionHappened = false;
		for (int i = 0; i < 10000; i++) {
			facade.advanceTime(world, 0.1);
			if (slime1.hasAsCollidingObject(slime4)){
				collisionHappened = true;
			}
		}
		if (collisionHappened){
			assertTrue(slime1.getSchool() == schoolA && slime4.getSchool() == schoolA);
			// New school decremented
			assertTrue(slime1.getHitPoints() == 99 && slime2.getHitPoints() == 99 && slime3.getHitPoints() == 99);
			// Old school incremented
			assertTrue(slime6.getHitPoints() == 101);
			// Transferring Slime
			assertTrue(slime4.getHitPoints() == 102);
		}
		
	}
	
	@Test
	public void SlimeSatisfiesClassInvariant(){
		facade.addSlime(world, slime1);
		assertTrue(Slime.isValidTime(slime1.getTime()));
		assertTrue(slime1.canHaveAsWorld(world));
		assertTrue(slime1.hasProperSchool());
		assertTrue(slime1.canHaveAsSprites(slime1.getAllImages()));
		assertTrue(slime1.hasProperCollidingObjects());
	}
	
	@Test
	public void SlimeDiesInWaterAtRightTimeAndOtherSlimesOfSchoolHaveCorrectHitPoints(){
		facade.setGeologicalFeature(world, 1, 1, FEATURE_WATER);
		facade.setGeologicalFeature(world, 2, 1, FEATURE_WATER);
		facade.setGeologicalFeature(world, 3, 1, FEATURE_WATER);
		
		facade.addSlime(world, slime1);
		facade.setMazub(world, alienOnTopOfHangingBlock);
		facade.startGame(world);
		assertTrue(slime1.getHitPoints() == Slime.getInitialHitPoints());
		for (int i = 0; i < 50; i++) {
			facade.advanceTime(world, 0.1);
		}
		assertTrue(slime1.getHitPoints() == 50);
		assertTrue(slime2.getHitPoints() == 75);
		for (int i = 0; i < 50; i++) {
			facade.advanceTime(world, 0.1);
		}
		assertTrue(!slime1.isAlive());
		assertTrue(slime2.getHitPoints() == 50);
	}
	
	@Test
	public void MagmaHitsSlimeImmediatelyAndSlimeFallsInLava(){
		facade.setGeologicalFeature(world, 1, 1, FEATURE_MAGMA);
		facade.addSlime(world, slime1);
		facade.setMazub(world, alienOnTopOfHangingBlock);
		facade.startGame(world);
		assertTrue(slime1.shouldFall());
			facade.advanceTime(world, 0.1);
		assertTrue(slime1.getHitPoints() == 50);
			facade.advanceTime(world, 0.09);
		assertTrue(slime1.getHitPoints() == 50);
			facade.advanceTime(world, 0.03);
		assertTrue(!slime1.isAlive());	
		
	}

	@Test
	public void SlimeShouldFallCorrectly(){
		// Slime1 on ground
		facade.addSlime(world, slime2);
		assertFalse(slime2.shouldFall());
		assertTrue(slime2.getYAcceleration() == 0);
		// Slime3 in air
		facade.addSlime(world, slime3);
		assertTrue(slime3.shouldFall());
		assertTrue(slime3.getYAcceleration() == -10);
		// Slime6 in water
		facade.addSlime(world,slime6);
		facade.setGeologicalFeature(world, 2, 1, FEATURE_WATER);
		assertTrue(slime6.shouldFall());
		assertTrue(slime6.getYAcceleration() == -10);

		
	}
}
