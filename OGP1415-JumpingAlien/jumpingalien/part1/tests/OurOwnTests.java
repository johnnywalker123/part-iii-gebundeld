package jumpingalien.part1.tests;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import jumpingalien.part1.facade.Facade;
import jumpingalien.part1.facade.IFacade;
import jumpingalien.model.Direction;
import jumpingalien.model.Mazub;
import jumpingalien.util.Util;

import org.junit.Test;

import static jumpingalien.tests.util.TestUtils.*;

public class OurOwnTests {
	
	// IMPORTANT TESTS
	
	@Test
	public void testConstructedObjectSatisfiesClassInvariants(){
		Facade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		
		assertTrue(Mazub.isValidXPixel(alien.getXPixel()));
		assertTrue(Mazub.isValidYPixel(alien.getYPixel()));
		assertEquals(alien.getXPixel(), Mazub.convertPositionToPixel(alien.getXPosition()));
		assertEquals(alien.getYPixel(), Mazub.convertPositionToPixel(alien.getYPosition()));
		assertTrue(alien.canHaveAsXVelocity(alien.getXVelocity()));
		assertTrue(Mazub.isValidInitialXVelocity(alien.getInitialXVelocity()));
		assertTrue(alien.canHaveAsMaxXVelocity(alien.getMaxXVelocity()));
		assertTrue(Mazub.isValidYVelocity(alien.getYVelocity()));
		assertTrue(Mazub.isValidXAcceleration(alien.getXAcceleration()));
		assertTrue(Mazub.isValidYAcceleration(alien.getYAcceleration()));
		assertTrue(Mazub.isValidTime(alien.getTime()));
		assertTrue(Mazub.isValidTime(alien.getAlternationTimer()));
		assertTrue(Mazub.isValidMovement(alien.getLastMovement()));
		assertTrue(alien.canHaveAsIndices(alien.getCurrentIndices()));
	}
	
	@Test
	public void hasMovedLastSecond_AfterCreation(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		assertFalse(alien.hasMovedLastSecond());
	}
	
	@Test
	public void hasMovedLastSecond_HasMoved(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		alien.startMove(Direction.RIGHT);
		facade.advanceTime(alien, 0.01);
		alien.endMove();
		facade.advanceTime(alien,0.01);
		assertTrue(alien.hasMovedLastSecond());
	}
	
	
	@Test
	public void testShouldFallWhenStartJump(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		
		facade.startJump(alien);
		facade.advanceTime(alien, 0.00001);
		assertTrue(alien.shouldFall());
	}
	
	@Test
	public void testNegativeYVelocityWhenEndJump(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		
		facade.startJump(alien);
		alien.advanceTime(0.19);
		facade.endJump(alien);
		assert(alien.getYVelocity() <= 0);
	}
	
	
	@Test public void testStrictNegativeAccelerationRightAfterJumping(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startJump(alien);
		alien.advanceTime(0.00001);
		assert(alien.getYAcceleration() < 0);
	}
	
	
	@Test
	public void testYVelocityZeroWhenBackOnGround(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startJump(alien);
		double timesteps = 10;
		alien.advanceTime(0.000001);
		for (int index = 0;index<timesteps;index++){
			System.out.println(alien.getYAcceleration());
			double dt = Math.abs(0.19);
			alien.advanceTime(dt);
		}
		System.out.println(alien.getYPosition());
		System.out.println(alien.shouldFall());
		assert(!alien.shouldFall());
		System.out.println(alien.getYVelocity());
		assertEquals(alien.getYVelocity(), 0, Util.DEFAULT_EPSILON);
	}
	
	@Test
	public void testDuck(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		
		facade.startDuck(alien);
		assert alien.isDucking();
		assertEquals(alien.getMaxXVelocity(),1,Util.DEFAULT_EPSILON);
		
		facade.endDuck(alien);
		assertFalse(alien.isDucking());
		assertEquals(alien.getMaxXVelocity(), 3,Util.DEFAULT_EPSILON);
		}
	
	
	// OTHER TESTS
	
	@Test
	public void getNearestValidXPosition_NegativePosition(){
		assertEquals(Mazub.getNearestValidXPosition(-3),0,Util.DEFAULT_EPSILON);
	}
	
	@Test
	public void getNearestValidXPosition_PositionHigherThanMaxPosition(){
		assertEquals(Mazub.getNearestValidXPosition(1055),10.23,Util.DEFAULT_EPSILON);
	}
	
	@Test
	public void getNearestValidYPosition_ValidPosition(){
		assertEquals(Mazub.getNearestValidYPosition(5),5,Util.DEFAULT_EPSILON);
	}
	
	@Test
	public void isValidXPixel_ValidPosition(){
		assertTrue(Mazub.isValidXPixel(3));
	}
	
	@Test
	public void isValidYPixel_InvalidPosition(){
		assertFalse(Mazub.isValidXPixel(-5));
	}
	
	@Test
	public void testConvertPixelToPosition(){
		assertEquals(Mazub.convertPixelToPosition(3),0.03,Util.DEFAULT_EPSILON);
	}
	
	@Test
	public void testConvertPositionToPixel(){
		assertEquals(Mazub.convertPositionToPixel(3.457),345);
	}
	
	@Test
	public void hasConstantVelocity_isNotRunning(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		assertTrue(alien.hasConstantXVelocity());
	}
	
	@Test
	public void hasConstantVelocity_MaxVelocity(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startMoveRight(alien);
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 100; i++) {
			facade.advanceTime(alien, 0.2 / 9);
		}
		assertTrue(alien.hasConstantXVelocity());
	}
	
	@Test
	public void hasConstantVelocity_VariableVelocity(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startMoveRight(alien);
		// still accelerating
		for (int i = 0; i < 88; i++) {
			facade.advanceTime(alien, 0.2 / 9);
		}
		assertFalse(alien.hasConstantXVelocity());
	}
	
	@Test
	public void canHaveAsXVelocity_LegalCase(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		assertTrue(alien.canHaveAsXVelocity(1.1));
	}
	
	@Test
	public void isValidInitialXVelocity_LegalCase(){
		assertTrue(Mazub.isValidInitialXVelocity(1.1));
	}
	
	@Test
	public void canHaveAsMaxXVelocity_IllegalCase(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		assertFalse(alien.canHaveAsMaxXVelocity(0.9));
	}
	
	@Test
	public void isValidYVelocity_IllegalCase(){
		assertFalse(Mazub.isValidYVelocity(Double.NaN));
	}
	
	@Test
	public void isValidXAcceleration_IlLegalCase(){
		assertFalse(Mazub.isValidXAcceleration(Double.NaN));
	}
	
	@Test
	public void isValidYAcceleration_LegalCase(){
		assertTrue(Mazub.isValidYAcceleration(Double.NEGATIVE_INFINITY));
	}
	
	@Test
	
	public void getImages_length(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		assertEquals(alien.getImages().length,alien.getNbSprites());
	}
	
	@Test
	public void canHaveAsIndex_IllegalCase(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		assertFalse(alien.canHaveAsIndex(alien.getImages().length + 1));
	}
	
	@Test
	public void canHaveAsIndices_LegalCase(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		int[] newArray = {1,2,alien.getImages().length - 1};
		assertTrue(alien.canHaveAsIndices(newArray));
	}
	
	@Test
	public void canHaveAsTime_IllegalCase(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.advanceTime(alien, 0.015);
		assertFalse(alien.canHaveAsTime(-0.05));
	}
	
	@Test
	public void isValidTime_IllegalCase(){
		assertFalse(Mazub.isValidTime(-5));
	}
	
	@Test
	public void isValidTimeInterval_IllegalCse(){
		assertFalse(Mazub.isValidTimeInterval(0.2));
	}
	
	@Test
	public void stateHasChanged_BeginToRun(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		int[] oldIndices = alien.getCurrentIndices();
		alien.startMove(Direction.RIGHT);
		facade.advanceTime(alien,0.001);
		assertTrue(alien.stateHasChanged(oldIndices));
	}

	
}
