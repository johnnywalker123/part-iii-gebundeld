package jumpingalien.part1.tests;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import jumpingalien.part1.facade.Facade;
import jumpingalien.part1.facade.IFacade;
import jumpingalien.model.Mazub;
import jumpingalien.util.Sprite;
import jumpingalien.util.Util;

import org.junit.Test;

import static jumpingalien.tests.util.TestUtils.*;

public class PartialFacadeTest {

	@Test
	public void startMoveRightCorrect() {
		IFacade facade = new Facade();

		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startMoveRight(alien);
		facade.advanceTime(alien, 0.1);

		// x_new [m] = 0 + 1 [m/s] * 0.1 [s] + 1/2 0.9 [m/s^2] * (0.1 [s])^2 =
		// 0.1045 [m] = 10.45 [cm], which falls into pixel (10, 0)

		assertArrayEquals(intArray(10, 0), facade.getLocation(alien));
	}

	@Test
	public void startMoveRightMaxSpeedAtRightTime() {
		IFacade facade = new Facade();

		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startMoveRight(alien);
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 100; i++) {
			facade.advanceTime(alien, 0.2 / 9);
		}

		assertArrayEquals(doubleArray(3, 0), facade.getVelocity(alien),
				Util.DEFAULT_EPSILON);
	}

	@Test
	public void testAccellerationZeroWhenNotMoving() {
		IFacade facade = new Facade();

		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		assertArrayEquals(doubleArray(0.0, 0.0), facade.getAcceleration(alien),
				Util.DEFAULT_EPSILON);
	}

	@Test
	public void testWalkAnimationLastFrame() {
		IFacade facade = new Facade();

		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Mazub alien = facade.createMazub(0, 0, sprites);

		facade.startMoveRight(alien);

		facade.advanceTime(alien, 0.005);
		for (int i = 0; i < m; i++) {
			facade.advanceTime(alien, 0.075);
		}

		assertEquals(sprites[8+m], facade.getCurrentSprite(alien));
	}

	@Test
	public void testStartJump(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		
		facade.startJump(alien);
		facade.advanceTime(alien, 0.00001);
		assert alien.shouldFall();
	}
	
	@Test
	public void testEndJump(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		
		facade.startJump(alien);
		alien.advanceTime(0.19);
		facade.endJump(alien);
		assert (alien.getYVelocity() <= 0);
	}
	
	
	@Test public void testAccelerationRightAfterJumping(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startJump(alien);
		alien.advanceTime(0.00001);
		assert (alien.getYAcceleration() < 0);
	}
	
	
	@Test
	public void testVelocityAfterJumping(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startJump(alien);
		double timesteps = 10;
		alien.advanceTime(0.000001);
		for (int index = 0;index<timesteps;index++){
			System.out.println(alien.getYAcceleration());
			double dt = Math.abs(0.19);
			alien.advanceTime(dt);
		}
		System.out.println(alien.getYPosition());
		System.out.println(alien.shouldFall());
		assert(!alien.shouldFall());
		System.out.println(alien.getYVelocity());
		assert(alien.getYVelocity() == 0);
	}
	
	@Test
	public void testDuck(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		
		facade.startDuck(alien);
		assert alien.isDucking();
		assert (alien.getMaxXVelocity() == 1);
		
		facade.endDuck(alien);
		assert !alien.isDucking();
		assert (alien.getMaxXVelocity() == 3);
		}
	
//	@Test
//	public void constructedObjectSatisfiesClassInvariants(){
//		Facade facade = new Facade();
//		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
//		
//		assert Mazub.isValidXPixel(alien.getXPixel());
//		assert Mazub.isValidYPixel(alien.getYPixel());
//		//expliciet door constructor
//		assert Mazub.isValidDouble(alien.getXPosition());
//		assert Mazub.isValidDouble(alien.getYPosition());
//		//default-waarden
//		assert Mazub.isValidDouble(alien.getXVelocity());
//		assert Mazub.isValidDouble(alien.getYVelocity());
//		assert Mazub.isValidDouble(alien.getXAcceleration());
//		assert Mazub.isValidDouble(alien.getXAcceleration());
//		
//		
//	}
	
}
