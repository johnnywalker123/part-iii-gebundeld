package jumpingalien.model;


import java.util.Arrays;
//import java.util.HashSet;

import jumpingalien.util.Sprite;
import be.kuleuven.cs.som.annotate.*;

/**
 * A class of a jumping alien Mazub involving a horizontal and vertical position, velocity and acceleration; an initial and maximum horizontal velocity;
 * a game time and alternation time; the time of the last horizontal movement; the indices of the current alternating sprites and the current sprite.
 * 
 * @invar	The horizontal pixel of the bottom-left pixel of each Mazub must 
 * 			be a valid horizontal pixel for any Mazub.
 * 			| isValidXPixel(getXPixel())
 * @invar	The vertical pixel of the bottom-left pixel of each Mazub must be 
 * 			a valid vertical pixel for any Mazub.
 * 			| isValidYPixel(getYPixel())
 * @invar	The x-position (in m) of this Mazub corresponds always to the x-pixel (in cm) of this Mazub
 * 			(you can only set the x-position and the x-pixel simultaneously in one method).
 * 			| getXPixel() == convertPositionToPixel(getXPosition()) 
 * @invar	The y-position (in m) of this Mazub corresponds always to the y-pixel (in cm) of this Mazub.
 * 			(you can only set the y-position and the y-pixel simultaneously in one method).
 * 			| getYPixel() == convertPositionToPixel(getYPosition()) 
 * @note	Because the position of this Mazub corresponds to the pixel of this Mazub, we only need to check if the pixel is a valid pixel.
 * 			If the pixel is a valid pixel, consequently the position will be valid as well.
 * @invar	Each Mazub can have its current horizontal velocity as 
 * 			horizontal velocity.
 * 			| canHaveAsXVelocity(getXVelocity())
 * @invar	The initial horizontal velocity of each Mazub must be a valid 
 *  		initial horizontal velocity for all Mazubs.
 * 			| isValidInitialXVelocity(getInitialXVelocity())
 * @invar	The maximum horizontal velocity of each Mazub must be a valid 
 * 			maximum horizontal velocity of all Mazubs.
 * 			| canHaveAsMaxXVelocity(getMaxXVelocity())
 * @invar	The current vertical velocity of each Mazub must be a 
 * 			valid vertical velocity for all Mazubs.
 * 			| isValidYVelocity(getYVelocity())
 * @invar 	The current horizontal acceleration of each Mazub must be a valid horizontal 
 * 			acceleration for all Mazubs.
 * 			| isValidXAcceleration(getXAcceleration())
 * @invar 	The current vertical acceleration of each Mazub must be a valid vertical
 * 			acceleration for all Mazubs.
 * 			| isValidYAcceleration(getYAcceleration())
 * @invar	The game time of each Mazub must be a valid time for all Mazubs.
 * 			| isValidTime(getTime())
 * @invar	The time of the alternation timer must be a valid time for all Mazubs
 * 			| isValidTime(getAlternationTimer())
 * @invar	The time of the last horizontal movement must be a valid movement for all Mazubs.
 * 			| isValidMovement(getLastMovement())
 * @invar	Each Mazub can have its current indices as current indices.
 * 			| canHaveAsIndices(getCurrentIndices())
 * @invar	The current sprite of this Mazub must be a valid sprite for all Mazubs.
 * @invar	The current array of possible sprites of this Mazub must contain valid sprites for all Mazubs.
 * 
 * 
 * 
 * @author Tuur Van Daele and Antoon Purnal
 * 
 * @version 15/03/2015
 *
 */
public class Mazub {
	
	// CONSTRUCTORS
	
	/**
	 * Initialize this new Mazub with the given initial x-pixel and y-pixel; zero current horizontal and vertical velocity; 1.0 for the initial horizontal velocity;
	 * zero game time and alternation time; 3.0 for the absolute maximum horizontal velocity; a false running state and ducking state; "not a number" for last movement 
	 * right for the horizontal direction; the given array of images associated with this Mazub; the sprite with index 0 of the given images for its current sprite.
	 * 
	 * 
	 * @param 	initialXPixel
	 * 			The initial horizontal pixel for this new Mazub.
	 * @param 	initialYPixel
	 * 			The initial vertical pixel for this new Mazub.
	 * @param 	images
	 * 			The array of sprites for this new Mazub.
	 * @pre		The supplied array of sprites is effective.
	 * 			| images != null
	 * @effect	Set the horizontal position to the nearest valid horizontal position of the 
	 * 			position corresponding with the given initial x-pixel.
	 * 			| setXPosition(getNearestValidXPosition(convertPixelToPosition(initialXPixel)))
	 * @effect	Set the vertical position to the nearest valid vertical position of the position
	 * 			corresponding with the given initial y-pixel.
	 * 			| setYPosition(getNearestValidYPosition(convertPixelToPosition(initialYPixel)))
	 * @post	The initial current horizontal velocity of this new Mazub 
	 * 			is equal to zero.
	 * 			| new.getXVelocity() == 0
	 * @post	The initial initial horizontal velocity of this new Mazub
	 * 			is equal to 1.
	 * 			| new.getInitialXVelocity() == 1
	 * @post	The initial absolute maximum horizontal velocity of this new Mazub while not ducking
	 * 			is equal to 3.
	 * 			| new.absoluteMaxXVelocity = 3
	 * @post	The initial current vertical velocity of this new Mazub is 
	 * 			equal to zero.
	 * 			| new.getYVelocity() == 0
	 * @post	The initial running state of this new Mazub is equal to false.
	 * 			| new.isRunning() == false
	 * @post	The initial horizontal direction of this new Mazub is equal to right.
	 * 			| new.getOrientation() == Direction.RIGHT
	 * @post	The initial ducking state of this new Mazub is equal to false.
	 * 			| new.isDucking() == false
	 * @post	The images of this new Mazub are equal to the given images.
	 * 			| new. getImages() == images
	 * @post	The initial game time of this new Mazub is equal to 0.
	 * 			| new.getTime() == 0
	 * @post	The initial time of the alternation timer of this new Mazub is equal to 0.
	 * 			| new.getAlternationTimer() == 0
	 * @post	The last movement of this new Mazub is equal to "not a number".
	 * 			| new.getLastMovement() == Double.NaN
	 * @post	The initial current sprite of this new Mazub is equal to the sprite 
	 * 			of the given array images with index zero.
	 * 			| new.getCurrentSprite == images[0]
	 * @note	You can't use @effect for setCurrentSprite(0) because the images of 
	 * 			this Mazub aren't initialized yet at this point.
	 * 
	 */
	public Mazub(int initialXPixel, int initialYPixel, Sprite[] images){
		assert (images != null);
		setXPosition(getNearestValidXPosition(convertPixelToPosition(initialXPixel)));
		setYPosition(getNearestValidYPosition(convertPixelToPosition(initialYPixel)));
		this.images = images;
		setCurrentSprite(0);
	}
	
	/**
	 * Initialize this Mazub at the bottom-left corner of the game world; zero current horizontal and vertical velocity; 1.0 for the initial horizontal velocity;
	 * zero game time and alternation time; 3.0 for the absolute maximum horizontal velocity; a false running state and ducking state; "not a number" for last movement
	 * right for the horizontal direction; the given array of images associated with this Mazub; the sprite with index 0 of the given images for its current sprite.
	 * 
	 * @param 	images
	 * 			The array of sprites for this new Mazub.
	 * @effect	This new Mazub is initialized with the given sprites as its sprites, and zero for both the
	 * 			horizontal and vertical positions.
	 * 			| this(0, 0, images)
	 */
	public Mazub(Sprite[] images){
		this(0,0,images);	
	}
	
	
	// X-POSITIONS
	
	/**
	 * Return the x-position (in cm) of this Mazub's bottom-left pixel.
	 */
	@Basic
	public int getXPixel(){
		return this.xPixel;
	}
	
	/**
	 * Return the current horizontal position (in m) of this Mazub.
	 */
	@Basic
	public double getXPosition(){
		return this.xPosition;
	}
	
	/**
	 * Return the nearest valid x-position of the given position.
	 * 
	 * @param 	position
	 * 			The position for which the nearest valid x-position will be returned.
	 * @return	Return the maximum x-pixel (i.e. the amount of x-pixel decremented with one)
	 * 			if the pixel corresponding with the given position is higher than or equal 
	 * 			to the amount of x-pixels.
	 * 			| if (convertPositionToPixel(position) >= AMOUNT_X_PIXELS)
	 * 			|	then result == convertPixelToPosition(AMOUNT_X_PIXELS-1)
	 * @return	Return zero if the pixel corresponding with the given 
	 * 			position is strict negative.
	 * 			| if (convertPositionToPixel(position) < 0)
	 * 			|	then result == 0
	 * @return	Return the given position if the pixel corresponding with the 
	 * 			given position is positive and smaller than the amount 
	 * 			of x-pixels.
	 * 			| pixel = convertPositionToPixel(position)
	 * 			| if (pixel >= 0 && pixel < AMOUNT_X_PIXELS)
	 * 			|	then result == position
	 */
	public static double getNearestValidXPosition(double position){
		int pixel = convertPositionToPixel(position);
		if (pixel >= AMOUNT_X_PIXELS)
			return convertPixelToPosition(AMOUNT_X_PIXELS-1);
		else if (pixel < 0)
			return 0;
		else
			return position;			
	}
	
	/**
	 * Check whether the given x-position is a valid position within the fixed grid of pixels.
	 * 
	 * @param 	pixel
	 * 			The x-position to check.
	 * @return	True if and only if the given x-position is positive
	 * 			and smaller than the fixed maximum amount of x-pixels (i.e. 1024).
	 * 			| result == (0 <= pixel && pixel < AMOUNT_X_PIXELS)
	 * @note	If the given x pixel is strict positive below the amount of x-pixels,
	 * 			then the given x-pixel is a  valid double. Therefore we don't need 
	 * 			to check this constraint no more to conclude that the given pixel is a valid double.
	 */
	public static boolean isValidXPixel(int pixel){
		return (0 <= pixel && pixel < AMOUNT_X_PIXELS);
	}
	
	
	/**
	 * Set the associated pair of x-position and x-pixel corresponding to the given position.
	 * 
	 * @param 	position
	 * 			The new x-position of this Mazub and the position corresponding with the new
	 * 			x-pixel of this Mazub.
	 * @post	The new x-position of this Mazub is equal to the given position
	 * 			|new.getXPosition == position
	 * @post	The new x-pixel of this Mazub is equal to the x-pixel corresponding 
	 * 			with the given position.
	 * 			| new.getXPixel == convertPositionToPixel(position)
	 * @throws 	IllegalArgumentException
	 * 			The x-pixel corresponding with given position is not a valid x-pixel.
	 * 			| ! isValidXPixel(convertPositionToPixel(position))
	 */
	@Model @Raw
	private void setXPosition(double position) throws IllegalArgumentException{
		int pixel = convertPositionToPixel(position);
		if (!isValidXPixel(pixel))
			throw new IllegalArgumentException();
		this.xPosition = position;
		this.xPixel = pixel;
	}
	
	/**
	 * Variable registering the horizontal pixel of this Mazub.
	 */
	private int xPixel;
	
	/**
	 * Variable registering the horizontal position (in m) of this Mazub.
	 */
	private double xPosition;
	
	/**
	 * Symbolic constant representing the fixed amount of pixels in the x-direction.
	 */
	private static final int AMOUNT_X_PIXELS = 1024;
	

	
	// Y-POSITIONS

	/**
	 * Return the y-position (in cm) of this Mazub's bottom-left pixel.
	 */
	@Basic
	public int getYPixel(){
		return this.yPixel;
	}
	
	/**
	 * Return the current vertical position (in m) of this Mazub.
	 */
	@Basic
	public double getYPosition(){
		return this.yPosition;
	}
	
	
	/**
	 * Return the nearest valid y-position of the given position.
	 * 
	 * @param 	position
	 * 			The position for which the nearest valid y-position will be returned.
	 * @return	Return the maximum y-pixel (i.e. the amount of x-pixel decremented with one)
	 * 			if the pixel corresponding with the given position is higher than or equal 
	 * 			to the amount of y-pixels.
	 * 			| if (convertPositionToPixel(position) >= AMOUNT_Y_PIXELS)
	 * 			|	then result == convertPixelToPosition(AMOUNT_Y_PIXELS-1)
	 * @return	Return zero if the pixel corresponding with the given 
	 * 			position is strict negative.
	 * 			| if (convertPositionToPixel(position) < 0)
	 * 			|	then result == 0
	 * @return	Return the given position if the pixel corresponding with the 
	 * 			given position is positive and smaller than the amount 
	 * 			of y-pixels.
	 * 			| pixel = convertPositionToPixel(position)
	 * 			| if (pixel >= 0 && pixel < AMOUNT_Y_PIXELS)
	 * 			|	then result == position
	 */
	public static double getNearestValidYPosition(double position){
		int pixel = convertPositionToPixel(position);
		if (pixel >= AMOUNT_Y_PIXELS)
			return convertPixelToPosition(AMOUNT_Y_PIXELS-1);
		else if (pixel < 0) // and not pixel <= 0 !
			return 0;
		else
			return position;			
	}
	
	
	/**
	 * Check whether the given y-position is a valid position within the fixed grid of pixels.
	 * 
	 * @param 	pixel
	 * 			The y-position to check.
	 * @return	True if and only if the given y-position is positive
	 * 			and smaller than the fixed maximum amount of y-pixels (i.e 768).
	 * 			| result == (0 <= pixel && pixel < AMOUNT_Y_PIXELS)
	 * @note	If the given y-pixel is strict positive below the amount of y-pixels,
	 * 			then the given y-pixel is a  valid double. Therefore we don't need 
	 * 			to check this constraint no more to conclude that the given pixel is a valid double.
	 */
	public static boolean isValidYPixel(int pixel){
		return (0 <= pixel && pixel < AMOUNT_Y_PIXELS);
	}
	
	
	/**
	 * Set the associated pair of y-position and y-pixel corresponding to the given position.
	 * 
	 * @param 	position
	 * 			The new y-position of this Mazub and the position corresponding with the new
	 * 			y-pixel of this Mazub.
	 * @post	The new y-position of this Mazub is equal to the given position
	 * 			|new.getYPosition == position
	 * @post	The new y-pixel of this Mazub is equal to the y-pixel corresponding 
	 * 			with the given position.
	 * 			| new.getYPixel == convertPositionToPixel(position)
	 * @throws 	IllegalArgumentException
	 * 			The y-pixel corresponding with given position is not a valid y-pixel.
	 * 			| ! isValidYPixel(convertPositionToPixel(position))
	 */
	@Model @Raw
	private void setYPosition(double position) throws IllegalArgumentException{
		int pixel = convertPositionToPixel(position);
		if (!isValidYPixel(pixel))
			throw new IllegalArgumentException();
		this.yPixel = pixel;
		this.yPosition = position;
	}
	
	/**
	 * Variable registering the vertical pixel of this Mazub.
	 */
	private int yPixel;
	
	/**
	 * Variable registering the vertical position of this Mazub.
	 */
	private double yPosition;
	
	/**
	 * Symbolic constant representing the fixed amount of pixels in the y-direction.
	 */
	private static final int AMOUNT_Y_PIXELS = 768;
	
	
	
	
	// X-POSITION AND Y-POSITION
	
	/**
	 * Convert the given position (in meters) to the corresponding amount of pixels (in centimeters) 
	 * and change the type to integer.
	 * 
	 * @param 	position
	 * 			The position which is to be converted to an amount of pixels.
	 * @pre		The given position is a valid number.
	 * 			| ! Double.isNaN(position)
	 * @return 	Return the pixel (in cm) corresponding with the given position(in m).
	 * 			| result == (int) (position/PIXEL_LENGTH)
	 */
	public static int convertPositionToPixel(double position){
		assert (!Double.isNaN(position));
		return (int) (position/PIXEL_LENGTH);
	}
	
	/**
	 * Convert the given amount of pixels (in cm) to the corresponding position (in m) and change the type to double.
	 * 
	 * @param 	pixel
	 * 			The amount of pixels which is to be converted to a position.
	 * @return 	Return the position (in m) corresponding to the given amount of pixels (in cm).
	 * 			| result == (double) (pixel*PIXEL_LENGTH)
	 */
	public static double convertPixelToPosition(int pixel){
		return (double) (pixel*PIXEL_LENGTH);
	}
	
	/**
	 * Variable registering the amount of pixels that make up one meter.
	 */
	private static final double PIXEL_LENGTH = 0.01;
	
	
	
	// HORIZONTAL VELOCITY
	
	/**
	 * Return this Mazub's current horizontal velocity (in m/s).
	 */
	@Basic
	public double getXVelocity(){
		return this.xVelocity;
	}
	
	/**
	 * Return the initial horizontal velocity (in m/s) of this Mazub.
	 */
	@Basic @Immutable
	public double getInitialXVelocity(){
		return this.initialXVelocity;
	}	
	
	/**
	 * Return the maximum horizontal velocity when ducking for this Mazub.
	 */
	@Basic @Immutable
	public static int getMaxXVelocityWhenDucking(){
		return maxXVelocityWhenDucking;
	}
	
	/**
	 * Return this Mazub's maximal horizontal velocity (in m/s).
	 */
	@Basic
	public double getMaxXVelocity(){
		if (isDucking())
			return getMaxXVelocityWhenDucking();
		return this.absoluteMaxXVelocity;
	}
	
	
	/**
	 * Check whether this Mazub currently has a constant horizontal velocity.
	 * 
	 * @return 	True if and only if this Mazub is either not running or running at the maximum 
	 * 			speed for this Mazub already.
	 * 			| result == (!isRunning() || getXVelocity() == getMaxXVelocity())
	 */
	public boolean hasConstantXVelocity(){
		return (!isRunning() || getXVelocity() == getMaxXVelocity());
	}
	
	/**
	 * Checks whether this Mazub can have the given velocity 
	 * as its current horizontal velocity.
	 * 
	 * @param 	velocity
	 * 			The velocity to check.
	 * @return	True if and only if the horizontal velocity is positive 
	 * 			and below or equal to the maximum horizontal velocity of this Mazub.
	 * 			| result == 
	 * 			| ( (velocity >= 0) &&
				| (velocity <= getMaxXVelocity()) )
	 * @note	If the given velocity is positive and below or equal to the maximum horizontal velocity, the 
	 * 			given velocity is a valid double. Therefore we don't need to check 
	 * 			this constraint no more to conclude that the given velocity is a 
	 * 			double.
	 */
	public boolean canHaveAsXVelocity(double velocity){
		return ( (velocity >= 0) &&
				(velocity <= getMaxXVelocity()) );
	}
	
	/**
	 * Check whether the given velocity is a valid initial horizontal velocity for all Mazubs.
	 * 
	 * @return 	True if and only if the given velocity is higher than or equal to 1.
	 * 			| result == (velocity >= 1)
	 * @note	If the given velocity is higher than or equal to 1, the given 
	 * 			velocity is a valid double. Therefore we don't need to check 
	 * 			this constraint anymore to conclude that the given velocity 
	 * 			is a valid double.		
 	 */
	public static boolean isValidInitialXVelocity(double velocity){
		return (velocity >= 1);
	}
	
	
	/**
	 * Check whether the given velocity is a valid maximum horizontal velocity for all Mazubs.
	 * 
	 * @return 	True if and only if the given velocity is higher than or equal to the initial horizontal velocity of this Mazub.
	 * 			| result == (velocity >= 1)
	 * @note	If the given velocity is higher than or equal to intial 
	 * 			horizontal velocity of this Mazub, the given velocity 
	 * 			is a valid double. Therefore we don't need to check 
	 * 			this constraint no more to conclude that the given velocity 
	 * 			is a valid double.		
 	 */
	public boolean canHaveAsMaxXVelocity(double velocity){
		return (velocity >= getInitialXVelocity());
	}
	
	/**
	 * Set this Mazub's horizontal velocity to the given velocity.
	 * 
	 * @param 	velocity
	 * 			The new horizontal velocity for this Mazub.
	 * @post 	If the given velocity is negative (and consequently a valid double),
	 * 			the new horizontal velocity of this Mazub is equal to zero.
	 * 			| if (velocity <= 0)
	 * 			|   then new.getXVelocity() = 0
	 * @post	If the given velocity is a valid double and above the maximum 
	 * 			horizontal velocity, the new horizontal velocity of this Mazub is 
	 * 			equal to the maximum horizontal velocity.
	 * 			| if ((velocity > getMaxXVelocity())
	 * 			|	then new.getXVelocity() = getMaxXVelocity()
	 * @post	If the given velocity is a valid double, is positive and below or equal to
	 * 			the maximum horizontal velocity; the new horizontal velocity 
	 * 			of this Mazub is equal to the given velocity.
	 * 			| if ((velocity > 0) && (velocity <= getMaxXVelocity()))
	 * 			|	then new.xVelocity() = velocity
	 */
	@Model
	private void setXVelocity(double velocity){
		if (velocity <= 0)
			this.xVelocity = 0;
		else if (!Double.isNaN(velocity))
			this.xVelocity = Math.min(velocity, getMaxXVelocity());		
	}
	

	/**
	 * Variable registering the horizontal velocity (in m) of this Mazub.
	 */
	private double xVelocity = 0;
	
	/**
	 * Variable registering the initial horizontal velocity (in m/s) for this Mazub.
	 */
	private final double initialXVelocity = 1;
	
	/**
	 * Variable registering the maximum horizontal velocity when ducking (in m/s) for any Mazub.
	 */
	private static final int maxXVelocityWhenDucking = 1;
	

	/**
	 * Variable registering the maximum horizontal velocity (in m/s) for this Mazub.
	 */
	private final double absoluteMaxXVelocity = 3;
	
	
	//VERTICAL VELOCITY
	
	/**
	 * Return the current vertical velocity (in m/s) of this Mazub.
	 */
	@Basic
	public double getYVelocity(){
		return this.yVelocity;
	}
	
	/**
	 * Checks whether this Mazub can have the given velocity 
	 * as its current vertical velocity.
	 * 
	 * @param 	velocity
	 * 			The velocity to check.
	 * @effect	True if and only if the given velocity is a valid double
	 * 			| !Double.isNaN(velocity)
	 */
	public static boolean isValidYVelocity(double velocity){
		return !(Double.isNaN(velocity));
				
	}
	
	/**
	 * Set the vertical velocity of this Mazub to a given value.
	 * 
	 * @param 	velocity
	 * 			The new vertical velocity for this Mazub.
	 * @post	This Mazub's new vertical velocity is equal to the given velocity.
	 * 			| new.getYVelocity == velocity
	 * @note	We allow negative values for the vertical velocity. In this way, the sign of the velocity
	 * 			directly implies the vertical orientation. This reduces the bookkeeping work.
	 */
	@Model
	private void setYVelocity(double velocity){
		if (! Double.isNaN(velocity))
			this.yVelocity = velocity;
	}
	
	
	/**
	 * Variable registering the vertical velocity of this Mazub.
	 */
	private double yVelocity;

	
	/**
	 * Symbolic constant representing the initial vertical velocity (in m/s) for any Mazub.
	 */
	private static final double INITIAL_Y_VELOCITY = 8;
	
	
	
	
	// HORIZONTAL ACCLERATION
	
	/**
	 * Return the current acceleration (in m/s^2) of this Mazub.
	 */
	@Basic
	public double getXAcceleration(){
		if (hasConstantXVelocity())
			return 0;
		return xAcceleration;
	}
	
	/**
	 * Check whether the given acceleration is a valid horizontal acceleration for all Mazubs.
	 * 
	 * @return 	True if and only if the given acceleration is higher than or equal to 0.
	 * 			| result == (acceleration >= 0)
	 * @note	If the given acceleration is higher than or equal to 0, the given 
	 * 			acceleration is a valid double. Therefore we don't need to check 
	 * 			this constraint no more to conclude that the given acceleration 
	 * 			is a double.		
 	 */
	public static boolean isValidXAcceleration(double acceleration){
		return (acceleration >= 0);
	}
	
	
	/**
	 * Variable representing the horizontal acceleration (in m/s^2) for any Mazub.
	 */
	private static final double xAcceleration = 0.9;
	
	
	
	
	// VERTICAL ACCELERATION
	
	/**
	 * Return the current vertical acceleration (in m/s^2) of this Mazub.
	 * 
	 * @return	If this Mazub should be falling, this method returns its vertical acceleration.
	 * 			| if (shouldFall())
	 * 			|  then result == Y_ACCELERATION
	 * @return 	If this Mazub should not be falling (and thus, is currently positioned at the bottom
	 * 			of the game world), this method returns zero.
	 * 			| if (!shouldFall()) 
	 * 			|  then result == 0 
	 * @note	As far as vertical movement is concerned, we allow the acceleration of any Mazub
	 * 			to be negative. This eliminates the need for a specific method/variable keeping
	 * 			track of the vertical orientation of Mazub.	
	 */
	@Basic 
	public double getYAcceleration(){
		if (!shouldFall()){
			return 0;
		}
		return Y_ACCELERATION;
	}
	
	/**
	 * Checks whether this Mazub can have the given acceleration 
	 * as its current vertical acceleration.
	 * 
	 * @param 	acceleration
	 * 			The acceleration to check.
	 * @effect	True if and only if the given acceleration is a valid double
	 * 			| !(Double.isNaN(acceleration))
	 */
	public static boolean isValidYAcceleration(double acceleration){
		return !(Double.isNaN(acceleration));
				
	}
	
	/**
	 * Symbolic constant representing the vertical acceleration (in m/s^2) for any Mazub.
	 */
	private static final double Y_ACCELERATION = -10;
	
	
	
	
	// RUNNING
	
	/**
	 * Check whether this Mazub is currently running (i.e. moving horizontally)
	 */
	@Basic
	public boolean isRunning(){
		return this.running;
	}
	

	/**
	 * Return the current orientation (i.e. left or right) of this Mazub.
	 */
	@Basic
	public Direction getOrientation(){
		return this.orientation;
	}
	
	/**
	 * Initiate horizontal movement for this Mazub.
	 * 
	 * @param 	direction
	 * 			The direction in which this Mazub is to start running.
	 * @effect	The new horizontal direction of this Mazub is set to the given direction.
	 * 			| setOrientation(direction)
	 * @effect	The current horizontal velocity of this Mazub is set to the initial 
	 * 			horizontal velocity.
	 * 			| setXVelocity(getInitialXVelocity())
	 * @effect	The new running state of this Mazub is set to true.
	 * 			| setRunning(true)
	 */
	public void startMove(Direction direction){
		setOrientation(direction);
		setXVelocity(getInitialXVelocity());
		setRunning(true);
	}
	
	/**
	 * End horizontal movement for this Mazub.
	 * 
	 * @effect	The current horizontal velocity of this Mazub is set to 0.
	 * 			| setXVelocity(0)
	 * @effect	The new running state of this Mazub is set to false.
	 * 			| setRunning(false)
	 */
	public void endMove(){
		setXVelocity(0);
		setRunning(false);
	}
	
	
	/**
	 * Return the unit of orientation (either +1 or -1) which this Mazub is facing horizontally.
	 * 
	 * @return	-1 if the current orientation of this Mazub is left.
	 * 			| if (getOrientation() == Direction.LEFT)
	 * 			|	then result == -1
	 * @return 	1 if the current orientation of this Mazub is right.
	 * 			| if (getOrientation() == Direction.Right)
	 * 			|	then result == 1
	 */
	private int getUnitDirection(){
		if (getOrientation() == Direction.LEFT)
			return -1;
		else
			return 1;
	}
	
	/**
	 * Set this Mazub's running state to the given running state.
	 * 
	 * @param 	flag
	 * 			The new running state for this Mazub.
	 * @post	This Mazub's new running state is equal to the given flag.
	 * 			| new.isRunning() == flag
	 */
	private void setRunning(boolean flag){
		this.running = flag;
	}
	
	
	/**
	 * Set this Mazub's orientation (i.e. left or right) to a given orientation.
	 * 
	 * @param 	orientation
	 * 			This Mazub's new orientation.
	 * @post	This Mazub's new orientation is equal to the given orientation.
	 * 			| new.getOrientation() == orientation
	 */
	@Model
	private void setOrientation(Direction orientation){
		this.orientation = orientation; 
	}
	
	/**
	 * Variable registering the running state of this Mazub.
	 */
	private boolean running = false;
	
	/**
	 * Variable registering the horizontal orientation of this Mazub.
	 */
	private Direction orientation = Direction.RIGHT;
	
	
	
	//JUMPING
	
	/**
	 * Check whether this Mazub should currently be falling.
	 * 
	 * @return 	True if and only if the vertical position of this Mazub is greater than zero.
	 * 			| result == (getYPosition() > 0)
	 */
	public boolean shouldFall(){
		return getYPosition() > 0;
	}
	
	/**
	 * Initiate jump for this Mazub.
	 * 
	 *@effect	Set the vertical velocity to the initial vertical velocity.
	 *		| setYVelocity(INITIAL_Y_VELOCITY)
	 */
	public void startJump(){
		setYVelocity(INITIAL_Y_VELOCITY);
	}
		
	
	/**
	 * End the jump for this Mazub.
	 * 
	 * @effect 	Set this Mazub's vertical velocity to zero if the current vertical velocity is greater than zero.
	 * 			| if (getYVelocity() > 0)
	 * 			|   then setYVelocity(0)
	 */
	public void endJump(){
		if (getYVelocity() > 0)
			setYVelocity(0);
	}
	
	
	
	// DUCKING
	
	/**
	 * Check whether this Mazub is currently ducking.
	 */
	@Basic
	public boolean isDucking(){
		return this.ducking;
	}
	
	/**
	 * Initiate duck for this Mazub.
	 * 
	 * @post	The new ducking state of this Mazub is set to the boolean value true.
	 * 			| new.isDucking() == true
	 */
	public void startDuck(){
		if (getXVelocity() > getMaxXVelocityWhenDucking())
			setXVelocity(getMaxXVelocityWhenDucking()); // Otherwise @Raw
		this.ducking = true;
	}
	
	/**
	 * End the duck for this Mazub.
	 * 
	 * @post	The new ducking state of this Mazub is set to the boolean value false.
	 * 			| new.isDucking() == false
	 */
	public void endDuck(){
		this.ducking = false;
	}

	
	/**
	 * Variable registering this Mazub's current ducking state.
	 */
	private boolean ducking;
	
	

	
	// SPRITES
	
	
	/**
	 * Return this Mazub's current sprite (i.e. its visual representation).
	 */
	@Basic
	public Sprite getCurrentSprite(){
		return this.currentSprite;
	}
	
	/**
	 * Return the current width of this Mazub, measured in pixels.
	 * 
	 * @return 	Return the width of the current sprite of this Mazub, 
	 * 			measured in pixels (cm).
	 * 			| result == getCurrentSprite().getWidth()
	 */
	public int getWidth(){
		return getCurrentSprite().getWidth();
	}
	
	/**
	 * Return the current height of this Mazub, measured in pixels (cm).
	 */
	public int getHeigth(){
		return getCurrentSprite().getHeight();
	}
	
	
	/**
	 * Return the image associated with this Mazub at the given index.
	 * 
	 * @param 	index
	 * 			The index of the sprite to return.
	 * @pre	  	The given index must be a valid index for this Mazub.
	 * 			| canHaveAsIndex(index)
	 * @note	We are currently leaking information, as we return an actual object and
	 * 			all its references. We will account for this in next versions of the class.
	 */
	@Basic
	public Sprite getImageAt(int index){
		assert (canHaveAsIndex(index));
		return this.images[index];
	}
	
	/**
	 * Return the number of sprites associated with this Mazub.
	 */
	@Basic @Immutable
	public int getNbSprites(){
		return this.images.length;
	}
	
	/**
	 * Return all the images associated with this Mazub.
	 * 
	 * @return 	The length of the resulting array is equal to the number
	 * 			of images associated with this Mazub.
	 * 			| result.length == getNbSprites()
	 * @return 	Each element of the resulting array is equal to the image associated with
	 * 			this Mazub at the corresponding index.
	 * 			| for each index in 0..result.length:
	 * 			| 	result[index].equals(getImageAt(index))
	 * @note	We are currently leaking information, as we return an actual object and
	 * 			all its references. We will account for this in next versions of the class.
	 */
	public Sprite[] getImages(){
		return this.images;
	}
	
	/**
	 * Return the indices of the current alternating images of this Mazub.
	 */
	@Basic
	public int[] getCurrentIndices(){
		return this.currentIndices;
	}
	
	/**
	 * Checks whether the array of images of this Mazub contains the given index.
	 * 
	 * @param 	index
	 * 			The index to check.
	 * @return	True if and only if the index is positive and
	 * 			lower than the number of sprites in the array images.
	 * 			| result == ( (index < getNbSprites()) && (index >= 0) )
	 */
	@Raw
	public boolean canHaveAsIndex(int index){
		return ( (index < getNbSprites()) && (index >= 0) );
	}
	
	/**
	 * Checks whether this Mazub can have the given indices as its
	 * current indices.
	 * 
	 * @param	indices
	 * 			The indices to check
	 * @effect	False if and only if an index in the array indices
	 * 			is not a valid index.
	 * 			| for (int index: indices)
	 * 			| 	 if (!(isValidIndex(index)))
	 * 			|		result == false
	 */
	public boolean canHaveAsIndices(int[] indices){
		// @invar	Mazub can have all indices before "index".
		//			| for each I in 0..index-1:
		//			|	canHaveAsIndex(index)
		// @invar	The number of handled indices doesn't exceed the length of the given array indices.
		//			| Arrays.asList(indices).indexOf(index) <= indices.length
 		// @variant	The number of unhandeld indices in the given array.
		//			| indices.length - Arrays.asList(indices).indexOf(index)
		for (int index: indices)
			if (!canHaveAsIndex(index))
				return false;
		return true;
	}
	
	/**
	 * Return the number of alternating images while running and not ducking.
	 * 
	 * @return The number of sprites decremented with 10 and further divided by 2.
	 * 			result == (getNbSprites() - 10) / 2
	 */
	@Immutable @Model
	private int computeM(){
		return (getNbSprites()-10)/2;
	}
	
	/**
	 * Create an array with all the indices of the alternating images for running in the direction 
	 * corresponding with the given startindex (in the correct order).
	 * 
	 * @param 	start
	 * 			The first index of the alternating images for running.
	 * @pre		This Mazub can have the given start as its index.
	 * 			| canHaveAsIndex(start)
	 * @return	The array with all the indices of the alternating images for running in the direction
	 * 			corresponding with the given startindex.
	 * 			|for(int index = 0; index < 1+m; index++)
	 * 			|	runningArray[index] = start+index
	 * 			|result == runningArray
	 */
	@Model
	private int[] createRunningArray(int start){
		assert canHaveAsIndex(start);
		int m = computeM();
		int[] runningArray = new int[1+m];
		// @invar	The running array contains all the indices for running which are smaller than the current index.
		//			| for each index in start..index
		//			|	Arrays.asList(runningArray).contains(index)
		// @invar	"index" doesn't exceed (1 + computeM())
		// 			| index < 1 + computeM()
		// @variant	The number of unhandled indices.
		//			| computeM() - index + 1
		for(int index = 0; index < 1+m; index++){
			runningArray[index] = start+index;
		}
		return runningArray;
	}

	/**
	 * Return an array of indices associated with the sprites that correspond with the current state of this Mazub.
	 * (table on page 7 in the assignment)
	 * @note	Below in comment, we have included an alternative implementation for this method using some properties
	 * 			of sets. It was not selected due to poor performance and some 'unorthodox'
	 * 			conversions between sets and arrays, and correction. However, we think that the underlying principle
	 * 			of intersections of sets of indices is far more elegant  than a series of nested if-clauses. When subject to some changes
	 * 			when we are more familiar with the	syntax and possibilities of sets, it might see daylight again 
	 * 			as we think it is more adaptable when the table for the images should change.
	 * 			It is included only for reference.
	 */
	private int[] getApplicableIndices(){
		if (!isRunning()){
			if (!hasMovedLastSecond()){
				if (!isDucking())
					return new int[] {0};
				else
					return new int[] {1};
			}
			else
				if (isDucking()){
					if (getUnitDirection() == 1)
						return new int[] {6};
					else
						return new int [] {7};
				} else {					
					if (getUnitDirection() == 1)
						return new int[] {2};
					else
						return new int [] {3};
				}
		}
		else {
			if (isDucking()){
				if (getUnitDirection() == 1)
					return new int[] {6};
				else
					return new int [] {7};
			} else {
				if (shouldFall()){
					if (getUnitDirection() == 1)
						return new int[] {4};
					else
						return new int [] {5};
					
				} else {
					if (getUnitDirection() == 1)
						return createRunningArray(8);
					else
						return createRunningArray(9+computeM());
				}
			}
		}
	}
	
//	private int[] getApplicableIndices(){
//		HashSet<Integer> completeSet = new HashSet<Integer>();
//		for (int index = 0; index < getNbSprites(); index++){
//			completeSet.add(index);
//		}
//		int[] movementRight = createRunningArray(8);
//		int[] movementLeft = createRunningArray(9+computeM());
//		
//		HashSet<Integer> jumpingSet = new HashSet<Integer>(Arrays.asList(1,2,3,4,5,6,7));
//		HashSet<Integer> duckingSet = new HashSet<Integer>(Arrays.asList(1,6,7));
//		HashSet<Integer> movingSet = new HashSet<Integer>(Arrays.asList(4,5,6,7,8,9));
//		HashSet<Integer> movedLastSecond = new HashSet<Integer>(Arrays.asList(2,3,4,5,6,7,8,9));
//		HashSet<Integer> orientationRight = new HashSet<Integer>(Arrays.asList(1,2,4,6));
//		HashSet<Integer> orientationLeft = new HashSet<Integer>(Arrays.asList(1,3,5,7));
//		
//		for (int element:movementRight){
//			movingSet.add(element);
//			orientationRight.add(element);			
//		}
//		
//		for (int element:movementLeft){
//			movingSet.add(element);
//			orientationLeft.add(element);	
//		}
//
//		// jumping
//		if (!shouldFall()){
//			completeSet.removeAll(jumpingSet);
//			completeSet.add(1);
//			completeSet.add(6);
//			completeSet.add(7);
//			completeSet.add(2);
//			completeSet.add(3);
//		}
//		else completeSet.retainAll(jumpingSet);
//		// ducking
//		if(!isDucking())
//			completeSet.removeAll(duckingSet);
//		else completeSet.retainAll(duckingSet);
//		// running
//		if(!isRunning()){
//			completeSet.removeAll(movingSet);
//			if (hasMovedLastSecond())
//				completeSet.retainAll(movedLastSecond);
//			else {
//				completeSet.removeAll(movedLastSecond);
//			}
//		}
//		else
//			completeSet.retainAll(movingSet);
//		// orientation
//		if (getOrientation() == Direction.LEFT)
//			completeSet.retainAll(orientationLeft);
//		else
//			completeSet.retainAll(orientationRight);	
//		if (completeSet.size() == 0){
//			completeSet = new HashSet<Integer>();
//			completeSet.add(0);
//		}
//		Integer[] array = new Integer[completeSet.size()];
//		completeSet.toArray(array);
//		int[] result = new int[completeSet.size()];
//		for (int index = 0; index <completeSet.size();index++){
//			result[index] = (int) array[index];
//		}
//		Arrays.sort(result);
//		return result;
//	}
	
	/**
	 * Set the current sprite of this Mazub to the image corresponding with the given index (of the array images).
	 * 
	 * @param 	index
	 * 			The index of the new set image in the array images.
	 * @post	The new current sprite is equal to the image in the array images as index the given index.
	 * 			| new.currentSprite = getImageAt(index)
	 */
	@Model
	private void setCurrentSprite(int index){
		this.currentSprite = getImageAt(index);
	}
	
	
	/**
	 * Set the indices of the current alternating images of this Mazub to the given indices.
	 * 
	 * @param 	newIndices
	 * 			The array of the new current indices of this Mazub.
	 * @pre		This Mazub can have the new indices as its current indices.
	 * 			| canHaveAsIndices(newIndices)
	 * @post	The array of the new current indices of this Mazub is equal to the given array of indices.
	 * 			| new.currentIndices == newIndices
	 */
	@Model
	private void setCurrentIndices(int[] newIndices ){
		assert (canHaveAsIndices(newIndices));
		this.currentIndices = newIndices;
	}
	
	/**
	 * Varaibel referencing the current sprite of this Mazub.
	 */
	private Sprite currentSprite;
	
	/**
	 * Variable referencing the array of sprites associated with this Mazub.
	 */
	private final Sprite[] images;
	
	/**
	 * Variable referencing the indices of the current alternating images of this Mazub.
	 */
	private int[] currentIndices = new int[] {0};
	
	
	
	// TIME
	
	/**
	 * Return the current value for the timer of this Mazub's lifetime. 
	 */
	@Basic
	public double getTime(){
		return this.time;
	}
	
	/**
	 * Return the current time (in s) of the alternation timer of this Mazub.
	 */
	@Basic
	public double getAlternationTimer(){
		return this.alternationTimer;
	}
	
	/**
	 * Return the timestamp (in s) of the last horizontal movement of this Mazub.
	 */
	@Basic
	public double getLastMovement(){
		return this.movement;
	}
	
	/**
	 * Check whether this Mazub has moved horizontally in the last second.
	 * 
	 * @return  True if and only if the current value for the time isn't greater than the timestamp
	 * 			of movement incremented by one second.
	 * 			| result == (getTime() <= getLastMovement +1)
	 */
	public boolean hasMovedLastSecond(){
		return ((getTime() <= getLastMovement() + 1));
	}
	
	/**
	 * Check whether the given time is a legal value to set for this Mazub.
	 * 
	 * @param 	time
	 * 			The time that has to be checked for validity.
	 * @return	True if and only if the given time value is greater than this Mazub's current
	 * 			time value.
	 * 			| result == time > getTime()
	 * @note	If the given time is NaN, the checker still gives the correct and intended result.
	 */
	public boolean canHaveAsTime(double time){
		return time > getTime(); // The time indication is positive, and flows in one direction only.
	}
	
	/**
	 * Checks whether the given time is a valid game time for all Mazub.
	 * 
	 * @param 	time
	 * 			The time to be checked
	 * @return	True if and only if the given time is positive.
	 * 			|result == (time >= 0)
	 * @note	If the given time is above 0 or equal to zero, the given time 
	 * 			is a valid double. Therefore we don't need to check this 
	 * 			constraint no more to conclude that the given time is a valid 
	 * 			double.
	 */
	public static boolean isValidTime(double time){
		return (time >= 0);
	}
	
	/**
	 * Check whether the given movement is a valid movement for all Mazubs.
	 * 
	 * @param 	movement
	 * 			The movement to be checked.
	 * @return	True if and only if the movement is positive or "not a number".
	 * 			| result == ( (movement >= 0) || Double.isNaN(movement) )
	 * @note	If movement is not smaller than zero, movement can be "not a number".
	 * 			Consequently you only need to check whether movement is not smaller than zero.
	 * @note	Movement can be "not a number" because before Mazub has run for the first time,
	 * 			it's impossible to know when he has been running for the last time.
	 * 			
	 */
	public static boolean isValidMovement(double movement){
		// result == ( (movement >= 0) || Double.isNaN(movement) )
		return ( !(movement < 0) );
	}
	
	
	/**
	 * Set the timer for this Mazub to the given value.
	 * 
	 * @param 	time
	 * 			The new value for this Mazub's timer.
	 * @post 	The new time for this Mazub is equal to the given time.
	 * 			| new.getTime() = time
	 * @throws 	IllegalArgumentException
	 * 			The given time is not a valid set time for this Mazub.
	 * 			| !canHaveAsTime(time)
	 */
	private void setTime(double time) throws IllegalArgumentException{
		if (!canHaveAsTime(time))
			throw new IllegalArgumentException();
		this.time = time;
	}
	
	
	/**
	 * Set the alternation timer of this Mazub to the given time.
	 * 
	 * @param 	newTime
	 * 			The new time of the alternation timer.
	 * @post	The new value of the alterationTimer 
	 * 			is equal to the given time.
	 * 			| new.alternationTimer = newTime
	 */
	@Model
	private void setAlternationTimer(double newTime){
		this.alternationTimer = newTime;
	}
	
	/**
	 * Set the value of the last movement of this Mazub to the given movement.
	 * 
	 * @param 	movement
	 * 			The movement time to be set.
	 * @pre		The given momement time is a valid movement time for any Mazub.
	 * 			| isValidMovement(movement)
	 * @post	The new movement of this Mazub is equal to the given movement.
	 * 			| new.getLastMovement == movement
	 * 
	 */
	@Model
	private void setLastMovement(double movement){
		assert isValidMovement(movement);
		this.movement = movement;
	}
	
	/**
	 * Update the timestamp for the last horizontal movement, according to this Mazub's current state.
	 * 
	 * @effect 	If this Mazub is currently running, set the timestamp of the last horizontal movement to the current time.
	 * 			| if (isRunning())
	 * 			|  then setLastMovement(getTime())
	 */
	@Model
	private void registerMovement(){
		if (isRunning()){
			setLastMovement(getTime());
		}
	}
	
	/**
	 * Variable registering the time (in s) that this Mazub has been alive for. 
	 */
	private double time=0;
	
	/**
	 * Variable registering the current time (in s) of the alternation timer of this Mazub.
	 */
	private double alternationTimer = 0;
	
	/**
	 * Variable to keep track of the time (in s) of the last horizontal movement (timestamp). 
	 */
	private double movement = Double.NaN;
	
	/**
	 * Symbolic constant registering the time per image while alternating (in s) several sprites of this Mazub.
	 */
	private static final double TIME_EACH_IMAGE = 0.075;
	





	// ADVANCE TIME
	
	/**
	 * Update the position, velocity, the sprite, timer, alternation 
	 * timer and the last movement of this Mazub.
	 * 
	 * @param 	dt
	 * 			The amount of time that needs to be advanced.
	 * @throws 	IllegalArgumentException
	 * 		 	The given time interval is not a valid time interval.
	 * 			| !isValidTimeInterval()
	 * @effect	Set the new game time to the old game time incremented 
	 * 			with the given time duration.
	 * 			| setTime(getTime() + dt)
	 * @effect	Set the new alternation timer to the old value incremented
	 * 			with the given time duration.
	 * 			| setAlternationTimer(getAlternationTimer() + dt)
	 * @effect	Register the last horizontal movement.
	 * 			| registerMovement()
	 * @effect	If Mazub is running, update the horizontal position with
	 * 			the given time duration.
	 * 			| if (isRunning())
	 * 			|	then updateXPosition(dt)
	 * @effect	If Mazub is running, update the horizontal velocity with
	 * 			the given time duration.
	 * 			| if (isRunning())
	 * 			|	then updateXVelocity(dt)
	 * @effect	Update the vertical position of this Mazub with the given		 * 			.
	 * 			time duration.
	 * 			| updateYPosition(dt)
	 * @effect	Update the vertical velocity of this Mazub with the given
	 * 			time duration.
	 * 			| updateYVelocity(dt)
	 * @effect	Update the current sprite of this Mazub with the given
	 * 			time duration.
	 * 			| updateCurrentSprite(dt)	
	 */
	public void advanceTime(double dt) throws IllegalArgumentException{
		if (!isValidTimeInterval(dt))
			throw new IllegalArgumentException("dt illegal value: " + dt);
		setTime(getTime()+dt);
		setAlternationTimer(getAlternationTimer()+ dt);
		registerMovement();
		// HORIZONTAL MOVEMENT
		if (isRunning()){
			updateXPosition(dt);
			updateXVelocity(dt);
		}
		// VERTICAL MOVEMENT
		updateYPosition(dt);
		updateYVelocity(dt);
		// SPRITES
		updateCurrentSprite(dt);
	}	
	
	/**
	 * Check whether the given time interval is a valid time interval for any Mazub to be advanced.
	 * 
	 * @param 	dt
	 * 			The time interval that needs to be checked for validity.
	 * @return	True if and only if the given time duration is strict positive
	 * 			and below the maximum time interval.
	 * 			| result == ( (dt > 0) && (dt < MAX_TIME_INTERVAL) )
	 * @note	If the given time interval is strict positive and below the
	 * 			maximum time duration, the given time interval is a valid 
	 * 			double. Therefore we don't need to check this constraint 
	 * 			no more to conclude that the given time duration is a valid double.
	 */
	public static boolean isValidTimeInterval(double dt){
		return (dt > 0 && dt < MAX_TIME_INTERVAL);
	}
		
	/**
	 * Return a boolean whether the current alternating images of this Mazub have just changed.
	 * 
	 * @param 	newIndices
	 * 			The indices of the new alternating images.
	 * @pre		This Mazub can have the given indices as its current alternating indices.
	 * 			| canHaveAsIndices(newIndices)
	 * @return	True if and only if the given new calculated indices 
	 * 			are not equal to the old current indices of this Mazub.
	 * 			| result == (!Arrays.equals(getCurrentIndices(), newIndices))
	 */
	public boolean stateHasChanged(int[] newIndices){
		assert canHaveAsIndices(newIndices);
		return (!Arrays.equals(getCurrentIndices(), newIndices));
	}		

	/**
	 * Update this Mazub's horizontal position.
	 * 
	 * @param 	dt
	 * 			The amount of in-game time that has passed since the last invocation of this very method
	 * 			against this Mazub.
	 * @pre		The given time interval is a valid time interval
	 * 			| isValidTimeInterval(dt)
	 * @effect  Set the new horizontal position for this Mazub to the position that has been computed
	 * 			according to the standard kinematic formula, corrected to fit within the fixed grid of pixels.
	 * 			| setXPosition(getNearestValidXPosition(computeXCorrection(dt)))
	 * @note	The computation of the correction term (i.e the difference between two succesive positions) is outsourced
	 * 			to another method, as it must be possible to turn to other formulae in the future. In this way, it is very easy
	 * 			to either use a different formula altogether, or to select a formula according to certain
	 * 			conditions. These conditions can then be implemented in this method.
	*/
	@Model
	private void updateXPosition(double dt){
		assert isValidTimeInterval(dt);
		double newPosition = getXPosition() + computeXCorrection(dt);
		setXPosition(getNearestValidXPosition(newPosition));
	}
	
	/**
	 * Compute the new horizontal correction term (i.e the difference between two succesive horizontal positions) 
	 * 		for this Mazub after a certain time.
	 * 
	 * @param 	dt
	 * 			The amount of time that has passed since the last computation of this Mazub's 
	 * 			horizontal position.
	 * @pre		The given time interval is a valid time interval
	 * 			| isValidTimeInterval(dt)
	 * @return  The resulting correction term is, in absolute value, less than or equal to the correction term of the standard
	 * 			Newtonian kinematic formula.
	 * 			| result <= Math.abs(getUnitDirection()*(getXVelocity()*dt + 0.5*getXAcceleration()*Math.pow(dt,2)))
	 * @note 	Note that it is possible to turn to other formulae for this computation in the future, as only
	 * 			the upper limit for the absolute value of the correction term is specified.
	 */
	@Model
	private double computeXCorrection(double dt){
		assert isValidTimeInterval(dt);
		return getUnitDirection()*(getXVelocity()*dt + 0.5*getXAcceleration()*Math.pow(dt,2));
	}
	
	
	/**
	 * Update this Mazub's horizontal velocity.
	 * 
	 * @param 	dt
	 * 			The amount of time that has passed since the last computation of this Mazub's 
	 * 			horizontal velocity.
	 * @pre		The given time interval is a valid time interval
	 * 			| isValidTimeInterval(dt)
	 * @effect	Set the old horizontal velocity to the new horizontal velocity 
	 * 			calculated with the standard Newtonian kinematic formula.
	 * 			| setXVelocity(getXVelocity() + getXAcceleration()*dt)
	 */
	@Model
	private void updateXVelocity(double dt){
		assert isValidTimeInterval(dt);
		setXVelocity(getXVelocity() + getXAcceleration()*dt);
		}
	
	
	/**
	 * Update this Mazub's horizontal position.
	 * 
	 * @param 	dt
	 * 			The amount of in-game time that has passed since the last invocation of this very method
	 * 			against this Mazub.
	 * @pre		The given time interval is a valid time interval
	 * 			| isValidTimeInterval(dt)
	 * @effect  Set the new horizontal position for this Mazub to the position that has been computed
	 * 			according to the standard kinematic formula, corrected to fit within the fixed grid of pixels
	 * 			| setYPosition(getNearestValidYPosition(computeYCorrection(dt)))
	 * @note	The actual computation of the new position is outsourced to another method, as it
	 * 			must be possible to turn to other formulae in the future. In this way, it is very easy
	 * 			to either use a different formula altogether, or to select a formula according to certain
	 * 			conditions. These conditions can then be implemented in this method.
	 * 	
	*/
	@Model
	private void updateYPosition(double dt){
		assert isValidTimeInterval(dt);
		double newPosition = getYPosition() + computeYCorrection(dt);
		setYPosition(getNearestValidYPosition(newPosition));
	}
	
	/**
	 *  Compute the new vertical correction term (i.e the difference between two successive vertical positions) 
	 * 		for this Mazub after a certain time.
	 * 
	 * @param 	dt
	 * 			The amount of time that has passed since the last computation of this Mazub's 
	 * 			horizontal position.
	 * @return  The resulting correction term is, in absolute value, less than or equal to the correction term of the standard
	 * 			Newtonian kinematic formula.
	 * 			| result <= getYVelocity()*dt + 0.5*getYAcceleration()*Math.pow(dt,2)
	 * @note 	Note that it is possible to turn to other formulae for this computation in the future, as only
	 * 			the upper limit for the absolute value of the correction term is specified.
	 */
	@Model
	private double computeYCorrection(double dt){
		return (getYVelocity()*dt
				+ 0.5*getYAcceleration()*Math.pow(dt,2));
	}
	
	
	/**
	 * Update the current vertical velocity of this Mazub.
	 * 
	 * @param 	dt
	 * 			The amount of time that has passed since the last computation of this Mazub's 
	 * 			vertical velocity.
	 * @pre		The given time interval is a valid time interval
	 * 			| isValidTimeInterval(dt)
	 * @effect	If this Mazub should not fall, the vertical velocity is set to zero
	 * 			| if (!shouldFall())
	 * 			|  then setYVelocity(0)
	 * @effect	If this Mazub should fall, the vertical velocity is updated
	 * 			according to the standard kinematic formula.
	 * 			| if (shouldFall())
	 * 			|  then setYVelocity(getYVelocity() - getYAcceleration()*dt) 
	 * @note 	As far as vertical movement is concerned, we allow the velocity of any Mazub
	 * 			to become negative. This eliminates the need for a specific method/variable keeping
	 * 			track of the vertical orientation of Mazub.
	 */
	private void updateYVelocity(double dt){
		assert isValidTimeInterval(dt);
		if (!shouldFall())
			setYVelocity(0);
		else
			setYVelocity(getYVelocity() + getYAcceleration()*dt);
	}	
	
	
	/**
	 * Update the current sprite to the appropriate sprite, according to Mazub's current state.
	 * 
	 * @param 	dt
	 * 			The amount of time that has passed since the last sprite has been set.
	 * @pre		The given time interval is a valid time interval
	 * 			| isValidTimeInterval(dt)
	 * @post	This Mazub's current sprite reflects the state of this Mazub according to the used table of sprites.
	 * 			| for some index in getApplicableIndices():
	 * 			| 		getImageAt(index) == getCurrentSprite
	 * @effect	If the state has changed, set the value of the alternationtimer to the given dt.
	 * 			| int[] newIndices = getApplicableIndices()
	 * 			| if (stateHasChanges(newIndices)
	 * 			|	then setAlternationTimer(dt)
	 * @effect	If the state has changed, set the current indices to the new applicable indices.
	 * 			| int[] newIndices = getApplicableIndices()
	 * 			| if (stateHasChanged(newIndices))
	 * 			|	then setCurrentIndices(getApplicableIndices())
	 * @effect	If the state has changed, set the current sprite to the image with the first index of the new applicable indices.
	 * 			| int[] newIndices = getApplicableIndices()
	 * 			| if (stateHasChanged(newIndices))
	 * 			| 	then setCurrentSprite(getApplicableIndices()[0])
	 * @effect	If the state has not changed and the length of the applicable indices is higher than 1,
	 * 			set the current sprite to the image corresponding with the given alternation time.
	 * 			| int[] newIndices = getApplicableIndices()
	 * 			| if ((!stateHasChanged(newIndices)) && (newIndices.length > 1))
	 * 			| 	then ( int newIndex = ((int) (getAlternationTimer()/TIME_EACH_IMAGE) % newIndices.length) &&
	 * 			| 		  setCurrentSprite(newIndices[newIndex]) )
	 */

	@Model
	private void updateCurrentSprite(double dt){
		assert isValidTimeInterval(dt);
		int[] newIndices = getApplicableIndices();
		if (stateHasChanged(newIndices)){
			setAlternationTimer(dt);
			setCurrentIndices(newIndices);
			setCurrentSprite(newIndices[0]);
		}
		else if (newIndices.length > 1){
				int newIndex = ((int) (getAlternationTimer()/TIME_EACH_IMAGE) % newIndices.length);
				setCurrentSprite(newIndices[newIndex]);
		}
	}
		
	/**
	 * Symbolic constant registering the lowest possible given time duration (in s) in advanceTime.
	 */
		private static final double MAX_TIME_INTERVAL = 0.2;
	

}
